<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('notifications', 'UserController@notifications');
Route::get('notificationRead/{id}', 'UserController@notificationRead');
Route::get('notifyUsers/{keyword}/{text}/{link}' , 'UserController@notifyUsers');

Route::get('invisible_registration', 'InvisibleController@index');
Route::get('invisible_login', 'InvisibleController@login')->name('invisible_login');
Route::post('addInvisible' , 'InvisibleController@addInvisible');
Route::post('loginInvisible' , 'InvisibleController@loginInvisible');

// Invisible Routes
Route::group(['middleware' => ['auth:invisible','prevent-back-history']], function() {
    // Invisible Users
    Route::get('invisible/dashboard', 'InvisibleController@dashboard')->name('i_home');

    // Invisible Order
    Route::get('i_orders','Invisible\OrderController@index');
    Route::get('ajaxInvisibleOrders/{year}/{month}','Invisible\OrderController@getOrders');
    Route::get('i_addOrder' , 'Invisible\OrderController@create');
    Route::post('i_placeOrder' , 'Invisible\OrderController@placeOrderHomeDelivery');
    Route::post('i_orderItems', 'Invisible\OrderController@orderItems');
    Route::post('i_bookOrder', 'Invisible\OrderController@bookOrder');
    Route::post('i_sendOrderEmails' , 'Invisible\OrderController@sendOrderEmails');
    Route::get('i_success' , 'Invisible\OrderController@success');

    // Invisible Orders
    Route::get('i_activities/{id}','InvisibleController@user_activities');

    // Invisible Sales
    Route::get('i_sales/{id}','InvisibleController@user_sales');
    Route::get('ajaxISales/{year}/{month}/{invisible}','InvisibleController@getSales')->name('ajaxISales');

    // Invisible Refferals
    Route::get('i_refferals/{id}','InvisibleController@getReferals');

    // Invisible Promos
    Route::get('ajaxIPromos/{id}','PromoController@getIPromos')->name('ajaxIPromos');
    Route::get('i_promos' , 'PromoController@i_index');
    Route::get('i_addPromo' , 'PromoController@i_Promo');
    Route::post('i_addPromo' , 'PromoController@i_addPromo')->name('i_addPromo');
    Route::get('i_editPromo/{id}','PromoController@i_editPromo')->name('i_editPromo');
    Route::post('i_updatePromo' , 'PromoController@i_updatePromo')->name('i_updatePromo');
    Route::get('i_deletePromo/{id}','PromoController@i_deletePromo');

    Route::post('invisible-logout', 'Auth\LoginController@invisibleLogout')->name('invisible.logout');

});


Route::group(['middleware' => ['auth:web','prevent-back-history']], function() {

Route::get('/', 'HomeController@dashboard')->name('dashboard');
Route::get('/home', 'HomeController@dashboard');
Route::get('/logout', 'Auth\LoginController@logout');

Route::group(['middleware' => ['permission:all|View Invisible Users']], function() {
// Invisble Routes
Route::get('ajaxInvisibles','InvisibleController@getInvisibleUsers')->name('ajaxInvisibles');
Route::get('invisible_users' , 'InvisibleController@invisibleUsers')->name('invisible_users');
Route::get('editInvisibleUser/{id}','InvisibleController@edit')->name('editInvisible');
Route::post('editInvisible','InvisibleController@update');
Route::get('deleteInvisibleUser/{id}','InvisibleController@delete');
Route::get('invisible_sales/{id}','InvisibleController@user_sales_for_admin');
Route::get('ajaxInvisibleSales/{year}/{month}/{invisible}','InvisibleController@getSales')->name('ajaxInvisibleSales');
});

Route::group(['middleware' => ['permission:all|View Products']], function() {
// Products Routes
// Get Ajax Data for Data Tables
Route::get('ajaxProducts','ProductController@getProducts')->name('ajaxProducts');
Route::resource('products' , 'ProductController');
Route::post('updateProduct' , 'ProductController@updateProduct');
Route::get('deleteProduct/{id}','ProductController@deleteProduct');
Route::get('vat/{val}','ProductController@vat');
Route::get('sort-product','ProductController@sortProduct')->name('products.sort');
Route::post('sort-product/update','ProductController@sortProductUpdate')->name('products.sort.update');
});

Route::group(['middleware' => ['permission:all|View Combo Products']], function() {
// Combos Products
Route::get('ajaxComboProducts','ComboProductController@getComboProducts')->name('ajaxComboProducts');
Route::get('combos','ComboProductController@combos')->name('combos');
Route::get('comboEdit/{id}','ComboProductController@comboEdit');
Route::get('addCombo','ComboProductController@addCombo');
Route::post('addComboProduct','ComboProductController@addComboProduct');
Route::post('editComboProduct','ComboProductController@editComboProduct');
Route::get('getCombos/{id}','ComboProductController@getCombos');
Route::get('deleteCombo/{id}','ComboProductController@deleteCombo');

});

Route::group(['middleware' => ['permission:all|View Inventory']], function() {
// Inventory Items
Route::get('ajaxInventories/{branch}','InventoryController@getInventories');
Route::get('inventories','InventoryController@inventories')->name('inventories');
Route::get('addInventory','InventoryController@addInventory');
Route::get('editInventory/{id}','InventoryController@editInventory')->name('editInventory');
Route::post('storeInventory','InventoryController@storeInventory')->name('storeInventory');
Route::post('updateInventory','InventoryController@updateInventory')->name('updateInventory');
Route::get('deleteInventory/{id}','InventoryController@deleteInventory');

});

Route::group(['middleware' => ['permission:all|View Users']], function() {
// Users Routes
Route::get('ajaxUsers','UserController@getUsers')->name('ajaxUsers');
Route::get('users' , 'UserController@index')->name('users.index');
Route::get('addUser' , 'UserController@User');
Route::post('addUser' , 'UserController@addUser')->name('addUser');
Route::get('editUser/{id}','UserController@editUser')->name('editUser');
Route::post('updateUser' , 'UserController@updateUser')->name('updateUser');
Route::get('deleteUser/{id}','UserController@deleteUser');
// Users Logs Checking
Route::get('userLogs/{id}','UserController@userLogs')->name('userLogs');

Route::get('userPermissions/{id}','UserController@userPermissions')->name('userPermissions');
Route::post('userPermissionsSubmit','UserController@userPermissionsSubmit')->name('userPermissionsSubmit');

});


Route::group(['middleware' => ['permission:all|View Categories']], function() {
// Categories Routes
Route::get('ajaxCategories','CategoryController@getCategories')->name('ajaxCategories');
Route::get('categories' , 'CategoryController@index')->name('categories');
Route::get('addCategory' , 'CategoryController@Category');
Route::post('addCategory' , 'CategoryController@addCategory')->name('addCategory');
Route::get('editCategory/{id}','CategoryController@editCategory')->name('editCategory');
Route::post('updateCategory' , 'CategoryController@updateCategory')->name('updateCategory');
Route::get('deleteCategory/{id}','CategoryController@deleteCategory');
Route::get('categoryProducts/{id}/{product_id?}','CategoryController@categoryProducts');
Route::get('sort-category','CategoryController@sortCategory')->name('category.sort');
Route::post('sort-category/update','CategoryController@sortCategoryUpdate')->name('category.sort.update');

});


Route::group(['middleware' => ['permission:all|View Menu']], function() {
    // Categories Routes
    Route::get('ajaxMenus','MenuController@getMenus')->name('ajaxMenus');
    Route::get('menus' , 'MenuController@index')->name('menus');
    Route::get('addMenu' , 'MenuController@Menu');
    Route::post('addMenu' , 'MenuController@addMenu')->name('addMenu');
    Route::get('editMenu/{id}','MenuController@editMenu')->name('editMenu');
    Route::post('updateMenu' , 'MenuController@updateMenu')->name('updateMenu');
    Route::get('deleteMenu/{id}','MenuController@deleteMenu');
    Route::get('sort-menu','MenuController@sortMenu')->name('menu.sort');
    Route::post('sort-menu/update','MenuController@sortMenuUpdate')->name('menu.sort.update');
    
    });


Route::group(['middleware' => ['permission:all|View Sub Menu']], function() {
    // Categories Routes
    Route::get('ajaxSubMenus','SubMenuController@getSubMenus')->name('ajaxSubMenus');
    Route::get('submenus' , 'SubMenuController@index')->name('submenus');
    Route::get('addSubMenu' , 'SubMenuController@SubMenu');
    Route::post('addSubMenu' , 'SubMenuController@addSubMenu')->name('addSubMenu');
    Route::get('editSubMenu/{id}','SubMenuController@editSubMenu')->name('editSubMenu');
    Route::post('updateSubMenu' , 'SubMenuController@updateSubMenu')->name('updateSubMenu');
    Route::get('deleteSubMenu/{id}','SubMenuController@deleteSubMenu');
    Route::get('sort-sub-menu','SubMenuController@sortSubMenu')->name('sub.menu.sort');
    Route::post('sort-sub-menu/update','SubMenuController@sortSubMenuUpdate')->name('submenu.sort.update');
    
});

Route::group(['middleware' => ['permission:all|View Sub Categories']], function() {
// Sub Categories Routes
Route::get('ajaxSubCategories','SubCategoryController@getSubCategories')->name('ajaxSubCategories');
Route::get('subcategories' , 'SubCategoryController@index')->name('subcategories');
Route::get('addSubCategory' , 'SubCategoryController@Category');
Route::post('addSubCategory' , 'SubCategoryController@addSubCategory')->name('addSubCategory');
Route::get('editSubCategory/{id}','SubCategoryController@editSubCategory')->name('editSubCategory');
Route::post('updateSubCategory' , 'SubCategoryController@updateSubCategory')->name('updateSubCategory');
Route::get('deleteSubCategory/{id}','SubCategoryController@deleteSubCategory');
Route::get('sort-subcategory','SubCategoryController@sortSubCategory')->name('subcategory.sort');
Route::post('sort-subcategory/update','SubCategoryController@sortSubCategoryUpdate')->name('subcategory.sort.update');

});

Route::group(['middleware' => ['permission:all|View Areas']], function() {
// Areas Routes
Route::get('ajaxAreas','AreaController@getAreas')->name('ajaxAreas');
Route::get('areas' , 'AreaController@index')->name('areas.index');
Route::get('addArea' , 'AreaController@Area');
Route::post('addArea' , 'AreaController@addArea')->name('addArea');
Route::get('editArea/{id}','AreaController@editArea')->name('editArea');
Route::post('updateArea' , 'AreaController@updateArea')->name('updateArea');
Route::get('deleteArea/{id}','AreaController@deleteArea');

});


Route::group(['middleware' => ['permission:all|View Aggregators']], function() {
    // Areas Routes
    Route::get('ajaxAggregators','AggregatorController@getAggregator')->name('ajaxAggregators');
    Route::get('aggregators' , 'AggregatorController@index')->name('aggregators.index');
    Route::get('addAggregator' , 'AggregatorController@Aggregator');
    Route::post('addAggregator' , 'AggregatorController@addAggregator')->name('addAggregator');
    Route::get('editAggregator/{id}','AggregatorController@editAggregator')->name('editAggregator');
    Route::post('updateAggregator' , 'AggregatorController@updateAggregator')->name('updateAggregator');
    Route::get('deleteAggregator/{id}','AggregatorController@deleteAggregator');
    
    });

Route::group(['middleware' => ['permission:all|View Messages']], function() {
// Messages Routes
Route::get('ajaxMessages','MessageController@getMessages')->name('ajaxMessages');
Route::get('messages' , 'MessageController@index')->name('messages.index');
Route::get('getMessage' , 'MessageController@Message');
Route::get('deleteMessage/{id}','MessageController@deleteMessage');

});

Route::group(['middleware' => ['permission:all|View Customers']], function() {

// Messages Routes
Route::get('ajaxCustomers','CustomerController@getCustomers')->name('ajaxCustomers');
Route::get('customers' , 'CustomerController@index')->name('customers.index');
});

Route::group(['middleware' => ['permission:all|View Bulk Emails']], function() {

// Messages Routes
Route::get('bulk-mails','CustomerController@bulk')->name('bulk');
Route::post('send-bulk' , 'CustomerController@sendBulk')->name('send-bulk');
});

Route::group(['middleware' => ['permission:all|View Popular Products']], function() {

// Popular Products Routes
Route::get('ajaxPopular/{year}/{month}/{day}','ProductController@getPopulars')->name('ajaxPopulars');
Route::get('popular-products' , 'ProductController@index_populars')->name('products.popular');
});


Route::group(['middleware' => ['permission:all|View Roles']], function() {

// Roles Routes
Route::get('ajaxRoles','RoleController@getRoles')->name('ajaxRoles');
Route::get('roles' , 'RoleController@index')->name('roles.index');
Route::get('addRole' , 'RoleController@Role');
Route::post('addRole' , 'RoleController@addRole')->name('addRole');
Route::get('editRole/{id}','RoleController@editRole')->name('editRole');
Route::post('updateRole' , 'RoleController@updateRole')->name('updateRole');
Route::get('deleteRole/{id}','RoleController@deleteRole');
Route::get('rolePermissions/{id}','RoleController@rolePermissions')->name('rolePermissions');
Route::post('rolePermissionsSubmit','RoleController@rolePermissionsSubmit')->name('rolePermissionsSubmit');

});

Route::group(['middleware' => ['permission:all|View Permissions']], function() {

// Permissions Routes
Route::get('ajaxPermissions','PermissionController@getPermissions')->name('ajaxPermissions');
Route::get('permissions' , 'PermissionController@index')->name('permissions');
Route::get('addPermission' , 'PermissionController@Permission');
Route::post('addPermission' , 'PermissionController@addPermission')->name('addPermission');
Route::get('editPermission/{id}','PermissionController@editPermission')->name('editPermission');
Route::post('updatePermission' , 'PermissionController@updatePermission')->name('updatePermission');
Route::get('deletePermission/{id}','PermissionController@deletePermission');
});

Route::group(['middleware' => ['permission:all|View Promos']], function() {

// Promos Routes
Route::get('ajaxPromos','PromoController@getPromos')->name('ajaxPromos');
Route::get('promos' , 'PromoController@index')->name('promos');
Route::get('addPromo' , 'PromoController@Promo');
Route::post('addPromo' , 'PromoController@addPromo')->name('addPromo');
Route::get('editPromo/{id}','PromoController@editPromo')->name('editPromo');
Route::post('updatePromo' , 'PromoController@updatePromo')->name('updatePromo');
Route::get('deletePromo/{id}','PromoController@deletePromo');
Route::get('print-qr/{id}','PromoController@QrSingle')->name('print-qr');
Route::get('download-qr/{id}','PromoController@QrDownload')->name('download-qr');

});

Route::group(['middleware' => ['permission:all|View Branches']], function() {

// Branches Routes
Route::get('ajaxBranches','BranchController@getBranches')->name('ajaxBranches');
Route::get('branches' , 'BranchController@index')->name('branches');
Route::get('addBranch' , 'BranchController@create');
Route::post('addBranch' , 'BranchController@store')->name('addBranch');
Route::get('editBranch/{id}','BranchController@edit')->name('editBranch');
Route::post('updateBranch' , 'BranchController@update')->name('updateBranch');
Route::get('deleteBranch/{id}','BranchController@delete');

});



Route::group(['middleware' => ['permission:all|View Expenses']], function() {

// Branches Routes
Route::get('ajaxExpenses','ExpenseController@getExpenses')->name('ajaxExpenses');
Route::get('expenses' , 'ExpenseController@index')->name('expenses');
Route::get('addExpense' , 'ExpenseController@create');
Route::post('addExpense' , 'ExpenseController@store')->name('addExpense');
Route::get('editExpense/{id}','ExpenseController@edit')->name('editExpense');
Route::post('updateExpense' , 'ExpenseController@update')->name('updateExpense');
Route::get('deleteExpense/{id}','ExpenseController@delete');

});


Route::group(['middleware' => ['permission:all|View Suppliers']], function() {

    // Supliers Routes
    Route::get('ajaxSuppliers','SupplierController@getSuppliers')->name('ajaxSuppliers');
    Route::get('suppliers' , 'SupplierController@index')->name('suppliers');
    Route::get('addSupplier' , 'SupplierController@create');
    Route::post('addSupplier' , 'SupplierController@store')->name('addSupplier');
    Route::get('editSupplier/{id}','SupplierController@edit')->name('editSupplier');
    Route::post('updateSupplier' , 'SupplierController@update')->name('updateSupplier');
    Route::get('deleteSupplier/{id}','SupplierController@delete');

});

Route::group(['middleware' => ['permission:all|View Home Delivery']], function() {

// Orders Routes
Route::get('ajaxOrders/{year}/{month}','OrderController@getOrders');
Route::get('orders' , 'OrderController@index')->name('orders');
Route::get('addOrder' , 'OrderController@create');
Route::post('addOrder' , 'OrderController@store')->name('addOrder');
Route::get('editOrder/{id}','OrderController@edit')->name('editOrder');
Route::post('updateOrder' , 'OrderController@update')->name('updateOrder');
Route::get('deleteOrder/{id}','OrderController@delete');

// Order Details and Modify Order
Route::get('orderUpdate/{id}' , 'OrderController@updateOrder');
Route::get('orderDetails/{id}' , 'OrderController@orderDetails');
Route::get('deleteOrderItem/{id}/{order_item}' , 'OrderController@deleteOrderItem');
Route::get('orderStatus/{id}/{status}' , 'OrderController@changeOrderStatus');
Route::post('orderUserAddress' , 'OrderController@orderUserAddress');
Route::post('orderChange' , 'OrderController@orderChange');
Route::post('updateOrderItems', 'OrderController@updateOrderItems');
});

Route::group(['middleware' => ['permission:all|View Sales']], function() {

// Sales Routes
Route::get('ajaxSales/{year}/{month}/{branch}','SalesController@getSales');
Route::get('ajaxOnlineSales/{year}/{month}/{branch}','SalesController@getOnlineSales');
Route::get('sales' , 'SalesController@index')->name('sales');
Route::get('online_sales' , 'SalesController@online_sales')->name('online_sales');
Route::post('cashDeposit' , 'SalesController@cashDeposit')->name('cashDeposit');

Route::get('summary-sales' , 'SalesController@summary')->name('summary-sales');
Route::get('ajaxSummary/{year}/{month}/{branch}/{start_date?}/{end_date?}','SalesController@getSummary');

Route::get('summary-charts' , 'SalesController@summaryChart')->name('summary-charts');
Route::get('ajaxSummaryCharts/{year}/{month}/{branch}/{start_date?}/{end_date?}','SalesController@getSummaryChart');

Route::get('other-summary-sales/{type}' , 'SalesController@other_summary_sales')->name('other-summary-sales');
Route::get('ajaxOtherSummary/{type}/{year}/{month}/{branch}/{start_date?}/{end_date?}','SalesController@getOtherSummary');

Route::get('otherSale/{type}' , 'SalesController@otherSale')->name('otherSale');
Route::get('otherSale/{year}/{month}/{branch}','SalesController@otherSales');

Route::get('addSale/{type}' , 'SalesController@addSalePage')->name('addSalePage');
Route::post('addSale' , 'SalesController@addSaleDB')->name('addSale');

Route::get('editSale/{id}/{type}' , 'SalesController@editSalePage')->name('editSalePage');
Route::post('editSale' , 'SalesController@editSaleDB')->name('editSale');

});

Route::group(['middleware' => ['permission:all|View Activities']], function() {

// Activities
Route::get('productActivities' , 'ActivityController@products');
Route::get('categoryActivities' , 'ActivityController@categories');
Route::get('userActivities' , 'ActivityController@users');
Route::get('inventoryActivities' , 'ActivityController@inventories');

});

Route::group(['middleware' => ['permission:all|View Franchise Inquiries']], function() {

// Inquiries
Route::get('franchise/inquiries' , 'FranchiseInquiryController@index')->name('franchise_inquiries.index');
Route::get('ajaxFranchisesInquiries','FranchiseInquiryController@inquiries')->name('ajaxFranchiseInquiries');
Route::get('deleteFranchiseInquiry/{id}','FranchiseInquiryController@delete')->name('deleteFranchiseInquiry');
Route::get('showFranchiseInquiry/{id}','FranchiseInquiryController@show')->name('showFranchiseInquiry');

});


});


Auth::routes([
    'register' => false, 
  ]);

