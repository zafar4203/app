<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
header('Access-Control-Allow-Origin:  *');
header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization');

// ProductController
Route::get('getVat', 'Api\ProductController@vat');
Route::get('products', 'Api\ProductController@getProducts');
Route::get('regularProducts', 'Api\ProductController@getRegularProducts');
Route::get('getProductsPOS', 'Api\ProductController@getProductsPOS');
Route::get('getCombos', 'Api\ComboController@getCombos');

// PromoController
Route::get('getPromos', 'Api\PromoController@getPromos');

// Booking Controller
Route::get('todayBookings', 'Api\BookingController@todayBookings');
Route::get('tomorrowBookings', 'Api\BookingController@tomorrowBookings');
Route::post('someDayBookings', 'Api\BookingController@someDayBookings');
Route::post('bookingUpdate', 'Api\BookingController@bookingUpdate');

// OrderController
Route::post('placeOrder', 'Api\OrderController@placeOrder');
Route::post('placeOrderHomeDelivery', 'Api\OrderController@placeOrderHomeDelivery');
Route::post('orderItems', 'Api\OrderController@orderItems');
Route::post('bookOrder', 'Api\OrderController@bookOrder');
Route::post('orders' , 'Api\OrderController@getOrders');
Route::post('sendOrderEmails' , 'Api\OrderController@sendOrderEmails');
Route::post('sendOrderEmailAdmin' , 'Api\OrderController@sendOrderEmailAdmin');
Route::get('getOrderItems/{id}' , 'Api\OrderController@getOrderItems');
Route::post('orderCancel' , 'Api\OrderController@orderCancel');
Route::get('order_drivers' , 'Api\OrderController@getOrderDrivers');
Route::get('orderStatus/{id}/{status}' , 'Api\OrderController@changeOrderStatus');

// Sale Controller 
Route::post('saleByDate' , 'Api\SaleController@getSaleByDate');
Route::post('updateSaleStatus' , 'Api\SaleController@updateSaleStatus');
Route::post('salesItemsCount' , 'Api\SaleController@getSalesItemsCount');

// Category Controller
Route::get('categories', 'Api\CategoryController@getCategories');

// Android POS
Route::post('placeOrderAndroid', 'Api\OrderController@placeOrderAndroid');
Route::post('login', 'Api\UserController@login');
Route::get('getUsers', 'Api\UserController@getUsers');
Route::post('login_driver', 'Api\UserController@login_driver');

Route::post('checkPromo', 'Api\UserController@checkPromo');
Route::post('franchise/inquiry/add', 'Api\FranchiseInquiryController@franchise_inquiry');

Route::post('message', 'Api\MessageController@addMessage');
Route::get('monthly', 'Api\MessageController@monthly');

