<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvisibleActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invisible_activities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('invisible_id');
            $table->string('activity');
            $table->foreign('invisible_id')->references('id')->on('invisibles');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invisible_activities');
    }
}
