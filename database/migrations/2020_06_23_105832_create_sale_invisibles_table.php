<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleInvisiblesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_invisibles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('invisible_id');
            $table->foreign('invisible_id')->references('id')->on('invisibles');
            $table->string('from')->nullable();
            $table->unsignedBigInteger('total');
            $table->string('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_invisibles');
    }
}
