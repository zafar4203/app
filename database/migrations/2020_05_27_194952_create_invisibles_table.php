<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvisiblesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invisibles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email');
            $table->string('phone');
            $table->string('dob');
            $table->string('gender');
            $table->string('address');
            $table->string('city');
            $table->string('password');
            $table->boolean('status')->default(1);
            $table->string('country');
            $table->string('my_ref_id')->nullable();
            $table->string('referal_name')->nullable();
            $table->string('referal_phone')->nullable();
            $table->string('fb_friends');
            $table->string('in_friends');
            $table->unsignedBigInteger('wallet')->default(0);
            $table->integer('p_percentage')->default(10);
            $table->integer('s_percentage')->default(5);            
            $table->string('remember_token')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invisibles');
    }
}
