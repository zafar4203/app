<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('description');
            $table->string('type');
            $table->string('amount');
            $table->float('discount')->default(0);
            $table->string('from')->nullable();
            $table->string('invisible_id')->nullable();
            $table->boolean('status');
            $table->dateTime('expiry_date');
            $table->foreign('invisible_id')->references('id')->on('invisibles');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promos');
    }
}
