<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        DB::table('order_types')->insert([
            'type' => 'Cash'
        ]);

        DB::table('order_types')->insert([
            'type' => 'Visa Card'
        ]);

        DB::table('order_types')->insert([
            'type' => 'Master Card'
        ]);



        DB::table('vat_options')->insert([
            'vat_option' => true
        ]);

        DB::table('referal_percentages')->insert([
            'percentage' => 1
        ]);

        DB::table('categories')->insert([
            'id' => 0,
            'name' => 'Combo',
            'description' => 'Best Combo Products',
            'block' => 0, 
            'image' => 'http://pos.brisktrips.com/public/images/categories/1582663244.png'
        ]);


        DB::table('branches')->insert([
            'name' => 'Kitchen',
            'branch_type' => 'Own',
            'annual_rent' => '5000', 
            'shop_type' => 'Kiosk Without Sitting',
            'location' => 'asdsadsadsad',
            'city' => 'Dubai',
            'country' => 'UAE'
        ]);


        // $faker = Faker::create();
    	// foreach (range(1,1000) as $index) {
	    //     DB::table('products')->insert([
	    //         'name' => $faker->name,
	    //         'price' => $faker->numberBetween(1,5),
        //         'image' => $faker->url,
        //         'block' => $faker->boolean
	    //     ]);
    	// }
    }
}
