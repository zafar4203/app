<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleInvisible extends Model
{
    protected $fillable = [
        'invisible_id',
        'from',
        'total',
        'date'
    ];
}
