<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvisibleActivity extends Model
{
    protected $fillable = [
        'invisible_id',
        'activity'
    ];
}
