<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aggregator extends Model
{
    protected $fillable = ['name', 'description', 'block', 'type'];

    public static function block($val){
        if($val == 0)
        return "Active";
        else
        return "De Active";
    }
}
