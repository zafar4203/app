<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductActivity extends Model
{
    protected $fillable = [
        'original_value' , 'modified_value' , 'attribute' , 'user_id' , 'product_id'
    ];
}
