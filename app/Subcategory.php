<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    protected $fillable = ['name', 'description', 'image', 'category_id' , 'block'];

    public static function block($val){
        if($val == 0)
        return "Active";
        else
        return "De Active";
    }

    public static function activeCategories($categories){
        $activeCategories = array();
        foreach($categories as $category){
            if(!$category->block){
                array_push($activeCategories, $category);
            }
        }
        return $activeCategories;
    }

    public static function categoryName($id){
        $category = Subcategory::where(['id' => $id])->first();
        return $category->name;
    }

    public function products()
    {
        return $this->hasMany('App\Product');
    }
    
    public function category()
    {
    	return $this->belongsTo('App\Category');
    }
}
