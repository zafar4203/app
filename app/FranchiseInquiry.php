<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FranchiseInquiry extends Model
{
    protected $fillable = [
        'name',
        'email',
        'nationality',
        'company',
        'whatsapp',
        'target_country',
        'number_of_branches',
        'type',
        'budget',
        'representer',
        'hear_about_us',
    ];
}
