<?php

namespace App;

use Spatie\Permission\Models\Role;
use App\UserActivity;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable , HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone', 'password' , 'role_id' , 'pin_code', 'branch_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function role($role_id){
        if($role_id != 0){
            $role = Role::where(['id' => $role_id])->first();
            return $role->name;
        }else{
            return "Admin";
        }
    }

    public static function activity($originalValue , $modifiedValue , $attribute , $user_id , $staff_id){
        UserActivity::create([
            'original_value' => $originalValue,
            'modified_value' => $modifiedValue,
            'attribute' => $attribute,
            'user_id' => $user_id,
            'staff_id' => $staff_id
        ]);
    }

    public static function userName($id){
        $user = User::where(['id' => $id])->first();
        return $user->name;
    }

    public function branch(){
        return $this->hasOne('App\Branch', 'id', 'branch_id');
    }
}
