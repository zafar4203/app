<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserActivity extends Model
{
    protected $fillable = [
        'original_value' , 'modified_value' , 'attribute' , 'user_id' , 'staff_id'
    ];
}
