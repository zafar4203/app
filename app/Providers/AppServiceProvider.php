<?php

namespace App\Providers;

use Schema;
use App\Menu;
use App\SubMenu;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*',function($settings){
            $settings->with('menus', Menu::with('subs')->orderBy('sequence')->get());
            $settings->with('submenus', SubMenu::orderBy('sequence')->get());
             
        });
    }
}
