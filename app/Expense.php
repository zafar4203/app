<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $fillable = [
        'id',
        'supplier_id',
        'date',
        'name',
        'invoice_number',
        'description',
        'amount',
        'type',
        'block'
    ];

    public static function expenseName($id){
        $expense = Expense::where(['id' => $id])->first();
        return $expense->name;
    }
}
    