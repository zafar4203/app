<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Invisible extends Authenticatable
{
    use Notifiable;

    protected $guard_name = 'invisible';
    
    protected $fillable = [
        'name' , 
        'email' , 
        'phone' , 
        'gender' , 
        'address' ,
        'city' ,
        'country' ,   
        'dob',
        'password',
        'referal_name',
        'referal_phone',
        'wallet',
        'fb_friends',
        'in_friends',
    ];

    public function orders(){
        return $this->hasMany('App\Order', 'invisible_id')->where(['order_status' => 2]);
    }

    public function totalOrdersMonth(){
        $date = date('m/d/Y');
        $month = date('m', strtotime($date));
        
        return $this->hasMany('App\Order', 'invisible_id')->whereMonth('created_at', '=', $month);
    }

    public function totalOrdersYear(){
        $date = date('m/d/Y');
        $month = date('Y', strtotime($date));
        
        return $this->hasMany('App\Order', 'invisible_id')->whereYear('created_at', '=', $month);
    }

    public function refferer(){
        return $this->hasOne('App\Invisible', 'my_ref_id' , 'referal_name');
    }

    public function activities(){
        return $this->hasMany('App\InvisibleActivity', 'invisible_id')->orderBy('id' , 'desc');;
    }

    public function refferals(){
        return $this->hasMany('App\Invisible', 'referal_name' , 'my_ref_id');
    }

}
