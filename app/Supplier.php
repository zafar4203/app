<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $fillable = [
        'id',
        'name',
        'block'
    ];

    public static function supplierName($id){
        $supplier = Supplier::where(['id' => $id])->first();
        return $supplier->name;
    }
}
    