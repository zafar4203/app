<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $fillable = ['name', 'delivery_charges', 'block'];

    public static function block($val){
        if($val == 0)
        return "Active";
        else
        return "De Active";
    }

}
