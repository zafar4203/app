<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CategoryActivity;

class Category extends Model
{
    protected $fillable = ['name', 'description', 'image' , 'block'];

    public static function block($val){
        if($val == 0)
        return "Active";
        else
        return "De Active";
    }

    public static function activeCategories($categories){
        $activeCategories = array();
        foreach($categories as $category){
            if(!$category->block){
                array_push($activeCategories, $category);
            }
        }
        return $activeCategories;
    }

    public static function activity($originalValue , $modifiedValue , $attribute , $user_id , $category_id){
        CategoryActivity::create([
            'original_value' => $originalValue,
            'modified_value' => $modifiedValue,
            'attribute' => $attribute,
            'user_id' => $user_id,
            'category_id' => $category_id
        ]);
    }

    public function products()
    {
        return $this->hasMany('App\Product');
    }

    public function subcategories()
    {
        return $this->hasMany('App\Subcategory');
    }
    
    public static function categoryName($id){
        $category = Category::where(['id' => $id])->first();
        return $category->name;
    }
}
