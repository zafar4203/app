<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryActivity extends Model
{
    protected $fillable = [
        'original_value' , 
        'modified_value' , 
        'attribute' , 
        'user_id' , 
        'branch_id',
        'product_id'
    ];
}
