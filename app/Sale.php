<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $fillable = [
        'cash',
        'card',
        'cash_deposit',
        'total',
        'vat',
        'source',
        'date',
        'branch_id',
    ];
}
