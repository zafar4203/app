<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderCancel extends Model
{
    protected $fillable = [
        'order_id',
        'user_id',
        'cancel_comments'    
    ];
}
