<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use Notifiable;

    protected $fillable = ['user_name' ,'user_email' ,'user_phone' ,'user_address' , 'user_id' , 'order_from' , 'total_price' ,'vat_price' ,'sub_total_price' , 'total_items' , 'delivery_date' , 'order_type', 'user_id', 'order_status', 'branch_id', 'invisible_id', 'discount', 'source', 'bd_cakes', 'area'];
    protected $casts = [
        'bd_cakes' => 'array',
    ];

    public function booking(){
        return $this->hasOne('App\Booking', 'order_id');
    }

    public static function orderStatus($val){
        if($val == "0"){
            return "New Order";
        }
        if($val == "1"){
            return "On Way";
        }
        if($val == "2"){
            return "Delivered";
        }
        if($val == "3"){
            return "Canceled";
        }
    }

    public static function statusClass($val){
        if($val == "0"){
            return "new";
        }
        if($val == "1"){
            return "on-way";
        }
        if($val == "2"){
            return "delivered";
        }
        if($val == "3"){
            return "canceled";
        }
    }

}
