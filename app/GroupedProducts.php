<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupedProducts extends Model
{
    protected $fillable = ['product_id' , 'selected_product_id'];
}
