<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CategoryActivity;

class Menu extends Model
{
    protected $fillable = ['name', 'link', 'icon' ,'permission' ,'sequence' ,'block'];

    public static function block($val){
        if($val == 0)
        return "Active";
        else
        return "De Active";
    }
 
    public function subs()
    {
        return $this->hasMany('App\SubMenu');
    }
}
