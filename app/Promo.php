<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promo extends Model
{
    protected $fillable = [
        'name',
        'description',
        'status',
        'amount',
        'type',
        'discount',
        'from',
        'invisible_id',
        'expiry_date'
    ];
}
