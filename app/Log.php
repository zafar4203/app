<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $fillable = ['date' , 'ip' , 'mac' , 'login_time' , 'logout_time' , 'login_status' , 'user_id', 'browser'];
}
