<?php

namespace App;

use App\InventoryActivity;
use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    protected $fillable = [
        'product_id', 
        'branch_id' , 
        'initial_qty',
        'current_qty',        
    ];

    public static function activity($originalValue , $modifiedValue , $attribute  , $staff_id , $product_id, $branch_id){
        InventoryActivity::create([
            'original_value' => $originalValue,
            'modified_value' => $modifiedValue,
            'attribute' => $attribute,
            'product_id' => $product_id,
            'branch_id' => $branch_id,
            'user_id' => $staff_id
        ]);
    }
}
