<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReferalPercentage extends Model
{
    protected $fillable = [
        'percentage'  
    ];
}
