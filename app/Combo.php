<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Combo extends Model
{
    protected $fillable = [
        'name',
        'product_id',
        'selected_product_id',
        'type',
        'quantity'
    ];
}
