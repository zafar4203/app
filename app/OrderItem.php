<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $fillable = [
        'vat',
        'order_id',
        'product_id',
        'product_name',
        'product_price',
        'product_quantity',
        'product_category_id'    
    ];

    public function product(){
        return $this->hasOne('App\Product', 'id', 'product_id');
    }

}
