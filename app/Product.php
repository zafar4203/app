<?php

namespace App;

use App\Category;
use App\ProductActivity;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name', 'short_name' , 'price', 'block', 'image', 'type', 'category_id', 'for' , 'invisible_price', 'flavour', 'flavour_price', 'input_items', 'output_items', 'combo_price', 'is_for', 'details'];
    public static function block($val){
        if($val == 0)
        return "Approve";
        else
        return "Block";
    }

    public static function activity($originalValue , $modifiedValue , $attribute , $user_id , $product_id){
        ProductActivity::create([
            'original_value' => $originalValue,
            'modified_value' => $modifiedValue,
            'attribute' => $attribute,
            'user_id' => $user_id,
            'product_id' => $product_id
        ]);
    }

    public static function productName($id){
        $product = Product::where(['id' => $id])->first();
        return $product->name;
    }

    public function subcategory()
    {
        return $this->belongsTo('App\Subcategory');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

}
