<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CategoryActivity;

class SubMenu extends Model
{
    protected $fillable = ['name', 'link', 'permission' ,'sequence' ,'menu_id','block'];

    public static function block($val){
        if($val == 0)
        return "Active";
        else
        return "De Active";
    }

 

}
