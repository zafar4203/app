<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VatOption extends Model
{
    protected $fillable = [
        'vat_option'
    ];
}
