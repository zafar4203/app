<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryActivity extends Model
{
    protected $fillable = [
        'original_value' , 'modified_value' , 'attribute' , 'user_id' , 'category_id'
    ];
}
