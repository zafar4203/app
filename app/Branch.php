<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $fillable = [
        'id',
        'name',
        'branch_type',
        'annual_rent',
        'shop_type',
        'location',
        'city',
        'country',
        'whatsapp',
        'phone',
        'map',
    ];

    public static function branchName($id){
        $branch = Branch::where(['id' => $id])->first();
        return $branch->name;
    }
}
