<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OtherSale extends Model
{
    protected $fillable = ['cash', 'aggregator_id' , 'date'];

}
