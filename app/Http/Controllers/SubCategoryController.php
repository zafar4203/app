<?php

namespace App\Http\Controllers;

use Auth;
use File;
use Response;
use Validator;
use DataTables;
use App\Product;
use App\Category;
use App\Subcategory;
use App\User;
use App\Notifications\Noti;
use Illuminate\Http\Request;

class SubCategoryController extends Controller
{
    public $details = [];
    public function __construct(){
        $this->middleware('auth');

        $this->details = [
            'read_at' => null,
            'data' => [
                'link' => '',
                'text' => ''
            ],
        ];
    }
    
    public function getSubCategories(){
        $subcategories = Subcategory::select('id', 'name', 'description', 'category_id', 'block' , 'created_at')->get();

        foreach($subcategories as $subcategory){
            $subcategory->isBlock = Subcategory::block($subcategory->block);
        }

        return DataTables::of($subcategories)
        ->addColumn('category', function ($id) {
            return $id->category->name;
        })
        ->addColumn('action', function ($id) {

            $data = "";
            if(Auth::user()->can('Update Sub Category') || Auth::user()->can('all')){
                $data = '<a href="'. route("editSubCategory" , $id) .'" class="text-primary"><i class="fa fa-edit"></i></a>';
            }
            if(Auth::user()->can('Delete Sub Category') || Auth::user()->can('all')){            
                $data = $data . ' <a class="text-primary" onClick="delete_click('.$id->id.')" ><i class="fa fa-trash"></i></button>'; 
            }
            return $data;
        })
        ->make(true);
    }

    public function index(){
        return view("subcategories.subcategories");
    }

    public function Category(){
        $categories = Category::where(['block' => 0])->get();
        return view("subcategories.addSubCategory",compact('categories'));
    }

    public function addSubCategory(Request $request){

        $validator = Validator::make($request->all(), [ 
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:255', 
            'description' => 'required|regex:/^[\pL\s\-]+$/u|max:500',
            'image' => 'required',
            'category_id' => 'required'
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $img = "";
        if($request->hasfile('image')){
            $file = $request->file('image');
            $filename = time().'.'.$file->getClientOriginalExtension();
            $destinationPath = public_path('/images/categories');
            $file->move($destinationPath, $filename);    
            $img = asset("public/images/categories/".$filename);
        }

        $subcategory = Subcategory::create([
            'name' => $request->name , 
            'description' => $request->description, 
            'block' => $request->block, 
            'image' => $img,
            'category_id' => $request->category_id
             ]);
        $subcategory->save();

        $this->notifyUsers('New Sub Category Added');

        return redirect('subcategories')->with('message', 'Sub Category Added Successfully');;
    }

    public function editSubCategory($id)
    {
        $categories = Category::where(['block' => 0])->get();
        $subcategory = Subcategory::where(['id' => $id])->first();
        return view('subcategories.editSubCategory' , compact('categories','subcategory'));
    }

    public function updateSubCategory(Request $request){
        $validator = Validator::make($request->all(), [ 
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:255', 
            'description' => 'required|regex:/^[\pL\s\-]+$/u|max:500',
            'category_id' => 'required',
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $subcategory = Subcategory::where(['id' => $request->id])->first();
        $img = $subcategory->image;
        if($request->hasfile('image')){

            $split = explode("/", $subcategory->image);
            $imageName  = $split[7];
    
            $destinationPath = public_path('/images/categories')."/".$imageName;
            if(File::exists($destinationPath)) {
                File::delete($destinationPath);            
            }
    

            $file = $request->file('image');
            $filename = time().'.'.$file->getClientOriginalExtension();
            $destinationPath = public_path('/images/categories');
            $file->move($destinationPath, $filename);    
            $img = asset("public/images/subcategories/".$filename);
        }        


        $subcategory->name = $request->name;
        $subcategory->description = $request->description;
        $subcategory->block = $request->block;
        $subcategory->image = $img;
        $subcategory->category_id = $request->category_id;

        $subcategory->save();

        $this->notifyUsers('Sub Category Updated');

        return redirect()->back()->with('message', 'Sub Category Updated Successfully');;
    }

    public function deleteSubCategory($id){

        Subcategory::where(['id' => $id])->delete();      
        $this->notifyUsers('Sub Category Deleted');
  
        return redirect()->back()->with('message', 'Sub Category Deleted Successfully');;
    }

    public function sortSubCategory(){
        $categories = Category::where(['block' => 0])->orderBy('sequence')->get();        
        $subcategories = SubCategory::where(['block' => 0])->orderBy('sequence')->get();        
        return view('subcategories.sequence',compact('subcategories','categories'));
    }

    public function sortSubCategoryUpdate(Request $request){
        $ids = $request->ids;
        foreach($ids as $key => $id){
            $category = Subcategory::where(['id'=> $id])->first();
            $category->sequence = $key;
            $category->save();
        }

        return response()->json(['status' => 200, 'msg' => "Success"]);
    }

    public function notifyUsers($message){
        $this->details['data']['link'] = route('subcategories');
        $this->details['data']['text'] = $message;
        $users = User::all();
        foreach($users as $user){
            if($user->can('View Sub Categories')  || $user->can('all')){
                $user->notify(new Noti($this->details));
            }
        }
    }
}
