<?php

namespace App\Http\Controllers;

use DataTables;
use DateTime;
use Carbon\Carbon;
use App\Order;
use App\User;
use App\OrderItem;
use Illuminate\Http\Request;
use DB;
use Validator;

use Notification;
use App\Notifications\AdminNotification;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getCustomers(){


        $orders = DB::table('orders')
        ->select('user_email','user_phone','user_name', DB::raw('COUNT(user_email) as count'))
        ->where('order_from' ,'!=','POS')
        ->groupBy('user_email','user_phone','user_name')
        ->get();

        foreach($orders as $key=> $order){
            if($order->count == 0){
                $orders->forget($key);
            }else{
                $order->orders = $order->count;
            }
        }

        return DataTables::of($orders)
        ->make(true);
    }

    public function index(){
        return view('customers.index');
    }

    public function bulk(){
        return view('customers.bulk');
    }

    public function sendBulk(Request $request){

        $validator = Validator::make($request->all(), [ 
            'subject' => 'required', 
            'details' => 'required'
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $orders = DB::table('orders')
        ->select('user_email','user_phone','user_name', DB::raw('COUNT(user_email) as count'))
        ->where('order_from' ,'!=','POS')
        ->groupBy('user_email','user_phone','user_name')
        ->get();

        foreach($orders as $key=> $order){
            if($order->count == 0){
                $orders->forget($key);
            }else{
                $order->orders = $order->count;
            }
        }

        foreach($orders as $order)
        {
            $details = [
                'greeting' => 'Hi '.$order->user_name,
                'subject' => $request->subject,
                'body' => $request->details,
                'thanks' => 'Thank you for choosing Papafluffy !'
            ];
                
            $user = new User();
            $user->email= $order->user_email;
            Notification::send($user , new AdminNotification($details));    
        } 

        return redirect('bulk-mails')->with('message', 'Email Sent Successfully');
    }
}
