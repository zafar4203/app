<?php

namespace App\Http\Controllers;

use Auth;
use Hash;
use Redirect;
use Validator;
use App\User;
use App\Branch;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Log;
use App\Notifications\Noti;
use DataTables;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public $details = [];
    public function __construct(){
        $this->middleware('auth');

        $this->details = [
            'read_at' => null,
            'data' => [
                'link' => '',
                'text' => ''
            ],
        ];
    }
    
    public function getUsers(){
        $users = User::select('id', 'name', 'email', 'phone', 'role_id', 'created_at')->get();

        foreach($users as $user){
            $user->role = User::role($user->role_id);
        }

        return DataTables::of($users)
        ->addColumn('action', function ($id) {

            $data = "";
            if(Auth::user()->can('Update User') || Auth::user()->can('all')){
                $data = '<a href="'. route("editUser" , $id) .'" class="text-primary"><i class="fa fa-edit"></i></a>';
            }
            if(Auth::user()->can('Delete User') || Auth::user()->can('all')){            
                $data = $data . ' <a class="text-primary" onClick="delete_click('.$id->id.')" ><i class="fa fa-trash"></i></a>';   
            }
            if(Auth::user()->can('User Permissions') || Auth::user()->can('all')){ 
                $data = $data . ' <a href="'. route("userPermissions" , $id->id) .'" class="text-primary"><i class="fa fa-eye"></i></a>';    
            }

            return $data;

        })
        ->addColumn('nameLink', function ($user) {
            return '<a href="'. route("userLogs" , $user->id) .'" class="text-primary">'.$user->name.'</a>';
        })
        ->rawColumns(['nameLink', 'action' , 'confirmed'])
        ->make(true);
    }

    public function index()
    {
        return view("users.users");
    }

    public function userPermissions($id){
        $user = User::where('id',$id)->first();
        $permissions = Permission::all();
        return view('users.userPermissions' , compact('user','permissions'));
    }

    public function userPermissionsSubmit(Request $request){
        $user = User::where(['id' => $request->user_id])->first();
        $permissions = Permission::all();

            foreach($permissions as $permission){
                if($user->hasPermissionTo($permission->name)){
                    $user->revokePermissionTo($permission);
                }
            }    

            foreach($permissions as $permission){
                if(isset($request->rp[$permission->name])){
                    $user->givePermissionTo($permission->name);
                }
            }    

        return redirect(route('userPermissions',$user->id))->with('message', 'Permissions Assigned Successfully');
    }


    public function User()
    {
        $roles = Role::all();
        $branches = Branch::all();
        if(count($roles) > 0){
            if(count($branches) > 0){
                return view("users.addUser" , compact('roles' , 'branches'));
            }else{
                return redirect('users')->with('errorUsers', 'Please Add Branches First');;                
            }
        }else{
            return redirect('users')->with('errorUsers', 'Please Add User Roles First');;
        }

    }

    public function addUser(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:255', 
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|string|unique:users|digits_between:11,14',
            'password' => 'required|string|min:8',
            'role' => 'required',
            'branch_id' => 'required',
            'pin_code' => 'required|digits:4|unique:users'
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $user = User::create(['name' => $request->name , 'email' => $request->email, 'password' => Hash::make($request->password) , 'role_id' => $request->role , 'phone' => $request->phone, 'pin_code' => $request->pin_code, 'branch_id' => $request->branch_id]);
        $user->save();

        $role = Role::where(['id' => $request->role])->first();
        $user->assignRole($role->name);

        $this->notifyUsers('New User Added');

        return redirect('users')->with('message', 'User Added Successfully');;
    }

    public function editUser($id)
    {
        $roles = Role::all();
        $branches = Branch::all();
        $user = User::where(['id' => $id])->first();
        return view('users.editUser' , compact('user' , 'roles', 'branches'));
    }

    public function updateUser(Request $request){
        $validator = Validator::make($request->all(), [ 
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:255', 
            'email' => 'required|string|email|max:255|unique:users,email,'.$request->id,
            'phone' => 'required|string|digits_between:11,14|unique:users,phone,'.$request->id,
            'password' => 'required|string|min:8',
            'role' => 'required',
            'branch_id' => 'required',
            'pin_code' => 'required|digits:4|unique:users,pin_code,'.$request->id
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $user = User::where(['id' => $request->id])->first();
        $oldUser = new user();
        $oldUser->name = $user->name;
        $oldUser->phone = $user->phone;
        $oldUser->email = $user->email;
        $oldUser->password = $user->password;
        $oldUser->role_id = $user->role_id;
        $oldUser->branch_id = $user->branch_id;
        $oldUser->pin_code = $user->pin_code;

        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->role_id = $request->role;
        $user->pin_code = $request->pin_code;
        $user->branch_id = $request->branch_id;

        if($user->isDirty('name')){
            User::activity($oldUser->name , $request->name , 'Name' , Auth::id() , $user->id );            
        }
        if($user->isDirty('phone')){
            User::activity($oldUser->phone , $request->phone , 'Phone' , Auth::id() , $user->id );            
        }
        if($user->isDirty('email')){
            User::activity($oldUser->email , $request->email , 'Email' , Auth::id() , $user->id );            
        }
        if($user->isDirty('password')){
            if(!Hash::check($request->password,$oldUser->password)){
                User::activity($oldUser->password , $request->password , 'Password' , Auth::id() , $user->id );            
            }
        }
        if($user->isDirty('role_id')){
            User::activity($oldUser->role_id , $request->role , 'Role' , Auth::id() , $user->id );            
        }
        if($user->isDirty('pin_code')){
            User::activity($oldUser->pin_code , $request->pin_code , 'Pin Code' , Auth::id() , $user->id );            
        }
        if($user->isDirty('branch_id')){
            User::activity($oldUser->branch_id , $request->branch_id , 'Branch' , Auth::id() , $user->id );            
        }

        $user->save();

        $user->roles()->detach();
        $role = Role::where(['id' => $request->role])->first();
        $user->assignRole($role->name);

        $this->notifyUsers('User Updated');

        return redirect('users')->with('message', 'User Updated Successfully');;
    }

    public function deleteUser($id){
        User::where(['id' => $id])->delete();        
        $this->notifyUsers('User Deleted');

        return redirect()->back()->with('message', 'User Deleted Successfully');;
    }

    public function userLogs($user_id){
        $user = User::where(['id' => $user_id])->first();
        $logs = Log::where(['user_id' => $user_id])->get();
        return view('users.userLogs' , compact('logs' , 'user'));
    }

    public function notifications()
    {
        $notifications = auth()->user()->unreadNotifications()->limit(5)->get();
        foreach($notifications as $notification){
            $notification->date = $notification->created_at->diffForHumans(); 
        }

        return response()->json(array('success' => true , 'count' => auth()->user()->unreadNotifications()->count() , 'notifications' => $notifications), 200);
    }

    public function notificationRead($id)
    {
        $notification = auth()->user()->notifications()->find($id);
        if($notification) {
            $notification->markAsRead();
            return 0;
        }
        return 1;
    }

    public function notifyiUsers($keyword , $text , $link){

        $details = [
            'read_at' => null,
            'data' => [
                'link' => $link,
                'permission' => $keyword,
                'text' => $text
            ],
        ];

        $users = User::all();
        foreach($users as $user){
            if(strpos($keyword, 'Product') !== false){
                if($user->can('View Products')  || $user->can('all')){
                    $user->notify(new Noti($details));
                }
            }

            if(strpos($keyword, 'Home Delivery') !== false || strpos($keyword, 'Add Items') !== false || strpos($keyword, 'Add Order') !== false){
                if($user->can('Home Delivery')  || $user->can('all')){
                    $user->notify(new Noti($details));
                }
            }

            if(strpos($keyword, 'Branch') !== false){
                if($user->can('View Branches')  || $user->can('all')){
                    $user->notify(new Noti($details));
                }
            }

            if(strpos($keyword, 'Sale') !== false){
                if($user->can('View Sales')  || $user->can('all')){
                    $user->notify(new Noti($details));
                }
            }

            if(strpos($keyword, 'Franchise') !== false){
                if($user->can('View Franchise Inquiries')  || $user->can('all')){
                    $user->notify(new Noti($details));
                }
            }

            if(strpos($keyword, 'Promo') !== false){
                if($user->can('View Promos')  || $user->can('all')){
                    $user->notify(new Noti($details));
                }
            }

            if(strpos($keyword, 'Invisible') !== false){
                if($user->can('View Invisible Users')  || $user->can('all')){
                    $user->notify(new Noti($details));
                }
            }

            if(strpos($keyword, 'Combo') !== false){
                if($user->can('View Combo Products')  || $user->can('all')){
                    $user->notify(new Noti($details));
                }
            }

            if(strpos($keyword, 'Inventory') !== false){
                if($user->can('View Inventory')  || $user->can('all')){
                    $user->notify(new Noti($details));
                }
            }

            if(strpos($keyword, 'User') !== false){
                if($user->can('View Users')  || $user->can('all')){
                    $user->notify(new Noti($details));
                }
            }

            if(strpos($keyword, 'Category') !== false){
                if($user->can('View Categories')  || $user->can('all')){
                    $user->notify(new Noti($details));
                }
            }

            if(strpos($keyword, 'Sub Category') !== false){
                if($user->can('View Sub Categories')  || $user->can('all')){
                    $user->notify(new Noti($details));
                }
            }

            if(strpos($keyword, 'Area') !== false){
                if($user->can('View Area')  || $user->can('all')){
                    $user->notify(new Noti($details));
                }
            }

            if(strpos($keyword, 'Messages') !== false){
                if($user->can('View Messages')  || $user->can('all')){
                    $user->notify(new Noti($details));
                }
            }

            if(strpos($keyword, 'Bulk Emails') !== false){
                if($user->can('View Bulk Emails')  || $user->can('all')){
                    $user->notify(new Noti($details));
                }
            }

            if(strpos($keyword, 'Role') !== false){
                if($user->can('View Roles')  || $user->can('all')){
                    $user->notify(new Noti($details));
                }
            }

            if(strpos($keyword, 'Permission') !== false){
                if($user->can('View Permissions') || $user->can('all')){
                    $user->notify(new Noti($details));
                }
            }


        }

        return $users;
    }


    public function notifyUsers($message){
        $this->details['data']['link'] = route('users.index');
        $this->details['data']['text'] = $message;
        $users = User::all();
        foreach($users as $user){
            if($user->can('View Users')  || $user->can('all')){
                $user->notify(new Noti($this->details));
            }
        }
    }

}
