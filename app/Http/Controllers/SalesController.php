<?php

namespace App\Http\Controllers;

use DB;
use DataTables;
use App\Sale;
use App\OtherSale;
use \Carbon\Carbon;
use Validator;
use App\Aggregator;
use App\Branch;
use Illuminate\Http\Request;

class SalesController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function getSales($year , $month , $branch){
        if($month < 10){
            $month = sprintf("%02d", $month);
        }
        $sales = Sale::select('id', 'cash', 'card', 'total' , 'cash_deposit' ,'branch_id', 'vat' ,'date' , 'created_at')
        ->whereYear('created_at', '=', $year)
        ->whereMonth('created_at', '=', $month)
        ->where('branch_id', '=', $branch)
        ->get();

        foreach($sales as $sale){
            if(!$sale->cash_deposit){
                $sale->cash_deposit = 0;
            }
        }
        
        return DataTables::of($sales)
        ->addColumn('action', function ($id) {
            return '<a href="#" class="text-primary"><i class="fa fa-edit"></i></a>'; 
        })
        ->addColumn('closing', function ($id) {
            if($id->cash_deposit != 0){
                return '<p class="text-center" style="font-weight:bold;">Closed</p>';
            }else{
                return ' <button class="btn-block btn btn-success" onClick="delete_click('.$id->id.')" >Closing</button>';    
            }
        })
        ->rawColumns(['action', 'closing'])
        ->make(true);
    }

    public function index(){
        $branches = Branch::where('name' ,'!=' , 'Kitchen')->get();
        return view('sales.sales' , compact('branches'));
    }

    public function create(){}

    public function store(){}

    public function addSalePage($type){
        $aggregators = Aggregator::where('block' , '!=' , 1)->where(['type' => $type])->get();
        if(count($aggregators) != 0){
            return view('sales.addSale' , compact('aggregators' , 'type'));
        }

        return redirect()->route('sales')->with('error', 'Use Valid Sale Type');
    }

    public function editSalePage($id , $type){
        $sale = OtherSale::where('id' , '=' , $id)->first();
        $aggregators = Aggregator::where('block' , '!=' , 1)->where(['type' => $type])->get();
        return view('sales.editSale' , compact('sale' , 'aggregators' , 'type'));
    }

    public function editSaleDB(Request $request){

        $validator = Validator::make($request->all(), [ 
            'aggregator_id' => 'required', 
            'cash' => 'required',
            'date' => 'required'
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $splitDate = explode('-', $request->date);
        $yr = $splitDate[0];
        $mn = $splitDate[1];
        $day = $splitDate[2];

        $date = $mn."/".$day."/".$yr;
        $sale = OtherSale::where(['id' => $request->id])->first();
        $sale->aggregator_id = $request->aggregator_id;
        $sale->cash = $request->cash;
        $sale->date = $date;
        $sale->save();

        return redirect()->route('otherSale' , $request->type)->with('message', 'Sale Updated Successfully');
    }

    public function addSaleDB(Request $request){

        $validator = Validator::make($request->all(), [ 
            'aggregator_id' => 'required', 
            'cash' => 'required',
            'date' => 'required'
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $splitDate = explode('-', $request->date);
        $yr = $splitDate[0];
        $mn = $splitDate[1];
        $day = $splitDate[2];

        $date = $mn."/".$day."/".$yr;
        $sale = OtherSale::where(['date' => $date , 'aggregator_id' => $request->aggregator_id ])->first();

        if(!is_null($sale)){
            $sale->cash = $sale->cash + $request->cash;
            $sale->date = $date;
            $sale->save();
        }else{
            OtherSale::create([
                'cash' => (float)$request->cash,
                'date' => $date,
                'aggregator_id' => $request->aggregator_id
            ]);
        }

        return redirect()->route('otherSale' , $request->type)->with('message', 'Sale Added Successfully');;
    }

    public function otherSale($type){
        if($type == "aggregator" || $type == "Aggregator"){
            $aggregators = Aggregator::where(['type' => 'aggregator'])->get();
            return view('sales.otherSales' , compact('aggregators' , 'type'));    
        }
        if($type == "b2b" || $type == "B2B"){
            $aggregators = Aggregator::where(['type' => 'b2b'])->get();
            return view('sales.otherSales' , compact('aggregators'  , 'type'));    
        }
        if($type == "other" || $type == "Other"){
            $aggregators = Aggregator::where(['type' => 'other'])->get();
            return view('sales.otherSales' , compact('aggregators' , 'type'));    
        }
    }

    public function otherSales($year , $month , $aggregator){
        if($month < 10){
            $month = sprintf("%02d", $month);
        }
        $agg = Aggregator::where(['id' => $aggregator])->first();
        $sales = OtherSale::select('id', 'cash' , 'date' , 'created_at')
        ->whereYear('created_at', '=', $year)
        ->whereMonth('created_at', '=', $month)
        ->where('aggregator_id', '=', $aggregator)
        ->get();
        
        return DataTables::of($sales)
        ->addColumn('action', function ($id) use ($agg) {
            return '<a href="'. route('editSalePage', [$id->id , $agg->type]) .'" class="text-primary"><i class="fa fa-edit"></i></a>'; 
        })
        ->make(true);
    }

    public function cashDeposit(Request $request){
        $sale = Sale::where(['id' => $request->sale_id])->first();
        $sale->cash_deposit = $request->cash_deposit;
        $sale->save();

        return response()->json('success', 200);        
    }

    public function online_sales(){
        $branches = Branch::where(['name' => 'Kitchen'])->get();
        return view('sales.online_sales' , compact('branches'));
    }

    public function getOnlineSales($year , $month , $branch){
        if($month < 10){
            $month = sprintf("%02d", $month);
        }
        $sales = Sale::select('id', 'cash', 'card', 'total' , 'cash_deposit' ,'branch_id', 'vat' ,'date' , 'created_at')
        ->whereYear('created_at', '=', $year)
        ->whereMonth('created_at', '=', $month)
        ->where('branch_id', '=', $branch)
        ->get();
        
        return DataTables::of($sales)
        ->addColumn('action', function ($id) {
            return '<a href="#" class="text-primary"><i class="fa fa-edit"></i></a>'; 
        })
        ->addColumn('closing', function ($id) {
            if($id->cash_deposit){
                return '<p class="text-center" style="font-weight:bold;">Closed</p>';
            }else{
                return ' <button class="btn-block btn btn-success" onClick="delete_click('.$id->id.')" >Closing</button>';    
            }
        })
        ->rawColumns(['action', 'closing'])
        ->make(true);
    }

    public function summary(){
        $branches = Branch::where('name' ,'!=' , 'Kitchen')->get();
        return view('sales.summary' , compact('branches'));
    }


    public function getSummary($year , $month , $branch , $start = null, $end = null){
        if($month < 10){
            $month = sprintf("%02d", $month);
        }

        if($branch != "all"){

            if($start && $end){
                $paydate_raw = DB::raw("STR_TO_DATE(`date`, '%m/%d/%Y')");
                
                $sales = Sale::select('id', 'cash', 'card', 'total' , 'cash_deposit' ,'branch_id', 'vat' ,'date' , 'created_at')
                ->where('branch_id', '=', $branch)
                ->whereDate($paydate_raw , '>=' , $start)
                ->whereDate($paydate_raw , '<=' , $end)
                ->get();   

            }else{
                $sales = Sale::select('id', 'cash', 'card', 'total' , 'cash_deposit' ,'branch_id', 'vat' ,'date' , 'created_at')
                ->whereYear('created_at', '=', $year)
                ->whereMonth('created_at', '=', $month)
                ->where('branch_id', '=', $branch)
                ->get();              
            }
    
            foreach($sales as $sale){
                $sale->branch = Branch::where(['id' => $branch])->first()->name;
            }
    
            $cash = 0;
            $card = 0;
            $total = 0;
            $vat = 0;
            $cash_deposit = 0;
            $id = 0;
            foreach($sales as $sale){
                $cash = $cash + $sale->cash;
                $card = $card + $sale->card;
                $total = $total + $sale->total;
                $vat = $vat + $sale->vat;
                $cash_deposit = $cash_deposit + $sale->cash_deposit;
                $id = $sale->id;
            }
    
            $sale = Sale::where(['id' => $id])->first();
            if($sale){
                $sale->cash = round($cash , 2);
                $sale->card = round($card , 2);
                $sale->total = round($total , 2);
                $sale->cash_deposit = round($cash_deposit , 2);
                $sale->vat = round($vat , 2);
                $sale->branch = Branch::where(['id' => $branch])->first()->name;
        
                $sales = [];
                array_push($sales , $sale);    
            }
    
            return DataTables::of($sales)
            ->addColumn('action', function ($id) {
                return '<a href="#" class="text-primary"><i class="fa fa-edit"></i></a>'; 
            })
            ->rawColumns(['action'])
            ->make(true);
    
        }else{
            $all_sales = [];
            $branches = Branch::where('name','!=','Kitchen')->get();
            foreach($branches as $key => $branch){

                if($start && $end){
                    $paydate_raw = DB::raw("STR_TO_DATE(`date`, '%m/%d/%Y')");
                    
                    $sales = Sale::select('id', 'cash', 'card', 'total' , 'cash_deposit' ,'branch_id', 'vat' ,'date' , 'created_at')
                    ->where('branch_id', '=', $branch->id)
                    ->whereDate($paydate_raw , '>=' , $start)
                    ->whereDate($paydate_raw , '<=' , $end)
                    ->get();   
    
                }else{
    
                $sales = Sale::select('id', 'cash', 'card', 'total' , 'cash_deposit' ,'branch_id', 'vat' ,'date' , 'created_at')
                ->whereYear('created_at', '=', $year)
                ->whereMonth('created_at', '=', $month)
                ->where('branch_id', '=', $branch->id)
                ->get();
                }        


                if($sales)
                foreach($sales as $sale){
                    $sale->branch = $branch->name;
                }
        
                $cash = 0;
                $card = 0;
                $total = 0;
                $vat = 0;
                $cash_deposit = 0;
                $id = 0;

                if($sales){
                    foreach($sales as $sale){
                        $cash = $cash + $sale->cash;
                        $card = $card + $sale->card;
                        $total = $total + $sale->total;
                        $vat = $vat + $sale->vat;
                        $cash_deposit = $cash_deposit + $sale->cash_deposit;
                        $id = $sale->id;
                    }
            
                    $sale = Sale::first();
                    $sale->cash = round($cash , 2);
                    $sale->card = round($card , 2);
                    $sale->total = round($total , 2);
                    $sale->cash_deposit = round($cash_deposit , 2);
                    $sale->vat = round($vat , 2);
                    $sale->branch = $branch->name;
            
                    array_push($all_sales , $sale);    
                }                     
            }

            return DataTables::of($all_sales)
            ->addColumn('action', function ($id) {
                return '<a href="#" class="text-primary"><i class="fa fa-edit"></i></a>'; 
            })
            ->rawColumns(['action'])
            ->make(true);

        }
    }
    
    public function other_summary_sales($type){
        if($type == "aggregator" || $type == "Aggregator"){
            $aggregators = Aggregator::where(['type' => 'aggregator'])->get();
            return view('sales.otherSummary' , compact('aggregators' , 'type'));    
        }
        if($type == "b2b" || $type == "B2B"){
            $aggregators = Aggregator::where(['type' => 'b2b'])->get();
            return view('sales.otherSummary' , compact('aggregators'  , 'type'));    
        }
        if($type == "other" || $type == "Other"){
            $aggregators = Aggregator::where(['type' => 'other'])->get();
            return view('sales.otherSummary' , compact('aggregators' , 'type'));    
        }
    }

    public function getOtherSummary($type , $year , $month , $aggregator , $start = null , $end = null){
        if($month < 10){
            $month = sprintf("%02d", $month);
        }

        if($aggregator != "all"){

            if($start && $end){
                $paydate_raw = DB::raw("STR_TO_DATE(`date`, '%m/%d/%Y')");
                
                $sales = OtherSale::select('id', 'cash' , 'date' , 'created_at')
                ->where('aggregator_id', '=', $aggregator)
                ->whereDate($paydate_raw , '>=' , $start)
                ->whereDate($paydate_raw , '<=' , $end)
                ->get();   

            }else{
                $sales = OtherSale::select('id', 'cash' , 'date' , 'created_at')
                ->whereYear('created_at', '=', $year)
                ->whereMonth('created_at', '=', $month)
                ->where('aggregator_id', '=', $aggregator)
                ->get();              
            }
    
            foreach($sales as $sale){
                $sale->branch = Aggregator::where(['id' => $aggregator])->first()->name;
            }
    
            $cash = 0;
            $id = 0;
            foreach($sales as $sale){
                $cash = $cash + $sale->cash;
                $id = $sale->id;
            }
    
            $sale = Sale::where(['id' => $id])->first();
            if($sale){
                $sale->cash = round($cash , 2);
                $sale->branch = Aggregator::where(['id' => $aggregator])->first()->name;
        
                $sales = [];
                array_push($sales , $sale);    
            }
    
            return DataTables::of($sales)
            ->make(true);
    
        }else{


            $all_sales = [];
            $branches = Aggregator::where('type','=',$type)->get();
            foreach($branches as $key => $branch){

                if($start && $end){
                    $paydate_raw = DB::raw("STR_TO_DATE(`date`, '%m/%d/%Y')");
                    
                    $sales = OtherSale::select('id', 'cash' , 'date' , 'created_at')
                    ->where('aggregator_id', '=', $branch->id)
                    ->whereDate($paydate_raw , '>=' , $start)
                    ->whereDate($paydate_raw , '<=' , $end)
                    ->get();   
    
                }else{
    
                    $sales = OtherSale::select('id', 'cash' , 'date' , 'created_at')
                    ->whereYear('created_at', '=', $year)
                    ->whereMonth('created_at', '=', $month)
                    ->where('aggregator_id', '=', $branch->id)
                    ->get();        
                }        
  
                $cash = 0;
                $id = 0;

                if($sales){
                    foreach($sales as $sale){
                        $cash = $cash + $sale->cash;
                        $id = $sale->id;
                    }
            
                    $sale = Sale::first();
                    $sale->cash = round($cash , 2);
                    $sale->branch = $branch->name;
            
                    array_push($all_sales , $sale);   
                }                     
            }

            return DataTables::of($all_sales)
            ->make(true);

        }
    }


    // Charts
    public function summaryChart(){
        $branches = Branch::where('name' ,'!=' , 'Kitchen')->get();
        return view('sales.chart' , compact('branches'));
    }

    public function getSummaryChart($year , $month , $branch , $start = null , $end = null){
        if($month < 10){
            $month = sprintf("%02d", $month);
        }

        if($branch != "all"){
            if($start && $end){                
                $paydate_raw = DB::raw("STR_TO_DATE(`date`, '%m/%d/%Y')");
                
                $sales = Sale::select('id', 'cash', 'card', 'total' , 'cash_deposit' ,'branch_id', 'vat' ,'date' , 'created_at')
                ->where('branch_id', '=', $branch)
                ->whereDate($paydate_raw , '>=' , $start)
                ->whereDate($paydate_raw , '<=' , $end)
                ->get();   
            }else{
                $sales = Sale::select('id', 'cash', 'card', 'total' , 'cash_deposit' ,'branch_id', 'vat' ,'date' , 'created_at')
                ->whereYear('created_at', '=', $year)
                ->whereMonth('created_at', '=', $month)
                ->where('branch_id', '=', $branch)
                ->get();              
            }
             
            $objs = [];
            $data = [];
            $labels = [];
            foreach($sales as $sale){
                array_push($data , $sale->total);
                array_push($labels , $sale->date);
            }
            $obj = new \stdClass();
            $obj->label = Branch::where(['id' => $branch])->first()->name;
            $obj->data = $data;
            $obj->backgroundColor =  '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);

            array_push($objs , $obj);
              
            return response()->json(array('success' => true , 'data' =>$objs , 'labels' => $labels , 'multi' => false , 'length' => count($sales)), 200);

        }else{

            $objs = [];
            $labels = [];

            $today = Carbon::createFromDate($year, $month);            
            for($i=1; $i < $today->daysInMonth + 1; ++$i) {
                $labels[] = \Carbon\Carbon::createFromDate($today->year, $today->month, $i)->format('m/d/Y');
            }

            $branches = Branch::where('name','!=','Kitchen')->get();
            foreach($branches as $key => $branch){

                if($start && $end){
                    $paydate_raw = DB::raw("STR_TO_DATE(`date`, '%m/%d/%Y')");
                    
                    $sales = Sale::select('id', 'cash', 'card', 'total' , 'cash_deposit' ,'branch_id', 'vat' ,'date' , 'created_at')
                    ->where('branch_id', '=', $branch->id)
                    ->whereDate($paydate_raw , '>=' , $start)
                    ->whereDate($paydate_raw , '<=' , $end)
                    ->get();   
    
                }else{
    
                    $sales = Sale::select('id', 'cash', 'card', 'total' , 'cash_deposit' ,'branch_id', 'vat' ,'date' , 'created_at')
                    ->whereYear('created_at', '=', $year)
                    ->whereMonth('created_at', '=', $month)
                    ->where('branch_id', '=', $branch->id)
                    ->get();
                }        
 
                $data = [];
                foreach($labels as $label){
                    array_push($data , 0);                    
                }
                foreach($sales as $sale){
                    foreach($labels as $key => $lb){
                        if($lb == $sale->date){
                            $data[$key] = $sale->total;
                        }
                    }
                }
                $obj = new \stdClass();
                $obj->label = $branch->name;
                $obj->data = $data;
                $obj->backgroundColor =  '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);

    
                array_push($objs , $obj);                               
                    
            }

            return response()->json(array('success' => true , 'data' =>$objs , 'labels' => $labels , 'multi' => true), 200);


        }
        
    }

}
