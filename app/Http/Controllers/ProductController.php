<?php

namespace App\Http\Controllers;

use Auth;
use File;
use Redirect;
use Validator;
use DataTables;
use App\VatOption;
use App\Order;
use App\User;
use App\OrderItem;
use App\Notifications\Noti;
use App\Image;
use App\Product;
use App\Category;
use App\Subcategory;
use App\GroupedProducts;
use DB;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public $details = [];
    public function __construct(){
        $this->middleware('auth');

        $this->details = [
            'read_at' => null,
            'data' => [
                'link' => '',
                'text' => ''
            ],
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getProducts(){
        $products = Product::select('id', 'name', 'short_name', 'block', 'image', 'category_id', 'subcategory_id', 'price', 'invisible_price', 'created_at')
        ->where(['type' => 'Regular'])
        ->get();

        foreach($products as $product){
            $product->block = Product::block($product->block);
            $product->category =  Category::where(['id' => $product->category_id])->first()->name;
            if($product->subcategory){
                $product->subcat =  $product->subcategory->name;
            }else{
                $product->subcat =  "";
            }
        }

        return DataTables::of($products)
        ->addColumn('action', function ($id) {
            $data = "";
            if(Auth::user()->can('Update Product') || Auth::user()->can('all')){
                $data = '<a href="'. route("products.edit" , $id->id) .'" class="text-primary"><i class="fa fa-edit"></i></a>';
            }
            if(Auth::user()->can('Delete Product') || Auth::user()->can('all')){            
                $data = $data . ' <a class="text-primary" onClick="delete_click('.$id->id.')" ><i class="fa fa-trash"></i></button>';
            }
            return $data;
        })
        ->make(true);
    }

    public function index(){
        return view("products.productsAll");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $allCategories = Category::all();
        $categories = Category::activeCategories($allCategories);
        $subcategories = SubCategory::where(['block'=>0])->get();
        if(count($categories) > 0){
            return view('products.addProduct' , compact('categories','subcategories'));
        }else{
            return redirect('products')->with('errorProducts', 'Please Add Category First');;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [ 
            'name' => 'required|max:50', 
            'short_name' => 'regex:/^[\pL\s\.]+$/u|min:11|max:20', 
            'price' => 'required|digits_between:1,6',
            'photo' => 'required',
            'category' => 'required',            
            'is_for' => 'required',            
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $product = Product::create([
            'name' => $request->name , 
            'short_name' => $request->short_name , 
            'price' => $request->price , 
            'invisible_price' => $request->invisible_price , 
            'combo_price' => $request->combo_price , 
            'block' => $request->block , 
            'image' => '' , 
            'category_id' => $request->category,
            'subcategory_id' => $request->subcategory,
            'type' => $request->type,
            'is_for' => $request->is_for,
            'details' => $request->details
             ]);
        $product->save();

        $imageUrl = "";


        if($request->photo){
            $image = $request->photo;
            list($type, $image) = explode(';', $image);
            list(, $image)      = explode(',', $image);
            $image = base64_decode($image);
            $image_name = time().str_random(8).'.png';
            $path = 'public/images/'.$image_name;
            file_put_contents($path, $image);
            $image = new Image();
            $image->name = $image_name;
            $image->imageUrl = asset($path);
            $image->product_id = $product->id;
            $image->save();

            $imageUrl = asset("public/images/".$image_name);
        }


        // if($request->hasfile('image')){
        //     $files = $request->file('image');
        //     for($i=0 ; $i<count($files) ; $i++){
        //         $img = "";
        //         $file = $files[$i];
        //         $filename = time().$i.'.'.$file->getClientOriginalExtension();
        //         $destinationPath = public_path('/images');
        //         $file->move($destinationPath, $filename);    
        //         $img = asset("public/images/".$filename);
        //         $image = new Image();
        //         $image->name = $filename;
        //         $image->imageUrl = $img;
        //         $image->product_id = $product->id;
        //         $image->save();
    
        //         if($i == 0){
        //             $imageUrl = asset("public/images/".$filename);
        //         }
        //     }
        // }

        $productUpdate = Product::where(['id' => $product->id])->first();
        $productUpdate->image = $imageUrl;
        $productUpdate->save();

        if($request->items){
            foreach($request->items as $item){
                GroupedProducts::create([
                    'product_id' => $productUpdate->id,
                    'selected_product_id' => $item
                ]);
            }
        }

        $this->notifyUsers('New Product Added');
 
        return redirect('products')->with('message', 'Product Added Successfully');;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $allCategories = Category::all();
        $categories = Category::activeCategories($allCategories);
        $subcategories = SubCategory::where(['block'=>0])->get();
        $product = Product::where(['id' => $id])->first();
        return view('products.editProduct' , compact('product' , 'categories', 'subcategories'));
    }

    public function updateProduct(Request $request){
        $validator = Validator::make($request->all(), [ 
            'name' => 'required|max:50', 
            'short_name' => 'regex:/^[\pL\s\.]+$/u|min:11|max:20', 
            'price' => 'required|digits_between:1,6',
            'category' => 'required'
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator);
        }

        $product = Product::where(['id' => $request->id])->first();
        $oldProduct = new Product();
        $oldProduct->name = $product->name;
        $oldProduct->short_name = $product->short_name;
        $oldProduct->price = $product->price;
        $oldProduct->block = $product->block;
        $oldProduct->type = $product->type;
        $oldProduct->category_id = $product->category_id;

        $product->name = $request->name;
        $product->short_name = $request->short_name;
        $product->price = $request->price;
        $product->block = $request->block;
        $product->type = $request->type;
        $product->combo_price = $request->combo_price;
        $product->invisible_price = $request->invisible_price;
        $product->category_id = $request->category;
        $product->subcategory_id = $request->subcategory;
        $product->is_for = $request->is_for;
        $product->details = $request->details;

        // Storing Changes in Product Activity Table 
        if($product->isDirty('name')){
            Product::activity($oldProduct->name , $request->name , 'name' , Auth::id() , $product->id );            
        }

        if($product->isDirty('short_name')){
            Product::activity($oldProduct->short_name , $request->short_name , 'Short Name' , Auth::id() , $product->id );            
        }

        if($product->isDirty('price')){
            Product::activity($oldProduct->price , $request->price , 'Price' , Auth::id() , $product->id );            
        }

        if($product->isDirty('type')){
            Product::activity($oldProduct->type , $request->type , 'Type' , Auth::id() , $product->id );            
        }

        if($product->isDirty('block')){
            Product::activity($oldProduct->block , $request->block , 'Block' , Auth::id() , $product->id );            
        }

        if($product->isDirty('category_id')){
            Product::activity($oldProduct->category_id , $request->category , 'Category' , Auth::id() , $product->id );            
        }
        

        $imageUrl = "";

        // if($request->hasfile('image')){
        //     // Removing previous Images
        //     $images = Image::where(['product_id' => $request->id])->get();
        //     foreach($images as $image){
        //         $destinationPath = public_path('/images')."/".$image->name;
        //         if(File::exists($destinationPath)) {
        //             File::delete($destinationPath);            
        //         }
        //         $image->delete();
        //     }            
            
        //     $files = $request->file('image');
        //     for($i=0 ; $i<count($files) ; $i++){
        //         $img = "";
        //         $file = $files[$i];
        //         $filename = time().$i.'.'.$file->getClientOriginalExtension();
        //         $destinationPath = public_path('/images');
        //         $file->move($destinationPath, $filename);    
        //         $img = asset("public/images/".$filename);
        //         $image = new Image();
        //         $image->name = $filename;
        //         $image->imageUrl = $img;
        //         $image->product_id = $product->id;
        //         $image->save();
    
        //         if($i == 0){
        //             $imageUrl = asset("public/images/".$filename);
        //         }
        //     }

        //     $product->image = $imageUrl;
        // }


        if($request->photo){

            $images = Image::where(['product_id' => $request->id])->get();
            foreach($images as $image){
                $destinationPath = public_path('/images')."/".$image->name;
                if(File::exists($destinationPath)) {
                    File::delete($destinationPath);            
                }
                $image->delete();
            }            

            $image = $request->photo;
            list($type, $image) = explode(';', $image);
            list(, $image)      = explode(',', $image);
            $image = base64_decode($image);
            $image_name = time().str_random(8).'.png';
            $path = 'public/images/'.$image_name;
            file_put_contents($path, $image);
            $image = new Image();
            $image->name = $image_name;
            $image->imageUrl = asset($path);
            $image->product_id = $product->id;
            $image->save();

            $imageUrl = asset("public/images/".$image_name);
            $product->image = $imageUrl;
        }


        $product->save();

        if($request->items){ 
            if(count($request->items) > 0){
                GroupedProducts::where(['product_id' => $product->id])->delete();
                foreach($request->items as $item){
                    GroupedProducts::create([
                        'product_id' => $product->id,
                        'selected_product_id' => $item
                    ]);
                }
            }
        }else{
            $groupedProducts = GroupedProducts::where(['product_id' => $product->id])->get();
            if(count($groupedProducts) > 0){
                foreach($groupedProducts as $groupedProduct){
                    $groupedProduct->delete();
                }
            }
        }

        $this->notifyUsers('Product Updated');

        return redirect()->back()->with('message', 'Product Updated Successfully');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function deleteProduct($id){
        $images = Image::where(['product_id' => $id])->get();
        foreach($images as $image){
            $destinationPath = public_path('/images')."/".$image->name;
            if(File::exists($destinationPath)) {
                File::delete($destinationPath);            
            }
            $image->delete();
        }

        Product::where(['id' => $id])->delete();
        
        return redirect()->back()->with('message', 'Product Deleted Successfully');;
    }

    public function vat($val){
        $vat = VatOption::where(['id' => 1])->first();
        if($val == 'true'){
            $vat->vat_option = true;
        }
        if($val == 'false'){
            $vat->vat_option = false;
        }
        $vat->save();

        return response()->json($vat , 200);
    }

    public function sortProduct(){
        $products = Product::where(['block' => 0])->orderBy('sequence')->get();        
        $categories = Category::where(['block' => 0])->orderBy('sequence')->get();        
        $subcategories = Subcategory::where(['block' => 0])->orderBy('sequence')->get();        
        return view('products.sequence',compact('products','subcategories','categories'));
    }

    public function sortProductUpdate(Request $request){
        $ids = $request->ids;
        foreach($ids as $key => $id){
            $category = Product::where(['id'=> $id])->first();
            $category->sequence = $key;
            $category->save();
        }

        return response()->json(['status' => 200, 'msg' => "Success"]);
    }

    public function index_populars(){
        return view('products.index_populars');
    }

    public function getPopulars($year , $month , $day){
        $products = Product::all();
        $productss = [];

        foreach($products as $product){
            $product->count = 0;
        }
        
        if($month < 10){
            $month = sprintf("%02d", $month);
        }
        $orders = Order::whereYear('created_at', '=', $year)
        ->whereMonth('created_at', '=', $month)
        ->whereDay('created_at', '=', $day)
        ->where('order_from' ,'!=', 'POS')
        ->get();

        
        foreach($orders as $order){
            $orderItems = OrderItem::where(['order_id' => $order->id])->get();
            foreach($orderItems as $orderItem){
                  foreach($products as $product){
                      if($orderItem->product_id == $product->id){
                          $product->count = $product->count + $orderItem->product_quantity; 
                      }
                  }  
            }
        }


        for ($i = 0; $i < count($products) ; ++$i)   
        {
            for ($j = $i + 1; $j < count($products); ++$j)
            {
                if ($products[$i]->count < $products[$j]->count)
                {
                    $prod = $products[$i];
                    $products[$i] = $products[$j];
                    $products[$j] = $prod;
                }
            }
        }

        return DataTables::of($products)
        ->make(true);
    }

    public function notifyUsers($message){
        $this->details['data']['link'] = route('products.index');
        $this->details['data']['text'] = $message;
        $users = User::all();
        foreach($users as $user){
            if($user->can('View Products')  || $user->can('all')){
                $user->notify(new Noti($this->details));
            }
        }
    }

}