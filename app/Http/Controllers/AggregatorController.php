<?php

namespace App\Http\Controllers;

use Auth;
use Response;
use Validator;
use DataTables;
use App\Aggregator;
use App\User;
use App\Notifications\Noti;
use Illuminate\Http\Request;

class AggregatorController extends Controller
{
    public $details = [];
    public function __construct(){
        $this->middleware('auth');

        $this->details = [
            'read_at' => null,
            'data' => [
                'link' => '',
                'text' => ''
            ],
        ];
    }
    
    public function getAggregator(){
        $aggregators = Aggregator::select('id', 'name', 'description', 'block', 'type' , 'created_at')->get();

        foreach($aggregators as $aggregator){
            $aggregator->isBlock = Aggregator::block($aggregator->block);
        }

        return DataTables::of($aggregators)
        ->addColumn('action', function ($id) {

            $data = "";
            if(Auth::user()->can('Update Aggregator') || Auth::user()->can('all')){
                $data = '<a href="'. route("editAggregator" , $id) .'" class="text-primary"><i class="fa fa-edit"></i></a>';
            }
            if(Auth::user()->can('Delete Aggregator') || Auth::user()->can('all')){            
                $data = $data . ' <a class="text-primary" onClick="delete_click('.$id->id.')" ><i class="fa fa-trash"></i></button>';  
            }
            return $data;

        })
        ->make(true);
    }

    public function index(){
        return view("aggregators.index");
    }

    public function Aggregator(){
        return view("aggregators.create");
    }

    public function addAggregator(Request $request){

        $validator = Validator::make($request->all(), [ 
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:255', 
            'description' => 'required',
            'type' => 'required',
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $aggregator = Aggregator::create([
            'name' => $request->name , 
            'description' => $request->description, 
            'block' => $request->block, 
            'type' => $request->type, 
             ]);
        $aggregator->save();

        $this->notifyUsers('New Aggregator Added');     

        return redirect('aggregators')->with('message', 'Aggregator Added Successfully');
    }

    public function editAggregator($id)
    {
        $aggregator = Aggregator::where(['id' => $id])->first();
        return view('aggregators.edit' , compact('aggregator'));
    }

    public function updateAggregator(Request $request){
        $validator = Validator::make($request->all(), [ 
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:255', 
            'description' => 'required',
            'type' => 'required',
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $aggregator = Aggregator::where(['id' => $request->id])->first();
        $aggregator->name = $request->name;
        $aggregator->description = $request->description;
        $aggregator->block = $request->block;
        $aggregator->type = $request->type;

        $aggregator->save();
        $this->notifyUsers('Aggregator Updated');     
        return redirect()->back()->with('message', 'Aggregator Updated Successfully');;
    }

    public function deleteAggregator($id){

        Aggregator::where(['id' => $id])->delete(); 
        $this->notifyUsers('Aggregator Deleted');            
        return redirect()->back()->with('message', 'Aggregator Deleted Successfully');;
    }

    public function notifyUsers($message){
        $this->details['data']['link'] = route('aggregators.index');
        $this->details['data']['text'] = $message;
        $users = User::all();
        foreach($users as $user){
            if($user->can('View Aggregators') || $user->can('all')){
                $user->notify(new Noti($this->details));
            }
        }
    }

}
