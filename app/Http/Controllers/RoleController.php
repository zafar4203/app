<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Validator;
use DataTables;
use App\User;
use App\Notifications\Noti;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleController extends Controller
{

    public $details = [];
    public function __construct(){
        $this->middleware('auth');

        $this->details = [
            'read_at' => null,
            'data' => [
                'link' => '',
                'text' => ''
            ],
        ];
    }
    
    public function getRoles(){
        $roles = Role::select('id', 'name' , 'created_at')->get();

        return DataTables::of($roles)
        ->addColumn('action', function ($id) {

            $data = "";
            if(Auth::user()->can('Update Role') || Auth::user()->can('all')){
                $data = '<a href="'. route("editRole" , $id) .'" class="text-primary"><i class="fa fa-edit"></i></a>';
            }
            if(Auth::user()->can('Delete Role') || Auth::user()->can('all')){            
                $data = $data . ' <a class="text-primary" onClick="delete_click('.$id->id.')" ><i class="fa fa-trash"></i></a>';    
            }
            if(Auth::user()->can('Role Permissions') || Auth::user()->can('all')){            
                $data = $data . ' <a href="'. route("rolePermissions" , $id->id) .'" class="text-primary"><i class="fa fa-eye"></i></a>';    
            }

            return $data;


        })
        ->make(true);
    }
    
    public function rolePermissions($id){
        $role = Role::where('id',$id)->first();
        $permissions = Permission::all();
        return view('roles.rolePermissions' , compact('role','permissions'));
    }

    public function rolePermissionsSubmit(Request $request){
        $role = Role::where(['id' => $request->role_id])->first();
        $permissions = Permission::all();

            foreach($permissions as $permission){
                $role->revokePermissionTo($permission->name);
            }    

            foreach($permissions as $permission){
                if(isset($request->rp[$permission->name])){
                    $role->givePermissionTo($permission->name);
                }
            }    

        return redirect(route('rolePermissions',$role->id))->with('message', 'Permissions Assigned Successfully');
    }

    public function index(){
        return view("roles.roles");
    }

    public function Role(){
        return view("roles.addRole");
    }

    public function addRole(Request $request){

        $validator = Validator::make($request->all(), [ 
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:255', 
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $role = Role::create(['name' => $request->name]);
        $role->save();

        $this->notifyUsers('New Role Added');

        return redirect('roles')->with('message', 'Role Added Successfully');;
    }

    public function editRole($id)
    {
        $role = Role::where(['id' => $id])->first();
        return view('roles.editRole' , compact('role'));
    }

    public function updateRole(Request $request){
        $validator = Validator::make($request->all(), [ 
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:255'
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $role = Role::where(['id' => $request->id])->first();

        $role->name = $request->name;
        $role->save();

        $this->notifyUsers('Role Updated');
        return redirect('roles')->with('message', 'Role Updated Successfully');;
    }

    public function deleteRole($id){

        Role::where(['id' => $id])->delete();    
        $this->notifyUsers('Role Deleted');    
        return redirect()->back()->with('message', 'Role Deleted Successfully');;
    }

    public function notifyUsers($message){
        $this->details['data']['link'] = route('roles.index');
        $this->details['data']['text'] = $message;
        $users = User::all();
        foreach($users as $user){
            if($user->can('View Roles')  || $user->can('all')){
                $user->notify(new Noti($this->details));
            }
        }
    }
}

