<?php

namespace App\Http\Controllers;

use Auth;
use File;
use Response;
use Validator;
use DataTables;
use App\Product;
use App\Category;
use App\GroupedProducts;
use App\User;
use App\Notifications\Noti;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public $details = [];
    public function __construct(){
        $this->middleware('auth');

        $this->details = [
            'read_at' => null,
            'data' => [
                'link' => '',
                'text' => ''
            ],
        ];
    }
    
    public function getcategories(){
        $categories = Category::select('id', 'name', 'description', 'block' , 'created_at')->get();

        foreach($categories as $category){
            $category->isBlock = Category::block($category->block);
        }

        return DataTables::of($categories)
        ->addColumn('action', function ($id) {

            $data = "";
            if(Auth::user()->can('Update Category') || Auth::user()->can('all')){
                $data = '<a href="'. route("editCategory" , $id) .'" class="text-primary"><i class="fa fa-edit"></i></a>';
            }
            if(Auth::user()->can('Delete Category') || Auth::user()->can('all')){            
                $data = $data . ' <a class="text-primary" onClick="delete_click('.$id->id.')" ><i class="fa fa-trash"></i></button>';
            }
            return $data;
        })
        ->make(true);
    }

    public function index(){
        return view("categories.categories");
    }

    public function Category(){
        return view("categories.addCategory");
    }

    public function addCategory(Request $request){

        $validator = Validator::make($request->all(), [ 
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:255', 
            'description' => 'required|regex:/^[\pL\s\-]+$/u|max:500',
            'image' => 'required'
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $img = "";
        if($request->hasfile('image')){
            $file = $request->file('image');
            $filename = time().'.'.$file->getClientOriginalExtension();
            $destinationPath = public_path('/images/categories');
            $file->move($destinationPath, $filename);    
            $img = asset("images/categories/".$filename);
        }

        $category = Category::create([
            'name' => $request->name , 
            'description' => $request->description, 
            'block' => $request->block, 
            'image' => $img
             ]);
        $category->save();

        $this->notifyUsers('Category Added');

        return redirect('categories')->with('message', 'Category Added Successfully');;
    }

    public function editCategory($id)
    {
        $category = Category::where(['id' => $id])->first();
        return view('categories.editCategory' , compact('category'));
    }

    public function updateCategory(Request $request){
        $validator = Validator::make($request->all(), [ 
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:255', 
            'description' => 'required|regex:/^[\pL\s\-]+$/u|max:500',
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $img = "";
        if($request->hasfile('image')){
            $file = $request->file('image');
            $filename = time().'.'.$file->getClientOriginalExtension();
            $destinationPath = public_path('/images/categories');
            $file->move($destinationPath, $filename);    
            $img = asset("images/categories/".$filename);
        }        

        $category = Category::where(['id' => $request->id])->first();
        $oldCategory = new Category();
        $oldCategory->name = $category->name;
        $oldCategory->description = $category->description;
        $oldCategory->block = $category->block;

        $split = explode("/", $category->image);
        $imageName  = $split[7];

        $destinationPath = public_path('/images/categories')."/".$imageName;
        if(File::exists($destinationPath)) {
            File::delete($destinationPath);            
        }

        $category->name = $request->name;
        $category->description = $request->description;
        $category->block = $request->block;
        $category->image = $img;

        // Storing Changes in Category Activity Table 
        if($category->isDirty('name')){
            Category::activity($oldCategory->name , $request->name , 'Name' , Auth::id() , $category->id );            
        }
        if($category->isDirty('description')){
            Category::activity($oldCategory->description , $request->description , 'Description' , Auth::id() , $category->id );            
        }
        if($category->isDirty('block')){
            Category::activity($oldCategory->block , $request->block , 'Block' , Auth::id() , $category->id );            
        }

        $category->save();

        $this->notifyUsers('Category Updated');

        return redirect()->back()->with('message', 'Category Updated Successfully');;
    }

    public function deleteCategory($id){

        Category::where(['id' => $id])->delete();   
        $this->notifyUsers('Category Deleted');
     
        return redirect()->back()->with('message', 'Category Deleted Successfully');;
    }

    public function categoryProducts($id , $product_id = null){
        $products = Product::where(['category_id' => $id])->where(['type' => 'Regular'])->where('id' , '!=' , $product_id)->get();

        if($product_id != null){
            $groupedProducts = GroupedProducts::where(['product_id' => $product_id])->get();
            foreach($products as $product){                
                $check = 0;
                foreach($groupedProducts as $groupedProduct){
                    if($product->id == $groupedProduct->selected_product_id){
                        $check = 1;
                    }
                }
                if($check == 0){
                    $product->check = false;
                }else{
                    $product->check = true;
                }
            }   
        }

        return Response::json(array('success' => true , 'products' => $products));
    }

    public function sortCategory(){
        $categories = Category::where(['block' => 0])->orderBy('sequence')->get();        
        return view('categories.sequence',compact('categories'));
    }

    public function sortCategoryUpdate(Request $request){
        $ids = $request->ids;
        foreach($ids as $key => $id){
            $category = Category::where(['id'=> $id])->first();
            $category->sequence = $key;
            $category->save();
        }

        return response()->json(['status' => 200, 'msg' => "Success"]);
    }

    public function notifyUsers($message){
        $this->details['data']['link'] = route('categories');
        $this->details['data']['text'] = $message;
        $users = User::all();
        foreach($users as $user){
            if($user->can('View Categories')  || $user->can('all')){
                $user->notify(new Noti($this->details));
            }
        }
    }
}
