<?php

namespace App\Http\Controllers;

use Auth;
use DataTables;
use Validator;
use Carbon\Carbon;
use App\Invisible;
use App\SaleInvisible;
use Illuminate\Http\Request;

class InvisibleController extends Controller
{
    public function index(){
        return view('invisible_registration');
    }

    public function invisibleUsers(){
        return view('invisible.users.users');
    }

    public function getInvisibleUsers(){
        $users = Invisible::select('id', 'name', 'email', 'phone', 'wallet', 'created_at')->get();

        foreach($users as $user){
            $user->wallet = "AED " . $user->wallet; 
        }
        return DataTables::of($users)
        ->addColumn('action', function ($id) {

            $data = "";
            if(Auth::user()->can('Update Invisible User') || Auth::user()->can('all')){
                $data = '<a href="'. route("editInvisible" , $id) .'" class="text-primary"><i class="fa fa-edit"></i></a>';
            }
            if(Auth::user()->can('Delete Invisible User') || Auth::user()->can('all')){            
                $data = $data . ' <a class="text-primary" onClick="delete_click('.$id->id.')" ><i class="fa fa-trash"></i></button>';    
            }
            return $data;

        })
        ->addColumn('view', function ($id) {
            return '<a href="'.url("invisible_sales").'/'.$id->id.'" class="btn btn-primary">Sales</a>';
        })
        ->rawColumns(['action' , 'view'])
        ->make(true);
    }

    public function edit($id){
        $invisible = Invisible::where(['id' => $id])->first();
        return view('invisible.users.editUser' , compact('invisible'));
    }

    public function addInvisible(Request $request){
        $validator = Validator::make($request->all(), [ 
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:50', 
            'email' => 'required|email|unique:invisibles,email',
            'phone' => 'required',
            'dob' => 'required',
            'gender' => 'required',
            'address' => 'required',
            'password' => 'required',
            'city'   => 'required|regex:/^[\pL\s\-]+$/u|max:20',
            'country' => 'required|regex:/^[\pL\s\-]+$/u|max:20',
            'fb_friends' => 'required|numeric',
            'in_friends' => 'required|numeric',
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $invisible = Invisible::create([
            'name' => $request->name , 
            'email' => $request->email, 
            'phone' => $request->phone,
            'dob' => $request->dob,
            'gender' => $request->gender,
            'address' => $request->address,
            'city' => $request->city,
            'country' => $request->country,
            'password' => $request->password,
            'referal_name' => $request->referal_name,
            'referal_phone' => $request->referal_phone,
            'fb_friends' => $request->fb_friends,
            'in_friends' => $request->in_friends,
        ]);
        $invisible->save();
        
        $invi = Invisible::where(['id' => $invisible->id])->first();
        $invi->my_ref_id = "Ref_User_".$invi->id;
        $invi->save();

        return redirect('invisible/welcome');
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [ 
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:50', 
            'email' => 'required|email|unique:invisibles,email,'.$request->id,
            'phone' => 'required',
            'dob' => 'required',
            'gender' => 'required',
            'address' => 'required',
            'password' => 'required',
            'city'   => 'required|regex:/^[\pL\s\-]+$/u|max:20',
            'country' => 'required|regex:/^[\pL\s\-]+$/u|max:20',
            'fb_friends' => 'required|numeric',
            'in_friends' => 'required|numeric',
            'p_percentage' => 'required|numeric',
            's_percentage' => 'required|numeric',
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $invisible = Invisible::where(['id' => $request->id])->first();
        $invisible->name = $request->name; 
        $invisible->email = $request->email; 
        $invisible->phone = $request->phone;
        $invisible->dob = $request->dob;
        $invisible->gender = $request->gender;
        $invisible->address = $request->address;
        $invisible->city = $request->city;
        $invisible->country = $request->country;
        $invisible->password = $request->password;
        $invisible->referal_name = $request->referal_name;
        $invisible->referal_phone = $request->referal_phone;
        $invisible->fb_friends = $request->fb_friends;
        $invisible->in_friends = $request->in_friends;
        $invisible->p_percentage = $request->p_percentage;
        $invisible->s_percentage = $request->s_percentage;
        $invisible->status = $request->status;
        $invisible->save();

        return redirect('invisible_users')->with('message', 'User Updated Successfully');;
    }

    public function dashboard(){
        $date = date('m/d/Y');
        $month = date('m', strtotime($date));
        $year = date('Y', strtotime($date));


        // Today records

        $sales = SaleInvisible::where(['invisible_id' => Auth::user()->id , 'from' => 'refferal'])
        ->whereDate('created_at', '=', Carbon::today())
        ->get();
        $refferals_total_today = 0;
        if($sales){
            foreach($sales as $sale){
                $refferals_total_today = $refferals_total_today + $sale->total;
            }
        }

        $sales = SaleInvisible::where(['invisible_id' => Auth::user()->id , 'from' => 'order'])
        ->whereDate('created_at', '=', Carbon::today())
        ->get();
        
        $comission_total_today = 0;
        if($sales){
            foreach($sales as $sale){
                $comission_total_today = $comission_total_today + $sale->total;
            }
        }


        // Monthly records
        $sales = SaleInvisible::where(['invisible_id' => Auth::user()->id , 'from' => 'refferal'])
        ->whereMonth('created_at', '=', $month)
        ->get();
        $refferals_total = 0;
        if($sales){
            foreach($sales as $sale){
                $refferals_total = $refferals_total + $sale->total;
            }
        }

        $sales = SaleInvisible::where(['invisible_id' => Auth::user()->id , 'from' => 'order'])
        ->whereMonth('created_at', '=', $month)
        ->get();
        $comission_total = 0;
        if($sales){
            foreach($sales as $sale){
                $comission_total = $comission_total + $sale->total;
            }
        }

        $invisible = Invisible::where(['id' => Auth::user()->id])->first();


        // Yearly Data
        $sales = SaleInvisible::where(['invisible_id' => Auth::user()->id , 'from' => 'refferal'])
        ->whereYear('created_at', '=', $year)
        ->get();
        $refferals_total_year = 0;
        if($sales){
            foreach($sales as $sale){
                $refferals_total_year = $refferals_total_year + $sale->total;
            }
        }

        $sales = SaleInvisible::where(['invisible_id' => Auth::user()->id , 'from' => 'order'])
        ->whereMonth('created_at', '=', $month)
        ->get();
        $comission_total_year = 0;
        if($sales){
            foreach($sales as $sale){
                $comission_total_year = $comission_total_year + $sale->total;
            }
        }

        return view('invisible.dashboard' , compact('refferals_total' , 'comission_total' , 'refferals_total_year' , 'comission_total_year', 'refferals_total_today' , 'comission_total_today' , 'invisible'));
    }

    public function login(){
        return view('invisible.login');
    }

    public function loginInvisible(Request $request){
        $validator = Validator::make($request->all(), [ 
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }
    
        $user = Invisible::where(['email' => $request->email])
        ->where(['password' => $request->password])
        ->first();

        if($user){
            if (Auth::guard('invisible')->loginUsingId($user->id)) {                  
                if(Auth::guard('invisible')->check()){
                    if(Auth::guard('invisible')->user()->status == 1){
                        return "User is Blocked";                        
                    }else{
                        Auth::guard('invisible')->login(Auth::guard('invisible')->user(), true);
                        return redirect()->route('i_home');                        
                    }
                }
            }
            return redirect('invisible/dashboard');
        }else{
            return redirect()->back()->with('error', 'Invalid Login Credentials');
        }
    }

    public function delete($id){
        Invisible::where(['id' => $id])->delete();        
        return redirect()->back()->with('message', 'Invisible Deleted Successfully');;
    }

    public function user_activities($id){
        $invisible = Invisible::where(['id' => $id])->first();
        return view('invisible.users.user_activities' , compact('invisible'));
    }

    public function getSales($year , $month , $invisible_id){
        if($month < 10){
            $month = sprintf("%02d", $month);
        }

        $sales = SaleInvisible::select('id','total','date' ,'from' ,'created_at')
        ->whereYear('created_at', '=', $year)
        ->whereMonth('created_at', '=', $month)
        ->where(['invisible_id' => $invisible_id])
        ->get();

        if($sales){
            foreach($sales as $sale){
                $sale->total = "AED " . $sale->total;
                if($sale->from == "refferal"){
                    $sale->from = "Refferals";
                }else{
                    $sale->from = "Orders";
                }
            }    
        }

        return DataTables::of($sales)
        ->make(true);
    }

    public function user_sales($id){
        $invisible = Invisible::where(['id' => $id])->first();
        return view('invisible.users.sales' , compact('invisible'));
    }

    public function user_sales_for_admin($id){
        $invisible = Invisible::where(['id' => $id])->first();
        return view('invisible.admin.sales' , compact('invisible'));
    }

    public function getReferals($id){
        $invisible = Invisible::where(['id' => $id])->first();
        return view('invisible.users.refferals' , compact('invisible'));
    }
}
