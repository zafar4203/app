<?php

namespace App\Http\Controllers;

use App\Image;
use Illuminate\Http\Request;

class ImageUploadController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function fileCreate()
    {
        return view('imageupload');
    }

    public function fileStore(Request $request)
    {
        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        $image->move(public_path('images'),$imageName);
        
        $imageUpload = new Image();
        $imageUpload->name = $imageName;
        $imageUpload->imageUrl =  asset("images/".$imageName);
        $imageUpload->product_id = $request->product_id;
        $imageUpload->save();
        return response()->json(['success'=>$imageName]);
    }
    
    public function fileDestroy(Request $request)
    {
        $filename =  $request->get('filename');
        Image::where('name',$filename)->delete();
        $path=public_path().'/images/'.$filename;
        if (file_exists($path)) {
            unlink($path);
        }
        return $filename;  
    }
}
