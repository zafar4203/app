<?php

namespace App\Http\Controllers;

use DataTables;
use Validator;
use App\Branch;
use App\Expense;
use App\Supplier;
use Auth;
use App\Product;
use App\Inventory;
use App\User;
use App\Notifications\Noti;
use Illuminate\Http\Request;

class ExpenseController extends Controller
{

    public $details = [];
    public function __construct(){
        $this->middleware('auth');

        $this->details = [
            'read_at' => null,
            'data' => [
                'link' => '',
                'text' => ''
            ],
        ];
    }

    public function getExpenses(){
        $branches = Expense::select('id', 'name','supplier_id','invoice_number', 'amount', 'type' , 'block' , 'date')->get();

        return DataTables::of($branches)
        ->addColumn('action', function ($id) {

            $data = "";
            if(Auth::user()->can('Update Expense') || Auth::user()->can('all')){
                $data = '<a href="'. route("editExpense" , $id) .'" class="text-primary"><i class="fa fa-edit"></i></a>';
            }
            if(Auth::user()->can('Delete Expense') || Auth::user()->can('all')){            
                $data = $data .' <a class="text-primary" onClick="delete_click('.$id->id.')" ><i class="fa fa-trash"></i></a>';  
            }

            return $data;

        })
        ->make(true);
    }

    public function index(){
        return view("expenses.expenses");
    }

    public function create(){
        $suppliers = Supplier::where(['block' => 0])->get();
        return view('expenses.addExpense', compact('suppliers'));
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'supplier_id' => 'required',
            'date' => 'required',
            'invoice_number' => 'required',
            'description' => 'required',
            'amount' => 'required',
            'type' => 'required',
            'block' => 'required',
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $expense = Expense::create([
            'name' => $request->name , 
            'supplier_id' => $request->supplier_id, 
            'date' => $request->date,
            'name' => $request->name,
            'invoice_number' => $request->invoice_number,
            'description' => $request->description,
            'amount' => $request->amount,
            'type' => $request->type,
            'block' => $request->block,
        ]);
        $expense->save();


        $this->notifyUsers('Expense Added');

        return redirect('expenses')->with('message', 'Expense Added Successfully');
    }

    public function edit($id){
        $suppliers = Supplier::where(['block' => 0])->get();
        $expense = Expense::where(['id' => $id])->first();
        return view('expenses.editExpense' , compact('expense' , 'suppliers'));
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'supplier_id' => 'required',
            'date' => 'required',
            'name' => 'required',
            'invoice_number' => 'required',
            'description' => 'required',
            'amount' => 'required',
            'type' => 'required',
            'block' => 'required',
            ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $expense = Expense::where(['id' => $request->id])->first();
        $expense->name = $request->name; 
        $expense->supplier_id = $request->supplier_id; 
        $expense->date = $request->date;
        $expense->name = $request->name;
        $expense->invoice_number = $request->invoice_number;
        $expense->description = $request->description;
        $expense->amount = $request->amount;
        $expense->type = $request->type;
        $expense->block = $request->block;
        $expense->save();

        $this->notifyUsers('Expense Updated');
        return redirect('expenses')->with('message', 'Expense Updated Successfully');
    }

    public function delete($id){
        Expense::where(['id' => $id])->delete();   

        $this->notifyUsers('Expense Deleted');     
        return redirect()->back()->with('message', 'Expense Deleted Successfully');;
    }

    public function notifyUsers($message){
        $this->details['data']['link'] = route('expenses');
        $this->details['data']['text'] = $message;
        $users = User::all();
        foreach($users as $user){
            if($user->can('View Expenses')  || $user->can('all')){
                $user->notify(new Noti($this->details));
            }
        }
    }
}
