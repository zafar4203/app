<?php

namespace App\Http\Controllers;

use Auth;
use File;
use Response;
use Validator;
use DataTables;
use App\Menu;
use App\SubMenu;
use App\User;
use App\Notifications\Noti;
use Illuminate\Http\Request;

class SubMenuController extends Controller
{
    public $details = [];
    public function __construct(){
        $this->middleware('auth');

        $this->details = [
            'read_at' => null,
            'data' => [
                'link' => '',
                'text' => ''
            ],
        ];
    }
    
    public function getSubMenus(){
        $menus = SubMenu::select('id', 'name', 'link', 'menu_id','block' , 'created_at')->get();

        foreach($menus as $menu){
            $menu->isBlock = SubMenu::block($menu->block);
        }

        return DataTables::of($menus)
        ->addColumn('action', function ($id) {

            $data = "";
            if(Auth::user()->can('Update Menu') || Auth::user()->can('all')){
                $data = '<a href="'. route("editSubMenu" , $id) .'" class="text-primary"><i class="fa fa-edit"></i></a>';
            }
            if(Auth::user()->can('Delete Menu') || Auth::user()->can('all')){            
                $data = $data . ' <a class="text-primary" onClick="delete_click('.$id->id.')" ><i class="fa fa-trash"></i></button>';
            }
            return $data;
        })
        ->make(true);
    }

    public function index(){
        return view("submenus.submenus");
    }

    public function SubMenu(){
        $menus = Menu::where(['block' => 0])->get();
        return view("submenus.addSubMenu" , compact('menus'));
    }

    public function addSubMenu(Request $request){

        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'link' => 'required',
            'menu_id' => 'required'
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }


        $menu = SubMenu::create([
            'name' => $request->name , 
            'link' => $request->link, 
            'block' => $request->block, 
            'menu_id' => $request->menu_id, 
            ]);
        $menu->save();

        $this->notifyUsers('Sub Menu Added');

        return redirect('submenus')->with('message', 'Sub Menu Added Successfully');;
    }

    public function editSubMenu($id)
    {
        $menu = SubMenu::where(['id' => $id])->first();
        $menus = Menu::where(['block' => 0])->get();
        return view('submenus.editSubMenu' , compact('menu' , 'menus'));
    }

    public function updateSubMenu(Request $request){
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'link' => 'required',
            'menu_id' => 'required'
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $menu = SubMenu::where(['id' => $request->id])->first();
        $menu->name = $request->name;
        $menu->link = $request->link;
        $menu->block = $request->block;
        $menu->menu_id = $request->menu_id;

        $menu->save();

        $this->notifyUsers('Sub Menu Updated');

        return redirect()->back()->with('message', 'Sub Menu Updated Successfully');;
    }

    public function deleteSubMenu($id){

        SubMenu::where(['id' => $id])->delete();   
        $this->notifyUsers('Sub Menu Deleted');
     
        return redirect()->back()->with('message', 'Sub Menu Deleted Successfully');;
    }


    public function sortSubMenu(){
        $menus = Menu::where(['block' => 0])->orderBy('sequence')->get();
        $submenus = SubMenu::where(['block' => 0])->orderBy('sequence')->get();        

        return view('submenus.sequence',compact('menus' , 'submenus'));
    }

    public function sortSubMenuUpdate(Request $request){
        $ids = $request->ids;
        foreach($ids as $key => $id){
            $submenu = SubMenu::where(['id'=> $id])->first();
            $submenu->sequence = $key;
            $submenu->save();
        }

        return response()->json(['status' => 200, 'msg' => "Success"]);
    }

    public function notifyUsers($message){
        $this->details['data']['link'] = route('categories');
        $this->details['data']['text'] = $message;
        $users = User::all();
        foreach($users as $user){
            if($user->can('View Categories')  || $user->can('all')){
                $user->notify(new Noti($this->details));
            }
        }
    }
}
