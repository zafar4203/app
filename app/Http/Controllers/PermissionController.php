<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use DataTables;
use App\User;
use App\Notifications\Noti;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    public $details = [];
    public function __construct(){
        $this->middleware('auth');

        $this->details = [
            'read_at' => null,
            'data' => [
                'link' => '',
                'text' => ''
            ],
        ];
    }
    
    public function getPermissions(){
        $permissions = Permission::select('id', 'name' , 'created_at')->get();

        return DataTables::of($permissions)
        ->addColumn('action', function ($id) {

            $data = "";
            if(Auth::user()->can('Update Permission') || Auth::user()->can('all')){
                $data = '<a href="'. route("editPermission" , $id) .'" class="text-primary"><i class="fa fa-edit"></i></a>';
            }
            if(Auth::user()->can('Delete Permission') || Auth::user()->can('all')){            
                $data = $data .' <a class="text-primary" onClick="delete_click('.$id->id.')" ><i class="fa fa-trash"></i></button>'; 
            }

            return $data;

        })
        ->make(true);
    }

    public function index(){
        return view("permissions.permissions");
    }

    public function Permission(){
        return view("permissions.addPermission");
    }

    public function addPermission(Request $request){

        $validator = Validator::make($request->all(), [ 
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:255', 
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $permission = Permission::create(['name' => $request->name]);
        $permission->save();

        $this->notifyUsers('Permission Added');
        return redirect('permissions')->with('message', 'Permission Added Successfully');;
    }

    public function editPermission($id)
    {
        $permission = Permission::where(['id' => $id])->first();
        return view('permissions.editPermission' , compact('permission'));
    }

    public function updatePermission(Request $request){
        $validator = Validator::make($request->all(), [ 
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:255'
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $permission = Permission::where(['id' => $request->id])->first();

        $permission->name = $request->name;
        $permission->save();

        $this->notifyUsers('Permission Updated');
        return redirect('permissions')->with('message', 'Permission Updated Successfully');;
    }

    public function deletePermission($id){

        Permission::where(['id' => $id])->delete();  
        $this->notifyUsers('Permission Deleted');
      
        return redirect()->back()->with('message', 'Permission Deleted Successfully');;
    }

    public function notifyUsers($message){
        $this->details['data']['link'] = route('permissions');
        $this->details['data']['text'] = $message;
        $users = User::all();
        foreach($users as $user){
            if($user->can('View Permissions')  || $user->can('all')){
                $user->notify(new Noti($this->details));
            }
        }
    }
}


