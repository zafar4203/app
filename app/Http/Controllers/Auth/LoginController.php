<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Session;
use App\Log;
use App\User;
use Redirect;
use Carbon\Carbon;
use Jenssegers\Agent\Agent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    public function __construct()
    {
        $this->middleware('guest')->except('logout' , 'invisibleLogout');
    }

    
    protected function authenticated(Request $request, $user){
        $agent = new Agent();
        $browser = $agent->browser();

        $mac = strtok(exec('getmac'),' '); 
        $ip = $_SERVER['REMOTE_ADDR'];

        $currentDate = explode(" ", Carbon::now('PST')->format('d-m-Y h:i:s'));

        if ( Auth::check() ) {
            $log = Log::create([
                'ip' => $ip,
                'mac' => $mac,
                'date' => $currentDate[0],
                'login_time' => $currentDate[1],
                'login_status' => 'Success',
                'browser' => $browser,
                'user_id' => $user->id                
            ]);

            $request->session()->put('log_id', $log->id);
            return redirect()->route('dashboard');
        }
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        $mac = strtok(exec('getmac'), ' '); 
        $ip = $_SERVER['REMOTE_ADDR'];

        $user = User::where(['email' => $request->email])->first();
        if($user){
            $log = Log::create([
                'ip' => $ip,
                'mac' => $mac,
                'date' => Carbon::now('PST')->format('d-m-Y h:i:s'),
                'login_status' => 'Failed',
                'user_id' => $user->id                
            ]);    
        }

        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);

    }

    public function logout(Request $request)
    {

        if($request->session()->has('log_id')){
            $id = $request->session()->get('log_id');
            $log = Log::where(['id' => $id])->first();
            $log->logout_time = explode(" ", Carbon::now('PST')->format('Y-m-d h:i:s'))[1];
            $log->save();

            $request->session()->forget('log_id');
        }

        $this->guard()->logout();

        $request->session()->invalidate();

        Auth::logout(); 
        Session::flush();

        return $this->loggedOut($request) ?: Redirect::route('login');        
    }

    public function invisibleLogout(Request $request)
    {
        $this->guard('invisible')->logout();

        $request->session()->invalidate();
        Session::flush();

        return $this->loggedOut($request) ?: Redirect::route('invisible_login');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
}
