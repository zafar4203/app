<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inventory;
use App\Branch;
use App\Product;
use App\User;
use App\Notifications\Noti;
use Auth;
use DataTables;
use Validator; 

class InventoryController extends Controller
{

    public $details = [];
    public function __construct(){
        $this->middleware('auth');

        $this->details = [
            'read_at' => null,
            'data' => [
                'link' => '',
                'text' => ''
            ],
        ];
    }

    public function getInventories($branch){
        $inventories = Inventory::select('id' , 'product_id', 'branch_id', 'initial_qty', 'current_qty')
        ->where('branch_id', '=', $branch)
        ->get();

        foreach($inventories as $inventory){
            $inventory->product_id = Product::productName($inventory->product_id);
            $inventory->branch_id = Branch::branchName($inventory->branch_id);
        }

        return DataTables::of($inventories)
        ->addColumn('action', function ($id) {

            $data = "";
            if(Auth::user()->can('Update Inventory') || Auth::user()->can('all')){
                $data = '<a href="'. route("editInventory" , $id) .'" class="text-primary"><i class="fa fa-edit"></i></a>';
            }
            if(Auth::user()->can('Delete Inventory') || Auth::user()->can('all')){            
                $data = $data . '<a class="text-primary" onClick="delete_click('.$id->id.')" ><i class="fa fa-trash"></i></a>'; 
            }
            return $data;

        })
        ->make(true);
    }

    public function inventories(){
        $branches = Branch::all();
        return view('inventories.inventories' , compact('branches'));
    }

    public function addInventory(){
        $branches = Branch::all();
        $products = Product::all();

      

        return view('inventories.addInventory' , compact('branches' , 'products'));
    }

    public function storeInventory(Request $request){
        $validator = Validator::make($request->all(), [ 
            'initial_qty' => 'required|numeric', 
            'current_qty' => 'required|numeric',
            'product_id' => 'required',
            'branch_id' => 'required'
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $inventory = Inventory::updateOrCreate(
            ['branch_id' => $request->branch_id , 'product_id' => $request->product_id],
            ['initial_qty' => $request->initial_qty , 'current_qty' => $request->current_qty]
        );
        $inventory->save();

        $this->notifyUsers('New Inventory Added');
        return redirect('inventories')->with('message', 'Inventory Added Successfully');;
    }

    public function editInventory($id){
        $inventory = Inventory::where(['id' => $id])->first();
        $branches = Branch::all();
        $products = Product::all();
        
        return view('inventories.editInventory' , compact('branches' , 'products' , 'inventory'));
    }

    public function updateInventory(Request $request){
        $validator = Validator::make($request->all(), [ 
            'initial_qty' => 'required|numeric', 
            'current_qty' => 'required|numeric',
            'product_id' => 'required',
            'branch_id' => 'required'
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $inventory = Inventory::where(['id' => $request->id])->first();

        $oldInventory = new Inventory();
        $oldInventory->current_qty = $inventory->current_qty;
        $oldInventory->initial_qty = $inventory->initial_qty;
        $oldInventory->branch_id = $inventory->branch_id;
        $oldInventory->product_id = $inventory->product_id;

        $inventory->current_qty = $request->current_qty;
        $inventory->initial_qty = $request->initial_qty;
        $inventory->branch_id = $request->branch_id;
        $inventory->product_id = $request->product_id;

        if($inventory->isDirty('current_qty')){
            Inventory::activity($oldInventory->current_qty , $request->current_qty , 'Current Quantity' , Auth::id() , $inventory->product_id , $inventory->branch_id  );            
        }
        if($inventory->isDirty('initial_qty')){
            Inventory::activity($oldInventory->initial_qty , $request->initial_qty , 'Initial Quantity' , Auth::id() , $inventory->product_id , $inventory->branch_id  );            
            $branch = Branch::where(['name' => 'Kitchen'])->first();
            if($branch->id != $request->branch_id){
                $inv = Inventory::where(['branch_id' => $branch->id , 'product_id' => $request->product_id])->first(); 
                if($inv){
                    $inv->current_qty = (int)$inv->current_qty - (int) $request->initial_qty;            
                    $inv->save();
                }
            }
        }
        if($inventory->isDirty('product_id')){
            Inventory::activity($oldInventory->product_id , $request->product_id , 'Product' , Auth::id() , $inventory->product_id , $inventory->branch_id  );            
        }
        if($inventory->isDirty('branch_id')){
            Inventory::activity($oldInventory->branch_id , $request->branch_id , 'Branch' , Auth::id() , $inventory->product_id , $inventory->branch_id  );            
        }

        $inventory->save();

        $this->notifyUsers('Inventory Updated');
        return redirect('inventories')->with('message', 'Inventory Updated Successfully');;        
    }

    public function deleteInventory($id){
        Inventory::where(['id' => $id])->delete();  
        $this->notifyUsers('Inventory Deleted');      
        return redirect()->back()->with('message', 'Inventory Deleted Successfully');;
    }

    public function notifyUsers($message){
        $this->details['data']['link'] = route('inventories');
        $this->details['data']['text'] = $message;
        $users = User::all();
        foreach($users as $user){
            if($user->can('View Inventories')  || $user->can('all')){
                $user->notify(new Noti($this->details));
            }
        }
    }
}
