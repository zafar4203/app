<?php

namespace App\Http\Controllers;

use DataTables;
use App\FranchiseInquiry;
use Illuminate\Http\Request;

class FranchiseInquiryController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }
    
    public function inquiries(){
        $franchices = FranchiseInquiry::all();

        return DataTables::of($franchices)
        ->addColumn('action', function ($id) {
            return '<a href="'. route("showFranchiseInquiry" , $id) .'" class="text-primary"><i class="fa fa-eye"></i></a>'.
            '<a class="ml-2 text-primary" onClick="delete_click('.$id->id.')" ><i class="fa fa-trash"></i></button>'; 
        })
        ->make(true);
    }

    public function index(){
        return view("franchise.inquiries");
    }

    public function show($id){
        $fi = FranchiseInquiry::find($id);
        return view("franchise.show",compact('fi'));
    }

    public function delete($id){
        $fi = FranchiseInquiry::find($id);
        $fi->delete();
        return redirect(route('franchise_inquiries.index'))->with('message', 'Franchise Inquiry Deleted Successfully');;
    }

}
