<?php

namespace App\Http\Controllers;

use DataTables;
use Validator;
use App\Supplier;
use App\User;
use Auth;
use App\Notifications\Noti;
use Illuminate\Http\Request;

class SupplierController extends Controller
{

    public $details = [];
    public function __construct(){
        $this->middleware('auth');

        $this->details = [
            'read_at' => null,
            'data' => [
                'link' => '',
                'text' => ''
            ],
        ];
    }

    public function getSuppliers(){
        $suppliers = Supplier::select('id', 'name', 'block')->get();

        return DataTables::of($suppliers)
        ->addColumn('action', function ($id) {

            $data = "";
            if(Auth::user()->can('Update Supplier') || Auth::user()->can('all')){
                $data = '<a href="'. route("editSupplier" , $id) .'" class="text-primary"><i class="fa fa-edit"></i></a>';
            }
            if(Auth::user()->can('Delete Supplier') || Auth::user()->can('all')){            
                $data = $data .' <a class="text-primary" onClick="delete_click('.$id->id.')" ><i class="fa fa-trash"></i></a>';  
            }

            return $data;

        })
        ->make(true);
    }

    public function index(){
        return view("suppliers.suppliers");
    }

    public function create(){
        return view('suppliers.addSupplier');
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'block' => 'required',
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $supplier = Supplier::create([
            'name' => $request->name , 
            'block' => $request->block,
        ]);
        $supplier->save();


        $this->notifyUsers('Supplier Added');

        return redirect('suppliers')->with('message', 'Supplier Added Successfully');
    }

    public function edit($id){
        $supplier = Supplier::where(['id' => $id])->first();
        return view('suppliers.editSupplier' , compact('supplier'));
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'block' => 'required',
            ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $supplier = Supplier::where(['id' => $request->id])->first();
        $supplier->name = $request->name; 
        $supplier->block = $request->block;
        $supplier->save();

        $this->notifyUsers('Supplier Updated');
        return redirect('suppliers')->with('message', 'Supplier Updated Successfully');
    }

    public function delete($id){
        Supplier::where(['id' => $id])->delete();   

        $this->notifyUsers('Supplier Deleted');     
        return redirect()->back()->with('message', 'Supplier Deleted Successfully');;
    }

    public function notifyUsers($message){
        $this->details['data']['link'] = route('suppliers');
        $this->details['data']['text'] = $message;
        $users = User::all();
        foreach($users as $user){
            if($user->can('View Suppliers')  || $user->can('all')){
                $user->notify(new Noti($this->details));
            }
        }
    }
}
