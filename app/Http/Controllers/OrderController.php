<?php

namespace App\Http\Controllers;

use DataTables;
use DateTime;
use Carbon\Carbon;
use App\Order;
use App\OrderCancel;
use App\ReferalPercentage;
use App\InvisibleActivity;
use App\Sale;
use App\SaleInvisible;
use App\Branch;
use App\Inventory;
use App\Invisible;
use App\OrderItem;
use App\User;
use App\Notifications\Noti;
use Illuminate\Http\Request;

use Notification;
use App\Notifications\AdminNotification;

class OrderController extends Controller
{
    public $details = [];
    public function __construct(){
        $this->middleware('auth');

        $this->details = [
            'read_at' => null,
            'data' => [
                'link' => '',
                'text' => ''
            ],
        ];
    }

    public function getOrders($year , $month){
        if($month < 10){
            $month = sprintf("%02d", $month);
        }
        $orders = Order::select('id', 'total_price','order_from','delivery_date' , 'created_at')
        ->whereYear('created_at', '=', $year)
        ->whereMonth('created_at', '=', $month)
        ->where(function ($q) {
            $q->where('order_from', "WEB")->orWhere('order_from', "PHONE")->orWhere('order_from', "INVISIBLE");
        })->get();

        foreach($orders as $order){
            $order->deliver = DateTime::createFromFormat('m/d/Y', $order->booking->date)->format('j F');
            $order->placed = $order->created_at->format('j F Y');
            $date = Carbon::parse($order->deliver)->startOfDay();
            $today = Carbon::now()->startOfDay();

            if($date == $today){
                $order->deliver = " Today" . " (" . $order->booking->time.")";                 
            }
            else{
                $order->deliver = $order->deliver . " (" . $order->booking->time.")";                 
            }
        }

        return DataTables::of($orders)
        ->addColumn('action', function ($id) {
            return '<a href="'.url("orderDetails").'/'.$id->id.'" style="text-decoration:underline; cursor:pointer;" class="text-primary">View</a>'; 
        })
        ->make(true);
    }

    public function index(){
        return view('orders.orders');
    }

    public function create(){
        return view('orders.addOrder');
    }

    public function store(Request $request){}

    public function edit($id){}

    public function update(Request $request){}

    public function delete($id){}

    public function orderDetails($id){
        $order = Order::where(['id' => $id])->first();
        $order->deliver = DateTime::createFromFormat('m/d/Y', $order->booking->date)->format('j F Y');

        $date = Carbon::parse($order->deliver)->startOfDay();
        $today = Carbon::now()->startOfDay();
        if($date < $today){
            $order->delivery_date_passed = true;
        }else{
            $order->delivery_date_passed = false;
        }
        if($date == $today){
            $order->delivery_date_passed = false;
        }

        $orders = OrderItem::where('order_id' , '=' , $id)->get();
        return view('orders.orderDetails' , compact('order' , 'orders'));
    }

    public function changeOrderStatus($id , $status){
        $order = Order::where(['id' => $id])->first();
        $order->order_status = $status;
        $order->save();

        if($order->order_status == "1"){
            $details = [
                'greeting' => 'Hi '.$order->user_name,
                'subject' => 'Your Order is Accepted',
                'body' => 'Your order with order number '.$order->id.' is accepted and will be deliver you soon',
                'thanks' => 'Thank you for choosing Papafluffy !',
                'order_id' => $order->id
            ];
                
            $user = new User();
            $user->email= $order->user_email;
            Notification::send($user , new AdminNotification($details));    
        }

        if($order->order_status == "2"){
            if($order->invisible_id != ""){

                $this->invisibleWork($order);
                $this->updateInventory($order , 'invisible');
            }else{
                $this->updateInventory($order , 'web');
            }
        }
        if($order->order_status != "3"){
            OrderCancel::where(['order_id' => $order->id])->delete();
        }

        $this->notifyUsers('Order Status Updated');

        return $order;
    }

    public function orderChange(Request $request){
        $order = Order::where(['id' => $request->id])->first();
        $orderItem = OrderItem::where(['id' => $request->item_id])->first();
        $orderItem->product_quantity = $request->quantity;
        $orderItem->save();

        $orderItems = OrderItem::where(['order_id' => $order->id])->get();
        $total = 0;
        $sub = 0;
        $vat = 0;
        foreach($orderItems as $orderItem){
            $total = $total + ((double)$orderItem->product_price * (double) $orderItem->product_quantity);
        }

        $vat = $total * 0.05;
        $sub = $total - $vat;

        if($total < 30){
            $total = (double)$total + 10;
        }

        $order->total_price = number_format($total,2);
        $order->vat_price = number_format($vat,2);
        $order->sub_total_price = number_format($sub,2);
        $order->save();

        return $order;
    }

    public function deleteOrderItem($id , $item){
        $order = Order::where(['id' => $id])->first();
        $orderItem = OrderItem::where(['id' => $item])->first();
        $orderItem->delete();

        $orders = OrderItem::where(['order_id' => $order->id])->get();
        $total = 0;
        $sub = 0;
        $vat = 0;
        $count = 0;
        foreach($orders as $orderItem){
            $total = $total + ((double)$orderItem->product_price * (double) $orderItem->product_quantity);
            $count++;
        }
        $vat = $total * 0.05;
        $sub = $total - $vat;

        $order->total_price = number_format($total,2);
        $order->vat_price = number_format($vat,2);
        $order->sub_total_price = number_format($sub,2);
        $order->total_items = $count;
        $order->save();

        return redirect()->back();
    }

    public function updateOrder($id){
        $order = Order::where(['id' => $id])->first();
        $orders = OrderItem::where(['order_id' => $order->id])->get();
        $total = 0;
        $sub = 0;
        $vat = 0;
        $count = 0;
        foreach($orders as $orderItem){
            $total = $total + ((double)$orderItem->product_price * (double) $orderItem->product_quantity);
            $count++;
        }
        $vat = $total * 0.05;
        $sub = $total - $vat;

        $order->total_price = number_format($total,2);
        $order->vat_price = number_format($vat,2);
        $order->sub_total_price = number_format($sub,2);
        $order->total_items = $count;
        $order->save();

        return response()->json('success' , 200);
    }

    public function orderUserAddress(Request $request){
        $order = Order::where(['id' => $request->order_id])->first();
        $order->user_address = $request->user_address;
        $order->save();

        return $order;
    }

    public function recordSale($sub , $vat , $total , $type , $branch , $source , $discount){
        $date = date('m/d/Y');
        $sale = Sale::where(['date' => $date , 'branch_id' => $branch , 'source' => $source])->first();

        if(!is_null($sale)){
            if($type == "cash"){
                $sale->cash = $sale->cash + (float)$total;
            }else{
                $sale->card = $sale->card + (float)$total;
            }
            $sale->total = $sale->cash + $sale->card;
            $sale->vat = $sale->vat + (float) $vat; 
            $sale->date = $date;
            $sale->discount = (float)$sale->discount + (float)$discount;
            $sale->source = $source;
            $sale->save();

        }else{
            if($type == "cash"){
                Sale::create([
                    'cash' => (float)$total,
                    'card' => 0,
                    'total' => (float)$total,
                    'vat' => (float)$vat ,
                    'date' => $date,
                    'discount' => $discount,
                    'source' => $source,
                    'branch_id' => $branch
                ]);
            }else{
                Sale::create([
                    'cash' => 0,
                    'card' => (float)$total,
                    'total' => (float) $total,
                    'vat' => (float)$vat,
                    'date' => $date,
                    'discount' => $discount,
                    'source' => $source,
                    'branch_id' => $branch
                ]);
            }
        }
    }

    // Update Order Items In Inventory on status changed to Delivered
    public function updateInventory($order , $source){
        $branch = Branch::where(['name' => 'Kitchen'])->first();
        $this->recordSale($order->sub_total_price , $order->vat_price , $order->total_price , $order->order_type , $branch->id , $source , $order->discount);

        $orderItems = OrderItem::where(['order_id' => $order->id])->get();
        if($orderItems){
            foreach($orderItems as $orderItem){
                $inventory = Inventory::where(['product_id' => $orderItem->product_id , 'branch_id' => $branch->id])->first();
                if($inventory){
                    $inventory->current_qty = $inventory->current_qty - $orderItem->product_quantity;
                    $inventory->save();     
                }
            }
        }
    }

    public function invisibleWork($order){
        $invisible = Invisible::where(['id' => $order->invisible_id])->first();
        $amount = 0;
        if($invisible->orders->count() == 1 ){
            $amount = $order->total_price * ($invisible->p_percentage/100); 
        }else{
            $amount = $order->total_price * ($invisible->s_percentage/100);                                         
        }
        $invisible->wallet = $invisible->wallet + $amount;
        $invisible->save();
        InvisibleActivity::create([
            'invisible_id' => $invisible->id,
            'activity' => "AED ".$amount." added to wallet from order worth of AED ". $order->total_price
        ]);
        $this->recordInvisibleSale('order' , $amount , $invisible->id);

        $percentage = ReferalPercentage::where(['id' => 1])->first();
        $refering_amount = 0;
        if($invisible->refferer){
            $invisible_referer = Invisible::where(['id' => $invisible->refferer->id])->first();
            $refering_amount = $order->total_price * ($percentage->percentage/100);                   
            $invisible_referer->wallet = $invisible_referer->wallet + $refering_amount;
            $invisible_referer->save();

            InvisibleActivity::create([
                'invisible_id' => $invisible_referer->id,
                'activity' => 'AED '.$refering_amount." added to wallet from Referal ".$invisible->my_ref_id." order worth AED ".$order->total_price
            ]);
            $this->recordInvisibleSale('refferal' , $refering_amount , $invisible->refferer->id);
        }
    }

    public function recordInvisibleSale($from, $total, $invisible_id){
        $date = date('m/d/Y');
        $sale = SaleInvisible::where(['date' => $date , 'from' => $from, 'invisible_id' => $invisible_id])->first();

        if($sale){
            $sale->total = $sale->total + $total;
            $sale->invisible_id = $invisible_id;
            $sale->date = $date;
            $sale->from = $from;
            $sale->save();

        }else{
            SaleInvisible::create([
                'total' => (float)$total,
                'invisible_id' => $invisible_id,
                'date' => $date,
                'from' => $from
            ]);

        }
    }

    public function notifyUsers($message){
        $this->details['data']['link'] = route('orders');
        $this->details['data']['text'] = $message;
        $users = User::all();
        foreach($users as $user){
            if($user->can('View Home Delivery')  || $user->can('all')){
                $user->notify(new Noti($this->details));
            }
        }
    }
}
