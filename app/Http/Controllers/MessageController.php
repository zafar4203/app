<?php

namespace App\Http\Controllers;

use Validator;
use DataTables;
use App\Message;
use App\User;
use App\Notifications\Noti;
use Illuminate\Http\Request;

class MessageController extends Controller
{

    public $details = [];
    public function __construct(){
        $this->middleware('auth');

        $this->details = [
            'read_at' => null,
            'data' => [
                'link' => '',
                'text' => ''
            ],
        ];
    }

    public function getMessages(){
        $messages = Message::select('id', 'name' , 'email', 'phone', 'message', 'created_at', 'updated_at')->get();

        return DataTables::of($messages)
        ->addColumn('action', function ($id) {
            return '<a class="text-primary" onClick="delete_click('.$id->id.')" ><i class="fa fa-trash"></i></button>'; 
        })
        ->make(true);
    }

    public function index(){
        return view("messages.messages");
    }

    public function Message(){
        return view("messages.single");
    }

    public function deleteMessage($id){
        Message::where(['id' => $id])->delete();   
        $this->notifyUsers('Message Deleted');
     
        return redirect()->back()->with('message', 'Message Deleted Successfully');
    }

    public function notifyUsers($message){
        $this->details['data']['link'] = route('messages.index');
        $this->details['data']['text'] = $message;
        $users = User::all();
        foreach($users as $user){
            if($user->can('View Messages')  || $user->can('all')){
                $user->notify(new Noti($this->details));
            }
        }
    }




}