<?php

namespace App\Http\Controllers;

use Auth;
use File;
use Response;
use Validator;
use DataTables;
use App\Product;
use App\Area;
use App\GroupedProducts;
use App\User;
use App\Events\MyEvent;
use App\Notifications\Noti;
use Illuminate\Http\Request;

class AreaController extends Controller
{
    public $details = [];
    public function __construct(){
        $this->middleware('auth');

        $this->details = [
            'read_at' => null,
            'data' => [
                'link' => '',
                'text' => ''
            ],
        ];
    }
    
    public function getAreas(){
        $areas = Area::select('id', 'name', 'delivery_charges', 'block' , 'created_at')->get();

        foreach($areas as $area){
            $area->isBlock = Area::block($area->block);
        }

        return DataTables::of($areas)
        ->addColumn('action', function ($id) {

            $data = "";
            if(Auth::user()->can('Update Area') || Auth::user()->can('all')){
                $data = '<a href="'. route("editArea" , $id) .'" class="text-primary"><i class="fa fa-edit"></i></a>';
            }
            if(Auth::user()->can('Delete Area') || Auth::user()->can('all')){            
                $data = $data . ' <a class="text-primary" onClick="delete_click('.$id->id.')" ><i class="fa fa-trash"></i></button>';  
            }
            return $data;

        })
        ->make(true);
    }

    public function index(){
        return view("areas.index");
    }

    public function Area(){
        return view("areas.create");
    }

    public function addArea(Request $request){

        $validator = Validator::make($request->all(), [ 
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:255', 
            'delivery_charges' => 'required|numeric',
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $area = Area::create([
            'name' => $request->name , 
            'delivery_charges' => $request->delivery_charges, 
            'block' => $request->block, 
             ]);
        $area->save();

        $this->notifyUsers('New Area Added');     

        return redirect('areas')->with('message', 'Area Added Successfully');
    }

    public function editArea($id)
    {
        $area = Area::where(['id' => $id])->first();
        return view('areas.edit' , compact('area'));
    }

    public function updateArea(Request $request){
        $validator = Validator::make($request->all(), [ 
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:255', 
            'delivery_charges' => 'required|numeric',
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $area = Area::where(['id' => $request->id])->first();
        $area->name = $request->name;
        $area->delivery_charges = $request->delivery_charges;
        $area->block = $request->block;

        $area->save();
        $this->notifyUsers('Area Updated');     

        return redirect()->back()->with('message', 'Area Updated Successfully');;
    }

    public function deleteArea($id){

        Area::where(['id' => $id])->delete();   
        $this->notifyUsers('Area Deleted');     
     
        return redirect()->back()->with('message', 'Area Deleted Successfully');;
    }

    public function notifyUsers($message){
        $this->details['data']['link'] = route('areas.index');
        $this->details['data']['text'] = $message;
        $users = User::all();
        foreach($users as $user){
            if($user->can('View Areas')  || $user->can('all')){
                $user->notify(new Noti($this->details));
            }
        }
    }


}
