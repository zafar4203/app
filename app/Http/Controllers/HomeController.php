<?php

namespace App\Http\Controllers;

use DB;
use App\Sale;
use App\Order;
use App\Aggregator;
use App\OtherSale;
use App\Menu;
use App\SubMenu;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function dashboard()
    {            
            $year = date('Y');
            $month = date('m');
            $ySale = 0;
            $yKiosk = 0;
            $yOnline = 0;
            $yInvisible = 0;

            $tSale = 0;
            $tKiosk = 0;
            $tOnline = 0;
            $tInvisible = 0;

            $mSale = 0;
            $mKiosk = 0;
            $mOnline = 0;
            $mInvisible = 0;

        
            // Monthly Sale
            $sales = Sale::whereYear('created_at', '=', $year)
            ->whereMonth('created_at', '=', $month)
            ->get();
            if($sales){
                foreach($sales as $sale){
                    $mSale = $mSale + $sale->total;
                }
            }

            // Online
            $sales = Sale::whereYear('created_at', '=', $year)
            ->whereMonth('created_at', '=', $month)
            ->where('source', '=', 'web')
            ->get();
            if($sales){
                foreach($sales as $sale){
                    $mOnline = $mOnline + $sale->total;
                }
            }

            // POS
            $sales = Sale::whereYear('created_at', '=', $year)
            ->whereMonth('created_at', '=', $month)
            ->where('source', '=', 'POS')
            ->get();
            if($sales){
                foreach($sales as $sale){
                    $mKiosk = $mKiosk + $sale->total;
                }
            }
            
            // Invisible
            $sales = Sale::whereYear('created_at', '=', $year)
            ->whereMonth('created_at', '=', $month)
            ->where('source', '=', 'invisible')
            ->get();
            if($sales){
                foreach($sales as $sale){
                    $mInvisible = $mInvisible + $sale->total;
                }
            }

            // Today All Sales
            $date = date('m/d/Y');
            $sales = Sale::where(['date' => $date])->get();
            if($sales){
                foreach($sales as $sale){
                    $tSale = $tSale+ $sale->total;
                }
            }

            // Online
            $sale = Sale::where(['date' => $date])
            ->where(['source' => 'web'])
            ->first();
            if($sale){
                $tOnline = $sale->total;
            }

            // Invisible
            $sale = Sale::where(['date' => $date])
            ->where(['source' => 'invisible'])
            ->first();
            if($sale){
                $tInvisible = $sale->total;
            }

            // POS
            $sale = Sale::where(['date' => $date])
            ->where(['source' => 'POS'])
            ->first();
            if($sale){
                $tKiosk = $sale->total;
            }
            

            // Yesterday All Sale
            $date = date('m/d/Y',strtotime("-1 days"));
            $sales = Sale::where(['date' => $date])->get();
            if($sales){
                foreach($sales as $sale){
                    $ySale = $ySale+ $sale->total;
                }
            }

            // Online
            $sale = Sale::where(['date' => $date])
            ->where(['source' => 'web'])
            ->first();
            if($sale){
                $yOnline = $sale->total;
            }

            // Invisible 
            $sale = Sale::where(['date' => $date])
            ->where(['source' => 'invisible'])
            ->first();
            if($sale){
                $yInvisible = $sale->total;
            }

            // POS 
            $sale = Sale::where(['date' => $date])
            ->where(['source' => 'POS'])
            ->first();
            if($sale){
                $yKiosk = $sale->total;
            }

            // Pending Orders
            $date = date('m/d/Y');
            $pOrders = 0;
            $orders = Order::join('bookings' , 'bookings.order_id' , '=' , 'orders.id')
            ->where(function ($query) {
                $query->where('order_from', '=', 'INVISIBLE')
                      ->orWhere('order_from', '=', 'WEB');
            })
            ->where(function ($query) {
                $query->where('order_status', '=', '0')
                      ->orWhere('order_status', '=', '1');
            })
            ->where('bookings.date', '=', $date)
            ->get();
            foreach($orders as $order){
                $pOrders++;
            }

            
            // total Orders
            $tOrders = 0;
            $orders = Order::join('bookings' , 'bookings.order_id' , '=' , 'orders.id')
            ->where(function ($query) {
                $query->where('order_from', '=', 'INVISIBLE')
                      ->orWhere('order_from', '=', 'WEB');
            })
            ->where('bookings.date', '=', $date)
            ->get();
            foreach($orders as $order){
                $tOrders++;
            }

            // Completed Orders
            $cOrders = 0;
            $orders = Order::where(function ($query) {
                $query->where('order_from', '=', 'INVISIBLE')
                      ->orWhere('order_from', '=', 'WEB');
            })
            ->join('bookings' , 'bookings.order_id' , '=' , 'orders.id')
            ->where('bookings.date', '=', $date)
            ->where(['orders.order_status' => 2])
            ->get();
            foreach($orders as $order){
                $cOrders++;
            }

            $menu = Menu::where(['name' => 'revenues'])->first();
            $subMenuD = SubMenu::where(['menu_id' => $menu->id])->get();

            $dSale = 0;
            $dData = [];

            foreach($subMenuD as $key=> $m){
                $dData[$key] = 0;
                if($m->name == 'Shop Sales'){
                    $sale = Sale::where(['date' => $date])
                    ->where('branch_id' , '!=' , 3)
                    ->first();

                    if($sale){
                        $dSale = $dSale + $sale->total;
                        $dData[$key] = $sale->total;
                    }
                }
                if($m->name == 'online sales'){
                    $sale = Sale::where(['date' => $date])
                    ->where(['source' => 'web'])
                    ->where('branch_id' , '=' , 3)
                    ->first();

                    if($sale){                  
                        $dSale = $dSale + $sale->total;
                        $dData[$key] = $sale->total;
                    }
                }
                if(strpos($m->link, '/aggregator') !== false){
                    $aggregators = Aggregator::where(['type' => 'Aggregator'])->get();
                    $st = 0;
                    foreach($aggregators as $ag){
                        $sales = OtherSale::where(['date' => $date])
                        ->where(['aggregator_id' => $ag->id])    
                        ->get();

                        foreach($sales as $s){
                            $st = $st + $s->cash;
                        }
                    }
                    $dSale = $dSale + $st;
                    $dData[$key] = $st;
                }
                if(strpos($m->link, '/b2b') !== false){
                    $aggregators = Aggregator::where(['type' => 'B2B'])->get();
                    $st = 0;
                    foreach($aggregators as $ag){
                        $sales = OtherSale::where(['date' => $date])
                        ->where(['aggregator_id' => $ag->id])    
                        ->get();

                        foreach($sales as $s){
                            $st = $st + $s->cash;
                        }
                    }
                    $dSale = $dSale + $st;
                    $dData[$key] = $st;
                }
                if(strpos($m->link, '/otherSale/other') !== false){
                    $aggregators = Aggregator::where(['type' => 'Other'])->get();
                    $st = 0;
                    foreach($aggregators as $ag){
                        $sales = OtherSale::where(['date' => $date])
                        ->where(['aggregator_id' => $ag->id])    
                        ->get();

                        foreach($sales as $s){
                            $st = $st + $s->cash;
                        }
                    }
                    $dSale = $dSale + $st;
                    $dData[$key] = $st;
                }
            }
            $dLabels = array();
            foreach($subMenuD as $key=> $m){
                array_push($dLabels , $m->name);
                if(strpos($m->link, '/otherSale/other') !== false){
                    $aggregators = Aggregator::where(['type' => 'other'])->get();
                    if($aggregators){
                        foreach($aggregators as $agg){
                            $sales = OtherSale::where(['date' => $date])
                            ->where(['aggregator_id' => $agg->id])    
                            ->get();
    
                            if($sales)
                            $st = 0;
                            foreach($sales as $sale){
                                $st = $st + $s->cash;
                            }
                            $agg->sale = $st;
                        }
                        $m->subs = $aggregators;    
                    }else{
                        $m->subs = "";    
                    }
                }



                if(strpos($m->link, '/b2b') !== false){
                    $aggregators = Aggregator::where(['type' => 'B2B'])->get();
                    if($aggregators){
                        foreach($aggregators as $agg){
                            $sales = OtherSale::where(['date' => $date])
                            ->where(['aggregator_id' => $agg->id])    
                            ->get();
    
                            if($sales)
                            $st = 0;
                            foreach($sales as $sale){
                                $st = $st + $s->cash;
                            }
                            $agg->sale = $st;
                        }
                        $m->subs = $aggregators;    
                    }else{
                        $m->subs = "";    
                    }
                }


                if(strpos($m->link, '/aggregator') !== false){
                    $aggregators = Aggregator::where(['type' => 'Aggregator'])->get();
                    if($aggregators){
                        foreach($aggregators as $agg){
                            $sales = OtherSale::where(['date' => $date])
                            ->where(['aggregator_id' => $agg->id])    
                            ->get();

                            $st = 0;
                            if($sales)
                            foreach($sales as $sale){
                                $st = $st + $sale->cash;
                            }
                            $agg->sale = $st;
                        }
                        $m->subs = $aggregators;    
                    }else{
                        $m->subs = "";    
                    }
                }

                if($dSale != 0){
                    $m->percentage = round($dData[$key]/$dSale * 100 , 2) . " %";
                }else{
                    $m->percentage = 0;
                }
            }


            // Monthly Pie Chart
            $subMenuM = SubMenu::where(['menu_id' => $menu->id])->get();

            $mSale = 0;
            $mData = [];

            foreach($subMenuM as $key=> $m){
                $mData[$key] = 0;
                if($m->name == 'Shop Sales'){
                    $paydate_raw = DB::raw("STR_TO_DATE(`date`, '%m/%d/%Y')");

                    $sales = Sale::where('branch_id' , '!=' , 3)
                    ->whereMonth($paydate_raw, '=', $month)
                    ->get();    

                    if($sales){
                        $sl = 0;
                        foreach($sales as $sale){
                            $sl = $sl + $sale->total;
                        }
                        $mSale = $mSale + $sl;
                        $mData[$key] = $sl;    
                    }
                }
                if($m->name == 'online sales'){
                    $paydate_raw = DB::raw("STR_TO_DATE(`date`, '%m/%d/%Y')");

                    $sales = Sale::where(['source' => 'web'])
                    ->where('branch_id' , '=' , 3)
                    ->whereMonth($paydate_raw, '=', $month)
                    ->get();

                    if($sales){
                        $sl = 0;
                        foreach($sales as $sale){
                            $sl = $sl + $sale->total;
                        }
                        $mSale = $mSale + $sl;
                        $mData[$key] = $sl;    
                    }
                }
                if(strpos($m->link, '/aggregator') !== false){
                    $paydate_raw = DB::raw("STR_TO_DATE(`date`, '%m/%d/%Y')");

                    $aggregators = Aggregator::where(['type' => 'Aggregator'])->get();
                    $st = 0;
                    foreach($aggregators as $ag){
                        $sales = OtherSale::where(['aggregator_id' => $ag->id])
                        ->whereMonth($paydate_raw, '=', $month)
                        ->get();

                        foreach($sales as $s){
                            $st = $st + $s->cash;
                        }
                    }
                    $mSale = $mSale + $st;
                    $mData[$key] = $st;
                }
                if(strpos($m->link, '/b2b') !== false){
                    $paydate_raw = DB::raw("STR_TO_DATE(`date`, '%m/%d/%Y')");

                    $aggregators = Aggregator::where(['type' => 'B2B'])->get();
                    $st = 0;
                    foreach($aggregators as $ag){
                        $sales = OtherSale::where(['aggregator_id' => $ag->id])    
                        ->whereMonth($paydate_raw, '=', $month)
                        ->get();

                        foreach($sales as $s){
                            $st = $st + $s->cash;
                        }
                    }
                    $mSale = $mSale + $st;
                    $mData[$key] = $st;
                }
                if(strpos($m->link, '/otherSale/other') !== false){
                    $paydate_raw = DB::raw("STR_TO_DATE(`date`, '%m/%d/%Y')");

                    $aggregators = Aggregator::where(['type' => 'Other'])->get();
                    $st = 0;
                    foreach($aggregators as $ag){
                        $sales = OtherSale::where(['aggregator_id' => $ag->id])   
                        ->whereMonth($paydate_raw, '=', $month)
                        ->get();

                        foreach($sales as $s){
                            $st = $st + $s->cash;
                        }
                    }
                    $mSale = $mSale + $st;
                    $mData[$key] = $st;
                }
            }
            $mLabels = array();
            foreach($subMenuM as $key=> $m){
                array_push($mLabels , $m->name);
                if(strpos($m->link, '/otherSale/other') !== false){
                    $aggregators = Aggregator::where(['type' => 'other'])->get();
                    if($aggregators){
                        foreach($aggregators as $agg){
                            $paydate_raw = DB::raw("STR_TO_DATE(`date`, '%m/%d/%Y')");

                            $sales = OtherSale::where(['aggregator_id' => $agg->id])
                            ->whereMonth($paydate_raw, '=', $month)
                            ->get();
        
                            if($sales)
                            $st = 0;
                            foreach($sales as $sale){
                                $st = $st + $s->cash;
                            }
                            $agg->sale = $st;
                        }
                        $m->subs = $aggregators;    
                    }else{
                        $m->subs = "";    
                    }
                }
                if(strpos($m->link, '/b2b') !== false){
                    $aggregators = Aggregator::where(['type' => 'B2B'])->get();
                    if($aggregators){
                        foreach($aggregators as $agg){
                            $paydate_raw = DB::raw("STR_TO_DATE(`date`, '%m/%d/%Y')");

                            $sales = OtherSale::where(['aggregator_id' => $agg->id])
                            ->whereMonth($paydate_raw, '=', $month)
                            ->get();
    
                            if($sales)
                            $st = 0;
                            foreach($sales as $sale){
                                $st = $st + $s->cash;
                            }
                            $agg->sale = $st;
                        }
                        $m->subs = $aggregators;    
                    }else{
                        $m->subs = "";    
                    }
                }
                if(strpos($m->link, '/aggregator') !== false){
                    $aggregators = Aggregator::where(['type' => 'Aggregator'])->get();
                    if($aggregators){
                        foreach($aggregators as $agg){
                            $paydate_raw = DB::raw("STR_TO_DATE(`date`, '%m/%d/%Y')");

                            $sales = OtherSale::where(['aggregator_id' => $agg->id])
                            ->whereMonth($paydate_raw, '=', $month)
                            ->get();

                            $st = 0;
                            if($sales)
                            foreach($sales as $sale){
                                $st = $st + $sale->cash;
                            }
                            $agg->sale = $st;
                        }
                        $m->subs = $aggregators;    
                    }else{
                        $m->subs = "";    
                    }
                }
                if($mSale != 0){
                    $m->percentage = round($mData[$key]/$mSale * 100 , 2) . " %";
                }else{
                    $m->percentage = 0;
                }
            }



            // Yearly Pie Chart
            $subMenuY = SubMenu::where(['menu_id' => $menu->id])->get();

            $ySale = 0;
            $yData = [];

            foreach($subMenuY as $key=> $m){
                $yData[$key] = 0;
                if($m->name == 'Shop Sales'){
                    $paydate_raw = DB::raw("STR_TO_DATE(`date`, '%m/%d/%Y')");

                    $sales = Sale::where('branch_id' , '!=' , 3)
                    ->whereYear($paydate_raw, '=', $year)
                    ->get();    

                    if($sales){
                        $sl = 0;
                        foreach($sales as $sale){
                            $sl = $sl + $sale->total;
                        }
                        $ySale = $ySale + $sl;
                        $yData[$key] = $sl;    
                    }
                }
                if($m->name == 'online sales'){
                    $paydate_raw = DB::raw("STR_TO_DATE(`date`, '%m/%d/%Y')");

                    $sales = Sale::where(['source' => 'web'])
                    ->where('branch_id' , '=' , 3)
                    ->whereYear($paydate_raw, '=', $year)
                    ->get();

                    if($sales){
                        $sl = 0;
                        foreach($sales as $sale){
                            $sl = $sl + $sale->total;
                        }
                        $ySale = $ySale + $sl;
                        $yData[$key] = $sl;    
                    }
                }
                if(strpos($m->link, '/aggregator') !== false){
                    $paydate_raw = DB::raw("STR_TO_DATE(`date`, '%m/%d/%Y')");

                    $aggregators = Aggregator::where(['type' => 'Aggregator'])->get();
                    $st = 0;
                    foreach($aggregators as $ag){
                        $sales = OtherSale::where(['aggregator_id' => $ag->id])
                        ->whereYear($paydate_raw, '=', $year)
                        ->get();

                        foreach($sales as $s){
                            $st = $st + $s->cash;
                        }
                    }
                    $ySale = $ySale + $st;
                    $yData[$key] = $st;
                }
                if(strpos($m->link, '/b2b') !== false){
                    $paydate_raw = DB::raw("STR_TO_DATE(`date`, '%m/%d/%Y')");

                    $aggregators = Aggregator::where(['type' => 'B2B'])->get();
                    $st = 0;
                    foreach($aggregators as $ag){
                        $sales = OtherSale::where(['aggregator_id' => $ag->id])    
                        ->whereYear($paydate_raw, '=', $year)
                        ->get();

                        foreach($sales as $s){
                            $st = $st + $s->cash;
                        }
                    }
                    $ySale = $ySale + $st;
                    $yData[$key] = $st;
                }
                if(strpos($m->link, '/otherSale/other') !== false){
                    $paydate_raw = DB::raw("STR_TO_DATE(`date`, '%m/%d/%Y')");

                    $aggregators = Aggregator::where(['type' => 'Other'])->get();
                    $st = 0;
                    foreach($aggregators as $ag){
                        $sales = OtherSale::where(['aggregator_id' => $ag->id])   
                        ->whereYear($paydate_raw, '=', $year)
                        ->get();

                        foreach($sales as $s){
                            $st = $st + $s->cash;
                        }
                    }
                    $ySale = $ySale + $st;
                    $yData[$key] = $st;
                }
            }
            $yLabels = array();
            foreach($subMenuY as $key=> $m){
                array_push($yLabels , $m->name);
                if(strpos($m->link, '/otherSale/other') !== false){
                    $aggregators = Aggregator::where(['type' => 'other'])->get();
                    if($aggregators){
                        foreach($aggregators as $agg){
                            $paydate_raw = DB::raw("STR_TO_DATE(`date`, '%m/%d/%Y')");
                            $sales = OtherSale::where(['aggregator_id' => $agg->id])    
                            ->whereYear($paydate_raw, '=', $year)
                            ->get();
    
                            if($sales)
                            $st = 0;
                            foreach($sales as $sale){
                                $st = $st + $s->cash;
                            }
                            $agg->sale = $st;
                        }
                        $m->subs = $aggregators;    
                    }else{
                        $m->subs = "";    
                    }
                }
                if(strpos($m->link, '/b2b') !== false){
                    $aggregators = Aggregator::where(['type' => 'B2B'])->get();
                    if($aggregators){
                        foreach($aggregators as $agg){
                            $paydate_raw = DB::raw("STR_TO_DATE(`date`, '%m/%d/%Y')");
                            $sales = OtherSale::where(['aggregator_id' => $agg->id])    
                            ->whereYear($paydate_raw, '=', $year)
                            ->get();
    
                            if($sales)
                            $st = 0;
                            foreach($sales as $sale){
                                $st = $st + $s->cash;
                            }
                            $agg->sale = $st;
                        }
                        $m->subs = $aggregators;    
                    }else{
                        $m->subs = "";    
                    }
                }
                if(strpos($m->link, '/aggregator') !== false){
                    $aggregators = Aggregator::where(['type' => 'Aggregator'])->get();
                    if($aggregators){
                        foreach($aggregators as $agg){
                            $paydate_raw = DB::raw("STR_TO_DATE(`date`, '%m/%d/%Y')");
                            $sales = OtherSale::where(['aggregator_id' => $agg->id])    
                            ->whereYear($paydate_raw, '=', $year)
                            ->get();

                            $st = 0;
                            if($sales)
                            foreach($sales as $sale){
                                $st = $st + $sale->cash;
                            }
                            $agg->sale = $st;
                        }
                        $m->subs = $aggregators;    
                    }else{
                        $m->subs = "";    
                    }
                }
                if($ySale != 0){
                    $m->percentage = round($yData[$key]/$ySale * 100 , 2) . " %";
                }else{
                    $m->percentage = 0;
                }
            }



            $Data = array(100 , 0);
            // Area Chart Data
            $months = array('Jan' , 'Feb' , 'Mar' , 'Apr' , 'May' , 'Jun' , 'Jul' , 'Aug' , 'Sep' , 'Oct' , 'Nov' , 'Dec');
            // Monthly Sale
            $months_sales = array($this->monthSales(1) , $this->monthSales(2) , $this->monthSales(3) , $this->monthSales(4), $this->monthSales(5), $this->monthSales(6), $this->monthSales(7), $this->monthSales(8), $this->monthSales(9), $this->monthSales(10), $this->monthSales(11), $this->monthSales(12));
            
            return view('dashboard' ,['ySale' => $ySale ,'yData' => $yData,'yLabels' => $yLabels ,'mSale' => $mSale ,'mData' => $mData,'mLabels' => $mLabels ,'dSale' => $dSale ,'dData' => $dData,'dLabels' => $dLabels , 'Data' => $Data , 'months' => $months , 'months_sales' => $months_sales]  , compact('tSale','tOnline','tKiosk','tInvisible','ySale','yOnline','yKiosk','yInvisible','mSale','mOnline','mKiosk','mInvisible','pOrders','tOrders','cOrders','subMenuD','subMenuM','subMenuY'));
        }
    
        public function monthSales($mon){
            if($mon < 10){
                $mon = sprintf("%02d", $mon);
            }
            $year = date('Y');
            $total = 0;
            $sales = Sale::whereYear('created_at', '=', $year)
            ->whereMonth('created_at', '=', $mon)
            ->get();
            if($sales){
                foreach($sales as $sale){
                    $total = $total + $sale->total;
                }
            }
    
            return $total;
        }
}
