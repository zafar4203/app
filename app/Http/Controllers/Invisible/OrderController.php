<?php

namespace App\Http\Controllers\Invisible;
use DataTables;
use Validator;
use Notification;
use DateTime;
use Auth;
use Carbon\Carbon;
use App\Order;
use App\User;
use App\Booking;
use App\Branch;
use App\OrderItem;
use Illuminate\Http\Request;
use App\Notifications\OrderNotification;
use App\Notifications\AdminNotification;

use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function getOrders($year , $month){
        if($month < 10){
            $month = sprintf("%02d", $month);
        }
        $orders = Order::select('id', 'total_price', 'delivery_date' , 'created_at')
        ->whereYear('created_at', '=', $year)
        ->whereMonth('created_at', '=', $month)
        ->where(['order_from' => 'INVISIBLE'])
        ->where(['invisible_id' => Auth::guard('invisible')->user()->id])
        ->get();
        
        foreach($orders as $order){
            $order->deliver = DateTime::createFromFormat('m/d/Y', $order->booking->date)->format('j F');
            $order->placed = $order->created_at->format('j F Y');
            $date = Carbon::parse($order->deliver)->startOfDay();
            $today = Carbon::now()->startOfDay();

            if($date == $today){
                $order->deliver = " Today" . " (" . $order->booking->time.")";                 
            }
            else{
                $order->deliver = $order->deliver . " (" . $order->booking->time.")";                 
            }
        }

        return DataTables::of($orders)
        // ->addColumn('action', function ($id) {
        //     return '<a href="'.url("orderDetails").'/'.$id->id.'" style="text-decoration:underline; cursor:pointer;" class="text-primary">View</a>'; 
        // })
        ->make(true);
    }

    public function index(){
        return view('invisible.orders.orders');
    }

    public function create(){
        return view('invisible.orders.addOrder');
    }

    public function placeOrderHomeDelivery(Request $request){
        $validator = Validator::make($request->all(), [ 
            'user_name' => 'required|regex:/^[\pL\s\-]+$/u|max:15', 
            'user_phone' => 'required|digits_between:11,14', 
            'user_address' => 'required|max:80',
            'order_from' => 'required',
            'sub_total_price' => 'required',
            'vat_price' => 'required',
            'total_price' => 'required',
            'total_items' => 'required',
            'delivery_date' => 'required',            
        ]);

        if ($validator->fails()) { 
            return response()->json($validator->messages(), 403);
        }

        $order = Order::create([
            'user_name' => $request->user_name,
            'user_phone' => $request->user_phone,
            'user_email' => $request->user_email,
            'user_address' => $request->user_address,
            'order_from' => "INVISIBLE" , 
            'sub_total_price' => $request->sub_total_price , 
            'vat_price' => $request->vat_price , 
            'total_price' => $request->total_price , 
            'total_items' => $request->total_items , 
            'delivery_date' => $request->delivery_date , 
            'order_type' => $request->order_type ,  
            'invisible_id' => $request->invisible_id ,  
            'order_status' => "0"   
        ]);

        return response()->json(array('success' => true , 'order' =>$order), 200);
        
    }

    public function orderItems(Request $request){
        OrderItem::create([
            'order_id' => $request->order_id,
            'product_id' => $request->product_id,
            'product_name' => $request->product_name,
            'product_price' => $request->product_price,
            'product_quantity' => $request->product_quantity,
            'vat' => $request->vat,
            'product_category_id' => $request->product_category_id,
        ]);

        return response()->json('success' , 200);
    }

    public function bookOrder(Request $request){

        if($request->other_check == true){
            $date = $request->order_date;
        }else if($request->tomorrow == true){
            $date = date('m/d/Y', strtotime('+1 day'));
        }else{
            $date = date('m/d/Y');
        }
        $booking = Booking::create([
            'date' => $date,
            'time' => $request->time,
            'order_id' => $request->order_id,
        ]);

        return response()->json(array('success' => true , 'booking' =>$booking), 200);        
    }


    public function sendOrderEmails(Request $request){

        if($request->other_check == true){
            $date = $request->order_date;
        }else if($request->tomorrow == true){
            $date = date('m/d/Y', strtotime('+1 day'));
        }else{
            $date = date('m/d/Y');
        }

        $splitDate = explode('/', $date);

        $month = $splitDate[0];
        $day = $splitDate[1];
        $year = $splitDate[2];

        $date = $day . "-".date('F', mktime(0, 0, 0, $month, 10))."-".$year; // March

        $last_name = !empty($splitName[1]) ? $splitName[1] : ''; // If last name doesn't exist, make it empty

        $order = Order::where(['id' => $request->order_id])->first();
        $orderItems = OrderItem::where(['order_id' => $request->order_id])->get();

        $details = [
            'greeting' => 'Hi '.$request->user_name,
            'subject' => 'Papa Fluffy | Order Received | Order Id #'.$order->id,
            'body' => '',
            'thanks' => 'Thank you for choosing Papafluffy !',
            'order_id' => $request->order_id
        ];

        $details['body'] = $details['body'] . "<b> Order Id : " . $order->id . "</b><br><br>"; 
        foreach($orderItems as $orderItem){
            $details['body'] = $details['body'] . $orderItem->product_name . " x " . $orderItem->product_quantity . "<br>"; 
        }
        $details['body'] = $details['body'] . "...........................................................<br>";  
        $details['body'] = $details['body'] . "Sub Total : AED " . $order->sub_total_price . "<br>";  
        $details['body'] = $details['body'] . "Vat : AED " . $order->vat_price . "<br>";  
        $details['body'] = $details['body'] . "Delivery : AED " . $request->delivery . "<br>";  
        $details['body'] = $details['body'] . "Total : AED " . $order->total_price . "<br>";  
        $details['body'] = $details['body'] . "............................................................<br>";  
        $details['body'] = $details['body'] . "Deliver At : " .$date ." | <b>". $request->time . "</b><br>";  
        $emailBody = $details['body'];
        $details['body'] = $details['body'] . "For assistance, feel free to call us at this number +971 56 682 5669 <br>";  

        $order->email = $order->user_email;
        Notification::send($order , new OrderNotification($details));

        $details = [
            'subject' => 'New order recieved',
            'body' => '',
        ];
        $details['body'] = $emailBody;        
        $details['body'] = $details['body'] . "............................................................<br>";  
        $details['body'] = $details['body'] . "Order Time : " . $order->created_at . "<br>";        
        $details['body'] = $details['body'] . "Customer Name : " . $order->user_name . "<br>";        
        $details['body'] = $details['body'] . "Customer Phone : " . $order->user_phone . "<br>";        
        $details['body'] = $details['body'] . "Customer Address : " . $order->user_address . "<br>";        
        if($order->order_type == "cash"){
            $details['body'] = $details['body'] . "Payment Type : Cash on Delivery <br>";        
        }else{
            $details['body'] = $details['body'] . "Payment Type : Pay through Card <br>";        
        }

        $user = new User();
        $user->email="balajiqsr@gmail.com";
        Notification::send($user , new AdminNotification($details));

        return response()->json('success', 200);        
    }

    public function success(){
        return view('invisible.orders.success');
    }
}
