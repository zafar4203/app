<?php

namespace App\Http\Controllers\Api;

use Validator;
use Auth;
use Illuminate\Support\Str;
use App\Order;
use App\OrderCancel;
use App\Sale;
use App\Booking;
use App\VatOption;
use App\Branch;
use App\Promo;
use App\OrderItem;
use App\Inventory;
use Notification;
use App\User;
use App\Notifications\Noti;
use App\Notifications\OrderNotification;
use App\Notifications\AdminNotification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{

    public $details = [];
    public function __construct(){
        $this->details = [
            'read_at' => null,
            'data' => [
                'link' => '',
                'text' => ''
            ],
        ];
    }

    public function placeOrder(Request $request){
        $validator = Validator::make($request->all(), [ 
            'user_name' => 'required|regex:/^[\pL\s\-]+$/u', 
            'user_phone' => 'required', 
            'user_email' => 'required|email',
            'user_address' => 'required',
            'order_from' => 'required',
            'sub_total_price' => 'required',
            'vat_price' => 'required',
            'total_price' => 'required',
            'total_items' => 'required',
            'delivery_date' => 'required',            
            'area' => 'required',            
        ]);

        if ($validator->fails()) { 
            return response()->json($validator->messages(), 403);
        }

        $order = Order::create([
            'user_name' => $request->user_name,
            'user_phone' => $request->user_phone,
            'user_email' => $request->user_email,
            'user_address' => $request->user_address,
            'order_from' => "WEB" , 
            'sub_total_price' => $request->sub_total_price , 
            'vat_price' => $request->vat_price , 
            'total_price' => $request->total_price , 
            'total_items' => $request->total_items , 
            'invisible_id' => $request->invisible_id , 
            'delivery_date' => $request->delivery_date , 
            'discount' => $request->discount , 
            'order_type' => $request->order_type ,  
            'order_status' => "0",
            'bd_cakes' => $request->bd_cakes,
            'area' => $request->area      
        ]);

        if($request->promo_id != null){
            $promo = Promo::where(['id' => $request->promo_id])->first();
            $promo->discount = (float)$promo->discount + (float)$request->discount;
            $promo->save();
        }

        return response()->json(array('success' => true , 'order' =>$order), 200);
    }

    public function placeOrderHomeDelivery(Request $request){
        $validator = Validator::make($request->all(), [ 
            'user_name' => 'required|regex:/^[\pL\s\-]+$/u|max:15', 
            'user_phone' => 'required|digits_between:11,14', 
            'user_address' => 'required|max:80',
            'order_from' => 'required',
            'sub_total_price' => 'required',
            'vat_price' => 'required',
            'total_price' => 'required',
            'total_items' => 'required',
            'delivery_date' => 'required',            
        ]);

        if ($validator->fails()) { 
            return response()->json($validator->messages(), 403);
        }

        $order = Order::create([
            'user_name' => $request->user_name,
            'user_phone' => $request->user_phone,
            'user_email' => $request->user_email,
            'user_address' => $request->user_address,
            'order_from' => "PHONE" , 
            'sub_total_price' => $request->sub_total_price , 
            'vat_price' => $request->vat_price , 
            'total_price' => $request->total_price , 
            'total_items' => $request->total_items , 
            'delivery_date' => $request->delivery_date , 
            'order_type' => $request->order_type ,  
            'order_status' => "0"   
        ]);

        $this->notifyUsers('New Order From Website');

        return response()->json(array('success' => true , 'order' =>$order), 200);
        
    }

    public function placeOrderAndroid(Request $request){
        $validator = Validator::make($request->all(), [ 
            'order_from' => 'required',
            'sub_total_price' => 'required',
            'vat_price' => 'required',
            'total_price' => 'required',
            'total_items' => 'required',
            'user_id' => 'required',
        ]);

        if ($validator->fails()) { 
            return response()->json($validator->messages(), 403);
        }

        $order = Order::create([
            'order_from' => $request->order_from , 
            'sub_total_price' => $request->sub_total_price , 
            'vat_price' => $request->vat_price , 
            'total_price' => $request->total_price , 
            'total_items' => $request->total_items , 
            'order_type' => $request->order_type , 
            'user_id' => $request->user_id , 
            'discount' => $request->discount , 
            'branch_id' => $request->branch_id , 
            'bd_cakes' => json_decode($request->bd_cakes) , 
        ]);

        if($request->promo_id != null){
            $promo = Promo::where(['id' => $request->promo_id])->first();
            if($promo){
                $promo->discount = (float)$promo->discount + (float)$request->discount;
                $promo->save();
            }
        }

        $this->recordSale($request->sub_total_price , $request->vat_price , $request->total_price , $request->order_type , $request->branch_id , "POS" , $request->discount);

        $order_items = json_decode($request->order_items , true);
        foreach($order_items as $item){
            OrderItem::create([
                'order_id' => $order->id,
                'product_id' => $item['product_id'],
                'product_name' => $item['product_name'],
                'product_price' => $item['product_price'],
                'product_quantity' => $item['product_quantity'],
                'vat' => $item['vat'],
                'product_category_id' => $item['product_category_id'],
            ]);
    
            $inventory = Inventory::where(['product_id' => $request->product_id , 'branch_id' => $request->branch_id])->first();
            if($inventory){
                $inventory->current_qty = (int) $inventory->current_qty - (int) $request->product_quantity; 
                $inventory->save();
            }
        }

        return response()->json($order, 200);
    }


    public function notifyUsers($message){
        $this->details['data']['link'] = route('orders');
        $this->details['data']['text'] = $message;
        $users = User::all();
        foreach($users as $user){
            if($user->can('View Home Delivery')  || $user->can('all')){
                $user->notify(new Noti($this->details));
            }
        }
    }

    public function orderItems(Request $request){
        OrderItem::create([
            'order_id' => $request->order_id,
            'product_id' => $request->product_id,
            'product_name' => $request->product_name,
            'product_price' => $request->product_price,
            'product_quantity' => $request->product_quantity,
            'vat' => $request->vat,
            'product_category_id' => $request->product_category_id,
        ]);

        $inventory = Inventory::where(['product_id' => $request->product_id , 'branch_id' => $request->branch_id])->first();
        if($inventory){
            $inventory->current_qty = (int) $inventory->current_qty - (int) $request->product_quantity; 
            $inventory->save();
        }

        return response()->json('success' , 200);
    }

    public function bookOrder(Request $request){

        if($request->other_check == true){
            $date = $request->order_date;
        }else if($request->tomorrow == true){
            $date = date('m/d/Y', strtotime('+1 day'));
        }else{
            $date = date('m/d/Y');
        }
        $booking = Booking::create([
            'date' => $date,
            'time' => $request->time,
            'order_id' => $request->order_id,
        ]);

        return response()->json(array('success' => true , 'booking' =>$booking), 200);
    } 

    public function sendOrderEmails(Request $request){

        $vat = VatOption::find(1);
        if($request->other_check == true){
            $date = $request->order_date;
        }else if($request->tomorrow == true){
            $date = date('m/d/Y', strtotime('+1 day'));
        }else{
            $date = date('m/d/Y');
        }

        $splitDate = explode('/', $date);

        $month = $splitDate[0];
        $day = $splitDate[1];
        $year = $splitDate[2];

        $date = $day . "-".date('F', mktime(0, 0, 0, $month, 10))."-".$year; // March

        $last_name = !empty($splitName[1]) ? $splitName[1] : ''; // If last name doesn't exist, make it empty

        $order = Order::where(['id' => $request->order_id])->first();
        $orderItems = OrderItem::where(['order_id' => $request->order_id])->get();

        $details = [
            'greeting' => 'Hi '.$request->user_name,
            'subject' => 'Papa Fluffy | Order Received | Order Id #'.$order->id,
            'body' => '',
            'thanks' => 'Thank you for choosing Papafluffy !',
            'order_id' => $request->order_id
        ];

        $details['body'] = $details['body'] . "<b> Order Id : " . $order->id . "</b><br><br>";
        foreach($orderItems as $orderItem){
            $details['body'] = $details['body'] . $orderItem->product_name . " x " . $orderItem->product_quantity . "<br>"; 
        }
        $details['body'] = $details['body'] . "...........................................................<br>";  
        if($vat && $vat->vat_option == 1){
            $details['body'] = $details['body'] . "Sub Total : AED " . $order->sub_total_price . "<br>";  
            $details['body'] = $details['body'] . "Vat : AED " . $order->vat_price . "<br>";      
        }else{
            $details['body'] = $details['body'] . "Sub Total : AED " . $order->sub_total_price . "<br>";      
        }
        $details['body'] = $details['body'] . "Delivery : AED " . $request->delivery . "<br>";  
        $details['body'] = $details['body'] . "Total : AED " . $order->total_price . "<br>";  
        $details['body'] = $details['body'] . "............................................................<br>";  
        $details['body'] = $details['body'] . "Deliver At : " .$date ." | <b>". $request->time . "</b><br>";  
        $emailBody = $details['body'];
        $details['body'] = $details['body'] . "For assistance, feel free to call us at this number +971 56 682 5669 <br><br>";  
        $details['body'] = $details['body'] . "Your order has been recieved. We shall call you back shortly.<br>";  

        $order->email = $order->user_email;
        Notification::send($order , new OrderNotification($details));

        $details = [
            'subject' => 'New order recieved',
            'body' => '',
        ];
        $details['body'] = $emailBody;
        $details['body'] = $details['body'] . "............................................................<br>";  
        $details['body'] = $details['body'] . "Order Time : " . $order->created_at . "<br>";      
        $details['body'] = $details['body'] . "Customer Name : " . $order->user_name . "<br>";      
        $details['body'] = $details['body'] . "Customer Phone : " . $order->user_phone . "<br>";    
        $details['body'] = $details['body'] . "Customer Address : " . $order->user_address . "<br>";
        if($order->order_type == "cash"){
            $details['body'] = $details['body'] . "Payment Type : Cash on Delivery <br>";
        }else{
            $details['body'] = $details['body'] . "Payment Type : Bring Card Machine on Delivery <br>";        
        }

        $user = new User();
        $user->email="balajiqsr@gmail.com";
        Notification::send($user , new AdminNotification($details));

        return response()->json('success', 200);        
    }


    public function sendOrderEmailAdmin(Request $request){

        if($request->other_check == true){
            $date = $request->order_date;
        }else if($request->tomorrow == true){
            $date = date('m/d/Y', strtotime('+1 day'));
        }else{
            $date = date('m/d/Y');
        }

        $splitDate = explode('/', $date);

        $month = $splitDate[0];
        $day = $splitDate[1];
        $year = $splitDate[2];

        $date = $day . "-".date('F', mktime(0, 0, 0, $month, 10))."-".$year; // March

        
        $order = Order::where(['id' => $request->order_id])->first();
        $orderItems = OrderItem::where(['order_id' => $request->order_id])->get();

        $details = [
            'subject' => 'New order recieved',
            'body' => '',
        ];

        $details['body'] = $details['body'] . "<b> Order Id : " . $order->id . "</b><br><br>"; 
        foreach($orderItems as $orderItem){
            $details['body'] = $details['body'] . $orderItem->product_name . " x " . $orderItem->product_quantity . "<br>"; 
        }
        $details['body'] = $details['body'] . "...........................................................<br>";  
        $details['body'] = $details['body'] . "Sub Total : AED " . $order->sub_total_price . "<br>";  
        $details['body'] = $details['body'] . "Vat : AED " . $order->vat_price . "<br>";  
        $details['body'] = $details['body'] . "Delivery : AED " . $request->delivery . "<br>";  
        $details['body'] = $details['body'] . "Total : AED " . $order->total_price . "<br>";  
        $details['body'] = $details['body'] . "............................................................<br>";  
        $details['body'] = $details['body'] . "Deliver At : " .$date ." | <b>". $request->time . "</b><br>";      
        $details['body'] = $details['body'] . "............................................................<br>";  
        $details['body'] = $details['body'] . "Order Time : " . $order->created_at . "<br>";        
        $details['body'] = $details['body'] . "Customer Name : " . $order->user_name . "<br>";        
        $details['body'] = $details['body'] . "Customer Phone : " . $order->user_phone . "<br>";        
        $details['body'] = $details['body'] . "Customer Address : " . $order->user_address . "<br>";        
        if($order->order_type == "cash"){
            $details['body'] = $details['body'] . "Payment Type : Cash on Delivery <br>";        
        }else{
            $details['body'] = $details['body'] . "Payment Type : Pay through Card <br>";        
        }

        $user = new User();
        $user->email="balajiqsr@gmail.com";
        Notification::send($user , new AdminNotification($details));

        return response()->json('success', 200);        
    }

    public function getOrders(Request $request){
        $orders = Order::where('id', 'like', '%' . $request->id . '%')
        ->where(['order_from' => "WEB"])
        ->get();

        return response()->json($orders , 200);                
    }

    public function changeOrderStatus($id , $status){
        $order = Order::where(['id' => $id])->first();
        $order->order_status = $status;
        $order->save();

        if($order->order_status == "2"){
            if($order->invisible_id != null){
                $this->updateInventory($order , 'invisible');
            }else{
                $this->updateInventory($order , 'web');
            }
        }
        if($order->order_status != "3"){
            OrderCancel::where(['order_id' => $order->id])->delete();
        }
        return $order;
    }

    public function getOrderItems($id){
        $orderItems = OrderItem::where(['order_id' => $id])->get();
        foreach($orderItems as $orderItem){
            $short_name = $orderItem->product['short_name'];
            $orderItem->short_name = $short_name;
            $orderItem->image = $orderItem->product['image'];
        }
        return response()->json($orderItems , 200);
    }

    public function recordSale($sub , $vat , $total , $type , $branch , $source , $discount){
        $date = date('m/d/Y');
        $sale = Sale::where(['date' => $date , 'branch_id' => $branch , 'source' => $source])->first();

        if(!is_null($sale)){
            if($type == "cash"){
                $sale->cash = $sale->cash + (float)$total;
            }else{
                $sale->card = $sale->card + (float)$total;
            }
            $sale->total = $sale->cash + $sale->card;
            $sale->vat = $sale->vat + (float) $vat; 
            $sale->date = $date;
            $sale->discount = $sale->discount + $discount;
            $sale->source = $source;
            $sale->save();
        }else{
            if($type == "cash"){
                Sale::create([
                    'cash' => (float)$total,
                    'card' => 0,
                    'total' => (float)$total,
                    'vat' => (float)$vat ,
                    'date' => $date,
                    'discount' => $discount,
                    'source' => $source,
                    'branch_id' => $branch
                ]);
            }else{
                Sale::create([
                    'cash' => 0,
                    'card' => (float)$total,
                    'total' => (float) $total,
                    'vat' => (float)$vat,
                    'date' => $date,
                    'discount' => $discount,
                    'source' => $source,
                    'branch_id' => $branch
                ]);
            }

        }
    }

    public function orderCancel(request $request){
        if($request->cancel_comments == null){
            $request->cancel_comments = "Order canceled By User";
        }

        $orderCancel = OrderCancel::updateOrCreate(
            ['order_id' => $request->order_id],
            ['user_id' => $request->user_id , 'cancel_comments' => $request->cancel_comments]
        );

        return response()->json(array('success' => true), 200);

    }

    public function getOrderDrivers(){
        $orders = Order::where('order_status' ,'!=', "2")
        ->where('order_from' , '!=' , "POS")
        ->get();

        foreach($orders as $order){
            $booking = Booking::where(['order_id' => $order->id])->first();
            if($booking){
                $order->booking = $booking->date ." (".$booking->time.")";
            }
        }

        return response()->json($orders , 200);        
    }

        // Update Order Items In Inventory on status changed to Delivered
        public function updateInventory($order , $source){
            $branch = Branch::where(['name' => 'Kitchen'])->first();
            $this->recordSale($order->sub_total_price , $order->vat_price , $order->total_price , $order->order_type , $branch->id , $source , $order->discount);
    
            $orderItems = OrderItem::where(['order_id' => $order->id])->get();
            if($orderItems){
                foreach($orderItems as $orderItem){
                    $inventory = Inventory::where(['product_id' => $orderItem->product_id , 'branch_id' => $branch->id])->first();
                    $inventory->current_qty = $inventory->current_qty - $orderItem->product_quantity;
                    $inventory->save(); 
                }
            }
        }
}
