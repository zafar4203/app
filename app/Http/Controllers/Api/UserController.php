<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Branch;
use App\Promo;
use App\Role;
use App\VatOption;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function login(Request $request){
        $user = User::where(['pin_code' => $request->pin_code])->first();
        if($user->branch_id != null){
            $user->branch_name = Branch::branchName($user->branch_id);
        }else{
            $user->branch_name = "null";
        }

        if($user != null){
            return response()->json($user, 200);
        }else{
            return response()->json('error' , 200);
        }
    }

    public function login_driver(Request $request){
        $role = Role::where(['name' => 'Driver'])->first();
        $user = User::where(['pin_code' => $request->pin_code])
        ->where(['role_id' => $role->id])
        ->first();

        if($user != null){
            return response()->json($user, 200);
        }else{
            return response()->json("error", 403);
        }
    }

    public function checkPromo(Request $request){
        $promo = Promo::where(['name' => $request->promo])
        ->where(['status' => 0])
        ->first();

        if(!$promo || Carbon::parse($promo->expiry_date)->isPast()){
            return response()->json('error' , 200);   
        }else{
            return response()->json($promo, 200);
        }
    }

    public function getUsers(){
        $users = User::all();
        foreach($users as $user){
            $vat = VatOption::where(['id' => 1])->first();
            $user->vat = $vat->vat_option;
        }
        return response()->json($users , 200);        
    }
}