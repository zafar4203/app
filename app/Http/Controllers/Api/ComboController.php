<?php

namespace App\Http\Controllers\Api;

use App\Combo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ComboController extends Controller
{
    public function getCombos(){
        $combos = Combo::all();
        return response()->json( $combos , 200);
    }
}
