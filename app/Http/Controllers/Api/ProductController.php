<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use App\VatOption;
use App\Product;
use App\Area;
use App\Branch;
use App\Subcategory;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function getProducts(){
        $category = Category::where(['name' => 'Sample'])->first();

        if($category){
            $products = Product::where(['block' => 0])   
            ->where('category_id' , '!=' , $category->id)
            ->whereIn('is_for', ['web', 'both'])
            ->orderBy('sequence')
            ->get();
        }else{
            $products = Product::where(['block' => 0])   
            ->whereIn('is_for', ['web', 'both'])
            ->orderBy('sequence')
            ->get();
        }

        $subcategories = Subcategory::where(['block' => 0])
        ->with(['products' => function($query){
            $query->where('block',0)->whereIn('is_for',['web','both']);
        }])
        ->orderBy('sequence')->get();
        
        $categories = Category::where(['block' => 0])->
        with(['products' => function($query){
            $query->where('block',0)->whereIn('is_for',['web','both']);
        }])
        ->orderBy('sequence')->get();
        $areas = Area::where(['block' => 0])->get();
        $branches = Branch::where('name' , '!=' , 'Kitchen')->get();

        foreach($categories as $category){
            $prods = [];    
            foreach($category->products as $product){
                if(!$product->subcategory_id){
                    $product->subcategory_id = 0;
                    array_push($prods , $product);
                }
            }
    
            $subcategory = new Subcategory();
            $subcategory->id = 0;
            $subcategory->name = "Other";
            $subcategory->description = "Other products";
            $subcategory->block = 0;
            $subcategory->image = '';
            $subcategory->category_id = $category->id;
            $subcategory->sequence = '';
            $subcategory->created_at = Carbon::now()->setTime(0,0)->format('Y-m-d H:i:s');
            $subcategory->updated_at = Carbon::now()->setTime(0,0)->format('Y-m-d H:i:s');
            $subcategory->products = $prods;
    
            $subcategories->push($subcategory);    

        }
        return response()->json(['status' => 200, 'products' => $products , 'subcategories' => $subcategories, 'categories' => $categories, 'areas' => $areas, 'branches' => $branches]);
    }

    public function getRegularProducts(){

        $products = Product::where(['type' => 'Regular'])
        ->where(['block' => 0])
        ->get();
        return response()->json($products, 200);
    }

    public function getProductsPOS(){

        $products = Product::where(['for' => NULL])
        ->whereIn('is_for', ['pos', 'both'])
        ->where(['block' => 0])
        ->get();

        return response()->json($products, 200);
    }

    public function vat(){
        $vat = VatOption::where(['id' => 1])->first();
        return response()->json($vat->vat_option, 200);
    }
}
