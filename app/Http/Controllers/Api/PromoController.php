<?php

namespace App\Http\Controllers\Api;

use App\Promo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PromoController extends Controller
{
    public function getPromos(){

        $promos = Promo::where(['status' => 0])
        ->where(['from' => "Admin"])
        ->get();
        return response()->json($promos, 200);        

    }
}
