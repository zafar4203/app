<?php

namespace App\Http\Controllers\Api;

use App\Sale;
use App\Product;
use App\Order;
use App\OrderItem;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SaleController extends Controller
{
    public function getSaleByDate(Request $request){
        $sale = Sale::where(['date' => $request->date])
        ->where(['branch_id' => $request->branch_id])->first();

        $orders = Order::whereDate('created_at', Carbon::today())
        ->where(['branch_id' => $request->branch_id])
        ->get();

        if($sale){
            $sale->total_orders = count($orders);
        }
        return response()->json($sale, 200);
    }

    public function getSalesItemsCount(Request $request){
        $products = Product::all();
        foreach($products as $product){
            $product->count = 0;
        }
        
        $orders = Order::whereDate('created_at', Carbon::today())
        ->where(['branch_id' => $request->branch_id])
        ->get();
       
        foreach($orders as $order){
            $orderItems = OrderItem::where(['order_id' => $order->id])->get();
            foreach($orderItems as $orderItem){
                  foreach($products as $product){
                      if($orderItem->product_id == $product->id){
                          $product->count = $product->count + $orderItem->product_quantity; 
                      }
                  }  
            }
        }

        return response()->json($products, 200);
    }

    public function updateSaleStatus(Request $request){
        $sale = Sale::where(['date' => $request->date])
        ->where(['branch_id' => $request->branch_id])->first();

        $sale->status = 1;
        $sale->save();

        return response()->json($sale, 200);
    }
}
