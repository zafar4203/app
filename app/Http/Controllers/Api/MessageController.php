<?php

namespace App\Http\Controllers\Api;

use DB;
use Notification;
use Validator;
use \Carbon\Carbon;
use App\Message;
use App\User;
use App\Notifications\Noti;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Notifications\AdminNotification;

class MessageController extends Controller
{

    public $details = [];
    public function __construct(){
        $this->details = [
            'read_at' => null,
            'data' => [
                'link' => '',
                'text' => ''
            ],
        ];
    }

    public function addMessage(Request $request){
        $validator = Validator::make($request->all(), [ 
            'user_name' => 'required', 
            'user_email' => 'required', 
            'user_phone' => 'required', 
            'user_message' => 'required', 
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 403);
        }

        $message = Message::create([
            'name' => $request->user_name,
            'email' => $request->user_email,
            'phone' => $request->user_phone,
            'message' => $request->user_message,
        ]);

        $message->save();


        $data = [
            'email_body' => $request->user_message
        ];

        $objDemo = new \stdClass();
        $objDemo->to = 'info@papafluffy.com';
        $objDemo->from = $request->user_email;
        $objDemo->title = "Papafluffy";
        $objDemo->subject = "New Message from Customer";

        try{
            Mail::send('email.mailbody',$data, function ($message) use ($objDemo) {
                $message->from($objDemo->from,$objDemo->title);
                $message->to($objDemo->to);
                $message->subject($objDemo->subject);
            });
        }
        catch (\Exception $e){
            //die($e->getMessage());
            return $e->getMessage();
        }

        $this->notifyUsers('New Message From Customer');

        return response()->json(['status' => 200, 'msg' => "Success"]);
    }

    public function monthly(){

        $orders = DB::table('orders')
        ->select('id','user_email','user_phone','user_name' , 'created_at' , DB::raw('COUNT(user_email) as count'))
        ->where('order_from' ,'!=','POS')
        ->groupBy('id','user_email','user_phone','user_name','created_at')
        ->get();

        foreach($orders as $key => $order){

            $formatted_dt1=Carbon::parse($order->created_at);
            $formatted_dt2=Carbon::parse(Carbon::now());
            $date_diff=$formatted_dt1->diffInDays($formatted_dt2);

            $check = 0;
            if($date_diff > 30){
                foreach($orders as $od){
                    if($od->user_email == $order->user_email && $od->id != $order->id){

                        $f_dt1=Carbon::parse($od->created_at);
                        $f_dt2=Carbon::parse(Carbon::now());
                        $d_diff=$f_dt1->diffInDays($f_dt2);

                        if($d_diff > 30){
                        }else{
                            $check = 1;
                        }

                    }
                }
                if($check == 0){

                    $details = [
                        'greeting' => 'Hi '.$order->user_name,
                        'subject' => 'Papafluffy',
                        'body' => 'We miss you',
                        'thanks' => 'Thank you for choosing Papafluffy !'
                    ];
                    $user = new User();
                    $user->email= $order->user_email;
                    Notification::send($user , new AdminNotification($details));    

                }
            }
        }

        return $orders;
    }

    public function notifyUsers($message){
        $this->details['data']['link'] = route('messages.index');
        $this->details['data']['text'] = $message;
        $users = User::all();
        foreach($users as $user){
            if($user->can('View Messages')  || $user->can('all')){
                $user->notify(new Noti($this->details));
            }
        }
    }
}