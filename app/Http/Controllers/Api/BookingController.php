<?php

namespace App\Http\Controllers\Api;

use App\Booking;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BookingController extends Controller
{
    public function todayBookings(){
        $date = date('m/d/Y');
        $bookings = Booking::where(['date' => $date])->get();

        return response()->json($bookings, 200);
    }

    public function tomorrowBookings(){
        $date = date('m/d/Y', strtotime('+1 day'));
        $bookings = Booking::where(['date' => $date])->get();

        return response()->json($bookings, 200);
    }

    public function someDayBookings(Request $request){
        $bookings = Booking::where(['date' => $request->date])->get();

        return response()->json($bookings, 200);
    }

    public function bookingUpdate(Request $request){
        $booking = Booking::where(['order_id' => $request->order_id])->first();
        $booking->date = $request->date;
        $booking->time = $request->time;
        $booking->save();

        return response()->json($booking, 200);
    }
}
