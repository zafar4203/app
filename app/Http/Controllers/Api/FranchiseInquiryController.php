<?php

namespace App\Http\Controllers\Api;

use App\FranchiseInquiry;
use Illuminate\Http\Request;
use App\User;
use App\Notifications\Noti;
use App\Http\Controllers\Controller;

class FranchiseInquiryController extends Controller
{
    
    public $details = [];
    public function __construct(){
        $this->details = [
            'read_at' => null,
            'data' => [
                'link' => '',
                'text' => ''
            ],
        ];
    }

    public function franchise_inquiry(Request $request){
        $data = $request->all();
        $franchise = new FranchiseInquiry();
        $franchise->fill($data)->save();

        $this->notifyUsers('New Franchise Inquiry From Client');

        return response()->json(['status' => 200, 'msg' => "Success"]);
    }

    public function notifyUsers($message){
        $this->details['data']['link'] = route('franchise_inquiries.index');
        $this->details['data']['text'] = $message;
        $users = User::all();
        foreach($users as $user){
            if($user->can('View Franchise Inquiries')  || $user->can('all')){
                $user->notify(new Noti($this->details));
            }
        }
    }
}
