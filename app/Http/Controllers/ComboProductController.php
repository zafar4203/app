<?php

namespace App\Http\Controllers;

use Validator;
use Auth;
use File;
use App\Combo;
use App\Image;
use App\Product;
use App\Category;
use DataTables;
use Illuminate\Http\Request;

class ComboProductController extends Controller
{

    public function getComboProducts(){
        $products = Product::select('id', 'name', 'short_name', 'block', 'image', 'category_id', 'price', 'for' , 'created_at')
        ->where(['type' => 'Combo'])
        ->get();

        foreach($products as $product){
            $product->block = Product::block($product->block);
            $product->category =  Category::where(['id' => $product->category_id])->first()->name;
        }

        return DataTables::of($products)
        ->addColumn('action', function ($id) {
            return '<a href="'. url("comboEdit").'/'.$id->id.'" class="text-primary"><i class="fa fa-edit"></i></a>
            <a class="text-primary" onClick="delete_click('.$id->id.')" ><i class="fa fa-trash"></i></button>'; 
        })
        ->make(true);
    }

    public function addCombo(){
        $category = Category::where(['name' => "Combo"])->first();
        if($category){
            return view("combos.addCombo");
        }else{
            return redirect('combos')->with('error', 'Add Combo Category First');
        }

    }

    public function combos(){
        return view("combos.combos");
    }

    public function addComboProduct(Request $request){
        $validator = Validator::make($request->all(), [ 
            'name' => 'required|max:50', 
            'short_name' => 'required|regex:/^[\pL\s\.]+$/u|min:11|max:15', 
            'price' => 'required|digits_between:1,6',
            'image' => 'required',
            'for' => 'required'
        ]);

        if ($validator->fails()) { 
            return response()->json($validator->messages(), 422);
        }

        if(empty($request->flavour))
        {
            $request['flavours'] = null;
            $request['flavour_price'] = null;
        }
        else{
                if(in_array(null, $request->flavour) || in_array(null, $request->flavour_price))
                {
                    $request['flavours'] = null;
                    $request['flavour_price'] = null;        
                }
                else
                {
                    $request['flavours']  = implode(',', $request->flavour);
                    $request['flavour_price']  = implode(',', $request->flavour_price);
                }
        }

        $category = Category::where(['name' => "Combo"])->first();
        $product = Product::create([
            'name' => $request->name , 
            'short_name' => $request->short_name , 
            'price' => $request->price , 
            'block' => $request->block , 
            'image' => '' , 
            'for' => $request->for,
            'category_id' => $category->id,
            'type' => "Combo",
            'input_items' => $request->input_items_count,
            'output_items' => $request->output_items_count,
            'flavour' => $request['flavours'],
            'flavour_price' => $request['flavour_price']
             ]);
        $product->save();

        $imageUrl = "";

        if($request->hasfile('image')){
            $files = $request->file('image');
            for($i=0 ; $i<count($files) ; $i++){
                $img = "";
                $file = $files[$i];
                $filename = time().$i.'.'.$file->getClientOriginalExtension();
                $destinationPath = public_path('/images');
                $file->move($destinationPath, $filename);    
                $img = asset("public/images/".$filename);
                $image = new Image();
                $image->name = $filename;
                $image->imageUrl = $img;
                $image->product_id = $product->id;
                $image->save();
    
                if($i == 0){
                    $imageUrl = asset("public/images/".$filename);
                }
            }
        }

        $productUpdate = Product::where(['id' => $product->id])->first();
        $productUpdate->image = $imageUrl;
        $productUpdate->save();

        if($request->input_items){  
            $input_items = json_decode($request->input_items , true);
            foreach($input_items as $input_product){
                Combo::create([
                    'product_id' => $productUpdate->id,
                    'selected_product_id' => $input_product['id'],
                    'name' => $input_product['short_name'],
                    'quantity' => $input_product['quantity'],
                    'type' => "Input"
                ]);
            }
        }

        if($request->output_items){
            $output_items = json_decode($request->output_items , true);
            foreach($output_items as $output_product){
                Combo::create([
                    'product_id' => $productUpdate->id,
                    'selected_product_id' => $output_product['id'],
                    'name' => $output_product['short_name'],
                    'quantity' => $output_product['quantity'],
                    'type' => "Output"
                ]);
            }
        }

        return response()->json(array('success' => true), 200);
    }

    public function comboEdit($id){
        $product = Product::where(['id' => $id])->first();
        return view('combos.editCombo' , compact('product'));
    }

    public function getCombos($id){
        $combos = Combo::where(['product_id' => $id])->get();
        return response()->json(array('success' => true , 'combos' =>$combos), 200);
    }

    public function editComboProduct(Request $request){
        $validator = Validator::make($request->all(), [ 
            'name' => 'required|max:50', 
            'short_name' => 'required|regex:/^[\pL\s\.]+$/u|min:11|max:15', 
            'price' => 'required|digits_between:1,6',
            'for' => 'required'
        ]);

        if ($validator->fails()) { 
            return response()->json($validator->messages(), 422);
        }

        $category = Category::where(['name' => "Combo"])->first();
        $product = Product::where(['id' => $request->id])->first();
        $product->name = $request->name;
        $product->short_name = $request->short_name;
        $product->price = $request->price;
        $product->block = $request->block;
        $product->for = $request->for;
        $product->input_items = $request->input_items_count;
        $product->output_items = $request->output_items_count;
        $product->type = "Combo";
        $product->category_id = $category->id;


        $imageUrl = "";

        if($request->hasfile('image')){
            // Removing previous Images
            $images = Image::where(['product_id' => $request->id])->get();
            foreach($images as $image){
                $destinationPath = public_path('/images')."/".$image->name;
                if(File::exists($destinationPath)) {
                    File::delete($destinationPath);            
                }
                $image->delete();
            }            
            
            $files = $request->file('image');
            for($i=0 ; $i<count($files) ; $i++){
                $img = "";
                $file = $files[$i];
                $filename = time().$i.'.'.$file->getClientOriginalExtension();
                $destinationPath = public_path('/images');
                $file->move($destinationPath, $filename);    
                $img = asset("public/images/".$filename);
                $image = new Image();
                $image->name = $filename;
                $image->imageUrl = $img;
                $image->product_id = $product->id;
                $image->save();
    
                if($i == 0){
                    $imageUrl = asset("public/images/".$filename);
                }
            }

            $product->image = $imageUrl;
        }

        $product->save();

        Combo::where(['product_id' => $product->id])->delete();
        if($request->input_items){  
            $input_items = json_decode($request->input_items , true);
            foreach($input_items as $input_product){
                Combo::create([
                    'product_id' => $product->id,
                    'selected_product_id' => $input_product['id'],
                    'name' => $input_product['short_name'],
                    'quantity' => $input_product['quantity'],
                    'type' => "Input"
                ]);
            }
        }

        if($request->output_items){
            $output_items = json_decode($request->output_items , true);
            foreach($output_items as $output_product){
                Combo::create([
                    'product_id' => $product->id,
                    'selected_product_id' => $output_product['id'],
                    'name' => $output_product['short_name'],
                    'quantity' => $output_product['quantity'],
                    'type' => "Output"
                ]);
            }
        }

        return response()->json(array('success' => true), 200);
    }

    public function deleteCombo($id){
        $product = Product::where(['id' => $id])->delete();
        Combo::where(['product_id' => $id])->delete();

        $images = Image::where(['product_id' => $id])->get();
        foreach($images as $image){
            $destinationPath = public_path('/images')."/".$image->name;
            if(File::exists($destinationPath)) {
                File::delete($destinationPath);            
            }
            $image->delete();
        }            
        Image::where(['product_id' => $id])->delete();

        return view('combos.combos');
    }
}
