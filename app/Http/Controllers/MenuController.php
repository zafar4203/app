<?php

namespace App\Http\Controllers;

use Auth;
use File;
use Response;
use Validator;
use DataTables;
use App\Menu;
use App\User;
use App\Notifications\Noti;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    public $details = [];
    public function __construct(){
        $this->middleware('auth');

        $this->details = [
            'read_at' => null,
            'data' => [
                'link' => '',
                'text' => ''
            ],
        ];
    }
    
    public function getMenus(){
        $menus = Menu::select('id', 'name', 'link', 'permission', 'block' , 'created_at')->get();

        foreach($menus as $menu){
            $menu->isBlock = Menu::block($menu->block);
        }

        return DataTables::of($menus)
        ->addColumn('action', function ($id) {

            $data = "";
            if(Auth::user()->can('Update Menu') || Auth::user()->can('all')){
                $data = '<a href="'. route("editMenu" , $id) .'" class="text-primary"><i class="fa fa-edit"></i></a>';
            }
            if(Auth::user()->can('Delete Menu') || Auth::user()->can('all')){            
                $data = $data . ' <a class="text-primary" onClick="delete_click('.$id->id.')" ><i class="fa fa-trash"></i></button>';
            }
            return $data;
        })
        ->make(true);
    }

    public function index(){
        return view("menus.menus");
    }

    public function Menu(){
        return view("menus.addMenu");
    }

    public function addMenu(Request $request){

        $validator = Validator::make($request->all(), [ 
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:255', 
            'link' => 'required',
            'permission' => 'required',
            'icon' => 'required',
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }


        $menu = Menu::create([
            'name' => $request->name , 
            'link' => $request->link, 
            'block' => $request->block, 
            'icon' => $request->icon, 
            'permission' => $request->permission, 
            ]);
        $menu->save();

        $this->notifyUsers('Menu Added');

        return redirect('menus')->with('message', 'Menu Added Successfully');;
    }

    public function editMenu($id)
    {
        $menu = Menu::where(['id' => $id])->first();
        return view('menus.editMenu' , compact('menu'));
    }

    public function updateMenu(Request $request){
        $validator = Validator::make($request->all(), [ 
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:255', 
            'link' => 'required',
            'permission' => 'required',
            'icon' => 'required',
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $menu = Menu::where(['id' => $request->id])->first();

        $menu->name = $request->name;
        $menu->link = $request->link;
        $menu->block = $request->block;
        $menu->icon = $request->icon;
        $menu->permission = $request->permission;

        $menu->save();

        $this->notifyUsers('Menu Updated');

        return redirect()->back()->with('message', 'Menu Updated Successfully');;
    }

    public function deleteMenu($id){

        Menu::where(['id' => $id])->delete();   
        $this->notifyUsers('Menu Deleted');
     
        return redirect()->back()->with('message', 'Menu Deleted Successfully');;
    }


    public function sortMenu(){
        $menus = Menu::where(['block' => 0])->orderBy('sequence')->get();        
        return view('menus.sequence',compact('menus'));
    }

    public function sortMenuUpdate(Request $request){
        $ids = $request->ids;
        foreach($ids as $key => $id){
            $menu = Menu::where(['id'=> $id])->first();
            $menu->sequence = $key;
            $menu->save();
        }

        return response()->json(['status' => 200, 'msg' => "Success"]);
    }

    public function notifyUsers($message){
        $this->details['data']['link'] = route('categories');
        $this->details['data']['text'] = $message;
        $users = User::all();
        foreach($users as $user){
            if($user->can('View Categories')  || $user->can('all')){
                $user->notify(new Noti($this->details));
            }
        }
    }
}
