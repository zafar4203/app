<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductActivity;
use App\CategoryActivity;
use App\InventoryActivity;
use App\UserActivity;
use App\User;
use App\Branch;
use App\Product;
use App\Category;
use Carbon\Carbon;

class ActivityController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function products(){
        $activities = ProductActivity::all();
        foreach($activities as $activity){
            $activity->date = explode(" ", $activity->created_at)[0]; 
            $activity->time = explode(" ", $activity->created_at)[1];

            if($activity->attribute == "Block"){
                $activity->activity = "<b>".Product::productName($activity->product_id)."</b> <u>".$activity->attribute ."</u> changed from <b>" .$this->block($activity->original_value)."</b> to <b>".$this->block($activity->modified_value)."</b>";
            }
            else if($activity->attribute == "Category"){
                $activity->activity = "<b>".Product::productName($activity->product_id)."</b> <u>".$activity->attribute . "</u> changed from <b>" .Category::categoryName($activity->original_value)."</b> to <b>".Category::categoryName($activity->modified_value)."</b>";
            }
            else{
                $activity->activity = "<b>".Product::productName($activity->product_id)."</b> <u>".$activity->attribute . "</u> changed from <b>" .$activity->original_value."</b> to <b>".$activity->modified_value."</b>";
            }
            $activity->user_name = User::userName($activity->user_id); 
        }
        return view('activities.products',compact('activities'));
    }

    public function categories(){
        $activities = CategoryActivity::all();
        foreach($activities as $activity){
            $activity->date = explode(" ", $activity->created_at)[0]; 
            $activity->time = explode(" ", $activity->created_at)[1];
            if($activity->attribute == "Block"){
                $activity->activity = "<b>".Category::categoryName($activity->category_id). "</b> <u>".$activity->attribute ."</u> status changed from <b>" .$this->block($activity->original_value)."</b> to <b>".$this->block($activity->modified_value)."</b>";
            }
            else{
                $activity->activity = "<b>".Category::categoryName($activity->category_id)."</b> <u>".$activity->attribute . "</u> changed from <b>" .$activity->original_value."</b> to <b>".$activity->modified_value."</b>";
            }
            $activity->user_name = User::userName($activity->user_id); 
        }
        return view('activities.categories',compact('activities'));
    }


    public function users(){
        $activities = UserActivity::all();
        foreach($activities as $activity){
            $activity->date = explode(" ", $activity->created_at)[0]; 
            $activity->time = explode(" ", $activity->created_at)[1];
            
            if($activity->attribute == "Role"){
                $activity->activity = "<b>".User::userName($activity->staff_id). "</b> <u>".$activity->attribute ."</u> changed from <b>" .User::role($activity->original_value)."</b> to <b>". User::role($activity->modified_value)."</b>";
            }else if($activity->attribute == "Pin Code"){
                $activity->activity = "<b>".User::userName($activity->staff_id). "</b> <u>".$activity->attribute ."</u> changed from <b>" .$activity->original_value."</b> to <b>". $activity->modified_value."</b>";
            }else if($activity->attribute == "Password"){
                $activity->activity = "<b>".User::userName($activity->staff_id). "</b> <u>".$activity->attribute ."</u> changed to <b>". $activity->modified_value."</b>";
            }else if($activity->attribute == "Branch"){
                $activity->activity = "<b>".User::userName($activity->staff_id). "</b> <u>".$activity->attribute ."</u> changed from <b>". Branch::branchName($activity->original_value) ."</b> to <b>". Branch::branchName($activity->modified_value) ."</b>";
            }else{
                $activity->activity = "<b>".User::userName($activity->staff_id)."</b> <u>".$activity->attribute . "</u> changed from <b>" .$activity->original_value."</b> to <b>".$activity->modified_value."</b>";
            }
            $activity->user_name = User::userName($activity->user_id); 
        }
        return view('activities.users',compact('activities'));
    }

    protected function block($val){
        if($val == 0){
            return "Approve";
        }else{
            return "Block";
        }
    }

    public function inventories(){
        $activities = InventoryActivity::all();
        foreach($activities as $activity){
            $activity->date = explode(" ", $activity->created_at)[0]; 
            $activity->time = explode(" ", $activity->created_at)[1];
            
            if($activity->attribute == "Product"){
                $activity->activity = "<b>".Branch::branchName($activity->branch_id). "'s </b> Product <b>".Product::productName($activity->product_id)."</b> <u>".$activity->attribute ."</u> changed from <b>". Product::productName($activity->original_value)."</b> to <b>". Product::productName($activity->modified_value)."</b>";
            }else if($activity->attribute == "Branch"){
                $activity->activity = "<b>".Branch::branchName($activity->branch_id). "'s </b> Product <b>".Product::productName($activity->product_id)."</b> <u>".$activity->attribute ."</u> changed from <b>". Branch::branchName($activity->original_value) ."</b> to <b>". Branch::branchName($activity->modified_value) ."</b>";
            }else{
                $activity->activity = "<b>".Branch::branchName($activity->branch_id)."'s  </b> Product <b>".Product::productName($activity->product_id)."</b> <u>".$activity->attribute . "</u> changed from <b>" .$activity->original_value."</b> to <b>".$activity->modified_value."</b>";
            }
            $activity->user_name = User::userName($activity->user_id); 
        }
        return view('activities.inventories',compact('activities'));
    }
}