<?php

namespace App\Http\Controllers;

use DataTables;
use Validator;
use App\Branch;
use Auth;
use App\Product;
use App\Inventory;
use App\User;
use App\Notifications\Noti;
use Illuminate\Http\Request;

class BranchController extends Controller
{

    public $details = [];
    public function __construct(){
        $this->middleware('auth');

        $this->details = [
            'read_at' => null,
            'data' => [
                'link' => '',
                'text' => ''
            ],
        ];
    }

    public function getBranches(){
        $branches = Branch::select('id', 'name','city','country', 'branch_type', 'annual_rent' , 'shop_type' , 'created_at')->get();

        return DataTables::of($branches)
        ->addColumn('action', function ($id) {

            $data = "";
            if(Auth::user()->can('Update Branch') || Auth::user()->can('all')){
                $data = '<a href="'. route("editBranch" , $id) .'" class="text-primary"><i class="fa fa-edit"></i></a>';
            }
            if(Auth::user()->can('Delete Branch') || Auth::user()->can('all')){            
                $data = $data .' <a class="text-primary" onClick="delete_click('.$id->id.')" ><i class="fa fa-trash"></i></a>';  
            }

            return $data;

        })
        ->make(true);
    }

    public function index(){
        return view("branches.branches");
    }

    public function create(){
        return view('branches.addBranch');
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [ 
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:50', 
            'annual_rent' => 'required|digits_between:3,10',
            'branch_type' => 'required',
            'shop_type' => 'required',
            'location' => 'required',
            'city' => 'required|alpha|max:20',
            'country' => 'required|alpha|max:20',
            ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $branch = Branch::create([
            'name' => $request->name , 
            'annual_rent' => $request->annual_rent, 
            'branch_type' => $request->branch_type,
            'shop_type' => $request->shop_type,
            'location' => $request->location,
            'city' => $request->city,
            'country' => $request->country,
            'phone' => $request->phone,
            'whatsapp' => $request->whatsapp,
            'map' => $request->map,
        ]);
        $branch->save();

        $products = Product::where(['type' => 'Regular'])->get();
        foreach($products as $product){
            Inventory::create([
                'branch_id' => $branch->id,
                'product_id' => $product->id,
                'initial_qty' => 0,
                'current_qty' => 0
            ]);
        }

        $this->notifyUsers('Branch Added');

        return redirect('branches')->with('message', 'Branch Added Successfully');;                
    }

    public function edit($id){
        $branch = Branch::where(['id' => $id])->first();
        return view('branches.editBranch' , compact('branch'));
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [ 
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:50', 
            'annual_rent' => 'required|numeric',
            'branch_type' => 'required',
            'shop_type' => 'required',
            'location' => 'required',
            'city' => 'required|alpha|max:20',
            'country' => 'required|alpha|max:20',
            ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $branch = Branch::where(['id' => $request->id])->first();
        $branch->name = $request->name; 
        $branch->annual_rent = $request->annual_rent; 
        $branch->location = $request->location;
        $branch->branch_type = $request->branch_type;
        $branch->shop_type = $request->shop_type;
        $branch->city = $request->city;
        $branch->country = $request->country;
        $branch->phone = $request->phone;
        $branch->whatsapp = $request->whatsapp;
        $branch->map = $request->map;
        $branch->save();

        $this->notifyUsers('Branch Updated');
        return redirect('branches')->with('message', 'Branch Updated Successfully');;                
    }

    public function delete($id){
        Branch::where(['id' => $id])->delete();   

        $this->notifyUsers('Branch Deleted');     
        return redirect()->back()->with('message', 'Branch Deleted Successfully');;
    }

    public function notifyUsers($message){
        $this->details['data']['link'] = route('branches');
        $this->details['data']['text'] = $message;
        $users = User::all();
        foreach($users as $user){
            if($user->can('View Branches')  || $user->can('all')){
                $user->notify(new Noti($this->details));
            }
        }
    }
}
