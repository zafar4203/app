<?php

namespace App\Http\Controllers;

use QrCode;
use Validator;
use DataTables;
use Auth;
use App\Promo;
use App\User;
use App\Notifications\Noti;
use Illuminate\Http\Request;

class PromoController extends Controller
{

    public $details = [];
    public function __construct(){
        $this->middleware('auth');

        $this->details = [
            'read_at' => null,
            'data' => [
                'link' => '',
                'text' => ''
            ],
        ];
    }

    public function getPromos(){
        $promos = Promo::select('id', 'name' , 'discount', 'description', 'status', 'amount', 'type', 'expiry_date')->get();

        foreach($promos as $promo){
            if($promo->type == "Discount"){
                $promo->type = "Percentage";
            }
        }

        return DataTables::of($promos)
        ->addColumn('qr', function ($promo) {
            return QrCode::size(100)->generate(url('https://papafluffy.com/order?discount='.$promo->name)); 
        })
        ->addColumn('action', function ($id) {
            $href = base64_encode(QrCode::format('png')->size(200)->errorCorrection('H')->generate(url('https://papafluffy.com/order?discount='.$id->name)));

            $data = "";
            if(Auth::user()->can('Update Promo') || Auth::user()->can('all')){
                $data = '<a href="'. route("editPromo" , $id) .'" class="text-primary"><i class="fa fa-edit"></i></a>';
            }
            if(Auth::user()->can('Delete Promo') || Auth::user()->can('all')){            
                $data = $data .' <a class="text-primary" onClick="delete_click('.$id->id.')" ><i class="fa fa-trash"></i></button>'; 
            }
            if(Auth::user()->can('Download Promo') || Auth::user()->can('all')){            
                $data = $data .' <a href="data:image/png;base64,'.$href.'" download="'.$id->name.'.png"><i class="fa fa-download"></i></a>';
            }

            return $data;
        })
        ->make(true);
    }

    public function index(){
        return view("promos.promos");
    }

    public function Promo(){
        return view("promos.addPromo");
    }

    public function addPromo(Request $request){

        $validator = Validator::make($request->all(), [ 
            'name' => 'required|unique:promos,name', 
            'description' => 'required', 
            'type' => 'required', 
            'status' => 'required', 
            'amount' => 'required|numeric', 
            'expiry_date' => 'required', 
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $promo = Promo::create([
            'name' => $request->name,
            'status' => $request->status,
            'description' => $request->description,
            'type' => $request->type,
            'from' => "Admin",
            'amount' => $request->amount,
            'expiry_date' => $request->expiry_date,
            ]);
        $promo->save();

        $this->notifyUsers('New Promo Code Added');

        return redirect('promos')->with('message', 'Promo Code Added Successfully');;
    }

    public function editPromo($id)
    {
        $promo = Promo::where(['id' => $id])->first();
        return view('promos.editPromo' , compact('promo'));
    }

    public function updatePromo(Request $request){
        $validator = Validator::make($request->all(), [ 
            'name' => 'required|unique:promos,name,'.$request->id, 
            'description' => 'required', 
            'type' => 'required', 
            'status' => 'required', 
            'amount' => 'required|numeric', 
            'expiry_date' => 'required', 

        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $promo = Promo::where(['id' => $request->id])->first();

        $promo->name = $request->name;
        $promo->description = $request->description;
        $promo->type = $request->type;
        $promo->amount = $request->amount;
        $promo->status = $request->status;
        $promo->expiry_date = $request->expiry_date;
        $promo->save();

        $this->notifyUsers('Promo Code Updated');

        return redirect('promos')->with('message', 'Promo Updated Successfully');;
    }

    public function deletePromo($id){
        Promo::where(['id' => $id])->delete();   
        $this->notifyUsers('Promo Code Deleted');     
        return redirect()->back()->with('message', 'Promo Deleted Successfully');;
    }





    // Invisible Promos
    public function getIPromos($id){
        $promos = Promo::select('id', 'invisible_id', 'name' , 'description', 'status', 'amount', 'type', 'expiry_date')
        ->where(['invisible_id' =>  $id])
        ->get();

        foreach($promos as $promo){
            if($promo->type == "Discount"){
                $promo->type = "Percentage";
            }
        }

        return DataTables::of($promos)
        ->addColumn('action', function ($id) {
            return '<a href="'. route("i_editPromo" , $id) .'" class="text-primary"><i class="fa fa-edit"></i></a>
            <a class="text-primary" onClick="delete_click('.$id->id.')" ><i class="fa fa-trash"></i></button>'; 
        })
        ->make(true);
    }

    public function i_index(){
        return view("invisible.promos.promos");
    }

    public function i_Promo(){
        return view("invisible.promos.addPromo");
    }

    public function i_addPromo(Request $request){

        $validator = Validator::make($request->all(), [ 
            'name' => 'required|unique:promos,name', 
            'description' => 'required', 
            'type' => 'required', 
            'status' => 'required', 
            'amount' => 'required|numeric', 
            'expiry_date' => 'required', 
        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $promo = Promo::create([
            'name' => $request->name,
            'status' => $request->status,
            'description' => $request->description,
            'type' => $request->type,
            'from' => "Invisible",
            'invisible_id' => $request->invisible_id,
            'amount' => $request->amount,
            'expiry_date' => $request->expiry_date,
            ]);
        $promo->save();

        return redirect('i_promos')->with('message', 'Promo Code Added Successfully');;
    }

    public function i_editPromo($id)
    {
        $promo = Promo::where(['id' => $id])->first();
        return view('invisible.promos.editPromo' , compact('promo'));
    }

    public function i_updatePromo(Request $request){
        $validator = Validator::make($request->all(), [ 
            'name' => 'required|unique:promos,name,'.$request->id, 
            'description' => 'required', 
            'type' => 'required', 
            'status' => 'required', 
            'amount' => 'required|numeric', 
            'expiry_date' => 'required', 

        ]);

        if ($validator->fails()) { 
            return redirect()->back()->withErrors($validator)->withInput();
        }
        
        $promo = Promo::where(['id' => $request->id])->first();

        $promo->name = $request->name;
        $promo->description = $request->description;
        $promo->type = $request->type;
        $promo->amount = $request->amount;
        $promo->status = $request->status;
        $promo->expiry_date = $request->expiry_date;
        $promo->save();

        return redirect('i_promos')->with('message', 'Promo Updated Successfully');;
    }

    public function i_deletePromo($id){
        Promo::where(['id' => $id])->delete();        
        return redirect()->back()->with('message', 'Promo Deleted Successfully');;
    }

    public function QrSingle($id){
        $promo  = Promo::where(['id' => $id])->first();  
        return view('promos.singleQr',compact('promo'));      
    }

    public function notifyUsers($message){
        $this->details['data']['link'] = route('promos');
        $this->details['data']['text'] = $message;
        $users = User::all();
        foreach($users as $user){
            if($user->can('View Promos')  || $user->can('all')){
                $user->notify(new Noti($this->details));
            }
        }
    }
}