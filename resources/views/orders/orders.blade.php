@extends('layouts.welcome')

@section('title' , 'Sales')

@section('styles')
    <link href="{{asset('public/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/1.0.7/css/responsive.dataTables.min.css">

  <style>
      .error{
        color:red;
        font-size:1rem;
      }
      .ml-00{
          margin-left:1px !important;
      }
     .active-yr, .btn:hover {
        background-color: #4841a8;
        color: white;
      }
      .active-mn, .btn:hover {
        background-color: #4841a8;
        color: white;
      }
      .active-br, .btn:hover {
        background-color: #4841a8;
        color: white;
      }

      div.dataTables_wrapper div.dataTables_filter input {
            margin-left: 0.5em;
            display: inline-block;
            width: auto;
            margin-right: 5px;
        }
        .dt-button{
          float:right;
          z-index:9999;
          position:relative;
        }
        
      @media screen and (max-width: 600px) {
        .btn-group{
          display:inline-block;
        }      
        .mn{
          margin-top:10px;
        }
        .yr{
          margin-top:10px;
        }
        .text-gray-800{
          padding-bottom:5% !important;
          border-bottom:1px solid #4e73df;

          padding-top:5% !important;
          border-top:1px solid #4e73df;
        }
        .float-right{
            float:none !important;
        }
        .row{
          display:block;
        }
        label {
            display: block;
            margin-bottom: .5rem;
        }
        .col-md-6 {
            padding-left: 0;
        }
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">

@if(session()->has('message'))
    <div id="alert" class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
      <!-- Page Heading -->
      <h1 class="h3 mb-2 text-gray-800">Home Delivery Orders List 
      
      @if(Auth::user()->can('Add Order') || Auth::user()->can('all') )
      <span class="float-right"><a href="{{ url('addOrder') }}" id="add_order" class="btn btn-primary">Add Order</a></span>
      @endif
      </h1>

        <div class="row">
            <div class="col-md-12">
                <div class="btn-group" id="years">
                    <button type="button" id="2020" class="yr btn btn-primary">2020</button>
                    <button type="button" id="2021" class="ml-00 yr btn btn-primary">2021</button>
                    <button type="button" id="2022" class="ml-00 yr btn btn-primary">2022</button>
                    <button type="button" id="2023" class="ml-00 yr btn btn-primary">2023</button>
                </div>
            </div>

            <div class="col-md-12 mt-2">
                <div class="btn-group" id="months">
                    <button type="button" id="1" class="btn mn btn-primary">Jan</button>
                    <button type="button" id="2" class="ml-00 mn btn btn-primary">Feb</button>
                    <button type="button" id="3" class="ml-00 mn btn btn-primary">March</button>
                    <button type="button" id="4" class="ml-00 mn btn btn-primary">April</button>
                    <button type="button" id="5" class="ml-00 mn btn btn-primary">May</button>
                    <button type="button" id="6" class="ml-00 mn btn btn-primary">June</button>
                    <button type="button" id="7" class="ml-00 mn btn btn-primary">July</button>
                    <button type="button" id="8" class="ml-00 mn btn btn-primary">Aug</button>
                    <button type="button" id="9" class="ml-00 mn btn btn-primary">Sep</button>
                    <button type="button" id="10" class="ml-00 mn btn btn-primary">Oct</button>
                    <button type="button" id="11" class="ml-00 mn btn btn-primary">Nov</button>
                    <button type="button" id="12" class="ml-00 mn btn btn-primary">Dec</button>
                </div>
            </div>            

        </div>


          <!-- DataTales Example -->
          <div class="card shadow mt-2 mb-4">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="orderTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Sr.No</th>
                      <th>Action</th>
                      <th>From</th>
                      <th>Date</th>
                      <th>Order Value</th>
                      <th>Delivery Time</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Sr.No</th>
                      <th>Action</th>
                      <th>From</th>
                      <th>Date</th>
                      <th>Order Value</th>
                      <th>Delivery Time</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->


          <!-- Logout Modal-->
  <div class="modal fade" id="deleteCategoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Are you sure to delete?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select <span class="error">"Delete"</span> below if you are ready to delete the Category.</div>
        <div class="modal-footer">
          <button class="btn btn-success" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-danger" id="deleteModel" href="#">Delete</a>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <!-- Page level plugins -->
  <script src="{{asset('public/vendor/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('public/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

  <!-- Page level custom scripts -->
  <script src="{{asset('public/js/demo/datatables-demo.js')}}"></script>
  <script src="{{asset('public/vendor/datatables/responsive_datatables.js')}}"></script>


  <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>  
  <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>  
  <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>  
  <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.colVis.min.js"></script>  

  <script>
     $("#alert").fadeTo(2000, 500).slideUp(500, function(){
       $("#alert").slideUp(600);
       });

        // Selection Area
        var d = new Date();
        var year = d.getFullYear();
        var month = d.getMonth() + 1;
        var branch = $('.br').eq(0).attr('id');
        $('#'+year).addClass('active-yr');
        $('#'+month).addClass('active-mn');
        $(".br:first").addClass('active-br');

        // Add active class to the current button (highlight it)
        var header = document.getElementById("years");
        var btns = header.getElementsByClassName("yr");
        for (var i = 0; i < btns.length; i++) {
        btns[i].addEventListener("click", function() {
        var current = document.getElementsByClassName("active-yr");
        current[0].className = current[0].className.replace(" active-yr", "");
        this.className += " active-yr";
        });
        }


                // Add active class to the current button (highlight it)
        var header = document.getElementById("months");
        var btns = header.getElementsByClassName("mn");
        for (var i = 0; i < btns.length; i++) {
          btns[i].addEventListener("click", function() {
          var current = document.getElementsByClassName("active-mn");
          current[0].className = current[0].className.replace(" active-mn", "");
          this.className += " active-mn";
          });
        }

        $('.yr').click(function(){
            year = this.id;
            getData();
        });

        $('.mn').click(function(){
            month = this.id;
            getData();
        });

       function delete_click(clicked_id){
          $('#deleteModel').attr("href","{{url('deleteCategory')}}/"+clicked_id)
          $('#deleteCategoryModal').modal('show');
        }

        var tbl = $('#orderTable');
        $('#orderTable').DataTable({
              "processing":true,
              "serverside":true,
              responsive:true,
              columnDefs: [ 
                { targets:"_all", orderable: false },
                { targets:[0,1,2,3,4,5], className: "desktop" },
                { targets:[1,2,4,5], className: "tablet, mobile" }
              ],
              "pageLength": 25,
              "ajax":"{{url('ajaxOrders')}}/"+year+"/"+month,
              "columns":[
                {"data" : "id"},
                {"data" : "action"},
                {"data" : "order_from"},
                {"data" : "placed"},
                {"data" : "total_price"},
                {"data" : "deliver"},
              ],
              order:[0,'desc'],
              dom: 'Bfrtip',
              buttons: [
                  {
                      extend: 'print',
                      exportOptions: {
                          columns: ':visible'
                      }
                  },
                  {
                      extend: 'excelHtml5',
                      exportOptions: {
                          columns: ':visible'
                      }
                  },
                  {
                      extend:'pdfHtml5',
                      text:'PDF',
                      orientation:'landscape',
                      exportOptions: {
                         columns: ':visible'
                      },
                      customize : function(doc){
                          var colCount = new Array();
                          $(tbl).find('tbody tr:first-child td').each(function(){
                              if($(this).attr('colspan')){
                                  for(var i=1;i<=$(this).attr('colspan');$i++){
                                      colCount.push('*');
                                  }
                              }else{ colCount.push('*'); }
                          });
                          doc.content[1].table.widths = colCount;
                      }
                  },
                  'colvis'
              ] 
            });

            $(".dt-button").addClass("mr-1 btn btn-sm btn-primary");

        function getData(){
          $('#orderTable').DataTable().ajax.url("{{url('ajaxOrders')}}/"+year+"/"+month).load();
        }

  </script>
@endsection