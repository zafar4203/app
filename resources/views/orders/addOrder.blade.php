@extends('layouts.welcome')

@section('title' , 'Add Order')

@section('styles')
    <style>
      .item-big-image{
            width:100%;
            display: block;
            margin-left: auto;
            margin-right: auto;
      }

/* add minus buttons */
.qty .count {
    color: #000;
    display: inline-block;
    vertical-align: top;
    font-size: 25px;
    font-weight: 700;
    line-height: 30px;
    padding: 0 2px
    ;min-width: 35px;
    text-align: center;
}
.qty .plus {
    cursor: pointer;
    display: inline-block;
    vertical-align: top;
    color: white;
    width: 30px;
    height: 30px;
    font: 30px/1 Arial,sans-serif;
    text-align: center;
    border-radius: 50%;
    }
.qty .minus {
    cursor: pointer;
    display: inline-block;
    vertical-align: top;
    color: white;
    width: 30px;
    height: 30px;
    font: 30px/1 Arial,sans-serif;
    text-align: center;
    border-radius: 50%;
    background-clip: padding-box;
}
#icons {
    text-align: center;
}
.minus:hover{
    background-color: #717fe0 !important;
}
.plus:hover{
    background-color: #717fe0 !important;
}
/*Prevent text selection*/
span{
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
}
input{  
    border: 0;
    width: 2%;
}
input.count::-webkit-outer-spin-button,
input.count::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
}
input:disabled{
    background-color:white;
}
.big-text{
    font-size:16px;
    color:#000000;
    font-weight:bold;
}


.border{
    padding-bottom:10px;
    padding-top:10px;
    background:white;
}
.total-box{
    background:#ffffff!important;
    color:#000000;
}
.total-box .row{
    padding:10px 0px;
    border-bottom:2px solid #4268D6;
}

.error{
    font-size:14px;
    color:red;
}
.custom-control-label{
    font-size:14px;
}



/* datepicker */
.vdp-datepicker__calendar {
    position: absolute;
    z-index: 100;
    background: #fff;
    width: 260px !important;
    border: 1px solid #ccc;
}
@media screen and (max-width: 600px) {
        .p-mobile-0{
          padding-right:0px !important;
		  padding-left:0px !important;
        }
      }
    </style>
@endsection

@section('content')
    <div class="container-fluid" id="app">
        <div class="row">
        <div class="col-md-8">
            <div class="row">
    
                <div v-for="product in products" :key="product" class='col-md-4 col-6 border'>                    
                    <div class='col-md-12'>
                        <!-- <img class='item-big-image' :src="product.image"> -->
                        <div class='col-md-12 text-center big-text'>@{{product.short_name}}</div>
                            <div class='p-mobile-0 col-md-12'>
                                <div id='icons' class='qty mt-1'>
                                    <span @click="decrement(product)" class='minus bg-primary'>-</span>
                                    <input type='number' class='count' name='qty' :value="product.quantity">
                                    <span @click="increment(product)" class='plus bg-primary'>+</span>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>

            <div class="row mb-5 ">
                <div class="col-md-5"></div>
                <div class="col-md-6 mt-5 total-box">
                    <div class="row">
                        <div class="col-md-6">Sub Total</div>
                        <div class="col-md-6 text-right">AED @{{subtotal}}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">Delivery Fee</div>
                        <div class="col-md-6 text-right">AED @{{deliveryFee}}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">Vat 5%</div>
                        <div class="col-md-6 text-right">AED @{{vat}}</div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">Total Order</div>
                        <div class="col-md-6 text-right">AED @{{total}}</div>
                    </div>
                </div>
                <div class="col-md-1"></div>
            </div>
        </div>
        <div class="col-md-4">
                    <form class="ps-form--checkout" @submit.prevent="onSubmit">
								<div class="form-group">
									<input class="form-control" type="text" name="user_name" v-model="user_name" v-validate="'required|alpha_spaces|max:15'" data-vv-as="Name" placeholder="Name">
									<div class="error" v-if="errors.has('user_name')">@{{errors.first('user_name')}}</div>
								</div>
								<div class="form-group">
									<input class="form-control" type="text" name="user_phone" v-model="user_phone" v-validate="'required|numeric|min:11|max:14'" data-vv-as="Mobile Number" placeholder="Mobile">
									<div class="error" v-if="errors.has('user_phone')">@{{errors.first('user_phone')}}</div>
								</div>
								<div class="form-group">
									<input class="form-control" type="email" name="user_email" v-model="user_email" v-validate="'email'" data-vv-as="Email Address" placeholder="Email">
									<div class="error" v-if="errors.has('user_email')">@{{errors.first('user_email')}}</div>
								</div>
								<div class="form-group">
									<input class="form-control" type="text" name="user_address" v-model="user_address" v-validate="'required|min:5|max:80'" data-vv-as="Address" placeholder="Address">
									<div class="error" v-if="errors.has('user_address')">@{{errors.first('user_address')}}</div>
								</div>

								<div class="form-group">
									<label style="display:block;">Payment</label>
									<div class="custom-control custom-radio custom-control-inline">
										<input @click="payment_type('cash')" type="radio" class="custom-control-input" id="cashRadio" name="order_type" checked value="cash">
										<label class="custom-control-label" for="cashRadio">Cash</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline">
										<input @click="payment_type('card')" type="radio" class="custom-control-input" id="cardRadio"  name="order_type" value="card">
										<label class="custom-control-label" for="cardRadio">Bring Card</label>
									</div>
								</div>


								<div class="form-group">
									<label style="display:block;">Timing</label>
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" class="custom-control-input" @click="today" id="customRadio" name="example" value="today">
										<label class="custom-control-label" for="customRadio">Today</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline">
										<input type="radio" class="custom-control-input" @click="tomorrow" id="customRadio2" name="example" value="tomorrow">
										<label class="custom-control-label" for="customRadio2">Tomorrow</label>
									</div>
									<div class="custom-control custom-radio custom-control-inline">
											<input type="radio" class="custom-control-input" @click="other" id="customRadio3" name="example" value="other">
											<label class="custom-control-label" for="customRadio3">Other</label>
									</div>
								</div>
								<div v-if="otherCheck" class="form-group">
									<label>Select Date</label>
									<vuejs-datepicker  :disabled-dates="disabledDates" @input="dateSelected()" input-class="form-control" placeholder="Select Date" name="other_date" v-model="other_date" v-validate="'required'" data-vv-as="Date" ></vuejs-datepicker>
									<div class="error" v-if="errors.has('other_date')">@{{errors.first('other_date')}}</div>
								</div>
								<div v-if="timeSelect" class="form-group">
										<label>Select Time</label>
										<select class="form-control" name="time" v-model="time" v-validate="'required'" >
											<option disabled value="">Select Time Please</option>
											<option v-for="timing in fixedTimings" :key="timing"  :value="timing.time" v-show="timing.show == true" :disabled="timing.status == false">@{{timing.time}} @{{booked(timing.status)}}</option>
										</select>
										<div class="error" v-if="errors.has('time')">@{{errors.first('time')}}</div>
								</div>

								<button :disable="orderPlaceCheck == true" class="btn btn-block btn-primary" type="submit">Place Order</button>
								<img v-if="orderPlaceCheck == true" class="order-loader" src="{{ asset('public/img/order_loader.gif') }}" alt="">
							</form>
        </div>

        </div>
    </div>
@endsection

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/vue"></script>
		<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/vee-validate/2.2.15/vee-validate.min.js"></script>
		<script src="https://unpkg.com/vuejs-datepicker"></script>


		<script>
			Vue.use(VeeValidate); 
			new Vue({
				el: '#app',
				components: {
					vuejsDatepicker
				},
				data: {
					disabledDates: {
					    to: new Date()
					},
					bookings:[],
					fixedTimings:[
						{time:'10 AM',status:true, show:true} , {time:'11 AM' , status:true,show:true} , {time:'12 PM' , status:true,show:true} , {time:'01 PM' , status:true,show:true} , {time:'02 PM' , status:true,show:true}, {time:'03 PM' , status:true,show:true}, {time:'04 PM' , status:true,show:true}, {time:'05 PM' , status:true,show:true}, {time:'06 PM' , status:true,show:true},{time:'07 PM' , status:true,show:true}, {time:'08 PM' , status:true,show:true},{time:'09 PM' , status:true,show:true},{time:'10 PM' , status:true,show:true}],
					deliveryFee : 10,
					products: [] ,
					user_name:'',
					user_email:'',
					user_phone:'',
					user_address:'',
					order_type:'cash',
					time:'',
					other_date:new Date(),
					timeSelect:false,
					tomorrowCheck:false,
					otherCheck:false,
					vatCheck:false,
					orderPlaceCheck:false,
				},
				mounted() {
					var vm = this;
					axios.get("{{ url('api/products') }}")
					.then(response => {				
						response.data.products.forEach((product) => {
							product.quantity = 0;
							vm.products.push(product);
						});		
						console.log(this.products);
					})

					axios.get("{{ url('api/getVat') }}")
					.then(response => {				
						if(response.data){
							this.vatCheck = true;
						}else{
							this.vatCheck = false;
						}
					})
				},
				computed:{
					subtotal:function(){
						let total = 0;
						this.products.forEach((product) => {
							total = total + (product.quantity * product.price)
						});		
						if(total >= 30){
							this.deliveryFee = 0;
						}else{
							this.deliveryFee = 10;
						}
						let sub = parseFloat(total) - parseFloat(total * 0.05);
						return sub.toFixed(2);
					},
					vat:function(){
						let total = 0;
						this.products.forEach((product) => {
							total = total + (product.quantity * product.price)
						});		
						let vat = parseFloat(total * 0.05);

						return vat.toFixed(2);
					},
					total:function(){
						return (parseFloat(this.subtotal) + parseFloat(this.vat)) + this.deliveryFee; 
					},
					total_items:function(){
						var count = 0;
						this.products.forEach((product) => {
							if(product.quantity > 0){
								count++;
							}
						});
						return count;
					}
				},
				methods: {
					increment:function (product){
						product.quantity = product.quantity + 1; 
					},
					decrement:function (product){
						if(product.quantity > 0){
							product.quantity = product.quantity - 1; 
						}
					},
					booked:function(val){
						if(val == false){
							return " (Booked) ";
						}
					},
					today:function(){
						this.time="";
						this.tomorrowCheck = false;
						this.otherCheck = false;
						this.fixedTimings.forEach((timing) => {
							timing.show = true;
							timing.status = true;
						});									
						axios.get("{{ url('api/todayBookings') }}")
						.then(response => {				
							this.bookings = response.data;
							this.bookings.forEach((booking) => {
								this.fixedTimings.forEach((timing) => {
									if(booking.time == timing.time){
										timing.status = false;
									}
								});									
							});
							this.updateTimings();
							this.timeSelect = true;
						})
					},
					dateSelected:function(){
						this.timeSelect = true;
						let date = this.minTwoDigits(parseInt(new Date(this.other_date).getMonth()+1))+"/"+this.minTwoDigits(new Date(this.other_date).getDate())+"/"+new Date(this.other_date).getFullYear();
						this.order_date = date;
						this.fixedTimings.forEach((timing) => {
							timing.show = true;
							timing.status = true;
						});			
						axios({
							method: 'post',
							url: "{{ url('api/someDayBookings') }}",
							data: {
								date : date,
							}})
						.then(response => {				
							this.bookings = response.data;
							this.bookings.forEach((booking) => {
								this.fixedTimings.forEach((timing) => {
									if(booking.time == timing.time){
										timing.status = false;
									}
								});									
							});
							this.timeSelect = true;
						})
					},
					other:function(){
						this.time="";
						this.tomorrowCheck = false;
						this.timeSelect = false;	
						this.otherCheck = true;					
					},
					tomorrow:function(){
						this.time="";
						this.tomorrowCheck = true;
						this.otherCheck = false;
						this.fixedTimings.forEach((timing) => {
							timing.show = true;
							timing.status = true;
						});									
						axios.get("{{ url('api/tomorrowBookings') }}")
						.then(response => {				
							this.bookings = response.data;
							this.bookings.forEach((booking) => {
								this.fixedTimings.forEach((timing) => {
									if(booking.time == timing.time){
										timing.status = false;
									}
								});									
							});
							this.timeSelect = true;
						})
					},
					minTwoDigits:function(n) {
						  return (n < 10 ? '0' : '') + n;
					},
					updateTimings:function(){
						let myTime = new Date().toLocaleString([], { hour: '2-digit'});
						if(myTime.split(" ")[0] == 10 && myTime.split(" ")[1] == "AM"){
							myTime = "12 PM";
						}else if(myTime.split(" ")[0] == 11 && myTime.split(" ")[1] == "AM"){
							myTime = "01 PM";
						}else if(myTime.split(" ")[0] == 12 && myTime.split(" ")[1] == "PM"){
							myTime = "02 PM";
						}else if(myTime.split(" ")[0] == 12 && myTime.split(" ")[1] == "AM"){
							myTime = "02 AM";
						}else{
							myTime = this.minTwoDigits((parseInt(myTime.split(" ")[0]) + 2)) +" "+ myTime.split(" ")[1];
						}


						if(myTime.split(" ")[1] == "PM"){
							this.fixedTimings.forEach((timing) => {
								let fullTime = timing.time;
								let time = fullTime.split(" ")[0];
								let am = fullTime.split(" ")[1];
								if(myTime.split(" ")[0] < 12){
									if(time >= myTime.split(" ")[0]  && myTime.split(" ")[1] == am && time != 12){
										timing.show = true;
									}else{
										timing.show = false;
									}
								}
								if(myTime.split(" ")[0] == 12 && myTime.split(" ")[1] == "PM"){
									if(time <= myTime.split(" ")[0]  && myTime.split(" ")[1] == am){
										timing.show = true;
									}else{
										timing.show = false;
									}
								}
							});									
						}else{
							this.fixedTimings.forEach((timing) => {
								let fullTime = timing.time;
								let time = fullTime.split(" ")[0];
								let am = fullTime.split(" ")[1];
								if(myTime.split(" ")[0] == 11 && myTime.split(" ")[1] == "AM"){
									if(time == "10" && am == "AM"){
										timing.show = false;
									}else{
										timing.show = true;
									}
								}
							});									
						}
					},
					bookOrder:function(id){
							var vm = this;
							axios({
							method: 'post',
							url: "{{ url('api/bookOrder') }}",
							data: {
								tomorrow : this.tomorrowCheck,
								other_check:this.otherCheck,
								order_date:this.order_date,
								time : this.time,
								order_id : id,
							}})
							.then(function (response) {
								console.log(response);
								this.orderPlaceCheck = false;
                                location.reload();
							})
							.catch(function (response) {
								//handle error
								console.log(response);
							});												
					},
					placeOrderItems : function(id){
						var vm = this;
						this.products.forEach((product) => {
							if(product.quantity > 0){
								axios({
								method: 'post',
								url: "{{ url('api/orderItems') }}",
								data: {
									order_id : id,
									product_id : product.id,
									product_name : product.name,
									product_price : product.price,
									product_quantity : product.quantity,
									vat : this.vatCheck ,
									product_category_id : product.category_id,
								}})
								.then(function (response) {
									console.log(response);
								})
								.catch(function (response) {
									//handle error
									console.log(response);
								});						
							}
						});

						axios({
						method: 'post',
						url: "{{ url('api/sendOrderEmailAdmin') }}",
						data: {
							order_id : id,
							user_name : this.user_name,
							user_email : this.user_email,
							order_date : this.order_date,
							time : this.time,
							delivery : this.deliveryFee,
							tomorrow : this.tomorrowCheck,
							other_check:this.otherCheck,
						}})
						.then(function (response) {
							console.log(response);
						})
						.catch(function (response) {
							//handle error
							console.log(response);
						});						

						vm.bookOrder(id);
					},
					onSubmit(){
						var vm = this;
						this.$validator.validateAll().then(result=> {
							if(result){
								if(this.subtotal>0){
									if(this.time != ""){
										this.orderPlaceCheck = true;
										axios({
										method: 'post',
										url: "{{ url('api/placeOrderHomeDelivery') }}",
										data: {
											user_name : this.user_name,
											user_email : this.user_email,
											user_phone : this.user_phone,
											user_address : this.user_address,
											order_from : "Phone",
											sub_total_price : this.subtotal,
											vat_price : this.vat,
											total_price : this.total,
											total_items : this.total_items,
											order_type : this.order_type,
											delivery_date : new Date().toLocaleString()
										}})
										.then(function (response) {
											if(response.data.success){
												vm.placeOrderItems(response.data.order.id);	
											}
										})
										.catch(function (response) {
											//handle error
											console.log(response);
										});
									}else{
										alert("Please Select Delivery Time");										
									}
								}else{
									alert("Please Add Some Product to buy");
								}
							}
						});
					},
					payment_type(val){
						this.order_type = val;
					}
				}
			})
		</script>
@endsection