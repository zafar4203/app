@extends('layouts.welcome')

@section('title' , 'Add Combo Product')

@section('styles')
    <style>
      .add-product-form{
        margin:0px 5px;
      }
      .error{
        color:red;
        font-size:1rem;
      }
      .submit-btn{
        margin-top:20px; 
        margin-bottom:20px; 
      }
      #grouped_items{
        display:none;
      }


      .item-image{
            width:50px;
            display: block;
            margin-left: auto;
            margin-right: auto;
      }
      .item-big-image{
            width:100%;
            display: block;
            margin-left: auto;
            margin-right: auto;
      }

      /* Add Item Model Css */
           /* add minus buttons */
           .qty .count {
    color: #000;
    display: inline-block;
    vertical-align: top;
    font-size: 25px;
    font-weight: 700;
    line-height: 30px;
    padding: 0 2px
    ;min-width: 35px;
    text-align: center;
}
.qty .plus {
    cursor: pointer;
    display: inline-block;
    vertical-align: top;
    color: white;
    width: 30px;
    height: 30px;
    font: 30px/1 Arial,sans-serif;
    text-align: center;
    border-radius: 50%;
    }
.qty .minus {
    cursor: pointer;
    display: inline-block;
    vertical-align: top;
    color: white;
    width: 30px;
    height: 30px;
    font: 30px/1 Arial,sans-serif;
    text-align: center;
    border-radius: 50%;
    background-clip: padding-box;
}
#icons {
    text-align: center;
}
.minus:hover{
    background-color: #717fe0 !important;
}
.plus:hover{
    background-color: #717fe0 !important;
}
/*Prevent text selection*/
span{
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
}
input.count{  
    border: 0;
    width: 2%;
}
input.count::-webkit-outer-spin-button,
input.count::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
}
input:disabled{
    background-color:white;
}
.big-text{
    font-size:16px;
    color:#000000;
    font-weight:bold;
}


.border{
    padding-bottom:10px;
}
#addItemsModal .modal-header,
#addItemsModal .modal-footer{
    border:0px;
}


#input_rows .row ,
#output_rows .row{
    border-bottom:1px solid #4e73df;
    padding-top:5px;
}
    </style>
@endsection

@section('content')
<div class="container-fluid">
      <div class="row">
            <div class="col-md-4">
                    <h6 class="text-center"><b>Input Products</b></h6>
                    <button id="add_input_btn" class="btn btn-primary btn-block">Add Items</button>

                    <div class="col-md-12 mt-3">
                        <div class="row">
                            <div class="col-md-8">
                                <b>Product</b>
                            </div>
                            <div class="col-md-4">
                                <b>Quantity</b>
                            </div>
                        </div>

                        <div id="input_rows">
                           
                        </div>
                    </div>
            </div>
            <div class="col-md-4">
                    <h6 class="text-center"><b>Output Products</b></h6>
                    <button id="add_output_btn" class="btn btn-primary btn-block">Add Items</button>

                    <div class="col-md-12 mt-3">
                        <div class="row">
                            <div class="col-md-8">
                                <b>Product</b>
                            </div>
                            <div class="col-md-4">
                                <b>Quantity</b>
                            </div>
                        </div>

                        <div id="output_rows">
                           
                        </div>
                    </div>
            </div>
            <div class="col-md-4">

                    <form  class="add-product-form"  id="add_product">
                    <div class="form-group">
                        <label for="productInput">Product Name</label>
                        <input type="text" id="name" name="name" class="form-control" id="productInput" value="{{ $product->name }}" aria-describedby="nameError" placeholder="Enter product name">
                        <small id="nameError" class="form-text error"></small>
                    </div>

                    <div class="form-group">
                        <label for="productShortInput">Product Short Name</label>
                        <input type="text" id="short_name" name="short_name" class="form-control" id="productShortInput" value="{{ $product->short_name }}" aria-describedby="short_nameError" placeholder="Enter product short name">
                        <small id="short_nameError" class="form-text error"></small>
                    </div>


                    <div class="form-group">
                        <label for="productPrice">Product Price</label>
                        <input type="text" id="price" name="price" class="form-control" id="productPrice" value="{{ $product->price }}" aria-describedby="priceError" placeholder="Enter product price">
                        <small id="priceError" class="form-text error"></small>
                    </div>

                    <div class="form-group">
                    <label for="for">Product For</label>
                        <select class="form-control" id="for" name="for" >
                            <option value="" disable> ---Select--- </option>
                            <option @if($product->for == "POS") selected @endif value="POS"> POS </option>
                            <option @if($product->for == "WEB") selected @endif value="WEB"> WEB </option>
                        </select>
                        <small id="forError" class="form-text error"></small>
                    </div>


                    <div class="form-group">
                        <div class="row">
                                <div class='col-md-6'>
                                    <label>Select Input Items</label>
                                    <select name="input_items_count" class="form-control" id="">
                                        <option @if($product->input_items == 1) selected @endif value="1">1</option>
                                        <option @if($product->input_items == 2) selected @endif value="2">2</option>
                                        <option @if($product->input_items == 3) selected @endif value="3">3</option>
                                        <option @if($product->input_items == 4) selected @endif value="4">4</option>
                                        <option @if($product->input_items == 5) selected @endif value="5">5</option>
                                        <option @if($product->input_items == 6) selected @endif value="6">6</option>
                                        <option @if($product->input_items == 7) selected @endif value="7">7</option>
                                        <option @if($product->input_items == 8) selected @endif value="8">8</option>
                                        <option @if($product->input_items == 9) selected @endif value="9">9</option>
                                    </select>
                                </div>
                                <div class='col-md-6'>
                                    <label>Select Output Items</label>
                                    <select name="output_items_count" class="form-control" id="">
                                        <option @if($product->output_items == 0) selected @endif value="0">0</option>
                                        <option @if($product->output_items == 1) selected @endif value="1">1</option>
                                        <option @if($product->output_items == 2) selected @endif value="2">2</option>
                                        <option @if($product->output_items == 3) selected @endif value="3">3</option>
                                        <option @if($product->output_items == 4) selected @endif value="4">4</option>
                                        <option @if($product->output_items == 5) selected @endif value="5">5</option>
                                        <option @if($product->output_items == 6) selected @endif value="6">6</option>
                                        <option @if($product->output_items == 7) selected @endif value="7">7</option>
                                        <option @if($product->output_items == 8) selected @endif value="8">8</option>
                                        <option @if($product->output_items == 9) selected @endif value="9">9</option>
                                    </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" id="customRadio2" value="0" name="block" @if($product->block == 0) checked @endif>
                            <label class="custom-control-label" for="customRadio2">Approve</label>
                        </div>

                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" class="custom-control-input" id="customRadio1" value="1" name="block" @if($product->block == 1) checked @endif>
                            <label class="custom-control-label" for="customRadio1">Block</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <input type="file" name="image[]"  multiple>
                        <small id="imageError" class="form-text error"></small>
                    </div>    

                    <button id="submit" class="btn btn-primary">Submit</button>
                    </form>

            </div>
      </div>
</div>


    <!-- Add Items Model -->
      <!-- The Modal -->
      <div class="modal fade" id="addItemsModal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Add Items</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
            <div id="items_row" class="row">
            </div>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" id="add_input_items_done" class="btn btn-secondary">Add</button>
        </div>
        
      </div>
    </div>
  </div>


    <!-- Add Items Model -->
      <!-- The Modal -->
      <div class="modal fade" id="addItemsOutputModal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Add Items</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
            <div id="items_row_output" class="row">
            </div>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" id="add_output_items_done" class="btn btn-secondary">Add</button>
        </div>
        
      </div>
    </div>
  </div>
@endsection

@section('scripts')
    <script>
    var products = [];
    var input_final_products = [];
    var output_final_products = [];

    // Add Order Items to Existing Order

    $('#add_input_btn').click(function(){
        products = [];
        $.ajax({
            headers:
            { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: "{{url('api/regularProducts')}}",
            method: 'GET',
            success: function(data) {
                if(data){           
                    products = data;
                    $('#items_row').html("");
                    data.forEach(function(product) {
                        var item = "<div class='col-md-3 border'>"+
                                "<div class='col-md-12'>"+
                                    "<img class='item-big-image' src="+product.image+">"+
                                    "<div class='col-md-12 text-center big-text'>"+product.short_name+"</div>"+
                                    "<div class='col-md-12'>"+
                                        "<div id='icons' class='qty mt-1'>"+
                                            "<span id="+product.id+" onClick='removeProduct("+product.id+")' class='minus bg-primary'>-</span>"+
                                            "<input type='number' id='count"+product.id+"' class='count' name='qty' value='0'>"+
                                            "<span id="+product.id+" onClick='addProduct("+product.id+")' class='plus bg-primary'>+</span>"+
                                        "</div>"+
                                    "</div>"+
                                "</div>"+
                            "</div>";
                            $('#items_row').append(item);
                        $('#addItemsModal').modal("show");
                    });
                }
            }
        });
    });

    $('#add_output_btn').click(function(){
        products = [];
        $.ajax({
            headers:
            { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: "{{url('api/regularProducts')}}",
            method: 'GET',
            success: function(data) {
                if(data){           
                    products = data;
                    $('#items_row_output').html("");
                    data.forEach(function(product) {
                        var item = "<div class='col-md-3 border'>"+
                                "<div class='col-md-12'>"+
                                    "<img class='item-big-image' src="+product.image+">"+
                                    "<div class='col-md-12 text-center big-text'>"+product.short_name+"</div>"+
                                    "<div class='col-md-12'>"+
                                        "<div id='icons' class='qty mt-1'>"+
                                            "<span id="+product.id+" onClick='removeProductu("+product.id+")' class='minus bg-primary'>-</span>"+
                                            "<input type='number' id='countu"+product.id+"' class='count' name='qty' value='0'>"+
                                            "<span id="+product.id+" onClick='addProductu("+product.id+")' class='plus bg-primary'>+</span>"+
                                        "</div>"+
                                    "</div>"+
                                "</div>"+
                            "</div>";
                            $('#items_row_output').append(item);
                        $('#addItemsOutputModal').modal("show");
                    });
                }
            }
        });
    });

    $(document).ready(function(){
		    $('.count').prop('disabled', true);
   			$(document).on('click','.plus',function(){
				$('#count'+this.id).val(parseInt($('#count'+this.id).val()) + 1 );
    		});
        	$(document).on('click','.minus',function(){
    			$('#count'+this.id).val(parseInt($('#count'+this.id).val()) - 1 );
    				if ($('#count'+this.id).val() <= 0) {
						$('#count'+this.id).val(0);
					}
            });

            $('.countu').prop('disabled', true);
   			$(document).on('click','.plus',function(){
				$('#countu'+this.id).val(parseInt($('#countu'+this.id).val()) + 1 );
    		});
        	$(document).on('click','.minus',function(){
    			$('#countu'+this.id).val(parseInt($('#countu'+this.id).val()) - 1 );
    				if ($('#countu'+this.id).val() <= 0) {
						$('#countu'+this.id).val(0);
					}
            });
 		});


    $('#add_input_items_done').click(function(){
          $('#addItemsModal').modal('hide');
          var quant = 0;
           products.forEach(function(product){
               if(product.quantity > 0){
                   quant ++;
               }
           }); 
           if(quant > 0 || input_final_products.length > 0){
            $('#input_rows').html("");
            input_final_products = [];
            var total = 0;
            products.forEach(function(product){
                    if(product.quantity > 0){
                        total = total + product.price;
                        input_final_products.push(product);
                        var item = "<div class='row text-dark'>"+
                                "<div class='col-md-8'>"+
                                    product.short_name+
                                "</div>"+
                                "<div class='col-md-4 text-center'>"+
                                    product.quantity+
                                "</div>"+
                            "</div>";
                        $('#input_rows').append(item);                            
                    }
                });
            $('#price').val(total);
 
           }else{
               alert("Please add some products");
           }
    });



    $('#add_output_items_done').click(function(){
          $('#addItemsOutputModal').modal('hide');
          var quant = 0;
           products.forEach(function(product){
               if(product.quantity > 0){
                   quant ++;
               }
           }); 
           if(quant > 0 || output_final_products.length > 0){
            $('#output_rows').html("");
            output_final_products = [];
            products.forEach(function(product){                
                    if(product.quantity > 0){
                        output_final_products.push(product);
                        var item = "<div class='row text-dark'>"+
                                "<div class='col-md-8'>"+
                                    product.short_name+
                                "</div>"+
                                "<div class='col-md-4 text-center'>"+
                                    product.quantity+
                                "</div>"+
                            "</div>";
                        $('#output_rows').append(item);                            
                    }
                }); 
           }else{
               alert("Please add some products");
           }
    });


    function addProduct(id){
            products.forEach(function(product){
                if(product.id == id){
                    var quant = parseInt($('#count'+id).val())+1;
                    if(quant > 0){
                        product.quantity = quant;
                    }else{
                        product.quantity = 0;
                    }
                    console.log(product);
                }
            });
        }

        function removeProduct(id){
            products.forEach(function(product){
                if(product.id == id){
                    var quant = parseInt($('#count'+id).val())-1;
                    if(quant > 0){
                        product.quantity = quant;
                    }else{
                        product.quantity = 0;
                    }
                    console.log(product);
                }
            });
       }


       function addProductu(id){
            products.forEach(function(product){
                if(product.id == id){
                    var quant = parseInt($('#countu'+id).val())+1;
                    if(quant > 0){
                        product.quantity = quant;
                    }else{
                        product.quantity = 0;
                    }
                    console.log(product);
                }
            });
        }

        function removeProductu(id){
            products.forEach(function(product){
                if(product.id == id){
                    var quant = parseInt($('#countu'+id).val())-1;
                    if(quant > 0){
                        product.quantity = quant;
                    }else{
                        product.quantity = 0;
                    }
                    console.log(product);
                }
            });
       }



       $('#submit').click(function(){
            event.preventDefault();
            if(input_final_products.length > 0){
                // if(output_final_products.length > 0){
                    var formData = new FormData($('#add_product')[0]);
                    formData.append('id' , {{$product->id}});
                    formData.append('input_items' , JSON.stringify(input_final_products));
                    formData.append('output_items' , JSON.stringify(output_final_products));

                    $.ajax({
                        type: 'POST',
                        headers:
                        { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        url: "{{url('editComboProduct')}}",
                        enctype: 'multipart/form-data',
                        data: formData,
                        processData: false,
                        contentType: false,
                        dataType: "json",
                        success: function(data) {
                            if(data.success){               
                                location.href="{{ url('combos') }}"
                            } 
                        },
                        error: function (reject) {
                        if( reject.status === 422 ) {
                            var errors = $.parseJSON(reject.responseText);
                                $.each(errors, function (key, val) {
                                    console.log(key);
                                    $("#" + key + "Error").text(val[0]);
                                });
                            }
                        }
                    });
                // }else{
                //     alert("Please add some output items");
                // }
            }else{
                alert("Please add some input items");
            }
       });


       $.ajax({
            headers:
            { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: "{{url('api/regularProducts')}}",
            method: 'GET',
            success: function(data) {
                if(data){           
                    products = data;
                    combosData();
                }
            }
        });

        function combosData(){
            $.ajax({
            headers:
            { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: "{{url('getCombos')}}/"+{{$product->id}},
            method: 'GET',
            success: function(data) {
                data.combos.forEach(function(combo) {
                    var item = "<div class='row text-dark'>"+
                                "<div class='col-md-8'>"+
                                    combo.name+
                                "</div>"+
                                "<div class='col-md-4 text-center'>"+
                                    combo.quantity+
                                "</div>"+
                            "</div>";
                    if(combo.type == "Input"){
                        products.forEach(function(product){
                            if(combo.selected_product_id == product.id){
                                product.quantity = combo.quantity;
                                input_final_products.push(product);
                            }
                        });                        
                        $('#input_rows').append(item);    
                    }        
                    if(combo.type == "Output"){
                        products.forEach(function(product){
                            if(combo.selected_product_id == product.id){
                                product.quantity = combo.quantity;
                                output_final_products.push(product);
                            }
                        });                        
                        $('#output_rows').append(item);                            
                    }        
                });
            } 
        });
        } 

    </script>
@endsection