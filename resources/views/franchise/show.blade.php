@extends('layouts.welcome')

@section('title' , 'Categories')

@section('styles')
    <link href="{{asset('public/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
    <style>
      .error{
        color:red;
        font-size:1rem;
      }
      .bold{
          font-weight:bold;
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">

@if(session()->has('message'))
    <div id="alert" class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
      <!-- Page Heading -->
      <h1 class="h3 mb-2 text-gray-800">Franchise Inquiries</h1>
          <p class="mb-4">You can view franchise inquiries here.</p>
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Franchise</h6>
            </div>
            <div class="card-body">
              <div class="row">
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <div class="col-md-6 bold">Name</div>
                            <div class="col-md-6">{{ $fi->name }}</div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-6 bold">Email</div>
                            <div class="col-md-6">{{ $fi->email }}</div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-6 bold">Whatsapp</div>
                            <div class="col-md-6">{{ $fi->whatsapp }}</div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-6 bold">Number of Branches</div>
                            <div class="col-md-6">{{ $fi->number_of_branches }}</div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-6 bold">Budget</div>
                            <div class="col-md-6">{{ $fi->budget }}</div>
                        </div>
                    </div>        
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <div class="col-md-6 bold">Nationality</div>
                            <div class="col-md-6">{{ $fi->nationality }}</div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-6 bold">Company Name</div>
                            <div class="col-md-6">{{ $fi->company }}</div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-6 bold">Target Country</div>
                            <div class="col-md-6">{{ $fi->target_country }}</div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-6 bold">Representer</div>
                            <div class="col-md-6">{{ $fi->representer }}</div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-6 bold">Hear About Us</div>
                            <div class="col-md-6">{{ $fi->hear_about_us }}</div>
                        </div>
                    </div>        
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->


          <!-- Logout Modal-->
  <div class="modal fade" id="deleteCategoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Are you sure to delete?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select <span class="error">"Delete"</span> below if you are ready to delete the Category.</div>
        <div class="modal-footer">
          <button class="btn btn-success" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-danger" id="deleteModel" href="#">Delete</a>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('scripts')
  <!-- Page level plugins -->
  <script src="{{asset('public/vendor/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('public/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

  <!-- Page level custom scripts -->
  <script src="{{asset('public/js/demo/datatables-demo.js')}}"></script>

@endsection