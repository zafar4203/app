@extends('layouts.welcome')

@section('title' , 'Sales')

@section('styles')
    <link href="{{asset('public/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
    <style>
      .error{
        color:red;
        font-size:1rem;
      }
      .ml-00{
          margin-left:1px !important;
      }
      .active-br, .btn:hover {
        background-color: #4841a8;
        color: white;
      }

      @media screen and (max-width: 600px) {
        .btn-group{
          display:inline-block;
        }
        .br{
          margin-top:10px;
        }
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">

@if(session()->has('message'))
    <div id="alert" class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
      <!-- Page Heading -->
      <h1 class="h3 mb-2 text-gray-800">Inventories <span class="float-right">
      @if(Auth::user()->can('Add Inventory') || Auth::user()->can('all'))
      <a href="{{ url('addInventory') }}"><button class="btn-sm btn btn-primary">Add inventory</button></a>
      @endif
      </span></h1>

        <div class="row">

            <div class="col-md-12 mt-2 mb-2">
                <div class="btn-group" id="branches">
                @foreach($branches as $branch)
                    <button type="button" id="{{$branch->id}}" class="ml-00 btn br btn-primary">{{$branch->name}}</button>
                 @endforeach 
                 </div>
            </div>


        </div>


          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="inventoryTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Product</th>
                      <th>Initial Qty</th>
                      <th>Current Qty</th>
                      <th>Branch</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Product</th>
                      <th>Initial Qty</th>
                      <th>Current Qty</th>
                      <th>Branch</th>
                      <th>Action</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->


          <!-- Logout Modal-->
  <div class="modal fade" id="deleteCategoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Are you sure to delete?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select <span class="error">"Delete"</span> below if you are ready to delete the Category.</div>
        <div class="modal-footer">
          <button class="btn btn-success" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-danger" id="deleteModel" href="#">Delete</a>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('scripts')
  <!-- Page level plugins -->
  <script src="{{asset('public/vendor/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('public/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

  <!-- Page level custom scripts -->
  <script src="{{asset('public/js/demo/datatables-demo.js')}}"></script>

  <script>
     $("#alert").fadeTo(2000, 500).slideUp(500, function(){
       $("#alert").slideUp(600);
     });

        // Selection Area
        var d = new Date();
        var branch = $('.br').eq(0).attr('id');
        $(".br:first").addClass('active-br');

        // Add active class to the current button (highlight it)
        var header = document.getElementById("branches");
        var btns = header.getElementsByClassName("br");
        for (var i = 0; i < btns.length; i++) {
          btns[i].addEventListener("click", function() {
          var current = document.getElementsByClassName("active-br");
          current[0].className = current[0].className.replace(" active-br", "");
          this.className += " active-br";
          });
        }

        $('.br').click(function(){
            branch = this.id;
            getData();
        });

       function delete_click(clicked_id){
          $('#deleteModel').attr("href","{{url('deleteInventory')}}/"+clicked_id)
          $('#deleteCategoryModal').modal('show');
        }

        $('#inventoryTable').DataTable({
              "processing":true,
              "serverside":true,
              "bPaginate": false,
              "bInfo" : false,
              "pageLength": 50,
              "ajax":"{{url('ajaxInventories')}}/"+branch,
              "columns":[
                {"data" : "product_id"},
                {"data" : "initial_qty"},
                {"data" : "current_qty"},
                {"data" : "branch_id"},
                {"data" : "action"},
              ]
            });

        function getData(){
          $('#inventoryTable').DataTable().ajax.url("{{url('ajaxInventories')}}/"+branch).load();
        }

  </script>
@endsection