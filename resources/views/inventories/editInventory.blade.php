@extends('layouts.welcome')

@section('title' , 'Edit Inventory')

@section('styles')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />

    <style>
      .add-inventory-form{
        margin:0px 20%;
      }
      .error{
        color:red;
        font-size:1rem;
      }
      #submit{
        margin-top:20px;
        margin-bottom:20px;
      }
      #product{
          width:100%;
      }
      @media screen and (max-width: 600px) {
        .add-inventory-form{
          margin:0px 5%;
        }
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">
<h4 class="text-center"> Add Inventory </h4>
<form  class="add-inventory-form" enctype="multipart/form-data"
 action="{{route('updateInventory')}}" id="add_inventory" method="post">
@csrf
    <input type="hidden" name="id" value="{{ $inventory->id }}">
    <div class="form-group">
        <label for="userRole">Branch</label>
            <select name="branch_id" class="form-control" id="userRole">
            <option value="" disable>Select Branch Please</option>
            @foreach($branches as $branch)
            <option @if($inventory->branch_id == $branch->id ) selected @endif value="{{ $branch->id }}"> {{ $branch->name }} </option>
            @endforeach
            </select>
        @if($errors->has('branch_id'))
        <small id="userRoleHelp" class="form-text error">{{ $errors->first('branch_id') }}</small>
        @endif
    </div>

    <div class="form-group">
        <label for="userRole">Product</label>
            <select class="form-control" id="products" name="product_id">
                <option value="" disable>Select Product Please</option>
                @foreach($products as $product)
                <option @if($inventory->product_id == $product->id ) selected @endif value="{{ $product->id }}"> {{ $product->name }} </option>
                @endforeach
            </select>
        @if($errors->has('product_id'))
        <small id="userRoleHelp" class="form-text error">{{ $errors->first('product_id') }}</small>
        @endif
    </div>




  <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label for="productInput">Initial Qty</label>
          <div class="form-group">
            <input type="number" class="form-control" value="{{ $inventory-> initial_qty }}" name="initial_qty">
          </div>
          @if($errors->has('initial_qty'))
          <small id="branchTypeHelp" class="form-text error">{{ $errors->first('initial_qty') }}</small>
          @endif
        </div>
      </div>

      <div class="col-md-6">
      <div class="form-group">
            <label for="productInput">Current Qty</label>
            <div class="form-group">
                <input type="number" class="form-control" value="{{ $inventory-> current_qty }}" name="current_qty">        
            </div>
            @if($errors->has('current_qty'))
                <small id="current_qtyHelp" class="form-text error">{{ $errors->first('current_qty') }}</small>
            @endif
        </div>
      </div>
  </div>


  <button id="submit" class="btn btn-primary">Submit</button>
</form>

</div>
@endsection
@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script>    
        $(document).ready(function() {
            $('#products').select2();
        });
    </script>
@endsection
