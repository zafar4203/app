@extends('layouts.main')

@section('title' , 'Register')

@section('styles')
    <style>
        .card{
            margin:10% 25%;
        }
        form.user .form-control-user {
            font-size: .8rem;
            border-radius: 0rem;
            padding: 1.5rem 1rem;
        }
        form.user .btn-user {
            font-size: .8rem;
            border-radius: 0rem;
            padding: .75rem 1rem;
        }
    </style>
@endsection

@section('content')
<div class="card o-hidden border-0 shadow-lg my-5">

      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-12">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
              </div>
                    <form method="POST" class="user" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group">
                                <input id="name" type="text" class="form-control form-control-user @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" placeholder="Enter Name" >
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>

                        <div class="form-group">
                                <input id="phone" type="text" class="form-control form-control-user @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone" placeholder="Enter Mobile Number" >
                                @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>


                        <div class="form-group">
                                <input type="email" name="email" value="{{ old('email') }}" class="form-control form-control-user @error('email') is-invalid @enderror" required id="email" autocomplete="email"  placeholder="Email Address">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>

                        <div class="form-group">
                                <input id="password" type="password" class="form-control form-control-user @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Enter Password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>

                        <div class="form-group">
                                    <input id="password-confirm" type="password" class="form-control form-control-user " name="password_confirmation" required autocomplete="new-password" placeholder="Repeat Password">
                        </div>

                        <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-user btn-block">
                                    {{ __('Register') }}
                                </button>
                        </div>
                    </form>
                    <hr>
                        <div class="text-center">
                            <a class="small" href="{{url('login')}}">Already have an account? Login!</a>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>        
@endsection
