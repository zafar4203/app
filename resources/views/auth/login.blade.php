@extends('layouts.main')

@section('title' , 'Login')

@section('styles')
    <style>
        .card{
            margin:10% 25%;
        }
        form.user .form-control-user {
            font-size: .8rem;
            border-radius: 0rem;
            padding: 1.5rem 1rem;
        }
        form.user .btn-user {
            font-size: .8rem;
            border-radius: 0rem;
            padding: .75rem 1rem;
        }
    </style>
@endsection

@section('content')
<div class="card o-hidden border-0 shadow-lg my-5">

      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-12">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Login to Your Account!</h1>
              </div>
                <form method="POST" class="user" action="{{ route('login') }}">
                @csrf

                <div class="form-group">
                    <input type="email" name="email" value="{{ old('email') }}" class="form-control form-control-user @error('email') is-invalid @enderror" required id="email" placeholder="Email Address">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>
                <div class="form-group">
                    <input id="password" type="password" name="password" class="form-control form-control-user @error('password') is-invalid @enderror" id="exampleLastName" placeholder="Password" autocomplete="current-password" required >
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                </div>
                <button type="submit" class="btn btn-primary btn-user btn-block">
                                    {{ __('Login') }}
                </button>
                <!-- <hr>
                <a href="index.html" class="btn btn-google btn-user btn-block">
                  <i class="fab fa-google fa-fw"></i> Register with Google
                </a>
                <a href="index.html" class="btn btn-facebook btn-user btn-block">
                  <i class="fab fa-facebook-f fa-fw"></i> Register with Facebook
                </a> -->
              </form>
              <hr>


              <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>


              @if (Route::has('password.request'))
                <div class="text-center">
                    <a class="small" href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
                </div>
                @endif

              <!-- <div class="text-center">
                <a class="small" href="{{url('register')}}">Don't have an account? Register!</a>
              </div>             -->

            </div>
          </div>
        </div>
      </div>
    </div>
@endsection


