@extends('layouts.main')

@section('title' , 'Register')

@section('styles')
    <style>
        .card{
            margin:10% 20%;
            padding:1% 3%;
        }
        form.user .form-control-user {
            font-size: .8rem;
            border-radius: 0rem;
            padding: 1.5rem 1rem;
        }
        form.user .btn-user {
            font-size: .8rem;
            border-radius: 0rem;
            padding: .75rem 1rem;
        }
    </style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" class="user" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group">
                                <input id="email" type="email" class="form-control form-control-user @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email Address" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>

                        <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-user btn-block">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
