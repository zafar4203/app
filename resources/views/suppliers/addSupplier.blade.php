@extends('layouts.welcome')

@section('title' , 'Add Supplier')

@section('styles')
    <style>
      .add-branch-form{
        margin:0px 20%;
      }
      .error{
        color:red;
        font-size:1rem;
      }
      #submit{
        margin-top:20px;
        margin-bottom:20px;
      }
      @media screen and (max-width: 600px) {
        .add-branch-form{
          margin:0px 5%;
        }
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">
<form  class="add-branch-form" enctype="multipart/form-data"
 action="{{route('addSupplier')}}" id="add_branch" method="post">
@csrf


  <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <label for="categoryInput">Supplier Name</label>
          <input type="text" name="name" class="form-control" id="branchInput" value="{{old('name')}}" aria-describedby="branchNameHelp" placeholder="Enter supplier name">
          @if($errors->has('name'))
          <small id="branchNameHelp" class="form-text error">{{ $errors->first('name') }}</small>
          @endif
        </div>
      </div>
  </div>

  <div class="form-group">
    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="customRadio2" value="0" name="block" checked>
        <label class="custom-control-label" for="customRadio2">Active</label>
    </div>

    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="customRadio1" value="1" name="block">
        <label class="custom-control-label" for="customRadio1">De Active</label>
      </div>
  </div>


  <button id="submit" class="btn btn-primary">Submit</button>
</form>

</div>
@endsection

