@extends('layouts.welcome')

@section('title' , 'Add Branch')

@section('styles')
    <style>
      .add-branch-form{
        margin:0px 20%;
      }
      .error{
        color:red;
        font-size:1rem;
      }
      #submit{
        margin-top:20px;
        margin-bottom:20px;
      }
      @media screen and (max-width: 600px) {
        .add-branch-form{
          margin:0px 5%;
        }
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">
<form  class="add-branch-form" enctype="multipart/form-data"
 action="{{route('updateExpense')}}" id="add_branch" method="post">
@csrf
<input type="hidden" value="{{ $expense->id }}" name="id">
  <div class="form-group">
    <label for="categoryInput">Expense Name</label>
    <input type="text" name="name" class="form-control" id="branchInput" value="{{$expense->name}}" aria-describedby="branchNameHelp" placeholder="Enter Expense Title">
    @if($errors->has('name'))
    <small id="branchNameHelp" class="form-text error">{{ $errors->first('name') }}</small>
    @endif
  </div>

  <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label for="productInput">Supplier</label>
          <div class="form-group">
            <select class="form-control" name="supplier_id" >
              <option value="" disable> Select Supplier</option>
              @foreach($suppliers as $supplier)
              <option {{ $supplier->id == $expense->supplier_id ? 'selected':'' }} value="{{ $supplier->id }}">{{ $supplier->name }}</option>
              @endforeach
            </select>
          </div>
          @if($errors->has('supplier_id'))
          <small id="branchTypeHelp" class="form-text error">{{ $errors->first('supplier_id') }}</small>
          @endif
        </div>
      </div>

      <div class="col-md-6">
      <div class="form-group">
        <label>Invoice Number</label>
        <input type="number" name="invoice_number" class="form-control" value="{{$expense->invoice_number}}" aria-describedby="branchNameHelp" placeholder="Enter Invoice Number">
        @if($errors->has('invoice_number'))
        <small id="branchNameHelp" class="form-text error">{{ $errors->first('invoice_number') }}</small>
        @endif
      </div>
      </div>

      <div class="col-md-6">
      <div class="form-group">
        <label>Date</label>
        <input type="date" name="date" class="form-control" value="{{$expense->date}}" aria-describedby="branchNameHelp" placeholder="Enter Date">
        @if($errors->has('date'))
        <small id="branchNameHelp" class="form-text error">{{ $errors->first('date') }}</small>
        @endif
      </div>
      </div>


      <div class="col-md-6">
      <div class="form-group">
        <label>Amount</label>
        <input type="number" name="amount" class="form-control" value="{{$expense->amount}}" aria-describedby="branchNameHelp" placeholder="Enter Amount">
        @if($errors->has('amount'))
        <small id="branchNameHelp" class="form-text error">{{ $errors->first('amount') }}</small>
        @endif
      </div>
      </div>


      <div class="col-md-12">
      <div class="form-group">
        <label>Description</label>
        <textarea name="description" class="form-control" aria-describedby="branchNameHelp" placeholder="Enter Description">{{$expense->description}}</textarea>
        @if($errors->has('description'))
        <small id="branchNameHelp" class="form-text error">{{ $errors->first('description') }}</small>
        @endif
      </div>
      </div>


      <div class="col-md-6">
      <div class="form-group">
        <label for="productInput">Cash/Credit</label>
        <div class="form-group">
          <select class="form-control" name="type" >
            <option value="" disable> Select Type </option>
            <option {{$expense->type == 'Cash'? 'selected' : ''}} value="Cash">Cash</option>
            <option {{$expense->type == 'Credit'? 'selected' : ''}} value="Credit">Credit</option>
          </select>
        </div>
        @if($errors->has('type'))
        <small id="branchTypeHelp" class="form-text error">{{ $errors->first('type') }}</small>
        @endif
      </div>
          </div>
      </div>




      <div class="form-group">
    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="customRadio2" value="0" name="block" @if($expense->block == 0) checked @endif>
        <label class="custom-control-label" for="customRadio2">Active</label>
    </div>

    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="customRadio1" value="1" name="block" @if($expense->block == 1) checked @endif>
        <label class="custom-control-label" for="customRadio1">De Active</label>
      </div>
  </div>


  <button id="submit" class="btn btn-primary">Submit</button>
</form>

</div>
@endsection

