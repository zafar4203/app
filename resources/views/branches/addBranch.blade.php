@extends('layouts.welcome')

@section('title' , 'Add Branch')

@section('styles')
    <style>
      .add-branch-form{
        margin:0px 20%;
      }
      .error{
        color:red;
        font-size:1rem;
      }
      #submit{
        margin-top:20px;
        margin-bottom:20px;
      }
      @media screen and (max-width: 600px) {
        .add-branch-form{
          margin:0px 5%;
        }
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">
<form  class="add-branch-form" enctype="multipart/form-data"
 action="{{route('addBranch')}}" id="add_branch" method="post">
@csrf
  <div class="form-group">
    <label for="categoryInput">Branch Name</label>
    <input type="text" name="name" class="form-control" id="branchInput" value="{{old('name')}}" aria-describedby="branchNameHelp" placeholder="Enter branch name">
    @if($errors->has('name'))
    <small id="branchNameHelp" class="form-text error">{{ $errors->first('name') }}</small>
    @endif
  </div>

  <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label for="productInput">Branch Type</label>
          <div class="form-group">
            <select class="form-control" name="branch_type" >
              <option value="" disable> Select Branch Type </option>
              <option value="Own">Own</option>
              <option value="Franchise">Franchise</option>
            </select>
          </div>
          @if($errors->has('branch_type'))
          <small id="branchTypeHelp" class="form-text error">{{ $errors->first('branch_type') }}</small>
          @endif
        </div>
      </div>
      <div class="col-md-6">
      <div class="form-group">
    <label for="productInput">Shop Type</label>
    <div class="form-group">
      <select class="form-control" name="shop_type" >
        <option value="" disable> Select Shop Type </option>
        <option value="Kiosk Without Sitting">Kiosk Without Sitting</option>
        <option value="Kiosk With Sitting">Kiosk With Sitting</option>
        <option value="Shop With Sitting">Shop With Sitting</option>
      </select>
    </div>
    @if($errors->has('shop_type'))
    <small id="branchTypeHelp" class="form-text error">{{ $errors->first('shop_type') }}</small>
    @endif
  </div>
      </div>
  </div>

  <div class="form-group">
    <label for="categoryInput">Branch Location</label>
    <input type="text" name="location" class="form-control" id="branchLocationInput" value="{{old('location')}}" aria-describedby="branchLocationHelp" placeholder="Enter branch location">
    @if($errors->has('location'))
    <small id="branchLocationHelp" class="form-text error">{{ $errors->first('location') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="categoryInput">Branch City</label>
    <input type="text" name="city" class="form-control" id="branchCityInput" value="{{old('city')}}" aria-describedby="branchCityHelp" placeholder="Enter branch city">
    @if($errors->has('city'))
    <small id="branchCityHelp" class="form-text error">{{ $errors->first('city') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="categoryInput">Branch Country</label>
    <input type="text" name="country" class="form-control" id="branchCountryInput" value="{{old('country')}}" aria-describedby="branchCountryHelp" placeholder="Enter branch country">
    @if($errors->has('country'))
    <small id="branchCountryHelp" class="form-text error">{{ $errors->first('country') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="categoryInput">Annual Rent</label>
    <input type="text" name="annual_rent" class="form-control" id="branchAnnualRentInput" value="{{old('annual_rent')}}" aria-describedby="branchAnnualRentHelp" placeholder="Enter Branch Annual Rent">
    @if($errors->has('annual_rent'))
    <small id="branchAnnualRentHelp" class="form-text error">{{ $errors->first('annual_rent') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="branchPhone">Phone</label>
    <input type="number" name="phone" class="form-control" id="branchPhone" value="{{old('phone')}}" aria-describedby="branchAnnualRentHelp" placeholder="Enter Branch Phone Number">
    @if($errors->has('phone'))
    <small class="form-text error">{{ $errors->first('phone') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="branchWhatsapp">Whatsapp</label>
    <input type="number" name="whatsapp" class="form-control" id="branchWhatsapp" value="{{old('whatsapp')}}" aria-describedby="branchAnnualRentHelp" placeholder="Enter Branch Whatsapp">
    @if($errors->has('whatsapp'))
    <small id="branchAnnualRentHelp" class="form-text error">{{ $errors->first('whatsapp') }}</small>
    @endif
  </div>

  <button id="submit" class="btn btn-primary">Submit</button>
</form>

</div>
@endsection

