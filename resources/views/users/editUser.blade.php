@extends('layouts.welcome')

@section('title' , 'Edit User')

@section('styles')
    <style>
      .add-user-form{
        margin:0px 20%;
      }
    .error{
      color:red;
      font-size:1rem;
    }
    #submit{
      margin-top:20px;
      margin-bottom:20px;
    }

    @media screen and (max-width: 600px) {
        .add-user-form{
          margin:0px 5%;
        }
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">
<form  class="add-user-form" enctype="multipart/form-data"
 action="{{route('updateUser')}}" id="edit_user" method="post">

@if(session()->has('message'))
    <div id="alert" class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif

@csrf
    <input name="id" type="hidden" value="{{ $user->id }}">

  <div class="form-group">
    <label for="userInput">User Name</label>
    <input type="text" name="name" class="form-control" id="userInput" aria-describedby="productNameHelp" value="{{ $user->name }}" placeholder="Enter User Name">
    @if($errors->has('name'))
    <small id="userNameHelp" class="form-text error">{{ $errors->first('name') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="userPhone">User Phone</label>
    <input type="text" name="phone" class="form-control" id="userPhone" aria-describedby="userPhoneHelp" value="{{ $user->phone }}" placeholder="Enter User Phone">
    @if($errors->has('phone'))
    <small id="userPhoneHelp" class="form-text error">{{ $errors->first('phone') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="userEmail">User Email</label>
    <input type="text" name="email" class="form-control" id="userEmail" aria-describedby="userEmailHelp" value="{{ $user->email }}" placeholder="Enter User Email">
    @if($errors->has('email'))
    <small id="userEmailHelp" class="form-text error">{{ $errors->first('email') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="userPassword">User Password</label>
    <input type="text" name="password" class="form-control" id="userPassword" aria-describedby="userPasswordHelp" placeholder="Enter User Password">
    @if($errors->has('password'))
    <small id="userPasswordHelp" class="form-text error">{{ $errors->first('password') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="userPassword">User Pin Code</label>
    <input type="text" name="pin_code" class="form-control" id="userPinCode" aria-describedby="userPinCodeHelp" value="{{ $user->pin_code }}" placeholder="Enter User 4 digits Pin Code">
    @if($errors->has('pin_code'))
    <small id="userPinCodeHelp" class="form-text error">{{ $errors->first('pin_code') }}</small>
    @endif
  </div>

  <div class="form-group">
  <label for="userRole">Role</label>
  <select name="role" class="form-control" id="userRole">
    <option value="" disable>Select Role Please</option>
    @foreach($roles as $role)
     <option value="{{ $role->id }}" @if($role->id ==  $user->role_id) selected @endif> {{ $role->name }} </option>
    @endforeach
  </select>
  @if($errors->has('role'))
    <small id="userRoleHelp" class="form-text error">{{ $errors->first('role') }}</small>
  @endif


  <div class="form-group">
  <label for="userBranch">Branch</label>
  <select name="branch_id" class="form-control" id="userBranch">
    <option value="" disable>Select Branch Please</option>
    @foreach($branches as $branch)
     <option value="{{ $branch->id }}" @if($branch->id ==  $user->branch_id) selected @endif> {{ $branch->name }} </option>
    @endforeach
  </select>
  @if($errors->has('branch_id'))
    <small id="userBranchHelp" class="form-text error">{{ $errors->first('branch_id') }}</small>
  @endif

  <button id="submit" class="btn btn-primary">Submit</button>
</form>

</div>
@endsection
