@extends('layouts.welcome')

@section('title' , 'Roles')

@section('styles')
    <link href="{{asset('public/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
    <style>
      .error{
        color:red;
        font-size:1rem;
      }
      h5{
        color:#282830;
        font-weight:bold;
        text-transform:uppercase;
      }
      label{
        color:#282830;
        font-size:18px;
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">

@if(session()->has('message'))
    <div id="alert" class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif


      <!-- Page Heading -->
      <h1 class="h3 mb-2 text-gray-800">{{ $user->name }} Permissions</h1>
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Role Permissions </h6>
            </div>
            <div class="card-body">

                <form action="{{ route('userPermissionsSubmit') }}" method="POST">
                @csrf
                <input type="hidden" name="user_id" value="{{$user->id}}" />
                @if(\Spatie\Permission\Models\Permission::where('name','View Dashboard')->first())
                  <div class="row mt-3">
                    <div class="col-md-12">
                        <h5>Dashboard</h5>
                    </div>
                    @foreach($permissions as $permission)
                    @if(Str::contains($permission->name, 'Dashboard'))
                    <div class="col-md-3">
                        <input type="checkbox" {{$user->hasPermissionTo($permission->id)?'checked' : ''}} id="{{$permission->id}}" name="rp[{{$permission->name}}]" /> 
                        <label for="{{ $permission->id }}">{{ $permission->name }}</label>
                    </div>
                    @endif
                    @endforeach
                  </div>
                @endif


                @if(\Spatie\Permission\Models\Permission::where('name','View Products')->first())
                  <div class="row mt-4">
                    <div class="col-md-12">
                        <h5>Products</h5>
                    </div>
                    @foreach($permissions as $permission)
                    @if($permission->name == 'View Products' || $permission->name == 'Add Product' || $permission->name == 'Update Product' || $permission->name == 'Delete Product')
                    <div class="col-md-3">
                        <input type="checkbox" {{$user->hasPermissionTo($permission->id)?'checked' : ''}} id="{{$permission->id}}" name="rp[{{$permission->name}}]" /> 
                        <label for="{{ $permission->id }}">{{ $permission->name }}</label>
                    </div>
                    @endif
                    @endforeach
                  </div>
                @endif

                @if(\Spatie\Permission\Models\Permission::where('name','View Home Delivery')->first())
                  <div class="row mt-4">
                    <div class="col-md-12">
                        <h5>Home Delivery</h5>
                    </div>
                    @foreach($permissions as $permission)
                        @if($permission->name == 'View Home Delivery' || $permission->name == 'Add Items' || $permission->name == 'Update Order Item' || $permission->name == 'Delete Order Item' || $permission->name == 'Add Order')
                        <div class="col-md-3">
                            <input type="checkbox" {{$user->hasPermissionTo($permission->id)?'checked' : ''}} id="{{$permission->id}}" name="rp[{{$permission->name}}]" /> 
                            <label for="{{ $permission->id }}">{{ $permission->name }}</label>
                        </div>
                        @endif
                    @endforeach
                  </div>
                @endif


                @if(\Spatie\Permission\Models\Permission::where('name','View Branches')->first())
                  <div class="row mt-4">
                    <div class="col-md-12">
                        <h5>Branches</h5>
                    </div>
                    @foreach($permissions as $permission)
                        @if(Str::contains($permission->name, 'Branch'))
                        <div class="col-md-3">
                            <input type="checkbox" {{$user->hasPermissionTo($permission->id)?'checked' : ''}} id="{{$permission->id}}" name="rp[{{$permission->name}}]" /> 
                            <label for="{{ $permission->id }}">{{ $permission->name }}</label>
                        </div>
                        @endif
                    @endforeach
                  </div>
                @endif

                @if(\Spatie\Permission\Models\Permission::where('name','View Sales')->first())
                  <div class="row mt-4">
                    <div class="col-md-12">
                        <h5>Sales</h5>
                    </div>
                    @foreach($permissions as $permission)
                        @if(Str::contains($permission->name, 'View Sales'))
                        <div class="col-md-3">
                            <input type="checkbox" {{$user->hasPermissionTo($permission->id)?'checked' : ''}} id="{{$permission->id}}" name="rp[{{$permission->name}}]" /> 
                            <label for="{{ $permission->id }}">{{ $permission->name }}</label>
                        </div>
                        @endif
                    @endforeach
                  </div>
                @endif

                @if(\Spatie\Permission\Models\Permission::where('name','View Activities')->first())
                  <div class="row mt-4">
                    <div class="col-md-12">
                        <h5>Activities</h5>
                    </div>
                    @foreach($permissions as $permission)
                        @if(Str::contains($permission->name, 'View Activities'))
                        <div class="col-md-3">
                            <input type="checkbox" {{$user->hasPermissionTo($permission->id)?'checked' : ''}} id="{{$permission->id}}" name="rp[{{$permission->name}}]" /> 
                            <label for="{{ $permission->id }}">{{ $permission->name }}</label>
                        </div>
                        @endif
                    @endforeach
                  </div>
                @endif

                @if(\Spatie\Permission\Models\Permission::where('name','View Franchise Inquiries')->first())
                  <div class="row mt-4">
                    <div class="col-md-12">
                        <h5>Franchise Inquiries</h5>
                    </div>
                    @foreach($permissions as $permission)
                        @if(Str::contains($permission->name, 'View Franchise Inquiries'))
                        <div class="col-md-3">
                            <input type="checkbox" {{$user->hasPermissionTo($permission->id)?'checked' : ''}} id="{{$permission->id}}" name="rp[{{$permission->name}}]" /> 
                            <label for="{{ $permission->id }}">{{ $permission->name }}</label>
                        </div>
                        @endif
                    @endforeach
                  </div>
                @endif

                @if(\Spatie\Permission\Models\Permission::where('name','View Promos')->first())
                  <div class="row mt-4">
                    <div class="col-md-12">
                        <h5>Promos</h5>
                    </div>
                    @foreach($permissions as $permission)
                        @if(Str::contains($permission->name, 'Promo'))
                        <div class="col-md-3">
                            <input type="checkbox" {{$user->hasPermissionTo($permission->id)?'checked' : ''}} id="{{$permission->id}}" name="rp[{{$permission->name}}]" /> 
                            <label for="{{ $permission->id }}">{{ $permission->name }}</label>
                        </div>
                        @endif
                    @endforeach
                  </div>
                @endif


                @if(\Spatie\Permission\Models\Permission::where('name','View Inventory')->first())
                  <div class="row mt-4">
                    <div class="col-md-12">
                        <h5>Inventory</h5>
                    </div>
                    @foreach($permissions as $permission)
                        @if(Str::contains($permission->name, 'Inventory'))
                        <div class="col-md-3">
                            <input type="checkbox" {{$user->hasPermissionTo($permission->id)?'checked' : ''}} id="{{$permission->id}}" name="rp[{{$permission->name}}]" /> 
                            <label for="{{ $permission->id }}">{{ $permission->name }}</label>
                        </div>
                        @endif
                    @endforeach
                  </div>
                @endif


                @if(\Spatie\Permission\Models\Permission::where('name','View Combo Products')->first())
                  <div class="row mt-4">
                    <div class="col-md-12">
                        <h5>Combo Products</h5>
                    </div>
                    @foreach($permissions as $permission)
                        @if(Str::contains($permission->name, 'Combo Product'))
                        <div class="col-md-3">
                            <input type="checkbox" {{$user->hasPermissionTo($permission->id)?'checked' : ''}} id="{{$permission->id}}" name="rp[{{$permission->name}}]" /> 
                            <label for="{{ $permission->id }}">{{ $permission->name }}</label>
                        </div>
                        @endif
                    @endforeach
                  </div>
                @endif


                @if(\Spatie\Permission\Models\Permission::where('name','View Invisible Users')->first())
                  <div class="row mt-4">
                    <div class="col-md-12">
                        <h5>Invisible Users</h5>
                    </div>
                    @foreach($permissions as $permission)
                      @if($permission->name == 'View Invisible Users' || $permission->name == 'Add Invisible User' || $permission->name == 'Delete Invisible User' || $permission->name == 'Update Invisible User')                        
                      <div class="col-md-3">
                          <input type="checkbox" {{$user->hasPermissionTo($permission->id)?'checked' : ''}} id="{{$permission->id}}" name="rp[{{$permission->name}}]" /> 
                            <label for="{{ $permission->id }}">{{ $permission->name }}</label>
                        </div>
                        @endif
                    @endforeach
                  </div>
                @endif

                @if(\Spatie\Permission\Models\Permission::where('name','View Users')->first())
                  <div class="row mt-4">
                    <div class="col-md-12">
                        <h5>Users</h5>
                    </div>
                    @foreach($permissions as $permission)
                        @if($permission->name == 'View Users' || $permission->name == 'Add User' || $permission->name == 'Delete User' || $permission->name == 'Update User')
                        <div class="col-md-3">
                            <input type="checkbox" {{$user->hasPermissionTo($permission->id)?'checked' : ''}} id="{{$permission->id}}" name="rp[{{$permission->name}}]" /> 
                            <label for="{{ $permission->id }}">{{ $permission->name }}</label>
                        </div>
                        @endif
                    @endforeach
                  </div>
                @endif

                @if(\Spatie\Permission\Models\Permission::where('name','View Categories')->first())
                  <div class="row mt-4">
                    <div class="col-md-12">
                        <h5>Categories</h5>
                    </div>
                    @foreach($permissions as $permission)
                      @if($permission->name == 'View Categories' || $permission->name == 'Add Category' || $permission->name == 'Delete Category' || $permission->name == 'Update Category')
                        <div class="col-md-3">
                            <input type="checkbox" {{$user->hasPermissionTo($permission->id)?'checked' : ''}} id="{{$permission->id}}" name="rp[{{$permission->name}}]" /> 
                            <label for="{{ $permission->id }}">{{ $permission->name }}</label>
                        </div>
                        @endif
                    @endforeach
                  </div>
                @endif


                @if(\Spatie\Permission\Models\Permission::where('name','View Sub Categories')->first())
                  <div class="row mt-4">
                    <div class="col-md-12">
                        <h5>Sub Categories</h5>
                    </div>
                    @foreach($permissions as $permission)
                      @if($permission->name == 'View Sub Categories' || $permission->name == 'Add Sub Category' || $permission->name == 'Delete Sub Category' || $permission->name == 'Update Sub Category')
                        <div class="col-md-3">
                            <input type="checkbox" {{$user->hasPermissionTo($permission->id)?'checked' : ''}} id="{{$permission->id}}" name="rp[{{$permission->name}}]" /> 
                            <label for="{{ $permission->id }}">{{ $permission->name }}</label>
                        </div>
                        @endif
                    @endforeach
                  </div>
                @endif

                @if(\Spatie\Permission\Models\Permission::where('name','View Areas')->first())
                  <div class="row mt-4">
                    <div class="col-md-12">
                        <h5>Areas</h5>
                    </div>
                    @foreach($permissions as $permission)
                      @if($permission->name == 'Add Area' || $permission->name == 'Delete Area' || $permission->name == 'Update Area')
                        <div class="col-md-3">
                            <input type="checkbox" {{$user->hasPermissionTo($permission->id)?'checked' : ''}} id="{{$permission->id}}" name="rp[{{$permission->name}}]" /> 
                            <label for="{{ $permission->id }}">{{ $permission->name }}</label>
                        </div>
                        @endif
                    @endforeach
                  </div>
                @endif


                @if(\Spatie\Permission\Models\Permission::where('name','View Messages')->first())
                  <div class="row mt-4">
                    <div class="col-md-12">
                        <h5>Messages</h5>
                    </div>
                    @foreach($permissions as $permission)
                      @if($permission->name == 'View Messages')
                        <div class="col-md-3">
                            <input type="checkbox" {{$user->hasPermissionTo($permission->id)?'checked' : ''}} id="{{$permission->id}}" name="rp[{{$permission->name}}]" /> 
                            <label for="{{ $permission->id }}">{{ $permission->name }}</label>
                        </div>
                        @endif
                    @endforeach
                  </div>
                @endif


                @if(\Spatie\Permission\Models\Permission::where('name','View Customers')->first())
                  <div class="row mt-4">
                    <div class="col-md-12">
                        <h5>Customers</h5>
                    </div>
                    @foreach($permissions as $permission)
                      @if($permission->name == 'View Customers')
                        <div class="col-md-3">
                            <input type="checkbox" {{$user->hasPermissionTo($permission->id)?'checked' : ''}} id="{{$permission->id}}" name="rp[{{$permission->name}}]" /> 
                            <label for="{{ $permission->id }}">{{ $permission->name }}</label>
                        </div>
                        @endif
                    @endforeach
                  </div>
                @endif


                @if(\Spatie\Permission\Models\Permission::where('name','View Bulk Emails')->first())
                  <div class="row mt-4">
                    <div class="col-md-12">
                        <h5>Bulk Emails</h5>
                    </div>
                    @foreach($permissions as $permission)
                      @if($permission->name == 'View Bulk Emails')
                        <div class="col-md-3">
                            <input type="checkbox" {{$user->hasPermissionTo($permission->id)?'checked' : ''}} id="{{$permission->id}}" name="rp[{{$permission->name}}]" /> 
                            <label for="{{ $permission->id }}">{{ $permission->name }}</label>
                        </div>
                        @endif
                    @endforeach
                  </div>
                @endif

                @if(\Spatie\Permission\Models\Permission::where('name','View Popular Products')->first())
                  <div class="row mt-4">
                    <div class="col-md-12">
                        <h5>Popular Products</h5>
                    </div>
                    @foreach($permissions as $permission)
                      @if($permission->name == 'View Popular Products')
                        <div class="col-md-3">
                            <input type="checkbox" {{$user->hasPermissionTo($permission->id)?'checked' : ''}} id="{{$permission->id}}" name="rp[{{$permission->name}}]" /> 
                            <label for="{{ $permission->id }}">{{ $permission->name }}</label>
                        </div>
                        @endif
                    @endforeach
                  </div>
                @endif

                @if(\Spatie\Permission\Models\Permission::where('name','View Roles')->first())
                  <div class="row mt-4">
                    <div class="col-md-12">
                        <h5>Roles</h5>
                    </div>
                    @foreach($permissions as $permission)
                      @if($permission->name == 'View Roles' || $permission->name == 'Add Role' || $permission->name == 'Update Role' || $permission->name == 'Delete Role')
                        <div class="col-md-3">
                            <input type="checkbox" {{$user->hasPermissionTo($permission->id)?'checked' : ''}} id="{{$permission->id}}" name="rp[{{$permission->name}}]" /> 
                            <label for="{{ $permission->id }}">{{ $permission->name }}</label>
                        </div>
                        @endif
                    @endforeach
                  </div>
                @endif


                @if(\Spatie\Permission\Models\Permission::where('name','View Permissions')->first())
                  <div class="row mt-4">
                    <div class="col-md-12">
                        <h5>Permissions</h5>
                    </div>
                    @foreach($permissions as $permission)
                      @if($permission->name == 'View Permissions' || $permission->name == 'Add Permission' || $permission->name == 'Update Permission' || $permission->name == 'Delete Permission')
                        <div class="col-md-3">
                            <input type="checkbox" {{$user->hasPermissionTo($permission->id)?'checked' : ''}} id="{{$permission->id}}" name="rp[{{$permission->name}}]" /> 
                            <label for="{{ $permission->id }}">{{ $permission->name }}</label>
                        </div>
                        @endif
                    @endforeach
                  </div>
                @endif

                <div class="col-md-12 mt-5">
                    <div class="row">
                        <div class="mx-auto col-md-3">
                            <button type="submit" name="submit" class="btn btn-block btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
                </form>

            </div>
          </div>

        </div>
        <!-- /.container-fluid -->




          <!-- Logout Modal-->
  <div class="modal fade" id="deleteProductModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Are you sure to delete?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select <span class="error">"Delete"</span> below if you are ready to delete the product.</div>
        <div class="modal-footer">
          <button class="btn btn-success" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-danger" id="deleteModel" href="#">Delete</a>
        </div>
      </div>
    </div>
  </div>



@endsection

@section('scripts')
  <!-- Page level plugins -->
  <script src="{{asset('public/vendor/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('public/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

  <!-- Page level custom scripts -->
  <script src="{{asset('public/js/demo/datatables-demo.js')}}"></script>

  <script>
     $("#alert").fadeTo(2000, 500).slideUp(500, function(){
       $("#alert").slideUp(600);
       });

     
  </script>
@endsection