@extends('layouts.welcome')

@section('title' , 'User Logs')

@section('styles')
    <link href="{{asset('vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
    <style>
      .error{
        color:red;
        font-size:1rem;
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">

@if(session()->has('message'))
    <div id="alert" class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
      <!-- Page Heading -->
      <h4 class="mb-2 text-gray-800">{{ $user->name }} Logs</h4>
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">History </h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="logTable" width="100%" cellspacing="0">
                  <thead>
                      <tr>
                          <th>Date</th>
                          <th>Ip</th>
                          <th>Mac</th>
                          <th>Browser</th>
                          <th>Login</th>
                          <th>Logout</th>
                          <th>Status</th>
                      </tr>
                  </thead>
                  <tfoot>
                          <tr>
                            <th>Date</th>
                            <th>Ip</th>
                            <th>Mac</th>
                            <th>Browser</th>
                            <th>Login</th>
                            <th>Logout</th>
                            <th>Status</th>
                        </tr>
                  </tfoot>

                  <tbody>
                        @foreach($logs as $log)
                            <tr>
                                <td>{{$log->date}}</td>
                                <td>{{$log->ip}}</td>
                                <td>{{$log->mac}}</td>
                                <td>{{$log->browser}}</td>
                                <td>{{$log->login_time}}</td>
                                <td>{{$log->logout_time}}</td>
                                <td>{{$log->login_status}}</td>
                            </tr>
                        @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->


@endsection

@section('scripts')
  <!-- Page level plugins -->
  <script src="{{asset('vendor/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

  <!-- Page level custom scripts -->
  <script src="{{asset('js/demo/datatables-demo.js')}}"></script>

  <script>
  
        $(document).ready(function(){
            $('#logTable').dataTable();
        });
  </script>
@endsection