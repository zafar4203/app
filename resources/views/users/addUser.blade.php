@extends('layouts.welcome')

@section('title' , 'Add User')

@section('styles')
    <style>
      .add-user-form{
        margin:0px 20%;
      }
    .error{
      color:red;
      font-size:1rem;
    }
    #submit{
      margin-bottom:40px;
    }
    @media screen and (max-width: 600px) {
        .add-user-form{
          margin:0px 5%;
        }
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">
<form  class="add-user-form" enctype="multipart/form-data"
 action="{{route('addUser')}}" id="add_user" method="post">
@csrf
  <div class="form-group">
    <label for="userInput">User Name</label>
    <input type="text" name="name" class="form-control" id="userInput" aria-describedby="productNameHelp" value="{{ old('name') }}" placeholder="Enter User Name">
    @if($errors->has('name'))
    <small id="userNameHelp" class="form-text error">{{ $errors->first('name') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="userPhone">User Phone</label>
    <input type="text" name="phone" class="form-control" id="userPhone" aria-describedby="userPhoneHelp" value="{{ old('phone') }}" placeholder="Enter User Phone">
    @if($errors->has('phone'))
    <small id="userPhoneHelp" class="form-text error">{{ $errors->first('phone') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="userEmail">User Email</label>
    <input type="text" name="email" class="form-control" id="userEmail" aria-describedby="userEmailHelp" value="{{ old('email') }}" placeholder="Enter User Email">
    @if($errors->has('email'))
    <small id="userEmailHelp" class="form-text error">{{ $errors->first('email') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="userPassword">User Password</label>
    <input type="text" name="password" class="form-control" id="userPassword" aria-describedby="userPasswordHelp" placeholder="Enter User Password">
    @if($errors->has('password'))
    <small id="userPasswordHelp" class="form-text error">{{ $errors->first('password') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="userPassword">User Pin Code</label>
    <input type="text" name="pin_code" class="form-control" id="userPinCode" aria-describedby="userPinCodeHelp" value="{{ old('pin_code') }}" placeholder="Enter User 4 digits Pin Code">
    @if($errors->has('pin_code'))
    <small id="userPinCodeHelp" class="form-text error">{{ $errors->first('pin_code') }}</small>
    @endif
  </div>

    <div class="form-group">
      <label for="userRole">Role</label>
      <select name="role" class="form-control" id="userRole">
        <option value="" disable>Select Role Please</option>
        @foreach($roles as $role)
         <option value="{{ $role->id }}"> {{ $role->name }} </option>
        @endforeach
      </select>
      @if($errors->has('role'))
        <small id="userRoleHelp" class="form-text error">{{ $errors->first('role') }}</small>
      @endif
    </div>


    <div class="form-group">
      <label for="userRole">Branch</label>
      <select name="branch_id" class="form-control" id="userRole">
        <option value="" disable>Select Branch Please</option>
        @foreach($branches as $branch)
         <option value="{{ $branch->id }}"> {{ $branch->name }} </option>
        @endforeach
      </select>
      @if($errors->has('branch_id'))
        <small id="userRoleHelp" class="form-text error">{{ $errors->first('branch_id') }}</small>
      @endif

    </div>

  <button id="submit" class="btn btn-primary">Submit</button>
</form>

</div>
@endsection
