@extends('layouts.welcome')

@section('title' , 'Add Sale')

@section('styles')
    <style>
      .add-area-form{
        margin:0px 20%;
      }
    .error{
      color:red;
      font-size:1rem;
    }
    @media screen and (max-width: 600px) {
        .add-area-form{
          margin:0px 5%;
        }
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">
<form  class="add-area-form" enctype="multipart/form-data"
 action="{{route('addSale')}}" id="add_category" method="post">
@csrf
  <input type="hidden" name="type"  value="{{ $type }}">
  <div class="form-group">
    <label for="categoryInput">Select Aggregator</label>
      <select class="form-control" name="aggregator_id" id="" required>
        <option value="">Select Aggregator</option>
        @foreach($aggregators as $aggregator)
            <option value="{{ $aggregator->id }}">{{ $aggregator->name }}</option>
        @endforeach
        @if($errors->has('aggregator_id'))
            <small id="categoryDescriptionHelp" class="form-text error">{{ $errors->first('aggregator_id') }}</small>
        @endif
      </select>
  </div>

  <div class="form-group">
    <label for="description">Amount</label>
    <input type="number" name="cash" class="form-control" id="description" value="{{old('cash')}}" placeholder="Enter Amount">
    @if($errors->has('cash'))
    <small id="categoryDescriptionHelp" class="form-text error">{{ $errors->first('cash') }}</small>
    @endif
  </div>


  <div class="form-group">
    <label for="description">Date</label>
    <input type="date" name="date" class="form-control" id="description" value="{{old('date')}}" placeholder="Enter Date">
    @if($errors->has('date'))
    <small id="categoryDescriptionHelp" class="form-text error">{{ $errors->first('date') }}</small>
    @endif
  </div>

  <button id="submit" class="btn btn-primary">Submit</button>
</form>

</div>
@endsection
