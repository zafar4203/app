@extends('layouts.welcome')
@section('title' , 'Sales')

@section('styles')
    <link href="{{asset('public/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
    <style>
      .error{
        color:red;
        font-size:1rem;
      }
      .ml-00{
          margin-left:1px !important;
      }
     .active-yr, .btn:hover {
        background-color: #4841a8;
        color: white;
      }
      .active-mn, .btn:hover {
        background-color: #4841a8;
        color: white;
      }
      .active-br, .btn:hover {
        background-color: #4841a8;
        color: white;
      }
      div.dataTables_wrapper div.dataTables_filter input {
            margin-left: 0.5em;
            display: inline-block;
            width: auto;
            margin-right: 5px;
        }
        .dt-button{
          float:right;
          z-index:9999;
          position:relative;
        }

      @media screen and (max-width: 600px) {
        .btn-group{
            display:inline-block;
        }
        .mn{
            margin-top:10px;
        }
        .br{
            margin-top:10px;
        }
        .dt-button{
            margin-top:5px;
            margin-bottom:5px;
        }
      }   
    </style>
@endsection

@section('content')
<div class="container-fluid">

@if(session()->has('message'))
    <div id="alert" class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif


@if(session()->has('error'))
    <div id="alert" class="alert alert-danger">
        {{ session()->get('error') }}
    </div>
@endif

      <!-- Page Heading -->
      <h1 class="h3 mb-2 text-gray-800">Shop Sales  </h1>

        <div class="row">
            <div class="col-md-12">
                <div class="btn-group" id="years">
                    <button type="button" id="2020" class="yr btn btn-primary">2020</button>
                    <button type="button" id="2021" class="ml-00 yr btn btn-primary">2021</button>
                    <button type="button" id="2022" class="ml-00 yr btn btn-primary">2022</button>
                    <button type="button" id="2023" class="ml-00 yr btn btn-primary">2023</button>
                </div>
            </div>

            <div class="col-md-12 mt-2">
                <div class="btn-group" id="months">
                    <button type="button" id="1" class="btn mn btn-primary">Jan</button>
                    <button type="button" id="2" class="ml-00 mn btn btn-primary">Feb</button>
                    <button type="button" id="3" class="ml-00 mn btn btn-primary">March</button>
                    <button type="button" id="4" class="ml-00 mn btn btn-primary">April</button>
                    <button type="button" id="5" class="ml-00 mn btn btn-primary">May</button>
                    <button type="button" id="6" class="ml-00 mn btn btn-primary">June</button>
                    <button type="button" id="7" class="ml-00 mn btn btn-primary">July</button>
                    <button type="button" id="8" class="ml-00 mn btn btn-primary">Aug</button>
                    <button type="button" id="9" class="ml-00 mn btn btn-primary">Sep</button>
                    <button type="button" id="10" class="ml-00 mn btn btn-primary">Oct</button>
                    <button type="button" id="11" class="ml-00 mn btn btn-primary">Nov</button>
                    <button type="button" id="12" class="ml-00 mn btn btn-primary">Dec</button>
                </div>
            </div>

            <div class="col-md-12 mt-2 mb-2">
                <div class="btn-group" id="branches">
                @foreach($branches as $branch)
                    @if($branch->name == "Kitchen")
                    <button type="button" id="{{$branch->id}}" class="ml-00 btn br btn-primary">Online Sale</button>
                    @else
                    <button type="button" id="{{$branch->id}}" class="ml-00 btn br btn-primary">{{$branch->name}}</button>
                    @endif
                 @endforeach 
                 @if(count($branches) > 0)
                     <button type="button" id="all" class="ml-00 btn br btn-primary">All Branches</button>
                 @endif
                 </div>
            </div>

            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <input type="date" class="form-control" id="start_date" />
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <input type="date" class="form-control" id="end_date" />
                        </div>
                    </div>
                    <div class="col-md-1">
                        <button class="btn btn-primary btn-block" id="filter_date">Filter</button>
                    </div>
                    <div class="col-md-2 pl-0">
                        <button class="btn btn-danger" id="clear_date">Clear Filter</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
        <div class="card-body">  
            <div id="chart">
                
            </div>
        </div>
        </div>

    </div>
    <!-- /.container-fluid -->


@endsection

@section('scripts')

   <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.2/Chart.min.js"></script>
 

  <script>
     $("#alert").fadeTo(2000, 500).slideUp(500, function(){
       $("#alert").slideUp(600);
       });

       var start_date = "";
       var end_date = "";


        // Selection Area
        var d = new Date();
        var year = d.getFullYear();
        var month = d.getMonth() + 1;
        var branch = $('.br').eq(0).attr('id');
        $('#'+year).addClass('active-yr');
        $('#'+month).addClass('active-mn');
        $(".br:first").addClass('active-br');

        // Add active class to the current button (highlight it)
        var header = document.getElementById("years");
        var btns = header.getElementsByClassName("yr");
        for (var i = 0; i < btns.length; i++) {
        btns[i].addEventListener("click", function() {
        var current = document.getElementsByClassName("active-yr");
        current[0].className = current[0].className.replace(" active-yr", "");
        this.className += " active-yr";
        });
        }

        // Add active class to the current button (highlight it)
        var header = document.getElementById("months");
        var btns = header.getElementsByClassName("mn");
        for (var i = 0; i < btns.length; i++) {
          btns[i].addEventListener("click", function() {
          var current = document.getElementsByClassName("active-mn");
          current[0].className = current[0].className.replace(" active-mn", "");
          this.className += " active-mn";
          });
        }

        // Add active class to the current button (highlight it)
        var header = document.getElementById("branches");
        var btns = header.getElementsByClassName("br");
        for (var i = 0; i < btns.length; i++) {
          btns[i].addEventListener("click", function() {
          var current = document.getElementsByClassName("active-br");
          current[0].className = current[0].className.replace(" active-br", "");
          this.className += " active-br";
          });
        }

        $('.yr').click(function(){
            year = this.id;
            getData();
        });

        $('.mn').click(function(){
            month = this.id;
            getData();
        });

        $('.br').click(function(){
            branch = this.id;
            getData();
        });

        $('#start_date').change(function() {
            var date = $(this).val();
            start_date = date;
            console.log(date, 'start')
        });

        $('#end_date').change(function() {
            var date = $(this).val();
            end_date = date;
            console.log(date, 'end')
        });

        $('#filter_date').click(function(){
            if(!start_date && !end_date){
                alert("Please Enter Start Date & End Date");
            }else if(start_date && !end_date){
                alert("Please Enter End Date");
            }else if(!start_date && end_date){
                alert("Please Enter Start Date");
            }

            if(start_date && end_date){
                if (Date.parse(start_date) > Date.parse(end_date)) {
                    alert('Start Date must be lesser than End Date');
                }else{
                    getData();
                }
            }
        });

        $('#clear_date').click(function(){
            clear_date();
        });

        function clear_date(){
            $('#start_date').val('');
            $('#end_date').val('');

            start_date = "";
            end_date = "";
        }

  
    $(document).ready(function(){
        getData();
    });

    function getData(){
        var url = "{{url('ajaxSummaryCharts')}}/"+year+"/"+month+"/"+branch+"/"+start_date+"/"+end_date;
        var max = 0;
        $.get(url, function(data, status){
            max = Math.max.apply(Math,data.data[0].data);
            max = max * 1.5;

            $('#chart').html("").append("<div id='msg'></div><canvas id='myChart'></canvas>");
            if(data.multi == false){
                if(data.length != 0){
                    $('#msg').html("");
                    var ctx = document.getElementById("myChart").getContext('2d');                  
                    var myChart = new Chart(ctx, {
                    type: 'line',
                    data: {
                        labels: data.labels,
                        datasets: data.data
                    },
                    options: {
                            responsive: true,
                            legend: {
                                position: 'bottom',
                            },
                            hover: {
                                mode: 'label'
                            },
                            scales: {
                                xAxes: [{
                                        display: true,
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Month'
                                        }
                                    }],
                                yAxes: [{
                                        display: true,
                                        ticks: {
                                            beginAtZero: true,
                                            steps: 10,
                                            stepValue: 5,
                                            max: max
                                        }
                                    }]
                            },            
                        }
                    });
                }else{
                    $('#msg').html("").append("<h2 class='text-center'>No Data Found</h2>");
                }
            }

            if(data.multi == true){
                console.log(data);
                var ctx = document.getElementById("myChart").getContext('2d');
                ctx.clearRect(0, 0, ctx.width, ctx.height);

                $('#msg').html("");
                $('#myChart').css("display" , 'block');
                var ctx = document.getElementById("myChart").getContext('2d');
                var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: data.labels,
                    datasets: data.data
                },
                });
            }
        });
    }

    $(".dt-button").addClass("mr-1 btn btn-sm btn-primary");
  
  </script>
@endsection