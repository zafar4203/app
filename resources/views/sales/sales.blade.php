@extends('layouts.welcome')

@section('title' , 'Sales')

@section('styles')
    <link href="{{asset('public/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
    <style>
      .error{
        color:red;
        font-size:1rem;
      }
      .ml-00{
          margin-left:1px !important;
      }
     .active-yr, .btn:hover {
        background-color: #4841a8;
        color: white;
      }
      .active-mn, .btn:hover {
        background-color: #4841a8;
        color: white;
      }
      .active-br, .btn:hover {
        background-color: #4841a8;
        color: white;
      }


      div.dataTables_wrapper div.dataTables_filter input {
            margin-left: 0.5em;
            display: inline-block;
            width: auto;
            margin-right: 5px;
        }
        .dt-button{
          float:right;
          z-index:9999;
          position:relative;
        }

      @media screen and (max-width: 600px) {
        .btn-group{
            display:inline-block;
        }
        .mn{
            margin-top:10px;
        }
        .br{
            margin-top:10px;
        }
        .dt-button{
            margin-top:5px;
            margin-bottom:5px;
        }
      }

   
    </style>
@endsection

@section('content')
<div class="container-fluid">

@if(session()->has('message'))
    <div id="alert" class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif


@if(session()->has('error'))
    <div id="alert" class="alert alert-danger">
        {{ session()->get('error') }}
    </div>
@endif

      <!-- Page Heading -->
      <h1 class="h3 mb-2 text-gray-800">Shop Sales  <a href="{{ route('summary-charts') }}"><small class="mr-1 btn btn-primary float-right">Chart</small></a><a href="{{ route('summary-sales') }}"><small class="mr-1 btn btn-primary float-right">Summary</small></a> <a href="#"><small class="mr-1 btn btn-primary float-right">Add Sale</small></a></h1>

        <div class="row">
            <div class="col-md-12">
                <div class="btn-group" id="years">
                    <button type="button" id="2020" class="yr btn btn-primary">2020</button>
                    <button type="button" id="2021" class="ml-00 yr btn btn-primary">2021</button>
                    <button type="button" id="2022" class="ml-00 yr btn btn-primary">2022</button>
                    <button type="button" id="2023" class="ml-00 yr btn btn-primary">2023</button>
                </div>
            </div>

            <div class="col-md-12 mt-2">
                <div class="btn-group" id="months">
                    <button type="button" id="1" class="btn mn btn-primary">Jan</button>
                    <button type="button" id="2" class="ml-00 mn btn btn-primary">Feb</button>
                    <button type="button" id="3" class="ml-00 mn btn btn-primary">March</button>
                    <button type="button" id="4" class="ml-00 mn btn btn-primary">April</button>
                    <button type="button" id="5" class="ml-00 mn btn btn-primary">May</button>
                    <button type="button" id="6" class="ml-00 mn btn btn-primary">June</button>
                    <button type="button" id="7" class="ml-00 mn btn btn-primary">July</button>
                    <button type="button" id="8" class="ml-00 mn btn btn-primary">Aug</button>
                    <button type="button" id="9" class="ml-00 mn btn btn-primary">Sep</button>
                    <button type="button" id="10" class="ml-00 mn btn btn-primary">Oct</button>
                    <button type="button" id="11" class="ml-00 mn btn btn-primary">Nov</button>
                    <button type="button" id="12" class="ml-00 mn btn btn-primary">Dec</button>
                </div>
            </div>

            <div class="col-md-12 mt-2 mb-2">
                <div class="btn-group" id="branches">
                @foreach($branches as $branch)
                    @if($branch->name == "Kitchen")
                    <button type="button" id="{{$branch->id}}" class="ml-00 btn br btn-primary">Online Sale</button>
                    @else
                    <button type="button" id="{{$branch->id}}" class="ml-00 btn br btn-primary">{{$branch->name}}</button>
                    @endif
                 @endforeach 
                 </div>
            </div>

            

        </div>

     <!-- DataTales Example -->
     <div class="card shadow mb-4">
        <div class="card-body">  
            <div id="chart">
                
            </div>
        </div>
      </div>



          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="categoryTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Date</th>
                      <th>Cash</th>
                      <th>Card</th>
                      <th>Total</th>
                      <th>Vat</th>
                      <th>Cash Deposit</th>
                      <th>Closing</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                    <th>Date</th>
                    <th id="cash">Cash</th>
                    <th id="card">Card</th>
                    <th id="total">Total</th>
                    <th id="vat">Vat</th>
                    <th id="cash_deposit">Cash Deposit</th>
                    <th id="closing">Closing</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->


          <!-- Logout Modal-->
  <div class="modal fade" id="deleteCategoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Enter Cash Deposit Amount</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="form-group mr-2 ml-2 mt-2">
          <input type="hidden" id="sale_id" value="">
          <input id="cash_amount" type="number" class="form-control" placeholder="Enter Cash Amount" />
        </div>
        <div class="modal-footer">
          <button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-success" style="color:white;" id="submitModel">Submit</a>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.2/Chart.min.js"></script>

  <!-- Page level plugins -->
  <script src="{{asset('public/vendor/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('public/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

  <!-- Page level custom scripts -->
  <script src="{{asset('public/js/demo/datatables-demo.js')}}"></script>

  <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>  
  <!-- <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>   -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>  
  <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>  
  <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>  
  <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.colVis.min.js"></script>  
  

  <script>
     $("#alert").fadeTo(2000, 500).slideUp(500, function(){
       $("#alert").slideUp(600);
       });

        // Selection Area
        var d = new Date();
        var year = d.getFullYear();
        var month = d.getMonth() + 1;
        var branch = $('.br').eq(0).attr('id');
        $('#'+year).addClass('active-yr');
        $('#'+month).addClass('active-mn');
        $(".br:first").addClass('active-br');

        // Add active class to the current button (highlight it)
        var header = document.getElementById("years");
        var btns = header.getElementsByClassName("yr");
        for (var i = 0; i < btns.length; i++) {
        btns[i].addEventListener("click", function() {
        var current = document.getElementsByClassName("active-yr");
        current[0].className = current[0].className.replace(" active-yr", "");
        this.className += " active-yr";
        });
        }


                // Add active class to the current button (highlight it)
        var header = document.getElementById("months");
        var btns = header.getElementsByClassName("mn");
        for (var i = 0; i < btns.length; i++) {
          btns[i].addEventListener("click", function() {
          var current = document.getElementsByClassName("active-mn");
          current[0].className = current[0].className.replace(" active-mn", "");
          this.className += " active-mn";
          });
        }

        // Add active class to the current button (highlight it)
        var header = document.getElementById("branches");
        var btns = header.getElementsByClassName("br");
        for (var i = 0; i < btns.length; i++) {
          btns[i].addEventListener("click", function() {
          var current = document.getElementsByClassName("active-br");
          current[0].className = current[0].className.replace(" active-br", "");
          this.className += " active-br";
          });
        }

        $('.yr').click(function(){
            year = this.id;
            getData();
        });

        $('.mn').click(function(){
            month = this.id;
            getData();
        });

        $('.br').click(function(){
            branch = this.id;
            getData();
        });

       function delete_click(clicked_id){
          $('#deleteCategoryModal').modal('show');
          $('#sale_id').val(clicked_id);
        }

        $('#submitModel').click(function(){
          var val = $('#cash_amount').val();
          if(!val){
            alert("Cash Deposit Amount is Required");
          }

          $.ajax({
                    headers:
                    { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    url: "{{route('cashDeposit')}}",
                    method: 'POST',
                    data:{
                        'sale_id' : $('#sale_id').val(),
                        'cash_deposit' : val,

                    },
                    success: function(data) {
                        if(data){               
                            location.reload();
                        } 
                    }
                });

        });

        var tbl = $('#categoryTable');
        $('#categoryTable').DataTable({
              "fnDrawCallback": function( oSettings ) {
                var cash = 0;
                $('#categoryTable > tbody  > tr td:nth-child(2)').each(function(index, td) { 
                    cash = cash + parseFloat($(td).text());
                });

                var card = 0;
                $('#categoryTable > tbody  > tr td:nth-child(3)').each(function(index, td) { 
                    card = card + parseFloat($(td).text());
                });

                var vat = 0;
                $('#categoryTable > tbody  > tr td:nth-child(5)').each(function(index, td) { 
                    vat = vat + parseFloat($(td).text());
                });

                var cd = 0;
                $('#categoryTable > tbody  > tr td:nth-child(6)').each(function(index, td) { 
                    cd = cd + parseFloat($(td).text());
                });

                var total = 0;
                $('#categoryTable > tbody  > tr td:nth-child(4)').each(function(index, td) { 
                    total = total + parseFloat($(td).text());
                });

                $('#total').text(total.toFixed(2));
                $('#vat').text(vat.toFixed(2));
                $('#cash').text(cash.toFixed(2));
                $('#card').text(card.toFixed(2));
                $('#cash_deposit').text(cd.toFixed(2));
              },
              "processing":true,
              "serverside":true,
              "bPaginate": false,
              "bInfo" : false,
              "pageLength": 50,
              "ajax":"{{url('ajaxSales')}}/"+year+"/"+month+"/"+branch,
              "columns":[
                {"data" : "date"},
                {"data" : "cash"},
                {"data" : "card"},
                {"data" : "total"},
                {"data" : "vat"},
                {"data" : "cash_deposit"},
                {"data" : "closing"},
              ],
              order:[0,'desc'],
              dom: 'Bfrtip',
              buttons: [
                  {
                      extend: 'print',
                      exportOptions: {
                          columns: ':visible'
                      }
                  },
                  {
                      extend: 'excelHtml5',
                      exportOptions: {
                          columns: ':visible'
                      }
                  },
                  // {
                  //     extend: 'pdfHtml5',
                  //     exportOptions: {
                  //        columns: ':visible'
                  //     },
                  //     orientation: 'portrait',
                  //     pageSize: 'LEGAL'
                  // },
                  {
                      extend:'pdfHtml5',
                      text:'PDF',
                      orientation:'landscape',
                      exportOptions: {
                         columns: ':visible'
                      },
                      customize : function(doc){
                          var colCount = new Array();
                          $(tbl).find('tbody tr:first-child td').each(function(){
                              if($(this).attr('colspan')){
                                  for(var i=1;i<=$(this).attr('colspan');$i++){
                                      colCount.push('*');
                                  }
                              }else{ colCount.push('*'); }
                          });
                          doc.content[1].table.widths = colCount;
                      }
                  },
                  'colvis'
              ]         
            });

            getData();

        function getData(){
        $('#categoryTable').DataTable().ajax.url("{{url('ajaxSales')}}/"+year+"/"+month+"/"+branch).load();


        var url = "{{url('ajaxSummaryCharts')}}/"+year+"/"+month+"/"+branch;
        var max = 0;
        $.get(url, function(data, status){
            max = Math.max.apply(Math,data.data[0].data);
            max = max * 1.5;

            $('#chart').html("").append("<div id='msg'></div><canvas id='myChart'></canvas>");
            if(data.multi == false){
                if(data.length != 0){
                    $('#msg').html("");
                    var ctx = document.getElementById("myChart").getContext('2d');                  
                    var myChart = new Chart(ctx, {
                    type: 'line',
                    data: {
                        labels: data.labels,
                        datasets: data.data
                    },
                    options: {
                            responsive: true,
                            legend: {
                                position: 'bottom',
                            },
                            hover: {
                                mode: 'label'
                            },
                            scales: {
                                xAxes: [{
                                        display: true,
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Month'
                                        }
                                    }],
                                yAxes: [{
                                        display: true,
                                        ticks: {
                                            beginAtZero: true,
                                            steps: 10,
                                            stepValue: 5,
                                            max: max
                                        }
                                    }]
                            },            
                        }
                    });
                }else{
                    $('#msg').html("").append("<h2 class='text-center'>No Data Found</h2>");
                }
            }

        });

        }

        $(".dt-button").addClass("mr-1 btn btn-sm btn-primary");

  </script>
@endsection