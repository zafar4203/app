@extends('layouts.welcome')

@section('title' , 'Edit Product')

@section('styles')
    <style>
      .edit-product-form{
        margin:0px 20%;
      }
    .error{
      color:red;
      font-size:1rem;
    }
    .submit-btn{
        margin-top:20px; 
        margin-bottom:20px; 
      }
      #grouped_items{
        display:none;
      }

      @media screen and (max-width: 600px) {
        .edit-product-form{
          margin:0px 5%;
        }
      }

      #landscape img{
        height:150px;
        width:150px;
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">

<form  class="edit-product-form" enctype="multipart/form-data"
 action="{{url('updateProduct')}}" id="edit_product" method="post">

 @if(session()->has('message'))
    <div id="alert" class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif


@csrf
  <input type="hidden" name="id" value="{{$product->id}}">

  <!-- <div class="form-group">
    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="customRadio2" value="Regular" name="type" @if($product->type == "Regular") checked @endif>
        <label class="custom-control-label" for="customRadio2">Regular</label>
    </div>

    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="customRadio1" value="Grouped" name="type" @if($product->type == "Grouped") checked @endif>
        <label class="custom-control-label" for="customRadio1">Grouped Item</label>
    </div>
  </div>  -->
  <input type="hidden" name="type" value="Regular" />

  <div class="form-group">
    <label for="productInput">Product Category</label>
    <div class="form-group">
      <select class="form-control" id="category" name="category" >
        @foreach($categories as $category)
         <option value="{{ $category->id }}" @if($category->id == $product->category_id) selected @endif> {{ $category->name }}</option>
        @endforeach
      </select>
    </div>
    @if($errors->has('category'))
    <small id="productCategoryHelp" class="form-text error">{{ $errors->first('category') }}</small>
    @endif
  </div>


  <div class="form-group">
    <label for="productInput">Product Sub Category</label>
    <div class="form-group">
      <select class="form-control" id="subcategory" name="subcategory" >
      <option value="" disable> Select SubCategory </option>
        @foreach($subcategories as $subcategory)
         <option value="{{ $subcategory->id }}" @if($subcategory->id == $product->subcategory_id) selected @endif> {{ $subcategory->name }}</option>
        @endforeach
      </select>
    </div>
    @if($errors->has('subcategory'))
      <small id="productCategoryHelp" class="form-text error">{{ $errors->first('subcategory') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="productInput">Product Name</label>
    <input type="text" name="name" class="form-control" value="{{$product->name}}" id="productInput" aria-describedby="productNameHelp" placeholder="Enter product name">
    @if($errors->has('name'))
    <small id="productNameHelp" class="form-text error">{{ $errors->first('name') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="productShortInput">Product Short Name</label>
    <input type="text" name="short_name" class="form-control" value="{{$product->short_name}}" id="productShortInput" aria-describedby="productShortNameHelp" placeholder="Enter product short name">
    @if($errors->has('short_name'))
    <small id="productShortNameHelp" class="form-text error">{{ $errors->first('short_name') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="productPrice">Product Price</label>
    <input type="text" name="price" class="form-control" value="{{$product->price}}" id="productPrice" aria-describedby="productPriceHelp" placeholder="Enter product price">
    @if($errors->has('price'))
    <small id="productPriceHelp" class="form-text error">{{ $errors->first('price') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="productPrice">Product Invisible Price</label>
    <input type="text" name="invisible_price" class="form-control" value="{{$product->invisible_price}}" id="productPrice" aria-describedby="productPriceHelp" placeholder="Enter product Invisible price">
    @if($errors->has('invisible_price'))
    <small id="productPriceHelp" class="form-text error">{{ $errors->first('invisible_price') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="productPrice">Product Combo Price</label>
    <input type="text" name="combo_price" class="form-control" value="{{$product->combo_price}}" id="productPrice" aria-describedby="productPriceHelp" placeholder="Enter product Combo price">
    @if($errors->has('combo_price'))
    <small id="productPriceHelp" class="form-text error">{{ $errors->first('combo_price') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="productPrice">Product Details</label>
    <textarea rows="5" type="text" name="details" class="form-control"  id="productDetails" aria-describedby="productPriceHelp" placeholder="Enter Product Details">{{ $product->details }}</textarea>
    @if($errors->has('details'))
    <small id="productDetails" class="form-text error">{{ $errors->first('details') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="productInput">Product For</label>
    <div class="form-group">
      <select class="form-control" name="is_for" >
        <option value="" disable> Product For ? </option>
        <option @if($product->is_for == "web") selected @endif value="web"> Web </option>
        <option @if($product->is_for == "pos") selected @endif value="pos"> POS </option>
        <option @if($product->is_for == "both") selected @endif value="both"> Both </option>
      </select>
    </div>
    @if($errors->has('is_for'))
    <small id="productCategoryHelp" class="form-text error">{{ $errors->first('is_for') }}</small>
    @endif
  </div>


  <div class="form-group">
    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="approve" value="0" name="block" @if($product->block == 0) checked @endif>
        <label class="custom-control-label" for="approve">Approve</label>
    </div>

    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="block" value="1" name="block" @if($product->block == 1) checked @endif>
        <label class="custom-control-label" for="block">Block</label>
      </div>
  </div>

  <!-- <div class="form-group">
        <input type="file" name="image[]" multiple>
        @if($errors->has('image'))
          <small id="productImageHelp" class="form-text error">{{ $errors->first('image') }}</small>
        @endif
  </div>     -->
  <input type="hidden" id="feature_photo" name="photo" value="{{ $product->photo }}" accept="image/*">

  <div class="panel panel-body">
    <div class="span4 cropme text-center" id="landscape"
      style="width: 100%; height: 285px; border: 1px dashed #ddd; background: #f1f1f1;">
      <a href="javascript:;" id="crop-image" class=" mybtn1" style="">
        <i class="icofont-upload-alt"></i> {{ __('Upload Image Here') }}
      </a>
    </div>
  </div>
  @if($errors->has('photo'))
    <small id="productImageHelp" class="form-text error">{{ $errors->first('photo') }}</small>
  @endif


  <div id="grouped_items">
    <h4>Grouped Items</h4>
      <div class="row">
        <div class="col-md-2">Select</div>
        <div class="col-md-5">Item Name</div>
        <div class="col-md-5">Item Price</div>
      </div>

      <div id="grouped_items_rows"></div>
  </div>


  <button id="submit" class="btn submit-btn btn-primary">Submit</button>
</form>

</div>
@endsection
@section('scripts')
<script>
     $("#alert").fadeTo(2000, 500).slideUp(500, function(){
       $("#alert").slideUp(600);
       });

    $(document).ready(function(){
        $("input[name='type']").click(function(){
          if ($("input[name='type']:checked").val() == "Grouped") {
            loadAjax();        
          }
          else {
            // alert('One of the radio buttons is checked!');
            $('#grouped_items').css('display' , 'none');
            $('#grouped_items_rows').html("");
          }
        });

        if ($("input[name='type']:checked").val() == "Grouped") {
            loadAjax();
        }
    });

    $('#category').change(function() {
      if($("input[name='type']:checked").val() == "Grouped"){
        loadAjax();
      }
    });
    
    function loadAjax(){
      $('#grouped_items_rows').html("");
          $.ajax({
            url: "{{url('categoryProducts')}}"+"/"+$('#category').find(":selected").val()+"/"+{{ $product->id }},
            headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            method: 'GET',
            success: function(data) {
              if(data.success) {
                  $('#grouped_items').css('display' , 'block');
                  var products = data.products;
                  if(products.length > 0){
                    var i = 0;
                    products.forEach(function(product) {       
                      var check = "";
                      if(product.check){
                        check = "checked";
                      }             
                      var html = "<div class='row'>"+
                        "<div class='col-md-2'>"+
                          "<div class='custom-control custom-checkbox'>"+
                            "<input type='checkbox' class='custom-control-input' id='customCheck"+i+"' value="+product.id+" name='items[]' "+check+">"+
                            "<label class='custom-control-label' for='customCheck"+i+"'></label>"+
                          "</div>"+
                        "</div>"+
                        "<div class='col-md-5'>"+
                              product.name+
                        "</div>"+
                        "<div class='col-md-5'>"+
                              product.price+
                        "</div>"+
                      "</div>";

                      $('#grouped_items_rows').append(html);
                      i++;
                  });
                  }else{
                    alert("No Products in this Category");
                  }
                } 
            },
            error: function(data) {
                // If you got an error code.
            }
        }); 

    }


    $(document).ready(function() {
      $('.cropme').simpleCropper();

let html = `<img src="{{ empty($product->image) ? '' : $product->image }}" alt="">`;
$(".span4.cropme").html(html);

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

});
    </script>
@endsection