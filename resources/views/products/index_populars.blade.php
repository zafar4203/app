@extends('layouts.welcome')

@section('title' , 'Popular Products')

@section('styles')
    <link href="{{asset('public/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
    <style>
      .error{
        color:red;
        font-size:1rem;
      }
      #days button{
          display:inline-block;
          border-radius:0px;
          margin-left:-5px !important;
      }

      #days button:first-child{
          border-top-left-radius:5px;
          border-bottom-left-radius:5px;
      }

      #days button:last-child{
          border-top-right-radius:5px;
          border-bottom-right-radius:5px;
      }



      .active-yr, .btn:hover {
        background-color: #4841a8;
        color: white;
      }
      .active-mn, .btn:hover {
        background-color: #4841a8;
        color: white;
      }
      .active-day, .btn:hover {
        background-color: #4841a8;
        color: white;
      }

      div.dataTables_wrapper div.dataTables_filter input {
            margin-left: 0.5em;
            display: inline-block;
            width: auto;
            margin-right: 5px;
        }
        .dt-button{
          float:right;
          z-index:9999;
          position:relative;
        }


      @media screen and (max-width: 600px) {
        .btn-group{
          display:inline-block;
        }
        .mn{
          margin-top:10px;
        }
        .br{
          margin-top:10px;
        }
      }
    </style>
    </style>
@endsection

@section('content')
<div class="container-fluid">

@if(session()->has('message'))
    <div id="alert" class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
      <!-- Page Heading -->
      <h1 class="h3 mb-2 text-gray-800">Most Selling Products</h1>

      <div class="row">
            <div class="col-md-12">
                <div class="btn-group" id="years">
                    <button type="button" id="2020" class="yr btn btn-primary">2020</button>
                    <button type="button" id="2021" class="ml-00 yr btn btn-primary">2021</button>
                    <button type="button" id="2022" class="ml-00 yr btn btn-primary">2022</button>
                    <button type="button" id="2023" class="ml-00 yr btn btn-primary">2023</button>
                    <button type="button" id="2024" class="ml-00 yr btn btn-primary">2024</button>
                    <button type="button" id="2025" class="ml-00 yr btn btn-primary">2025</button>
                </div>
            </div>

            <div class="col-md-12 mt-2">
                <div class="btn-group" id="months">
                    <button type="button" id="1" class="btn mn btn-primary">Jan</button>
                    <button type="button" id="2" class="ml-00 mn btn btn-primary">Feb</button>
                    <button type="button" id="3" class="ml-00 mn btn btn-primary">March</button>
                    <button type="button" id="4" class="ml-00 mn btn btn-primary">April</button>
                    <button type="button" id="5" class="ml-00 mn btn btn-primary">May</button>
                    <button type="button" id="6" class="ml-00 mn btn btn-primary">June</button>
                    <button type="button" id="7" class="ml-00 mn btn btn-primary">July</button>
                    <button type="button" id="8" class="ml-00 mn btn btn-primary">Aug</button>
                    <button type="button" id="9" class="ml-00 mn btn btn-primary">Sep</button>
                    <button type="button" id="10" class="ml-00 mn btn btn-primary">Oct</button>
                    <button type="button" id="11" class="ml-00 mn btn btn-primary">Nov</button>
                    <button type="button" id="12" class="ml-00 mn btn btn-primary">Dec</button>
                </div>
            </div>

            <div class="col-md-12 mt-2 ml-2">
                <div class="btn-group" id="days">
                    <button type="button" id="1" class="btn day btn-primary">1</button>
                    <button type="button" id="2" class="ml-00 day btn btn-primary">2</button>
                    <button type="button" id="3" class="ml-00 day btn btn-primary">3</button>
                    <button type="button" id="4" class="ml-00 day btn btn-primary">4</button>
                    <button type="button" id="5" class="ml-00 day btn btn-primary">5</button>
                    <button type="button" id="6" class="ml-00 day btn btn-primary">6</button>
                    <button type="button" id="7" class="ml-00 day btn btn-primary">7</button>
                    <button type="button" id="8" class="ml-00 day btn btn-primary">8</button>
                    <button type="button" id="9" class="ml-00 day btn btn-primary">9</button>
                    <button type="button" id="10" class="ml-00 day btn btn-primary">10</button>
                    <button type="button" id="11" class="ml-00 day btn btn-primary">11</button>
                    <button type="button" id="12" class="ml-00 day btn btn-primary">12</button>
                    <button type="button" id="13" class="ml-00 day btn btn-primary">13</button>
                    <button type="button" id="14" class="ml-00 day btn btn-primary">14</button>
                    <button type="button" id="15" class="ml-00 day btn btn-primary">15</button>
                    <button type="button" id="16" class="ml-00 day btn btn-primary">16</button>
                    <button type="button" id="17" class="ml-00 day btn btn-primary">17</button>
                    <button type="button" id="18" class="ml-00 day btn btn-primary">18</button>
                    <button type="button" id="19" class="ml-00 day btn btn-primary">19</button>
                    <button type="button" id="20" class="ml-00 day btn btn-primary">20</button>
                    
                </div>

                <div class="btn-groups mt-2" id="days">                                   
                    <button type="button" id="21" class="ml-00 day btn btn-primary">21</button>
                    <button type="button" id="22" class="ml-00 day btn btn-primary">22</button>
                    <button type="button" id="23" class="ml-00 day btn btn-primary">23</button>
                    <button type="button" id="24" class="ml-00 day btn btn-primary">24</button>
                    <button type="button" id="25" class="ml-00 day btn btn-primary">25</button>
                    <button type="button" id="26" class="ml-00 day btn btn-primary">26</button>
                    <button type="button" id="27" class="ml-00 day btn btn-primary">27</button>
                    <button type="button" id="28" class="ml-00 day btn btn-primary">28</button>
                    <button type="button" id="29" class="ml-00 day btn btn-primary">29</button>
                    <button type="button" id="30" class="ml-00 day btn btn-primary">30</button>
                    <button type="button" id="31" class="ml-00 day btn btn-primary">31</button>
                </div>

            </div>
      </div>

          <!-- DataTales Example -->
          <div class="card shadow mb-4 mt-2">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Popular Products</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="customerTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                        <th>Name</th>
                        <th>Short Name</th>
                        <th>Price</th>
                        <th>Count</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                        <th>Name</th>
                        <th>Short Name</th>
                        <th>Price</th>
                        <th>Count</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->


          <!-- Logout Modal-->
  <div class="modal fade" id="deleteBranchModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Are you sure to delete?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select <span class="error">"Delete"</span> below if you are ready to delete the Category.</div>
        <div class="modal-footer">
          <button class="btn btn-success" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-danger" id="deleteModel" href="#">Delete</a>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('scripts')
  <!-- Page level plugins -->
  <script src="{{asset('public/vendor/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('public/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

  <!-- Page level custom scripts -->
  <script src="{{asset('public/js/demo/datatables-demo.js')}}"></script>

  <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>  
  <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>  
  <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>  
  <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.colVis.min.js"></script>  

  <script>
     $("#alert").fadeTo(2000, 500).slideUp(500, function(){
       $("#alert").slideUp(600);
       });


        // Selection Area
        var d = new Date();
        var year = d.getFullYear();
        var month = d.getMonth() + 1;
        var day = $('.day').eq(0).attr('id');
        $('#'+year).addClass('active-yr');
        $('#'+month).addClass('active-mn');
        $(".day:first").addClass('active-day');

        // Add active class to the current button (highlight it)
        var header = document.getElementById("years");
        var btns = header.getElementsByClassName("yr");
        for (var i = 0; i < btns.length; i++) {
        btns[i].addEventListener("click", function() {
        var current = document.getElementsByClassName("active-yr");
        current[0].className = current[0].className.replace(" active-yr", "");
        this.className += " active-yr";
        });
        }


                // Add active class to the current button (highlight it)
        var header = document.getElementById("months");
        var btns = header.getElementsByClassName("mn");
        for (var i = 0; i < btns.length; i++) {
          btns[i].addEventListener("click", function() {
          var current = document.getElementsByClassName("active-mn");
          current[0].className = current[0].className.replace(" active-mn", "");
          this.className += " active-mn";
          });
        }

        // Add active class to the current button (highlight it)
        var header = document.getElementById("days");
        var btns = header.getElementsByClassName("day");
        for (var i = 0; i < btns.length; i++) {
          btns[i].addEventListener("click", function() {
          var current = document.getElementsByClassName("active-day");
          current[0].className = current[0].className.replace(" active-day", "");
          this.className += " active-day";
          });
        }

        $('.yr').click(function(){
            year = this.id;
            getData();
        });

        $('.mn').click(function(){
            month = this.id;
            getData();
        });

        $('.day').click(function(){
            day = this.id;
            getData();
        });

        var tbl = $('#customerTable');
        $('#customerTable').DataTable({
              "processing":true,
              "serverside":true,
              "bPaginate": false,
              "bInfo" : false,
              "pageLength": 50,
              "order": [],
              "ajax":"{{url('ajaxPopular')}}/"+year+"/"+month+"/"+day,
              "columns":[
                {"data" : "name"},
                {"data" : "short_name"},
                {"data" : "price"},
                {"data" : "count"},
              ],
              order:[0,'desc'],
              dom: 'Bfrtip',
              buttons: [
                  {
                      extend: 'print',
                      exportOptions: {
                          columns: ':visible'
                      }
                  },
                  {
                      extend: 'excelHtml5',
                      exportOptions: {
                          columns: ':visible'
                      }
                  },
                  {
                      extend:'pdfHtml5',
                      text:'PDF',
                      orientation:'landscape',
                      exportOptions: {
                         columns: ':visible'
                      },
                      customize : function(doc){
                          var colCount = new Array();
                          $(tbl).find('tbody tr:first-child td').each(function(){
                              if($(this).attr('colspan')){
                                  for(var i=1;i<=$(this).attr('colspan');$i++){
                                      colCount.push('*');
                                  }
                              }else{ colCount.push('*'); }
                          });
                          doc.content[1].table.widths = colCount;
                      }
                  },
                  'colvis'
              ] 
            });

            $(".dt-button").addClass("mr-1 btn btn-sm btn-primary");

        function getData(){
          $('#customerTable').DataTable().ajax.url("{{url('ajaxPopular')}}/"+year+"/"+month+"/"+day).load();
        }
  </script>
@endsection