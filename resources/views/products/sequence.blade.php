@extends('layouts.welcome')
@section('title' , 'Categories')
@section('styles')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">

<style>
      .error{
        color:red;
        font-size:1rem;
      }
      /* #sortable1, #sortable2 {
        border: 1px solid #eee;
        width: 142px;
        min-height: 20px;
        list-style-type: none;
        margin: 0;
        padding: 5px 0 0 0;
        float: left;
        margin-right: 10px;
    }
    #sortable1 li, #sortable2 li {
        margin: 0 5px 5px 5px;
        padding: 5px;
        font-size: 1.2em;
        width: 120px;
    } */
    .box-shadow{
        border: 1px solid lightgrey; 
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        padding:20px;
        cursor:pointer;
    }
    .heading{
        padding:10px;
        font-weight:normal;
        font-size:30px;
        width:200px;
    }
    .subheading{
        padding:10px;
        font-weight:normal;
        font-size:20px;
    }
    .text{
        text-align:center;
        font-weight:bold;
        color:#0e0e0e;
        font-size:20px;
    }
</style>
@endsection

@section('content')
<div class="container-fluid">

    <div id="alert" style="display:none;" class="alert alert-success">
        Data Updated Successfully
    </div>
    
    <div id="sortable2" class="connectedSortable row">
    @foreach($categories as $category)
        @if(count($category->products) > 0)
        <hr style="color:red; width:100%;">
        <div class="col-md-12">
            <h3><span class="heading badge badge-warning badge-lg">{{ $category->name }}</span></h3>
        </div>
        <hr style="color:red; width:100%;">
                @if(count($category->products) > 0)
                    @foreach($category->products as $product)
                        @if(!$product->subcategory_id)
                        <div class="mb-3 col-md-3 box-shadow">
                            <div data-id="{{ $product->id }}" class="text">{{ $product->name }}</div>
                        </div>
                        @endif
                    @endforeach
                @endif
                @if(count($category->subcategories) > 0)
                @foreach($subcategories as $subcategory)
                    @if($subcategory->category_id == $category->id && count($subcategory->products) > 0)
                    <div class="col-md-12">
                        <h5 class="subheading badge badge-info">{{ $subcategory->name }}</h5>
                    </div>
                        @foreach($products as $product)
                            @if($product->subcategory_id == $subcategory->id)
                            <div class="mb-3 col-md-3 box-shadow">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img style="width: 100px; padding-top: 20%;" src="{{ $product->image }}" alt="">
                                    </div>
                                    <div class="col-md-8 pt-5">
                                        <div data-id="{{ $product->id }}" class="text">{{ $product->name }}</div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        @endforeach
                    @endif
                @endforeach
                @endif
    @endif
    @endforeach
    </div>

    <button class="btn btn-primary sorts">Update</button>
</div>
@endsection

@section('scripts')
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#sortable2" ).sortable({
      connectWith: ".connectedSortable"
    }).disableSelection();
  });

  $('.sorts').click(function(){
      var ids = [];
      var token = '{{ csrf_token() }}';
      $('.text').each(function(i, obj) {
          ids.push($(obj).data('id'));          
      });
      console.log(ids);
      $.ajax({
            url: "{{ route('products.sort.update') }}",
            type: 'POST',
            headers: {'X-CSRF-TOKEN': token},
            data:{
                ids:ids,
            },
            success: function(data) {
                if(data.status == 200) {
                    $('#alert').css('display','block');
                }
            },
            complete:function(){
                location.reload();
            } 
        }); 
  });
  </script>
@endsection