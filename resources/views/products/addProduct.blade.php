@extends('layouts.welcome')

@section('title' , 'Add Product')

@section('styles')
    <style>
      .add-product-form{
        margin:0px 20%;
      }
      .error{
        color:red;
        font-size:1rem;
      }
      .submit-btn{
        margin-top:20px; 
        margin-bottom:20px; 
      }
      #grouped_items{
        display:none;
      }
      @media screen and (max-width: 600px) {
        .add-product-form{
          margin:0px 5%;
        }
      }
      #landscape img{
        height:150px;
        width:150px;
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">
<form  class="add-product-form" enctype="multipart/form-data"
 action="{{route('products.store')}}" id="add_product" method="post">
@csrf

  <!-- <div class="form-group">
      <div class="custom-control custom-radio custom-control-inline">
          <input type="radio" class="custom-control-input" id="customRadio2" value="Regular" name="type" @if(old('type') == "Regular") checked @endif checked>
          <label class="custom-control-label" for="customRadio2">Regular</label>
      </div>

      <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="customRadio1" value="Grouped" name="type" @if(old('type') == "Grouped") checked @endif>
        <label class="custom-control-label" for="customRadio1">Grouped Item</label>
      </div>

      <span class="float-right"><a href="{{url('addCategory')}}">Add Category</a></span>
  </div> -->
  <input type="hidden" name="type" value="Regular" />
  <div class="form-group">
    <label for="productInput">Product Category</label>
    <div class="form-group">
      <select class="form-control" id="category" name="category" >
        <option value="" disable> Select Category </option>
        @foreach($categories as $category)
         <option value="{{ $category->id }}"> {{ $category->name }} </option>
        @endforeach
      </select>
    </div>
    @if($errors->has('category'))
    <small id="productCategoryHelp" class="form-text error">{{ $errors->first('category') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="productInput">Product Category</label>
   <div class="form-group">
      <select class="form-control" id="subcategory" name="subcategory" >
        <option value="" disable> Select SubCategory </option>
        @foreach($subcategories as $subcategory)
         <option value="{{ $subcategory->id }}"> {{ $subcategory->name }} </option>
        @endforeach
      </select>
    </div>
    @if($errors->has('subcategory'))
    <small id="productCategoryHelp" class="form-text error">{{ $errors->first('subcategory') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="productInput">Product Name</label>
    <input type="text" name="name" class="form-control" id="productInput" value="{{old('name')}}" aria-describedby="productNameHelp" placeholder="Enter product name">
    @if($errors->has('name'))
    <small id="productNameHelp" class="form-text error">{{ $errors->first('name') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="productShortInput">Product Short Name</label>
    <input type="text" name="short_name" class="form-control" id="productShortInput" value="{{old('short_name')}}" aria-describedby="productShortNameHelp" placeholder="Enter product short name">
    @if($errors->has('short_name'))
    <small id="productShortNameHelp" class="form-text error">{{ $errors->first('short_name') }}</small>
    @endif
  </div>


  <div class="form-group">
    <label for="productPrice">Product Price</label>
    <input type="text" name="price" class="form-control" value="{{old('price')}}" id="productPrice" aria-describedby="productPriceHelp" placeholder="Enter product price">
    @if($errors->has('price'))
    <small id="productPriceHelp" class="form-text error">{{ $errors->first('price') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="productPrice">Product Invisible Price</label>
    <input type="text" name="invisible_price" class="form-control" value="{{old('invisible_price')}}" id="productPrice" aria-describedby="productPriceHelp" placeholder="Enter product invisible price">
    @if($errors->has('invisible_price'))
    <small id="productPriceHelp" class="form-text error">{{ $errors->first('invisible_price') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="productPrice">Product Combo Price</label>
    <input type="text" name="combo_price" class="form-control" value="{{old('combo_price')}}" id="productPrice" aria-describedby="productPriceHelp" placeholder="Enter product combo price">
    @if($errors->has('combo_price'))
    <small id="productPriceHelp" class="form-text error">{{ $errors->first('combo_price') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="productPrice">Product Details</label>
    <textarea rows="5" type="text" name="details" class="form-control"  id="productDetails" aria-describedby="productPriceHelp" placeholder="Enter Product Details">{{old('details')}}</textarea>
    @if($errors->has('details'))
    <small id="productDetails" class="form-text error">{{ $errors->first('details') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="productInput">Product For</label>
    <div class="form-group">
      <select class="form-control" name="is_for" >
        <option value="" disable> Product For ? </option>
        <option value="web"> Web </option>
        <option value="pos"> POS </option>
        <option value="both"> Both </option>
      </select>
    </div>
    @if($errors->has('is_for'))
    <small id="productCategoryHelp" class="form-text error">{{ $errors->first('is_for') }}</small>
    @endif
  </div>

  <div class="form-group">
    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="customRadio2" value="0" name="block" checked>
        <label class="custom-control-label" for="customRadio2">Approve</label>
    </div>

    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="customRadio1" value="1" name="block">
        <label class="custom-control-label" for="customRadio1">Block</label>
      </div>
  </div>

  <!-- <div class="form-group">
        <input type="file" name="image[]"  multiple>
        @if($errors->has('image'))
          <small id="productImageHelp" class="form-text error">{{ $errors->first('image') }}</small>
        @endif
  </div>     -->
  <input type="hidden" id="feature_photo" name="photo" value="">


  <div class="panel panel-body">
    <div class="span4 cropme text-center" id="landscape"
      style="width: 100%; height: 285px; border: 1px dashed #ddd; background: #f1f1f1;">
      <a href="javascript:;" id="crop-image" class=" mybtn1" style="">
        <i class="icofont-upload-alt"></i> {{ __('Upload Image Here') }}
      </a>
    </div>
  </div>
  @if($errors->has('photo'))
    <small id="productImageHelp" class="form-text error">{{ $errors->first('photo') }}</small>
  @endif


<div id="grouped_items">
  <h4>Grouped Items</h4>
    <div class="row">
      <div class="col-md-2">Select</div>
      <div class="col-md-5">Item Name</div>
      <div class="col-md-5">Item Price</div>
    </div>

    <div id="grouped_items_rows"></div>
</div>

  <button id="submit" class="btn btn-primary submit-btn">Submit</button>
</form>

</div>
@endsection

<script src="/path/to/cropper.js"></script><!-- Cropper.js is required -->
<link  href="/path/to/cropper.css" rel="stylesheet">
<script src="/path/to/jquery-cropper.js"></script>

@section('scripts')
    <script>
    
  	$('.cropme').simpleCropper();
    $(document).ready(function(){
        $("input[name='type']").click(function(){
          if ($("input[name='type']:checked").val() == "Grouped") {
            loadAjax();        
          }
          else {
            // alert('One of the radio buttons is checked!');
            $('#grouped_items').css('display' , 'none');
          }
        });

        if ($("input[name='type']:checked").val() == "Grouped") {
          loadAjax();
        }
    });

    $('#category').change(function() {
      if($("input[name='type']:checked").val() == "Grouped"){
        loadAjax();
      }
    });

    function loadAjax(){
      $('#grouped_items_rows').html("");
          $.ajax({
            url: "{{url('categoryProducts')}}"+"/"+$('#category').find(":selected").val(),
            headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            method: 'GET',
            success: function(data) {
              if(data.success) {
                  $('#grouped_items').css('display' , 'block');
                  var products = data.products;
                  if(products.length > 0){
                    var i = 0;
                    products.forEach(function(product) {                    
                      var html = "<div class='row'>"+
                        "<div class='col-md-2'>"+
                          "<div class='custom-control custom-checkbox'>"+
                            "<input type='checkbox' class='custom-control-input' id='customCheck"+i+"' value="+product.id+" name='items[]'>"+
                            "<label class='custom-control-label' for='customCheck"+i+"'></label>"+
                          "</div>"+
                        "</div>"+
                        "<div class='col-md-5'>"+
                              product.name+
                        "</div>"+
                        "<div class='col-md-5'>"+
                              product.price+
                        "</div>"+
                      "</div>";

                      $('#grouped_items_rows').append(html);
                      i++;
                  });
                  }else{
                    alert("No Products in this Category");
                    $('#grouped_items').css('display' , 'none');
                  }
                } 
            },
            error: function(data) {
                // If you got an error code.
            }
        }); 

    }
    </script>
@endsection