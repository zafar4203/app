@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            Papafluffy
        @endcomponent
    @endslot

    {{-- Body --}}
    {{ $slot }}


    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }} Papafluffy . @lang('All rights reserved.')
        @endcomponent
    @endslot
@endcomponent
