    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-cookie fa-pulse fa-spin fa-3x"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Sales</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">


      @foreach($menus as $key => $menu)

      @if(Auth::user()->can($menu->permission) || Auth::user()->can('all'))
      <li class="nav-item"> 
          <a class="nav-link" href="{{ $menu->link }}" @if(count($menu->subs)) data-toggle="collapse" data-target="#menu{{$key}}" aria-controls="collapsePages" aria-expanded="false" @endif>
            <i class="{{ $menu->icon }}"></i>
            <span>{{ $menu->name }}</span>
          </a>

          @if(count($menu->subs))
          <div id="menu{{$key}}" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar" style="">
            <div class="bg-white py-2 collapse-inner rounded">
            @if(count($menu->subs) > 0)
              @foreach($submenus as $s)
                @foreach($menu->subs as $sub)
                  @if($s->id == $sub->id)
                    @if(Auth::user()->can($sub->permission) || Auth::user()->can('all'))
                      <a class="collapse-item" href="{{ $sub->link }}">{{ $sub->name }}</a>
                    @endif
                  @endif
                @endforeach
              @endforeach
            @endif
            </div>
          </div>
          @endif
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">
        @endif


      @endforeach

      <br><br><br><br>

        <!-- @php 
          $p_ch = 0;
          if(!Auth::user()->can('all') && !Auth::user()->can('View Products') && !Auth::user()->can('View Combo Products') && !Auth::user()->can('View Categories') && !Auth::user()->can('View Sub Categories') && !Auth::user()->can('View Inventory') && !Auth::user()->can('View Areas') && !Auth::user()->can('View Popular Products')){
              $p_ch = 1;
          }        
        @endphp

        @if($p_ch == 0)
        <li class="nav-item"> 
          <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapseProducts" aria-controls="collapsePages" aria-expanded="false">
            <i class="fas fa-fw fa-cart-plus"></i>
            <span>Products</span>
          </a>
          <div id="collapseProducts" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar" style="">
            <div class="bg-white py-2 collapse-inner rounded">
              @if(Auth::user()->can('View Products') || Auth::user()->can('all'))
              <a class="collapse-item" href="{{url('products')}}">Products</a>
              @endif
              @if(Auth::user()->can('View Combo Products') || Auth::user()->can('all'))
              <a class="collapse-item" href="{{url('combos')}}">Combo Products</a>
              @endif
              @if(Auth::user()->can('View Categories') || Auth::user()->can('all'))
              <a class="collapse-item" href="{{url('categories')}}">Categories</a>
              @endif
              @if(Auth::user()->can('View Sub Categories') || Auth::user()->can('all'))
              <a class="collapse-item" href="{{url('subcategories')}}">SubCategories</a>
              @endif
              @if(Auth::user()->can('View Inventory') || Auth::user()->can('all'))
              <a class="collapse-item" href="{{url('inventories')}}">Inventory</a>
              @endif
              @if(Auth::user()->can('View Areas') || Auth::user()->can('all'))
              <a class="collapse-item" href="{{route('areas.index')}}">Delivery Areas</a>              
              @endif
              @if(Auth::user()->can('View Popular Products') || Auth::user()->can('all'))
              <a class="collapse-item" href="{{url('popular-products')}}">Popular Products</a>
              @endif
            </div>
          </div>
        </li>

      <hr class="sidebar-divider">
        @endif -->



      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->