    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion toggled" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Sales</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="{{url('invisible/dashboard')}}">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
        <a class="nav-link collapsed" href="{{url('i_orders')}}">
          <i class="fas fa-fw fa-motorcycle"></i>
          <span>Place Order</span>
        </a>
        </li>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <li class="nav-item"> 
        <a class="nav-link" href="{{url('i_sales')}}/{{Auth::user()->id}}">
          <i class="fas fa-fw fa-file"></i>
          <span>Sales</span>
        </a>
      </li>

            <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

        <li class="nav-item"> 
          <a class="nav-link" href="{{url('i_refferals')}}/{{Auth::user()->id}}">
            <i class="fas fa-fw fa-file"></i>
            <span>Refferals</span>
          </a>
        </li>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <li class="nav-item"> 
        <a class="nav-link" href="{{url('i_activities')}}/{{Auth::user()->id}}">
          <i class="fas fa-fw fa-tasks"></i>
          <span>Actvities</span>
        </a>
      </li>

        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
        <a class="nav-link collapsed" href="{{url('i_promos')}}">
          <i class="fas fa-fw fa-dollar-sign"></i>
          <span>Promos</span>
        </a>
        </li>


      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->