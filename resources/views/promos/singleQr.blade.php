@extends('layouts.welcome')

@section('title' , 'Add Promo')

@section('styles')
    <style>
      .print-btn{
        color:white;
        background:blue;
        padding:10px 20px;
        border-radius:5px;
        cursor:pointer;
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">
      <div class="col-md-12 text-center mb-4">
                <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(200)->generate(url('https://papafluffy.com?discount='.$promo->name))) !!} ">


      </div>
      <div class="row">
          <a style="width:100%;" class="text-center" href="data:image/png;base64, {!! base64_encode(QrCode::encoding('UTF-8')->format('png')->size(200)->errorCorrection('H')->generate(url('https://papafluffy.com?discount='.$promo->name))) !!} " download="filename.png">  
              <span class="print-btn">Download</span>
          </a>

    </div>


</div>
@endsection
@section('scripts')

@endsection