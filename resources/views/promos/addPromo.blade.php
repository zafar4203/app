@extends('layouts.welcome')

@section('title' , 'Add Promo')

@section('styles')
    <style>
      .add-promo-form{
        margin:0px 20%;
      }
    .error{
      color:red;
      font-size:1rem;
    }
    @media screen and (max-width: 600px) {
        .add-promo-form{
          margin:0px 5%;
        }
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">
<form  class="add-promo-form" action="{{route('addPromo')}}" id="add_promo" method="post">
@csrf
  <div class="form-group">
    <label>Promo Code</label>
    <input type="text" name="name" class="form-control" aria-describedby="promoNameHelp" value="{{ old('name') }}" placeholder="Enter Promo Code">
    @if($errors->has('name'))
    <small id="promoNameHelp" class="form-text error">{{ $errors->first('name') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label>Description</label>
    <input type="text" name="description" class="form-control" aria-describedby="promodescriptionHelp" value="{{ old('description') }}" placeholder="Enter Promo Description">
    @if($errors->has('description'))
    <small id="promodescriptionHelp" class="form-text error">{{ $errors->first('description') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label>Type</label>
    <select name="type" class="form-control" id="">
      <option value="Discount">Percentage</option>
      <option value="Direct">Direct</option>
    </select>
    @if($errors->has('type'))
    <small class="form-text error">{{ $errors->first('type') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label>Amount</label>
    <input type="text" name="amount" class="form-control" aria-describedby="promodescriptionHelp" value="{{ old('amount') }}" placeholder="Enter Promo Amount">
    @if($errors->has('amount'))
    <small id="promoamountHelp" class="form-text error">{{ $errors->first('amount') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label>Expiry Date</label>
    <input type="date" rows="5" name="expiry_date" class="form-control" value="{{old('expiry_date')}}" placeholder="Enter Expiry date">
    @if($errors->has('expiry_date'))
    <small class="form-text error">{{ $errors->first('expiry_date') }}</small>
    @endif
  </div>

  <div class="form-group">
    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="customRadio2" value="0" name="status" checked>
        <label class="custom-control-label" for="customRadio2">Active</label>
    </div>

    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="customRadio1" value="1" name="status">
        <label class="custom-control-label" for="customRadio1">InActive</label>
      </div>
  </div>

  <button id="submit" class="btn btn-primary">Submit</button>
</form>

</div>
@endsection
