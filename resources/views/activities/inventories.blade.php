@extends('layouts.welcome')

@section('title' , 'Inventory Table Activities')

@section('styles')
    <link href="{{asset('public/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
    <style>
      .error{
        color:red;
        font-size:1rem;
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">


      <!-- Page Heading -->
      <h4 class="mb-2 text-gray-800">Inventory Table Activities</h4>
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Inventory Activities </h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="activitiesTable" width="100%" cellspacing="0">
                  <thead>
                      <tr>
                          <th>Date</th>
                          <th>Time</th>
                          <th>Activity</th>
                          <th>User</th>
                      </tr>
                  </thead>
                  <tfoot>
                        <tr>
                          <th>Date</th>
                          <th>Time</th>
                          <th>Activity</th>
                          <th>User</th>
                        </tr>
                  </tfoot>

                  <tbody>
                        @foreach($activities as $activity)
                            <tr>
                                <td>{{$activity->date}}</td>
                                <td>{{$activity->time}}</td>
                                <td>{!! $activity->activity !!}</td>
                                <td>{{$activity->user_name}}</td>
                            </tr>
                        @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->


@endsection

@section('scripts')
  <!-- Page level plugins -->
  <script src="{{asset('public/vendor/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('public/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

  <!-- Page level custom scripts -->
  <script src="{{asset('public/js/demo/datatables-demo.js')}}"></script>

  <script>
  
        $(document).ready(function(){
            $('#activitiesTable').dataTable();
        });
  </script>
@endsection