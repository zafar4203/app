@extends('layouts.welcome')

@section('title' , 'Areas')

@section('styles')
    <link href="{{asset('public/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
    <style>
      .error{
        color:red;
        font-size:1rem;
      }
      div.dataTables_wrapper div.dataTables_filter input {
            margin-left: 0.5em;
            display: inline-block;
            width: auto;
            margin-right: 5px;
        }
        .dt-button{
          float:right;
          z-index:9999;
          position:relative;
        }
    </style>
@endsection

@section('content')
<div class="container-fluid">

@if(session()->has('message'))
    <div id="alert" class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
      <!-- Page Heading -->
      <h1 class="h3 mb-2 text-gray-800">Delivery Areas</h1>
          <p class="mb-4">You can Add , Edit and Delete Areas from there. <span class="float-right"><span><i class="fa fa-edit text-primary"></i></span> for Update <span><i class="fa fa-trash text-primary"></i></span> for Delete </span></p>
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Areas  
              @if(Auth::user()->can('Add Area') || Auth::user()->can('all'))
              <a href="{{url('addArea')}}" title="Add Area"><span class="float-right"><button class="btn btn-sm btn-primary">Add Area</button></span></a>
              @endif
              </h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="areaTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Name</th>
                      <th>Delivery Charges</th>
                      <th>Block</th>
                      <th>Created</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Delivery Charges</th>
                    <th>Block</th>
                    <th>Created</th>
                    <th>Action</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->


          <!-- Logout Modal-->
  <div class="modal fade" id="deleteCategoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Are you sure to delete?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select <span class="error">"Delete"</span> below if you are ready to delete the Area.</div>
        <div class="modal-footer">
          <button class="btn btn-success" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-danger" id="deleteModel" href="#">Delete</a>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('scripts')
  <!-- Page level plugins -->
  <script src="{{asset('public/vendor/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('public/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

  <!-- Page level custom scripts -->
  <script src="{{asset('public/js/demo/datatables-demo.js')}}"></script>

  <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>  
  <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>  
  <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>  
  <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.colVis.min.js"></script>  

  <script>
     $("#alert").fadeTo(2000, 500).slideUp(500, function(){
       $("#alert").slideUp(600);
       });

       function delete_click(clicked_id){
          $('#deleteModel').attr("href","{{url('deleteArea')}}/"+clicked_id)
          $('#deleteCategoryModal').modal('show');
        }

        $(document).ready(function(){
          var tbl = $('#areaTable');
            $('#areaTable').DataTable({
              "processing":true,
              "serverside":true,
              "ajax":"{{route('ajaxAreas')}}",
              "columns":[
                {"data" : "id"},
                {"data" : "name"},
                {"data" : "delivery_charges"},
                {"data" : "isBlock"},
                {"data" : "created_at"},
                {"data" : "action"}
              ],
              order:[0,'desc'],
              dom: 'Bfrtip',
              buttons: [
                  {
                      extend: 'print',
                      exportOptions: {
                          columns: ':visible'
                      }
                  },
                  {
                      extend: 'excelHtml5',
                      exportOptions: {
                          columns: ':visible'
                      }
                  },
                  // {
                  //     extend: 'pdfHtml5',
                  //     exportOptions: {
                  //        columns: ':visible'
                  //     },
                  //     orientation: 'portrait',
                  //     pageSize: 'LEGAL'
                  // },
                  {
                      extend:'pdfHtml5',
                      text:'PDF',
                      orientation:'landscape',
                      exportOptions: {
                         columns: ':visible'
                      },
                      customize : function(doc){
                          var colCount = new Array();
                          $(tbl).find('tbody tr:first-child td').each(function(){
                              if($(this).attr('colspan')){
                                  for(var i=1;i<=$(this).attr('colspan');$i++){
                                      colCount.push('*');
                                  }
                              }else{ colCount.push('*'); }
                          });
                          doc.content[1].table.widths = colCount;
                      }
                  },
                  'colvis'
              ]
            });
            $(".dt-button").addClass("mr-1 btn btn-sm btn-primary");
        });
  </script>
@endsection