@extends('layouts.welcome')

@section('title' , 'Add Area')

@section('styles')
    <style>
      .add-area-form{
        margin:0px 20%;
      }
    .error{
      color:red;
      font-size:1rem;
    }
    @media screen and (max-width: 600px) {
        .add-area-form{
          margin:0px 5%;
        }
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">
<form  class="add-area-form" enctype="multipart/form-data"
 action="{{route('addArea')}}" id="add_category" method="post">
@csrf
  <div class="form-group">
    <label for="categoryInput">Area Name</label>
    <input type="text" name="name" class="form-control" id="categoryInput" value="{{old('name')}}" aria-describedby="categoryNameHelp" placeholder="Enter Area name">
    @if($errors->has('name'))
    <small id="categoryNameHelp" class="form-text error">{{ $errors->first('name') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="delivery_charges">Delivery Charges</label>
    <input type="number" name="delivery_charges" class="form-control" id="delivery_charges" value="{{old('delivery_charges')}}" placeholder="Enter Delivery Charges">
    @if($errors->has('delivery_charges'))
    <small id="categoryDescriptionHelp" class="form-text error">{{ $errors->first('delivery_charges') }}</small>
    @endif
  </div>

  <div class="form-group">
    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="customRadio2" value="0" name="block" checked>
        <label class="custom-control-label" for="customRadio2">Active</label>
    </div>

    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="customRadio1" value="1" name="block">
        <label class="custom-control-label" for="customRadio1">De Active</label>
      </div>
  </div>


  <button id="submit" class="btn btn-primary">Submit</button>
</form>

</div>
@endsection
