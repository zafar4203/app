@extends('layouts.welcome')

@section('title' , 'Edit Area')

@section('styles')
    <style>
      .edit-category-form{
        margin:0px 20%;
      }
    .error{
      color:red;
      font-size:1rem;
    }
    @media screen and (max-width: 600px) {
        .edit-category-form{
          margin:0px 5%;
        }
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">

<form  class="edit-category-form" enctype="multipart/form-data"
 action="{{url('updateArea')}}" id="edit_category" method="post">

 @if(session()->has('message'))
    <div id="alert" class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif


@csrf
  <input type="hidden" name="id" value="{{$area->id}}">
  <div class="form-group">
    <label for="categoryInput">Area Name</label>
    <input type="text" name="name" class="form-control" value="{{$area->name}}" id="categoryInput" aria-describedby="categoryNameHelp" placeholder="Enter Area Name">
    @if($errors->has('name'))
    <small id="categoryNameHelp" class="form-text error">{{ $errors->first('name') }}</small>
    @endif
  </div>
  <div class="form-group">
    <label for="categoryDescription">Delivery Charges</label>
    <input type="number" name="delivery_charges" class="form-control" value="{{$area->delivery_charges}}" id="categoryDescription" aria-describedby="categoryDescriptionHelp" placeholder="Enter Delivery Charges" /> 
    @if($errors->has('delivery_charges'))
    <small id="categoryDescriptionHelp" class="form-text error">{{ $errors->first('delivery_charges') }}</small>
    @endif
  </div>

  <div class="form-group">
    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="customRadio2" value="0" name="block" @if($area->block == 0) checked @endif>
        <label class="custom-control-label" for="customRadio2">Active</label>
    </div>

    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="customRadio1" value="1" name="block" @if($area->block == 1) checked @endif>
        <label class="custom-control-label" for="customRadio1">De Active</label>
      </div>
  </div>
  
  <button id="submit" class="btn btn-primary">Submit</button>
</form>

</div>
@endsection
@section('scripts')
<script>
     $("#alert").fadeTo(2000, 500).slideUp(500, function(){
       $("#alert").slideUp(600);
       });
  </script>
@endsection