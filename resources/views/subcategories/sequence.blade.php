@extends('layouts.welcome')
@section('title' , 'Sub Categories')
@section('styles')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">

<style>
      .error{
        color:red;
        font-size:1rem;
      }
      /* #sortable1, #sortable2 {
        border: 1px solid #eee;
        width: 142px;
        min-height: 20px;
        list-style-type: none;
        margin: 0;
        padding: 5px 0 0 0;
        float: left;
        margin-right: 10px;
    }
    #sortable1 li, #sortable2 li {
        margin: 0 5px 5px 5px;
        padding: 5px;
        font-size: 1.2em;
        width: 120px;
    } */
</style>
@endsection

@section('content')
<div class="container-fluid">

    <div id="alert" style="display:none;" class="alert alert-success">
        Data Updated Successfully
    </div>
    
    <div id="sortable2" class="connectedSortable row">
    @foreach($categories as $category)    
        @if(count($category->subcategories))
            <div class="col-md-12">
                <h3>{{$category->name}}</h3>
            </div>
            @foreach($subcategories as $subcategory)
                @if($subcategory->category_id == $category->id)
                <div class="mb-3 col-md-4">
                    <div data-id="{{ $subcategory->id }}" class="btn btn-success btn-block btn-lg ui-state-highlight">{{ $subcategory->name }}</div>
                </div>
                @endif
            @endforeach
        @endif
    @endforeach
    </div>

    <button class="btn btn-primary sorts">Update</button>
</div>
@endsection

@section('scripts')
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#sortable2" ).sortable({
      connectWith: ".connectedSortable"
    }).disableSelection();
  });

  $('.sorts').click(function(){
      var ids = [];
      var token = '{{ csrf_token() }}';
      $('.ui-state-highlight').each(function(i, obj) {
          ids.push($(obj).data('id'));          
      });
      $.ajax({
            url: "{{ route('subcategory.sort.update') }}",
            type: 'POST',
            headers: {'X-CSRF-TOKEN': token},
            data:{
                ids:ids,
            },
            success: function(data) {
                if(data.status == 200) {
                    $('#alert').css('display','block');
                }
            },
            complete:function(){
                location.reload();
            } 
        }); 
  });
  </script>
@endsection