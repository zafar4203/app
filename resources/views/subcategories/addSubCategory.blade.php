@extends('layouts.welcome')

@section('title' , 'Add Category')

@section('styles')
    <style>
      .add-category-form{
        margin:0px 20%;
      }
    .error{
      color:red;
      font-size:1rem;
    }
    @media screen and (max-width: 600px) {
        .add-category-form{
          margin:0px 5%;
        }
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">
<form  class="add-category-form" enctype="multipart/form-data"
 action="{{route('addSubCategory')}}" id="add_category" method="post">
@csrf
  <div class="form-group">
    <label for="categoryInput">SubCategory Name</label>
    <input type="text" name="name" class="form-control" id="categoryInput" value="{{old('name')}}" aria-describedby="categoryNameHelp" placeholder="Enter category name">
    @if($errors->has('name'))
    <small id="categoryNameHelp" class="form-text error">{{ $errors->first('name') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="categoryDescription">SubCategory Description</label>
    <textarea type="text" name="description" class="form-control" id="categoryDescription" aria-describedby="categoryDescriptionHelp" placeholder="Enter Category Description">{{old('description')}}</textarea>
    @if($errors->has('description'))
    <small id="categoryDescriptionHelp" class="form-text error">{{ $errors->first('description') }}</small>
    @endif
  </div>

    <div class="form-group">
    <label for="categoryDescription">Category</label>
    <select class="form-control" name="category_id" id="category_id">
      @foreach($categories as $category)
      <option value="{{$category->id}}">{{ $category->name }}</option>
      @endforeach
    </select>
    @if($errors->has('category_id'))
    <small id="categoryDescriptionHelp" class="form-text error">{{ $errors->first('category_id') }}</small>
    @endif
  </div>

  <div class="form-group">
    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="customRadio2" value="0" name="block" checked>
        <label class="custom-control-label" for="customRadio2">Active</label>
    </div>

    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="customRadio1" value="1" name="block">
        <label class="custom-control-label" for="customRadio1">De Active</label>
      </div>
  </div>

  <div class="form-group">
        <input type="file" name="image">
        @if($errors->has('image'))
          <small id="categoryImageHelp" class="form-text error">{{ $errors->first('image') }}</small>
        @endif
  </div>    

  <button id="submit" class="btn btn-primary">Submit</button>
</form>

</div>
@endsection
