@extends('layouts.welcome')

@section('title' , 'Edit Category')

@section('styles')
    <style>
      .edit-category-form{
        margin:0px 20%;
      }
    .error{
      color:red;
      font-size:1rem;
    }
    @media screen and (max-width: 600px) {
        .edit-category-form{
          margin:0px 5%;
        }
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">

<form  class="edit-category-form" enctype="multipart/form-data"
 action="{{url('updateSubCategory')}}" id="edit_category" method="post">

 @if(session()->has('message'))
    <div id="alert" class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif


@csrf
  <input type="hidden" name="id" value="{{$subcategory->id}}">
  <div class="form-group">
    <label for="categoryInput">SubCategory Name</label>
    <input type="text" name="name" class="form-control" value="{{$subcategory->name}}" id="categoryInput" aria-describedby="categoryNameHelp" placeholder="Enter SubCategory Name">
    @if($errors->has('name'))
    <small id="categoryNameHelp" class="form-text error">{{ $errors->first('name') }}</small>
    @endif
  </div>
  <div class="form-group">
    <label for="categoryDescription">SubCategory Price</label>
    <textarea type="text" name="description" class="form-control" id="categoryDescription" aria-describedby="categoryDescriptionHelp" placeholder="Enter SubCategory description">{{$subcategory->description}}</textarea>
    @if($errors->has('description'))
    <small id="categoryDescriptionHelp" class="form-text error">{{ $errors->first('description') }}</small>
    @endif
  </div>

<div class="form-group">
    <label for="categoryDescription">Category</label>
    <select class="form-control" name="category_id" id="category_id">
      @foreach($categories as $category)
          <option {{$category->id == $subcategory->category_id?'selected':''}} value="{{$category->id}}">{{ $category->name }}</option>
      @endforeach
    </select>
    @if($errors->has('category_id'))
    <small id="categoryDescriptionHelp" class="form-text error">{{ $errors->first('category_id') }}</small>
    @endif
  </div>

  <div class="form-group">
    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="customRadio2" value="0" name="block" @if($subcategory->block == 0) checked @endif>
        <label class="custom-control-label" for="customRadio2">Active</label>
    </div>

    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="customRadio1" value="1" name="block" @if($subcategory->block == 1) checked @endif>
        <label class="custom-control-label" for="customRadio1">De Active</label>
      </div>
  </div>

  <div class="form-group">
        <input type="file" name="image">
        @if($errors->has('image'))
          <small id="categoryImageHelp" class="form-text error">{{ $errors->first('image') }}</small>
        @endif
  </div>    
  
  <button id="submit" class="btn btn-primary">Submit</button>
</form>

</div>
@endsection
@section('scripts')
<script>
     $("#alert").fadeTo(2000, 500).slideUp(500, function(){
       $("#alert").slideUp(600);
       });
  </script>
@endsection