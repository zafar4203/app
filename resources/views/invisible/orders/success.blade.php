@extends('layouts.invisible')

@section('title' , 'Orders placed Successfully')

@section('styles')
<style>
    .image{
        width:100%;
        height:300px;
    }
</style>
@endsection

@section('content')
<div class="container-fluid">

@if(session()->has('message'))
    <div id="alert" class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
      <!-- Page Heading -->
        <h1 class="h3 mt-5 mb-5 text-gray-800 text-center">Hurrah Order Placed Successfully </h1>

        <div class="row mt-5">
            <img class="image" src="{{ asset('public/img/delivery.svg') }}" alt="">           
        </div>
        <!-- /.container-fluid -->

</div>  
@endsection