<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
<body>

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<style>
    .divider-text {
    position: relative;
    text-align: center;
    margin-top: 15px;
    margin-bottom: 15px;
}
.divider-text span {
    padding: 7px;
    font-size: 12px;
    position: relative;   
    z-index: 2;
}
.divider-text:after {
    content: "";
    position: absolute;
    width: 100%;
    border-bottom: 1px solid #ddd;
    top: 55%;
    left: 0;
    z-index: 1;
}

.btn-facebook {
    background-color: #405D9D;
    color: #fff;
}
.btn-twitter {
    background-color: #42AEEC;
    color: #fff;
}
.small-text{
    color:red;
}
</style><script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
<style>
    .divider-text {
    position: relative;
    text-align: center;
    margin-top: 15px;
    margin-bottom: 15px;
}
.divider-text span {
    padding: 7px;
    font-size: 12px;
    position: relative;   
    z-index: 2;
}
.divider-text:after {
    content: "";
    position: absolute;
    width: 100%;
    border-bottom: 1px solid #ddd;
    top: 55%;
    left: 0;
    z-index: 1;
}

.btn-facebook {
    background-color: #405D9D;
    color: #fff;
}
.btn-twitter {
    background-color: #42AEEC;
    color: #fff;
}
.mt-10-percent{
    margin-top:10%;
}
</style>

<div class="container">
<div class="card bg-light mt-10-percent">
<article class="card-body mx-auto" style="max-width: 400px;">
    <h2 class="card-title mt-3 text-center">Invisible Employment</h2>
	<h4 class="card-title mt-3 text-center">Login Account</h4>
	
    @if(session()->has('error'))
        <div id="alert" class="alert alert-danger">
            {{ session()->get('error') }}
        </div>
    @endif

	<form action="{{ url('loginInvisible') }}" method="post">

    @csrf

    @if($errors->has('email'))
        <span class="small-text">{{ $errors->first('email') }}</span>
    @endif
    <div class="form-group input-group">
    	<div class="input-group-prepend">
		    <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
		 </div>
        <input name="email" class="form-control" placeholder="Email address" id="email" name="email" value="{{ old('email') }}" type="email">
    </div> <!-- form-group// -->


    @if($errors->has('password'))
        <span class="small-text">{{ $errors->first('password') }}</span>
    @endif
    <div class="form-group input-group">
    	<div class="input-group-prepend">
		    <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
		 </div>
        <input name="password" class="form-control" placeholder="Password" id="password" value="{{ old('password') }}" type="password">
    </div> <!-- form-group// -->
    
    <div class="form-group">
        <button type="submit" class="btn btn-primary btn-block"> Login Account  </button>
    </div> <!-- form-group// -->      
</form>
</article>
</div> <!-- card.// -->

</div> 
        
    </body>
</html>