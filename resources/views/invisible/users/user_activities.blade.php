@extends('layouts.invisible')

@section('title' , 'Invisible User Activities')

@section('styles')
    <style>
      .error{
        color:red;
        font-size:1rem;
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">

@if(session()->has('message'))
    <div id="alert" class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
      <!-- Page Heading -->
      <h4 class="mb-2 text-gray-800">{{ $invisible->name }} Activities for Wallet</h4>
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">History </h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="logTable" width="100%" cellspacing="0">
                  <thead>
                      <tr>
                          <th>Activitiy</th>
                          <th>Date</th>
                      </tr>
                  </thead>
                  <tfoot>
                          <tr>
                            <th>Activitiy</th>
                            <th>Date</th>
                        </tr>
                  </tfoot>

                  <tbody>
                        @foreach($invisible->activities as $activity)
                            <tr>
                                <td>{{$activity->activity}}</td>
                                <td>{{$activity->created_at}}</td>
                            </tr>
                        @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->


@endsection

