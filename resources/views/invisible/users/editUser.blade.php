<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
<body>

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<style>
    .divider-text {
    position: relative;
    text-align: center;
    margin-top: 15px;
    margin-bottom: 15px;
}
.divider-text span {
    padding: 7px;
    font-size: 12px;
    position: relative;   
    z-index: 2;
}
.divider-text:after {
    content: "";
    position: absolute;
    width: 100%;
    border-bottom: 1px solid #ddd;
    top: 55%;
    left: 0;
    z-index: 1;
}

.btn-facebook {
    background-color: #405D9D;
    color: #fff;
}
.btn-twitter {
    background-color: #42AEEC;
    color: #fff;
}
.small-text{
    color:red;
}
</style><script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
<style>
    .divider-text {
    position: relative;
    text-align: center;
    margin-top: 15px;
    margin-bottom: 15px;
}
.divider-text span {
    padding: 7px;
    font-size: 12px;
    position: relative;   
    z-index: 2;
}
.divider-text:after {
    content: "";
    position: absolute;
    width: 100%;
    border-bottom: 1px solid #ddd;
    top: 55%;
    left: 0;
    z-index: 1;
}

.btn-facebook {
    background-color: #405D9D;
    color: #fff;
}
.btn-twitter {
    background-color: #42AEEC;
    color: #fff;
}
</style>

<div class="container">
<div class="card bg-light">
<article class="card-body mx-auto" style="max-width: 400px;">
	<h4 class="card-title mt-3 text-center">Edit Account</h4>
	<p class="text-center">Get started with your free account</p>
	
	<form action="{{ url('editInvisible') }}" method="post">

    @csrf

    <input type="hidden" name="id" value="{{$invisible->id}}">
    @if($errors->has('name'))
        <span class="small-text">{{ $errors->first('name') }}</span>
    @endif
	<div class="form-group input-group">
		<div class="input-group-prepend">
		    <span class="input-group-text"><i class="fa fa-user"></i></span>
		 </div>
        <input name="name" class="form-control" placeholder="Full name" id="full_name" name="full_name" value="{{ $invisible->name }}" type="text">
    </div> <!-- form-group// -->

    @if($errors->has('email'))
        <span class="small-text">{{ $errors->first('email') }}</span>
    @endif
    <div class="form-group input-group">
    	<div class="input-group-prepend">
		    <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
		 </div>
        <input name="email" class="form-control" placeholder="Email address" id="email" name="email" value="{{ $invisible->email }}" type="email">
    </div> <!-- form-group// -->

    @if($errors->has('password'))
        <span class="small-text">{{ $errors->first('password') }}</span>
    @endif
    <div class="form-group input-group">
    	<div class="input-group-prepend">
		    <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
		 </div>
        <input name="password" class="form-control" placeholder="Password" id="password" value="{{ $invisible->password }}" type="text">
    </div> <!-- form-group// -->

    @if($errors->has('phone'))
        <span class="small-text">{{ $errors->first('phone') }}</span>
    @endif
    <div class="form-group input-group">
    	<div class="input-group-prepend">
		    <span class="input-group-text"> <i class="fa fa-phone"></i> </span>
		</div>
		<select class="custom-select" style="max-width: 120px;" >
		    <option selected="">+971</option>
		</select>
    	<input name="phone" class="form-control" placeholder="Phone number" id="phone" name="phone" value="{{ $invisible->phone }}" type="text">
    </div> <!-- form-group// -->

    @if($errors->has('dob'))
        <span class="small-text">{{ $errors->first('dob') }}</span>
    @endif    
    <div class="form-group input-group">
    	<div class="input-group-prepend">
		    <span class="input-group-text"> <i class="fa fa-calendar"></i> </span>
		</div>
        <input class="form-control" type="date" id="dob" value="{{ $invisible->dob }}" name="dob">
    </div> <!-- form-group// -->

    @if($errors->has('gender'))
        <span class="small-text">{{ $errors->first('gender') }}</span>
    @endif    
    <div class="form-group input-group">
    	<div class="input-group-prepend">
		    <span class="input-group-text"> <i class="fa fa-building"></i> </span>
		</div>
		<select class="form-control" id="gender" name="gender">
			<option value=""> Gender</option>
			<option @if($invisible->gender == "Male") selected @endif value="Male">Male</option>
			<option @if($invisible->gender == "Female") selected @endif value="Female">Female</option>
		</select>
	</div> <!-- form-group end.// -->

    @if($errors->has('address'))
        <span class="small-text">{{ $errors->first('address') }}</span>
    @endif    
    <div class="form-group input-group">
    	<div class="input-group-prepend">
		    <span class="input-group-text"> <i class="fa fa-home"></i> </span>
		</div>
        <input class="form-control" placeholder="Address" type="text" id="address" value="{{ $invisible->address }}" name="address">
    </div> <!-- form-group// -->

    @if($errors->has('city'))
        <span class="small-text">{{ $errors->first('city') }}</span>
    @endif    
    <div class="form-group input-group">
    	<div class="input-group-prepend">
		    <span class="input-group-text"> <i class="fa fa-globe"></i> </span>
		</div>
        <input class="form-control" placeholder="City name" type="text" id="city" value="{{ $invisible->city }}" name="city">
    </div> <!-- form-group// -->

    @if($errors->has('country'))
        <span class="small-text">{{ $errors->first('country') }}</span>
    @endif    
    <div class="form-group input-group">
    	<div class="input-group-prepend">
		    <span class="input-group-text"> <i class="fa fa-globe"></i> </span>
		</div>
        <input class="form-control" placeholder="Country name" type="text" id="country" value="{{ $invisible->country }}" name="country">
    </div> <!-- form-group// -->

    @if($errors->has('referal_name'))
        <span class="small-text">{{ $errors->first('referal_name') }}</span>
    @endif    
    <div class="form-group input-group">
    	<div class="input-group-prepend">
		    <span class="input-group-text"> <i class="fa fa-user"></i> </span>
		</div>
        <input class="form-control" placeholder="Name of friend who refered you" type="text" id="ref_name" value="{{ $invisible->referal_name }}" name="referal_name">
    </div> <!-- form-group// -->

    @if($errors->has('referal_phone'))
        <span class="small-text">{{ $errors->first('referal_phone') }}</span>
    @endif    
    <div class="form-group input-group">
    	<div class="input-group-prepend">
		    <span class="input-group-text"> <i class="fa fa-phone"></i> </span>
		</div>
        <input class="form-control" placeholder="Phone no of referal" type="text" id="ref_phone" value="{{ $invisible->referal_phone }}" name="referal_phone">
    </div> <!-- form-group// -->

    @if($errors->has('fb_friends'))
        <span class="small-text">{{ $errors->first('fb_friends') }}</span>
    @endif    
    <div class="form-group input-group">
    	<div class="input-group-prepend">
		    <span class="input-group-text"> <i class="fab fa-facebook-f"></i> </span>
		</div>
        <input class="form-control" placeholder="Number of friends on facebook" type="text" id="fb_friends" value="{{ $invisible->fb_friends }}" name="fb_friends">
    </div> <!-- form-group// -->
    
    @if($errors->has('in_friends'))
        <span class="small-text">{{ $errors->first('in_friends') }}</span>
    @endif    
    <div class="form-group input-group">
    	<div class="input-group-prepend">
		    <span class="input-group-text"> <i class="fab fa-instagram"></i> </span>
		</div>
        <input class="form-control" placeholder="Number of followers on instagram" type="text" id="in_friends" value="{{ $invisible->in_friends }}" name="in_friends">
    </div> <!-- form-group// -->

    @if($errors->has('p_percentage'))
        <span class="small-text">{{ $errors->first('p_percentage') }}</span>
    @endif    
    <div class="form-group input-group">
    	<div class="input-group-prepend">
		    <span class="input-group-text"> <i class="fa fa-percent"></i> </span>
		</div>
        <input class="form-control" placeholder="Set Primary Percentage" type="text" id="p_percentage" value="{{ $invisible->p_percentage }}" name="p_percentage">
    </div> <!-- form-group// -->

    @if($errors->has('s_percentage'))
        <span class="small-text">{{ $errors->first('s_percentage') }}</span>
    @endif    
    <div class="form-group input-group">
    	<div class="input-group-prepend">
		    <span class="input-group-text"> <i class="fa fa-percent"></i> </span>
		</div>
        <input class="form-control" placeholder="Set Secondary Percentage" type="text" id="s_percentage" value="{{ $invisible->s_percentage }}" name="s_percentage">
    </div> <!-- form-group// -->

    <div class="form-group">
    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="approve" value="0" name="status" @if($invisible->status == 0) checked @endif>
        <label class="custom-control-label" for="approve">Approve</label>
    </div>

    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="block" value="1" name="status" @if($invisible->status == 1) checked @endif>
        <label class="custom-control-label" for="block">Block</label>
      </div>
  </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary btn-block"> Update Account  </button>
    </div> <!-- form-group// -->      
</form>
</article>
</div> <!-- card.// -->

</div> 
        
    </body>
</html>