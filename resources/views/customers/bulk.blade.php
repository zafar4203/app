@extends('layouts.welcome')
@section('title' , 'Send Bulk Email')
@section('styles')
    <style>
      .add-product-form{
        margin:0px 20%;
      }
      .error{
        color:red;
        font-size:1rem;
      }
      .submit-btn{
        margin-top:20px; 
        margin-bottom:20px; 
      }
      #grouped_items{
        display:none;
      }
      @media screen and (max-width: 600px) {
        .add-product-form{
          margin:0px 5%;
        }
      }
      #landscape img{
        height:150px;
        width:150px;
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">

<form  class="add-product-form" enctype="multipart/form-data"
 action="{{route('send-bulk')}}" id="add_product" method="post">

 @if(session()->has('message'))
    <div id="alert" class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif

@if(session()->has('error'))
    <div id="alert" class="alert alert-danger">
        {{ session()->get('error') }}
    </div>
@endif


@csrf


  <div class="form-group">
    <label for="productInput">Subject</label>
    <input type="text" name="subject" class="form-control" id="productInput" value="{{old('subject')}}" aria-describedby="productNameHelp" placeholder="Enter Subject">
    @if($errors->has('subject'))
    <small id="productNameHelp" class="form-text error">{{ $errors->first('subject') }}</small>
    @endif
  </div>


  <div class="form-group">
    <label for="productPrice">Message</label>
    <textarea rows="5" type="text" name="details" class="form-control"  id="productDetails" aria-describedby="productPriceHelp" placeholder="Enter Message Here">{{old('details')}}</textarea>
    @if($errors->has('details'))
    <small id="productDetails" class="form-text error">{{ $errors->first('details') }}</small>
    @endif
  </div>


  <button id="submit" class="btn btn-primary submit-btn">Submit</button>
</form>

</div>
@endsection

