@extends('layouts.welcome')
@section('title' , 'Sub Menus')
@section('styles')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">

<style>
      .error{
        color:red;
        font-size:1rem;
      }
    .box-shadow{
        border: 1px solid lightgrey; 
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        padding:20px;
        cursor:pointer;
    }
    .heading{
        padding:10px;
        font-weight:normal;
        font-size:30px;
        width:200px;
    }
    .subheading{
        padding:10px;
        font-weight:normal;
        font-size:20px;
    }
    .text{
        text-align:center;
        font-weight:bold;
        color:#0e0e0e;
        font-size:20px;
    }
</style>
@endsection

@section('content')
<div class="container-fluid">

    <div id="alert" style="display:none;" class="alert alert-success">
        Data Updated Successfully
    </div>
    
    <div id="sortable2" class="connectedSortable row">
    @foreach($menus as $menu)
        @if(count($menu->subs) > 0)
        <hr style="color:red; width:100%;">
        <div class="col-md-12">
            <h3><span class="heading badge badge-warning badge-lg">{{ $menu->name }}</span></h3>
        </div>
        <hr style="color:red; width:100%;">
                @if(count($menu->subs) > 0)
                    @foreach($submenus as $s)
                        @foreach($menu->subs as $sub)
                            @if($s->id == $sub->id)
                            <div class="mb-3 col-md-3 box-shadow">
                                <div data-id="{{ $sub->id }}" class="text">{{ $sub->name }}</div>
                            </div>
                            @endif
                        @endforeach
                    @endforeach
                @endif
    @endif
    @endforeach
    </div>

    <button class="btn btn-primary sorts">Update</button>
</div>
@endsection

@section('scripts')
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#sortable2" ).sortable({
      connectWith: ".connectedSortable"
    }).disableSelection();
  });

  $('.sorts').click(function(){
      var ids = [];
      var token = '{{ csrf_token() }}';
      $('.text').each(function(i, obj) {
          ids.push($(obj).data('id'));          
      });
      console.log(ids);
      $.ajax({
            url: "{{ route('submenu.sort.update') }}",
            type: 'POST',
            headers: {'X-CSRF-TOKEN': token},
            data:{
                ids:ids,
            },
            success: function(data) {
                if(data.status == 200) {
                    $('#alert').css('display','block');
                }
            },
            complete:function(){
                location.reload();
            } 
        }); 
  });
  </script>
@endsection