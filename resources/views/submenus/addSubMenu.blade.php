@extends('layouts.welcome')
@section('title' , 'Add Sub Menu')
@section('styles')
    <style>
      .add-category-form{
        margin:0px 20%;
      }
    .error{
      color:red;
      font-size:1rem;
    }
    @media screen and (max-width: 600px) {
        .add-category-form{
          margin:0px 5%;
        }
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">
<form  class="add-category-form" enctype="multipart/form-data"
 action="{{route('addSubMenu')}}" id="add_category" method="post">
@csrf
  <div class="form-group">
    <label for="categoryInput">Sub Menu Name</label>
    <input type="text" name="name" class="form-control" id="categoryInput" value="{{old('name')}}" aria-describedby="categoryNameHelp" placeholder="Enter category name">
    @if($errors->has('name'))
    <small id="categoryNameHelp" class="form-text error">{{ $errors->first('name') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="categoryDescription">Menu</label>
    <select name="menu_id" required class="form-control">
      @foreach($menus as $menu)
          <option value="{{ $menu->id }}">{{ $menu->name }}</option>
      @endforeach
    </select>
    @if($errors->has('menu_id'))
    <small id="categoryDescriptionHelp" class="form-text error">{{ $errors->first('menu_id') }}</small>
    @endif
  </div>


  <div class="form-group">
    <label for="categoryDescription">Menu Link</label>
    <select required class="form-control" name="link" id="">
      <option value="#">#</option>
      <option value="{{ route('dashboard') }}">Dashboard</option>
      <option value="{{ route('orders') }}">Home Delivery</option>
      <option value="{{ route('products.index') }}">Products</option>      
      <option value="{{ route('combos') }}">Combos</option>      
      <option value="{{ route('inventories') }}">Inventories</option>      
      <option value="{{ route('users.index') }}">Users</option>      
      <option value="{{ route('categories') }}">Categories</option>      
      <option value="{{ route('menus') }}">Menus</option>      
      <option value="{{ route('submenus') }}">Sub Menus</option>      
      <option value="{{ route('subcategories') }}">Sub Categories</option>      
      <option value="{{ route('areas.index') }}">Areas</option>      
      <option value="{{ route('aggregators.index') }}">Aggregators</option>      
      <option value="{{ route('messages.index') }}">Messages</option>      
      <option value="{{ route('customers.index') }}">Customers</option>      
      <option value="{{ route('products.popular') }}">Popular Products</option>      
      <option value="{{ route('roles.index') }}">Roles</option>      
      <option value="{{ route('permissions') }}">Permissions</option>      
      <option value="{{ route('promos') }}">Promos</option>      
      <option value="{{ route('branches') }}">Branches</option>      
      <option value="{{ route('sales') }}">Sales</option>    
      <option value="{{ route('online_sales') }}">Online Sales</option>    
      <option value="{{ route('otherSale' , 'aggregator') }}">Aggregator Sale</option>    
      <option value="{{ route('otherSale' , 'b2b') }}">B2B Sale</option>    
      <option value="{{ route('otherSale' , 'other') }}">Other Sale</option>    

      <option value="{{ route('franchise_inquiries.index') }}">Franchise Inquiries</option>    
      <option value="{{ route('invisible_users') }}">Invisible Users</option>    
      <option value="{{ route('bulk') }}">Bulk Emails</option>    
    </select>
    @if($errors->has('link'))
    <small id="categoryDescriptionHelp" class="form-text error">{{ $errors->first('link') }}</small>
    @endif
  </div>


  <div class="form-group">
    <label for="categoryDescription">Menu Permissions</label>
    <select required class="form-control" name="permission" id="">
      <option value="View Dashboard">Dashboard</option>
      <option value="View Home Delivery">Home Delivery</option>
      <option value="View Products">Products</option>      
      <option value="View Combos">Combos</option>      
      <option value="View Inventory">Inventories</option>      
      <option value="View Users">Users</option>      
      <option value="View Categories">Categories</option>      
      <option value="View Menus">Menus</option>      
      <option value="View Sub Menus">Sub Menus</option>      
      <option value="View Sub Categories">Sub Categories</option>      
      <option value="View Area">Areas</option>      
      <option value="View Aggregators">Aggregators</option>      
      <option value="View Messages">Messages</option>      
      <option value="View Customers">Customers</option>      
      <option value="View Popular Products">Popular Products</option>      
      <option value="View Roles">Roles</option>      
      <option value="View Permissions">Permissions</option>      
      <option value="View Promos">Promos</option>      
      <option value="View Branches">Branches</option>      
      <option value="View Sales">Sales</option>    
      <option value="View Online Sales">Online Sales</option>    
      <option value="View Aggregator Sales">Aggregator Sales</option>    
      <option value="View b2b Sales">B2B Sales</option>    
      <option value="View Other Sales">Other Sales</option>    

      <option value="View Home Delivery">Home Delivery</option>    
      <option value="View Franchise Inquiries">Franchise Inquiries</option>    
      <option value="View Invisible Users">Invisible Users</option>    
      <option value="View Bulk Emails">Bulk Emails</option>    

    </select>
    @if($errors->has('permission'))
    <small id="categoryDescriptionHelp" class="form-text error">{{ $errors->first('permission') }}</small>
    @endif
  </div>


  <div class="form-group">
    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="customRadio2" value="0" name="block" checked>
        <label class="custom-control-label" for="customRadio2">Active</label>
    </div>

    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="customRadio1" value="1" name="block">
        <label class="custom-control-label" for="customRadio1">De Active</label>
      </div>
  </div>


  <button id="submit" class="btn btn-primary">Submit</button>
</form>

</div>
@endsection
