@extends('layouts.welcome')
@section('title' , 'Add Menu')
@section('styles')
    <style>
      .add-category-form{
        margin:0px 20%;
      }
    .error{
      color:red;
      font-size:1rem;
    }
    @media screen and (max-width: 600px) {
        .add-category-form{
          margin:0px 5%;
        }
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">
<form  class="add-category-form" enctype="multipart/form-data"
 action="{{route('updateSubMenu')}}" id="add_category" method="post">

 @if(session()->has('message'))
    <div id="alert" class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif


@csrf
  <input type="hidden" name="id" value="{{ $menu->id }}">
  <div class="form-group">
    <label for="categoryInput">Menu Name</label>
    <input type="text" name="name" class="form-control" id="categoryInput" value="{{$menu->name}}" aria-describedby="categoryNameHelp" placeholder="Enter category name">
    @if($errors->has('name'))
    <small id="categoryNameHelp" class="form-text error">{{ $errors->first('name') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="categoryDescription">Menu</label>
    <select name="menu_id" required class="form-control">
      @foreach($menus as $m)
          <option {{ $m->id == $menu->menu_id ? 'selected' : '' }} value="{{ $m->id }}">{{ $m->name }}</option>
      @endforeach
    </select>
    @if($errors->has('menu_id'))
    <small id="categoryDescriptionHelp" class="form-text error">{{ $errors->first('menu_id') }}</small>
    @endif
  </div>


  <div class="form-group">
    <label for="categoryDescription">Menu Link</label>
    <select required class="form-control" name="link" id="">
      <option {{ $menu->link == '#' ? 'selected' : ''}} value="#">#</option>
      <option {{ $menu->link ==  route('dashboard') ? 'selected' : ''}} value="{{ route('dashboard') }}">Dashboard</option>
      <option {{ $menu->link ==  route('orders') ? 'selected' : ''}} value="{{ route('orders') }}">Home Delivery</option>
      <option {{ $menu->link ==  route('products.index') ? 'selected' : ''}} value="{{ route('products.index') }}">Products</option>      
      <option {{ $menu->link ==  route('combos') ? 'selected' : ''}} value="{{ route('combos') }}">Combos</option>      
      <option {{ $menu->link ==  route('inventories') ? 'selected' : ''}} value="{{ route('inventories') }}">Inventories</option>      
      <option {{ $menu->link ==  route('users.index') ? 'selected' : ''}} value="{{ route('users.index') }}">Users</option>      
      <option {{ $menu->link ==  route('categories') ? 'selected' : ''}} value="{{ route('categories') }}">Categories</option>      
      <option {{ $menu->link ==  route('menus') ? 'selected' : ''}} value="{{ route('menus') }}">Menus</option>      
      <option {{ $menu->link ==  route('submenus') ? 'selected' : ''}} value="{{ route('submenus') }}">Sub Menus</option>      
      <option {{ $menu->link ==  route('subcategories') ? 'selected' : ''}} value="{{ route('subcategories') }}">Sub Categories</option>      
      <option {{ $menu->link ==  route('areas.index') ? 'selected' : ''}} value="{{ route('areas.index') }}">Areas</option>      
      <option {{ $menu->link ==  route('aggregators.index') ? 'selected' : ''}} value="{{ route('aggregators.index') }}">Aggregators</option>      
      <option {{ $menu->link ==  route('messages.index') ? 'selected' : ''}} value="{{ route('messages.index') }}">Messages</option>      
      <option {{ $menu->link ==  route('customers.index') ? 'selected' : ''}} value="{{ route('customers.index') }}">Customers</option>      
      <option {{ $menu->link ==  route('products.popular') ? 'selected' : ''}} value="{{ route('products.popular') }}">Popular Products</option>      
      <option {{ $menu->link ==  route('roles.index') ? 'selected' : ''}} value="{{ route('roles.index') }}">Roles</option>      
      <option {{ $menu->link ==  route('permissions') ? 'selected' : ''}} value="{{ route('permissions') }}">Permissions</option>      
      <option {{ $menu->link ==  route('promos') ? 'selected' : ''}} value="{{ route('promos') }}">Promos</option>      
      <option {{ $menu->link ==  route('branches') ? 'selected' : ''}} value="{{ route('branches') }}">Branches</option>      
      <option {{ $menu->link ==  route('sales') ? 'selected' : ''}} value="{{ route('sales') }}">Sales</option>    
      <option {{ $menu->link ==  route('online_sales') ? 'selected' : ''}} value="{{ route('online_sales') }}">Online Sales</option>    
      <option {{ $menu->link ==  route('otherSale' , 'aggregator') ? 'selected' : ''}} value="{{ route('otherSale' , 'aggregator') }}">Aggregator Sale</option>    
      <option {{ $menu->link ==  route('otherSale' , 'b2b') ? 'selected' : ''}} value="{{ route('otherSale' , 'b2b') }}">B2B Sale</option>    
      <option {{ $menu->link ==  route('otherSale' , 'other') ? 'selected' : ''}} value="{{ route('otherSale' , 'other') }}">Other Sale</option>    

      <option {{ $menu->link ==  route('franchise_inquiries.index') }} ? 'selected' : ''}} value="{{ route('franchise_inquiries.index') }}">Franchise Inquiries</option>    
      <option {{ $menu->link ==  route('invisible_users') }} ? 'selected' : ''}} value="{{ route('invisible_users') }}">Invisible Users</option>    
      <option {{ $menu->link ==  route('bulk') }} ? 'selected' : ''}} value="{{ route('bulk') }}">Bulk Emails</option>    
    </select>
    @if($errors->has('link'))
    <small id="categoryDescriptionHelp" class="form-text error">{{ $errors->first('link') }}</small>
    @endif
  </div>


  <div class="form-group">
    <label for="categoryDescription">Menu Permissions</label>
    <select required class="form-control" name="permission" id="">
      <option {{$menu->permission == 'View Dashboard' ? 'selected' : ''}} value="View Dashboard">Dashboard</option>
      <option {{$menu->permission == 'View Home Delivery' ? 'selected' : ''}} value="View Home Delivery">Home Delivery</option>    
      <option {{$menu->permission == 'View Products' ? 'selected' : ''}} value="View Products">Products</option>      
      <option {{$menu->permission == 'View Combos' ? 'selected' : ''}} value="View Combos">Combos</option>      
      <option {{$menu->permission == 'View Inventory' ? 'selected' : ''}} value="View Inventory">Inventories</option>      
      <option {{$menu->permission == 'View Users' ? 'selected' : ''}} value="View Users">Users</option>      
      <option {{$menu->permission == 'View Categories' ? 'selected' : ''}} value="View Categories">Categories</option>      
      <option {{$menu->permission == 'View Menus' ? 'selected' : ''}} value="View Menus">Menus</option>      
      <option {{$menu->permission == 'View Sub Menus' ? 'selected' : ''}} value="View Sub Menus">Sub Menus</option>      
      <option {{$menu->permission == 'View Sub Categories' ? 'selected' : ''}} value="View Sub Categories">Sub Categories</option>      
      <option {{$menu->permission == 'View Area' ? 'selected' : ''}} value="View Area">Areas</option>      
      <option {{$menu->permission == 'View Aggregators' ? 'selected' : ''}} value="View Aggregators">Aggregators</option>      
      <option {{$menu->permission == 'View Messages' ? 'selected' : ''}} value="View Messages">Messages</option>      
      <option {{$menu->permission == 'View Customers' ? 'selected' : ''}} value="View Customers">Customers</option>      
      <option {{$menu->permission == 'View Popular Products' ? 'selected' : ''}} value="View Popular Products">Popular Products</option>      
      <option {{$menu->permission == 'View Roles' ? 'selected' : ''}} value="View Roles">Roles</option>      
      <option {{$menu->permission == 'View Permissions' ? 'selected' : ''}} value="View Permissions">Permissions</option>      
      <option {{$menu->permission == 'View Promos' ? 'selected' : ''}} value="View Promos">Promos</option>      
      <option {{$menu->permission == 'View Branches' ? 'selected' : ''}} value="View Branches">Branches</option>      
      <option {{$menu->permission == 'View Sales' ? 'selected' : ''}} value="View Sales">Sales</option>    
      <option {{$menu->permission == 'View Online Sales' ? 'selected' : ''}} value="View Online Sales">Online Sales</option>    
      <option {{$menu->permission == 'View Aggregator Sales' ? 'selected' : ''}} value="View Aggregator Sales">Aggregator Sales</option>    
      <option {{$menu->permission == 'View b2b Sales' ? 'selected' : ''}} value="View b2b Sales">B2B Sales</option>    
      <option {{$menu->permission == 'View Other Sales' ? 'selected' : ''}} value="View Other Sales">Other Sales</option>    

      <option {{$menu->permission == 'View Franchise Inquiries' ? 'selected' : ''}} value="View Franchise Inquiries">Franchise Inquiries</option>    
      <option {{$menu->permission == 'View Invisible Users' ? 'selected' : ''}} value="View Invisible Users">Invisible Users</option>    
      <option {{$menu->permission == 'View Bulk Emails' ? 'selected' : ''}} value="View Bulk Emails">Bulk Emails</option>    

    </select>
    @if($errors->has('permission'))
    <small id="categoryDescriptionHelp" class="form-text error">{{ $errors->first('permission') }}</small>
    @endif
  </div>

  <div class="form-group">
    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="customRadio2" value="0" name="block" @if($menu->block == 0) checked @endif>
        <label class="custom-control-label" for="customRadio2">Active</label>
    </div>

    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="customRadio1" value="1" name="block" @if($menu->block == 1) checked @endif>
        <label class="custom-control-label" for="customRadio1">De Active</label>
      </div>
  </div>


  <button id="submit" class="btn btn-primary">Submit</button>
</form>

</div>
@endsection
