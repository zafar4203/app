@extends('layouts.welcome')

@section('title' , 'Dashboard')

@section('styles')
@endsection

@section('content')
<div class="container-fluid">

    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Revenue &amp; Expense</h1>
</div>

<!-- Content Row -->
<div class="row">

  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-primary shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="font-weight-bold text-primary text-uppercase mb-1">Earnings (Today)</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800">AED {{$tSale}}</div>
            <br>
            <div class="text-xs font-weight-bold text-primary text-uppercase">Kiosk</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800">AED {{$tKiosk}}</div>
            <div class="text-xs font-weight-bold text-primary text-uppercase mt-2">Online</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800">AED {{$tOnline}}</div>
            <div class="text-xs font-weight-bold text-primary text-uppercase mt-2">Invisible Partner</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800">AED {{$tInvisible}}</div>
          </div>
          <div class="col-auto">
            <i class="fas fa-calendar fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-success shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="font-weight-bold text-success text-uppercase mb-1">Earnings (Yesterday)</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800">AED {{$ySale}}</div>
            <br>
            <div class="text-xs font-weight-bold text-success text-uppercase">KIOSK</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800">AED {{$yKiosk}}</div>
            <div class="text-xs font-weight-bold text-success text-uppercase mt-2">Online</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800">AED {{$yOnline}}</div>
            <div class="text-xs font-weight-bold text-success text-uppercase mt-2">Invisible Partner</div>
            <div class="h5 mb-0 font-weight-bold text-gray-800">AED {{$yInvisible}}</div>
          </div>
          <div class="col-auto">
            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Earnings (Monthly) Card Example -->
  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-info shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="font-weight-bold text-info text-uppercase mb-1">Earnings (Monthly)</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">AED {{$mSale}}</div>
              <br>
              <div class="text-xs font-weight-bold text-info text-uppercase">KIOSK</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">AED {{$mKiosk}}</div>
              <div class="text-xs font-weight-bold text-info text-uppercase mt-2">Online</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">AED {{$mOnline}}</div>
              <div class="text-xs font-weight-bold text-info text-uppercase mt-2">Invisible Partner</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">AED {{$mInvisible}}</div>
          </div>
          <div class="col-auto">
            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Pending Requests Card Example -->
  <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-warning shadow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col mr-2">
            <div class="font-weight-bold text-warning text-uppercase mb-1">Today's Home Deliveries</div>
            <br>
            <div class="text-xs font-weight-bold text-warning text-uppercase ">Total Orders</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">{{$tOrders}}</div>
              <div class="text-xs font-weight-bold text-warning text-uppercase mt-2">Pending Orders</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">{{$pOrders}}</div>
              <div class="text-xs font-weight-bold text-warning text-uppercase mt-2">Completed Orders</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">{{$cOrders}}</div>

          </div>
          <div class="col-auto">
            <i class="fas fa-file fa-2x text-gray-300"></i>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Content Row -->

<div class="row">
   <div class="col-md-6 mb-4">
      <div class="card border-left-warning shadow h-100 py-2">
        <div class="card-body">
          <h3>Daily Sale</h3>
          <canvas id="dChart" width="600" height="400"></canvas>
        
          <table class="table mt-4">  
              <tr>
                <th>Sales</th>
                <th>Amount</th>
                <th>Percentage</th>
              </tr>          
            @foreach($subMenuD as $key => $menu)
              <tr {{$menu->subs != "" ? 'style=background:#e3dad0' : ''}} >
                <td>{{ $menu->name }}</td>
                <td>{{ $dData[$key] }}</td>
                <td>{{ $menu->percentage }}</td>
                @if($menu->subs)
                  @foreach($menu->subs as $sub)
                    @if($sub->sale > 0)
                    <tr>
                        <td>{{ $sub->name }}</td>
                        <td>{{ $sub->sale }}</td>
                        <td>-</td>
                    </tr>
                    @endif
                  @endforeach
                @endif
              </tr>
            @endforeach
            <tr>
                <td>Total</td>
                <td>{{ $dSale }}</td>
                <td>100%</td>
            </tr>
          </table>
        </div>
      </div>
    </div>    



    <div class="col-md-6 mb-4">
      <div class="card border-left-warning shadow h-100 py-2">
        <div class="card-body">
          <h3>Monthly Sale</h3>
          <canvas id="mChart" width="600" height="400"></canvas>
        
          <table class="table mt-4">  
              <tr>
                <th>Sales</th>
                <th>Amount</th>
                <th>Percentage</th>
              </tr>          
            @foreach($subMenuM as $key => $menu)
              <tr {{$menu->subs != "" ? 'style=background:#e3dad0' : ''}} >
                <td>{{ $menu->name }}</td>
                <td>{{ $mData[$key] }}</td>
                <td>{{ $menu->percentage }}</td>
                @if($menu->subs)
                  @foreach($menu->subs as $sub)
                    @if($sub->sale > 0)
                    <tr>
                        <td>{{ $sub->name }}</td>
                        <td>{{ $sub->sale }}</td>
                        <td>-</td>
                    </tr>
                    @endif
                  @endforeach
                @endif
              </tr>
            @endforeach
            <tr>
                <td>Total</td>
                <td>{{ $mSale }}</td>
                <td>100%</td>
            </tr>
          </table>
        </div>
      </div>
    </div>    





    <div class="col-md-6 mb-4">
      <div class="card border-left-warning shadow h-100 py-2">
        <div class="card-body">
          <h3> Yearly Sale </h3>
          <canvas id="yChart" width="600" height="400"></canvas>
        
          <table class="table mt-4">  
              <tr>
                <th>Sales</th>
                <th>Amount</th>
                <th>Percentage</th>
              </tr>          
            @foreach($subMenuY as $key => $menu)
            <tr {{$menu->subs != "" ? 'style=background:#e3dad0' : ''}} >
                <td>{{ $menu->name }}</td>
                <td>{{ $yData[$key] }}</td>
                <td>{{ $menu->percentage }}</td>
                @if($menu->subs)
                  @foreach($menu->subs as $sub)
                    @if($sub->sale > 0)
                    <tr>
                        <td>{{ $sub->name }}</td>
                        <td>{{ $sub->sale }}</td>
                        <td>-</td>
                    </tr>
                    @endif
                  @endforeach
                @endif
              </tr>
            @endforeach
            <tr>
                <td>Total</td>
                <td>{{ $ySale }}</td>
                <td>100%</td>
            </tr>
          </table>
        </div>
      </div>
    </div>    
</div>

<div class="row">

  <!-- Area Chart -->
  <div class="col-xl-12 col-lg-12">
    <div class="card shadow mb-4">
      <!-- Card Header - Dropdown -->
      <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Earnings Overview</h6>
        <div class="dropdown no-arrow">
          <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
            <div class="dropdown-header">Dropdown Header:</div>
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Something else here</a>
          </div>
        </div>
      </div>
      <!-- Card Body -->
      <div class="card-body">
        <div class="chart-area">
          <canvas id="myArea"></canvas>
        </div>
      </div>
    </div>
  </div>



</div>
<!-- /.container-fluid -->

@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.2/Chart.min.js"></script>

  <script>
    // Area Chart Example
var ctx = document.getElementById("myArea");
var myLineChart = new Chart(ctx, {
  type: 'line',
  data: {
    labels: <?php echo json_encode($months); ?>,
    datasets: [{
      label: "Earnings",
      lineTension: 0.3,
      backgroundColor: "rgba(78, 115, 223, 0.05)",
      borderColor: "rgba(78, 115, 223, 1)",
      pointRadius: 3,
      pointBackgroundColor: "rgba(78, 115, 223, 1)",
      pointBorderColor: "rgba(78, 115, 223, 1)",
      pointHoverRadius: 3,
      pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
      pointHoverBorderColor: "rgba(78, 115, 223, 1)",
      pointHitRadius: 10,
      pointBorderWidth: 2,
      data: <?php echo json_encode($months_sales); ?>,
    }],
  },
  options: {
    maintainAspectRatio: false,
    layout: {
      padding: {
        left: 10,
        right: 25,
        top: 25,
        bottom: 0
      }
    },
    scales: {
      xAxes: [{
        time: {
          unit: 'date'
        },
        gridLines: {
          display: false,
          drawBorder: false
        },
        ticks: {
          maxTicksLimit: 7
        }
      }],
      yAxes: [{
        ticks: {
          maxTicksLimit: 5,
          padding: 10,
          // Include a dollar sign in the ticks
          callback: function(value, index, values) {
            return 'AED ' + number_format(value);
          }
        },
        gridLines: {
          color: "rgb(234, 236, 244)",
          zeroLineColor: "rgb(234, 236, 244)",
          drawBorder: false,
          borderDash: [2],
          zeroLineBorderDash: [2]
        }
      }],
    },
    legend: {
      display: false
    },
    tooltips: {
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      titleMarginBottom: 10,
      titleFontColor: '#6e707e',
      titleFontSize: 14,
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      intersect: false,
      mode: 'index',
      caretPadding: 10,
      callbacks: {
        label: function(tooltipItem, chart) {
          var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
          return datasetLabel + ': AED ' + number_format(tooltipItem.yLabel);
        }
      }
    }
  }
});
  </script>



<script>
    var oilCanvas = document.getElementById("dChart");

    Chart.defaults.global.defaultFontFamily = "Lato";
    Chart.defaults.global.defaultFontSize = 18;

    var oilData = {
        labels: <?php echo json_encode($dLabels); ?>,
        datasets: [
            {
                data: <?php echo json_encode($dData); ?>,
                backgroundColor: [
                    "#FF6384",
                    "#20c9a6",
                    "#fd7e14",
                    "#8463FF",
                    "#6384FF"
                ]
            }]
    };

    var pieChart = new Chart(oilCanvas, {
    type: 'pie',
    data: oilData,
    options: {
            responsive: true,
            legend: {
                position: 'right',
                labels: {
                    fontColor: "black",
                    boxWidth: 20,
                    padding: 20
                }
            },
            tooltips: {
              callbacks: {
                label: function(tooltipItem, data) {
                  //get the concerned dataset
                  var dataset = data.datasets[tooltipItem.datasetIndex];
                  //calculate the total of this data set
                  var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                    return previousValue + currentValue;
                  });
                  //get the current items value
                  var currentValue = dataset.data[tooltipItem.index];
                  //calculate the precentage based on the total and current item, also this does a rough rounding to give a whole number
                  var percentage = Math.floor(((currentValue/total) * 100)+0.5);

                  return percentage + "%";
                }
              }
            }

      
        }
    });        
</script>



<script>
    var oilCanvas = document.getElementById("mChart");

    Chart.defaults.global.defaultFontFamily = "Lato";
    Chart.defaults.global.defaultFontSize = 18;

    var oilData = {
        labels: <?php echo json_encode($mLabels); ?>,
        datasets: [
            {
                data: <?php echo json_encode($mData); ?>,
                backgroundColor: [
                    "#FF6384",
                    "#20c9a6",
                    "#fd7e14",
                    "#8463FF",
                    "#6384FF"
                ]
            }]
    };

    var pieChart = new Chart(oilCanvas, {
    type: 'pie',
    data: oilData,
    options: {
            responsive: true,
            legend: {
                position: 'right',
                labels: {
                    fontColor: "black",
                    boxWidth: 20,
                    padding: 20
                }
            },
            tooltips: {
              callbacks: {
                label: function(tooltipItem, data) {
                  //get the concerned dataset
                  var dataset = data.datasets[tooltipItem.datasetIndex];
                  //calculate the total of this data set
                  var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                    return previousValue + currentValue;
                  });
                  //get the current items value
                  var currentValue = dataset.data[tooltipItem.index];
                  //calculate the precentage based on the total and current item, also this does a rough rounding to give a whole number
                  var percentage = Math.floor(((currentValue/total) * 100)+0.5);

                  return percentage + "%";
                }
            }
          }

      
        }
    });        
</script>





<script>
    var oilCanvas = document.getElementById("yChart");

    Chart.defaults.global.defaultFontFamily = "Lato";
    Chart.defaults.global.defaultFontSize = 18;

    var oilData = {
        labels: <?php echo json_encode($yLabels); ?>,
        datasets: [
            {
                data: <?php echo json_encode($yData); ?>,
                backgroundColor: [
                    "#FF6384",
                    "#20c9a6",
                    "#fd7e14",
                    "#8463FF",
                    "#6384FF"
                ]
            }]
    };

    var pieChart = new Chart(oilCanvas, {
    type: 'pie',
    data: oilData,
    options: {
            responsive: true,
            legend: {
                position: 'right',
                labels: {
                    fontColor: "black",
                    boxWidth: 20,
                    padding: 20
                }
            },
            
        
            tooltips: {
              callbacks: {
                label: function(tooltipItem, data) {
                  //get the concerned dataset
                  var dataset = data.datasets[tooltipItem.datasetIndex];
                  //calculate the total of this data set
                  var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                    return previousValue + currentValue;
                  });
                  //get the current items value
                  var currentValue = dataset.data[tooltipItem.index];
                  //calculate the precentage based on the total and current item, also this does a rough rounding to give a whole number
                  var percentage = Math.floor(((currentValue/total) * 100)+0.5);

                  return percentage + "%";
                }
            }
} 
      
        }
    });        




    

</script>
@endsection