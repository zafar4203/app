@extends('layouts.welcome')

@section('title' , 'Edit Permission')

@section('styles')
    <style>
      .edit-role-form{
        margin:0px 20%;
      }
    .error{
      color:red;
      font-size:1rem;
    }

    @media screen and (max-width: 600px) {
        .edit-role-form{
          margin:0px 5%;
        }
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">
<form  class="edit-role-form" action="{{route('updatePermission')}}" id="edit_role" method="post">

@if(session()->has('message'))
    <div id="alert" class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif

@csrf
    <input name="id" type="hidden" value="{{ $permission->id }}">

  <div class="form-group">
    <label for="roleInput">Permission Name</label>
    <input type="text" name="name" class="form-control" id="roleInput" aria-describedby="roleNameHelp" value="{{ $permission->name }}" placeholder="Enter Permission Name">
    @if($errors->has('name'))
    <small id="roleNameHelp" class="form-text error">{{ $errors->first('name') }}</small>
    @endif
  </div>

  <button id="submit" class="btn btn-primary">Submit</button>
</form>

</div>
@endsection
