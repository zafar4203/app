@extends('layouts.welcome')

@section('title' , 'Add Aggregator')

@section('styles')
    <style>
      .add-area-form{
        margin:0px 20%;
      }
    .error{
      color:red;
      font-size:1rem;
    }
    @media screen and (max-width: 600px) {
        .add-area-form{
          margin:0px 5%;
        }
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">
<form  class="add-area-form" enctype="multipart/form-data"
 action="{{route('addAggregator')}}" id="add_category" method="post">
@csrf
  <div class="form-group">
    <label for="categoryInput">Area Aggregator</label>
    <input type="text" name="name" class="form-control" id="categoryInput" value="{{old('name')}}" aria-describedby="categoryNameHelp" placeholder="Enter Aggregator name">
    @if($errors->has('name'))
    <small id="categoryNameHelp" class="form-text error">{{ $errors->first('name') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="description">Description</label>
    <input type="text" name="description" class="form-control" id="description" value="{{old('description')}}" placeholder="Enter Description">
    @if($errors->has('description'))
    <small id="categoryDescriptionHelp" class="form-text error">{{ $errors->first('description') }}</small>
    @endif
  </div>


  <div class="form-group">
    <label for="type">Type</label>
    <select class="form-control" name="type" id="">
      <option value="Aggregator">Aggregator</option>
      <option value="B2B">B2B</option>
      <option value="Other">Other</option>
    </select>
    @if($errors->has('type'))
    <small id="categoryDescriptionHelp" class="form-text error">{{ $errors->first('type') }}</small>
    @endif
  </div>

  <div class="form-group">
    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="customRadio2" value="0" name="block" checked>
        <label class="custom-control-label" for="customRadio2">Active</label>
    </div>

    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="customRadio1" value="1" name="block">
        <label class="custom-control-label" for="customRadio1">De Active</label>
      </div>
  </div>


  <button id="submit" class="btn btn-primary">Submit</button>
</form>

</div>
@endsection
