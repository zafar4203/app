@extends('layouts.welcome')

@section('title' , 'Edit Aggregator')

@section('styles')
    <style>
      .edit-category-form{
        margin:0px 20%;
      }
    .error{
      color:red;
      font-size:1rem;
    }
    @media screen and (max-width: 600px) {
        .edit-category-form{
          margin:0px 5%;
        }
      }
    </style>
@endsection

@section('content')
<div class="container-fluid">

<form  class="edit-category-form" enctype="multipart/form-data"
 action="{{url('updateAggregator')}}" id="edit_category" method="post">

 @if(session()->has('message'))
    <div id="alert" class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif


@csrf
  <input type="hidden" name="id" value="{{$aggregator->id}}">
  <div class="form-group">
    <label for="categoryInput">Aggregator Name</label>
    <input type="text" name="name" class="form-control" value="{{$aggregator->name}}" id="categoryInput" aria-describedby="categoryNameHelp" placeholder="Enter Aggregator Name">
    @if($errors->has('name'))
    <small id="categoryNameHelp" class="form-text error">{{ $errors->first('name') }}</small>
    @endif
  </div>
  <div class="form-group">
    <label for="categoryDescription">Description</label>
    <input type="text" name="description" class="form-control" value="{{$aggregator->description}}" id="categoryDescription" aria-describedby="categoryDescriptionHelp" placeholder="Enter Description" /> 
    @if($errors->has('description'))
    <small id="categoryDescriptionHelp" class="form-text error">{{ $errors->first('description') }}</small>
    @endif
  </div>

  <div class="form-group">
    <label for="type">Type</label>
    <select class="form-control" name="type" id="">
      <option {{$aggregator->type == "Aggregator" ? 'selected' : ''}} value="Aggregator">Aggregator</option>
      <option {{$aggregator->type == "B2B" ? 'selected' : ''}} value="B2B">B2B</option>
      <option {{$aggregator->type == "Other" ? 'selected' : ''}} value="Other">Other</option>
    </select>
    @if($errors->has('type'))
    <small id="categoryDescriptionHelp" class="form-text error">{{ $errors->first('type') }}</small>
    @endif
  </div>



  <div class="form-group">
    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="customRadio2" value="0" name="block" @if($aggregator->block == 0) checked @endif>
        <label class="custom-control-label" for="customRadio2">Active</label>
    </div>

    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" class="custom-control-input" id="customRadio1" value="1" name="block" @if($aggregator->block == 1) checked @endif>
        <label class="custom-control-label" for="customRadio1">De Active</label>
      </div>
  </div>
  
  <button id="submit" class="btn btn-primary">Submit</button>
</form>

</div>
@endsection
@section('scripts')
<script>
     $("#alert").fadeTo(2000, 500).slideUp(500, function(){
       $("#alert").slideUp(600);
       });
  </script>
@endsection