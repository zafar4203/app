window.Pusher = require('pusher-js');
import Echo from "laravel-echo";

window.Echo = new Echo({
    broadcaster: 'pusher',
    key: '67692438b1fd5ab38c1e',
    cluster: 'ap2',
    encrypted: true
});

var notifications = [];

const NOTIFICATION_TYPES = {
    noti: 'App\\Notifications\\Noti'
};

$(document).ready(function() {
    // check if there's a logged in user
    if(Laravel.userId) {
        $.get('notifications', function (data) {
            addNotifications(data.notifications, "#notifications");
            $('.has-notifications').text(data.count + '+');
        });
    }

    $(document).on('click','#notifications a.dropdown-item' , function(){
        var id = this.id;
        var link = $(this).data('href');
        $.get('notificationRead/'+id, function (data) {
            if(data == 0){
                window.location.href = link;        
            }
        });
    });
});

function addNotifications(newNotifications, target) {
    notifications = newNotifications;
    // show only last 5 notifications
    notifications.slice(0, 5);
    showNotifications(notifications, target);
}


function showNotifications(notifications, target) {
    if(notifications.length) {
        var htmlElements = notifications.map(function (notification) {
            return makeNotification(notification);
        });
        $(target).html(htmlElements.join(''));
        $('.has-notifications').css('display' , 'block');
    } else {
        $(target).html(
            '<a class="dropdown-item d-flex align-items-center">'+
                '<div class="text-center">'+
                'No Notification'+
                '</div>'+
            '</a>'
        );
        $('.has-notifications').css('display' , 'none');
    }
}

function makeNotification(notification) {
    var to = routeNotification(notification);
    var notificationText = makeNotificationText(notification);
    return '<a data-href="'+ notification.data.data.link +'" class="dropdown-item d-flex align-items-center" id="'+notification.id+'" >'+
    '<div class="mr-3">'+
      '<div class="icon-circle bg-success">'+
        '<i class="fas fa-donate text-white"></i>'+
      '</div>'+
    '</div>'+
    '<div>'+
      '<div class="small text-gray-500">'+notification.date+'</div>'+
      notificationText+
    '</div>'+
  '</a>';
}

// get the notification route based on it's type
function routeNotification(notification) {
    var to = '?read=' + notification.id;
    return '/' + to;
}

function makeNotificationText(notification) {
    var text = '';
    console.log(notification);
    if(notification.type === NOTIFICATION_TYPES.noti) {
        const name = notification.data.data.text;
        text += '' + name + '';
    }
    return text;
}
