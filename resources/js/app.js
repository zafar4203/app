require('./bootstrap');

window.Pusher = require('pusher-js');

import Echo from "laravel-echo";

window.Echo = new Echo({
    // authEndpoint : 'http://localhost/app/public/broadcasting/auth',
    authEndpoint : 'https://papafluffy.com/portal/public/broadcasting/auth',
    broadcaster: 'pusher',
    key: '67692438b1fd5ab38c1e',
    cluster: 'ap2',
    encrypted: true, 

});

var db = 0;
var count = 0;
// var mainurl = "http://localhost/app/";
var mainurl = "https://papafluffy.com/portal/";
var notifications = [];

const NOTIFICATION_TYPES = {
    noti: 'App\\Notifications\\Noti'
};

$(document).ready(function() {
    // check if there's a logged in user
    if(Laravel.userId) {

        $.get(mainurl + 'notifications', function (data) {
            addNotifications(data.notifications, "#notifications");
            count = data.count;
            localStorage.setItem('count' , count);
            $('.has-notifications').text(count + '+');
        });


        window.Echo.private(`App.User.${Laravel.userId}`)
        .notification((notification) => {
            db = 1;
            var noti_count = localStorage.getItem('count' , count);
            noti_count++;
            localStorage.setItem('count' , noti_count);
            $('.has-notifications').text(noti_count + '+');
            addNotifications([notification], '#notifications');
        });
    }

    $(document).on('click','#notifications a.dropdown-item' , function(){
        var id = this.id;
        var link = $(this).data('href');
        $.get('notificationRead/'+id, function (data) {
            if(data == 0){
                window.location.href = link;        
            }
        });
    });
});

function addNotifications(newNotifications, target) {
    if(db == 1){
        notifications = newNotifications.concat(notifications);
    }else{
        notifications = _.concat(notifications, newNotifications);
    }
    // show only last 5 notifications
    notifications.slice(0, 5);
    showNotifications(notifications, target);
}


function showNotifications(notifications, target) {
    if(notifications.length) {
        var htmlElements = notifications.map(function (notification) {
            return makeNotification(notification);
        });
        $(target).html(htmlElements.join(''));
        $('.has-notifications').css('display' , 'block');
    } else {
        $(target).html(
            '<a class="dropdown-item d-flex align-items-center">'+
                '<div class="text-center">'+
                'No Notification'+
                '</div>'+
            '</a>'
        );
        $('.has-notifications').css('display' , 'none');
    }
}

function makeNotification(notification) {
    var to = routeNotification(notification);
    var notificationText = makeNotificationText(notification);
    var id = notification.id;
    var date = notification.date;        

    if(db == 0){
        var link = notification.data.data.link;
    }else{
        if(notification.data.link != undefined){
            var link = notification.data.link;
            var date = "Just Now";        
        }else{
            var link = notification.data.data.link;
        }
    }

    return '<a data-href="'+ link +'" class="dropdown-item d-flex align-items-center" id="'+id+'" >'+
    '<div class="mr-3">'+
      '<div class="icon-circle bg-success">'+
        '<i class="fas fa-donate text-white"></i>'+
      '</div>'+
    '</div>'+
    '<div>'+
      '<div class="small text-gray-500">'+date+'</div>'+
      notificationText+
    '</div>'+
  '</a>';
}

// get the notification route based on it's type
function routeNotification(notification) {
    var to = '?read=' + notification.id;
    return '/' + to;
}

function makeNotificationText(notification) {
    var text = '';
    if(db == 0){
        var txt = notification.data.data.text;
    }else{
        if(notification.data.text != undefined){
            var txt = notification.data.text;
        }else{
            var txt = notification.data.data.text;
        }
    }

    if(notification.type === NOTIFICATION_TYPES.noti) {
        const name = txt;
        text += '' + name + '';
    }
    return text;
}

